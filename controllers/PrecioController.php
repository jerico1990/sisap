<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Precio;
use app\models\Parametro;
use app\models\UsuarioPerfil;
use app\models\TRPrecio;

use PDO;

class PrecioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionCreate(){
        $this->layout = 'privado';
        $request = Yii::$app->request;
        $parametro = Parametro::findOne(1);
        $fechaActual = date('Y-m-d');
        if($parametro->FLG_HABILITADO==1){
            $fechaFin = date('Y-m-d');
            $fechaInicio = date("Y-m-d",strtotime($fechaFin."- ".$parametro->NUM_VALOR." days"));
        }else{
            $fechaFin = date('Y-m-d');
            $fechaInicio = "1900-01-01";
        }
        
        $perfilAdministrado = UsuarioPerfil::find()->where('ID_PERFIL=1 AND ID_USUARIO=:ID_USUARIO',[':ID_USUARIO'=>Yii::$app->user->id])->one();
        if($perfilAdministrado){
            $fechaActual = date('Y-m-d');
            $fechaFin = date('Y-m-d');
            $fechaInicio = "1900-01-01";
        }
        
        $model = new Precio();
        $model->titulo = 'Registrar precio';
        
        return $this->render('create', [
            'model' => $model,
            'fechaInicio' => $fechaInicio,
            'fechaFin' => $fechaFin,
            'fechaActual' => $fechaActual
        ]);
    }

    public function actionUpdate($idProductoMercado,$fecRegistro){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Precio::find()->where("ID_PRODUCTO_MERCADO=:ID_PRODUCTO_MERCADO and FLG_HABILITADO in (1,2) and FEC_REGISTRO=TO_DATE(:FEC_REGISTRO,'YYYY-MM-DD')",[':ID_PRODUCTO_MERCADO'=>$idProductoMercado,':FEC_REGISTRO'=>$fecRegistro])->one();
        if(!$model){
            $model = new Precio;
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $valor=0; //out 
        $mensaje=" "; //out 
        \Yii::$app->db->createCommand('CALL SP_R_PRECIO_PROMEDIO(:P_NUM_PRECIO,:P_ID_PRODUCTO_MERCADO,:P_FEC_REGISTRO,:P_RESULTADO_VALOR,:P_RESULTADO_MENSAJE)')
                      ->bindValue(':P_NUM_PRECIO' , str_replace('.',',',str_replace('S/ ', '', $_POST['numPrecio'])))
                      ->bindValue(':P_ID_PRODUCTO_MERCADO', $idProductoMercado)
                      ->bindValue(':P_FEC_REGISTRO', $_POST['fecRegistro'])
                      ->bindParam(":P_RESULTADO_VALOR",$valor,PDO::PARAM_INPUT_OUTPUT, 10)
                      ->bindParam(":P_RESULTADO_MENSAJE",$mensaje,PDO::PARAM_INPUT_OUTPUT, 200)
                      ->execute();
        
       // if($valor==0){
       //     return ['success'=>true,'valor'=>0,'msg'=>$mensaje];
       // }

        //var_dump($model);die;
        $model->titulo = 'Actualizar precio';
        if($request->isAjax){
            if ($request->post()) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->ID_PRODUCTO_MERCADO = $idProductoMercado;
                $model->NUM_PRECIO_1 = (!empty($_POST['numPrecioPosicion']) && $_POST['numPrecioPosicion']=="1")?str_replace('.',',',str_replace('S/ ', '', $_POST['numPrecio'])):$model->NUM_PRECIO_1;
                $model->NUM_PRECIO_2 = (!empty($_POST['numPrecioPosicion']) && $_POST['numPrecioPosicion']=="2")?str_replace('.',',',str_replace('S/ ', '', $_POST['numPrecio'])):$model->NUM_PRECIO_2;
                $model->NUM_PRECIO_3 = (!empty($_POST['numPrecioPosicion']) && $_POST['numPrecioPosicion']=="3")?str_replace('.',',',str_replace('S/ ', '', $_POST['numPrecio'])):$model->NUM_PRECIO_3;
                $model->NUM_PRECIO_4 = (!empty($_POST['numPrecioPosicion']) && $_POST['numPrecioPosicion']=="4")?str_replace('.',',',str_replace('S/ ', '', $_POST['numPrecio'])):$model->NUM_PRECIO_4;
                //$model->FEC_REGISTRO_DEMO = (!empty($_POST['fecRegistro']))?date('d-m-Y H:i:s',strtotime($_POST['fecRegistro'])):$model->FEC_REGISTRO_DEMO;
                $model->FEC_REGISTRO = (!empty($_POST['fecRegistro']))?date('d-m-Y',strtotime($_POST['fecRegistro'])):$model->FEC_REGISTRO;
                $model->ID_USUARIO_REGISTRO =  Yii::$app->user->id ;
                /* 
                $model->ID_PRODUCTO = (!empty($_POST['idProducto']))?$_POST['idProducto']:$model->ID_PRODUCTO;
                $model->ID_ENVASE = (!empty($_POST['idEnvase']))?$_POST['idEnvase']:$model->ID_ENVASE;
                $model->NUM_EQUIVALENCIA = (!empty($_POST['numEquivalencia']))?$_POST['numEquivalencia']:$model->NUM_EQUIVALENCIA;
                $model->FLG_PUBLICADO = (!empty($_POST['flgHabilitado']))?$_POST['flgHabilitado']:$model->FLG_PUBLICADO; */
                $model->FLG_HABILITADO=1;
                if ($model->save()) {
                    return ['success' => true, 'valor' => $valor, 'msg' => $mensaje];
                } else {
                    return ['success' => false, 'valor' => 0, 'msg' => $mensaje];
                }
            }
        }
    }


    public function actionGetListaProductosMercadoPrecio(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productosMercadoPrecio = (new \yii\db\Query())
                ->select(["
                            TM_PRECIO.ID_PRECIO,
                            TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO,
                            TG_PRODUCTO_GENERO.TXT_CODIGO_GENERO,
                            TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO,
                            TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO,
                            TG_PRODUCTO_MERCADO.ID_MERCADO,
                            TG_PRODUCTO.TXT_PRODUCTO,
                            TG_PRODUCTO.TXT_CODIGO_PRODUCTO,
                            TG_ENVASE.NUM_EQUIVALENCIA,
                            TG_ENVASE.TXT_ENVASE,
                            TG_UNIDAD_MEDIDA.TXT_UNIDAD_MEDIDA,
                            TO_CHAR(TM_PRECIO.NUM_PRECIO_1,'9999.99') NUM_PRECIO_1,
                            TO_CHAR(TM_PRECIO.NUM_PRECIO_2,'9999.99') NUM_PRECIO_2,
                            TO_CHAR(TM_PRECIO.NUM_PRECIO_3,'9999.99') NUM_PRECIO_3,
                            TO_CHAR(TM_PRECIO.NUM_PRECIO_4,'9999.99') NUM_PRECIO_4,
                            TM_PRECIO.FEC_REGISTRO
                        "])
                ->from('TG_PRODUCTO_MERCADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO AND TG_PRODUCTO.FLG_HABILITADO=1')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->leftJoin('TG_ENVASE','TG_ENVASE.ID_ENVASE=TG_PRODUCTO_MERCADO.ID_ENVASE')
                ->leftJoin('TG_UNIDAD_MEDIDA','TG_UNIDAD_MEDIDA.ID_UNIDAD_MEDIDA=TG_ENVASE.ID_UNIDAD_MEDIDA');

                if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                    $productosMercadoPrecio = $productosMercadoPrecio->leftJoin('TM_PRECIO',"TM_PRECIO.ID_PRODUCTO_MERCADO=TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO and TM_PRECIO.FLG_HABILITADO in (1,2) and TM_PRECIO.FEC_REGISTRO=TO_DATE('".$_POST['fecRegistro']."','YYYY-MM-DD')");
                }
                
                if(isset($_POST['idProductoGrupo']) && $_POST['idProductoGrupo']!=''){
                    $productosMercadoPrecio = $productosMercadoPrecio->andWhere(['=', 'TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO',$_POST['idProductoGrupo']]);
                }
                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $productosMercadoPrecio = $productosMercadoPrecio->andWhere(['=', 'TG_PRODUCTO_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }
            
            $productosMercadoPrecio = $productosMercadoPrecio->orderBy('TG_PRODUCTO.TXT_CODIGO_PRODUCTO asc,TG_ENVASE.TXT_ENVASE asc, TG_UNIDAD_MEDIDA.TXT_UNIDAD_MEDIDA asc,TG_ENVASE.NUM_EQUIVALENCIA asc ')->all();
            
            if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                $mercado = (new \yii\db\Query())->select('ID_MERCADO_TIPO')->from('TG_MERCADO')->where('ID_MERCADO=:ID_MERCADO',[':ID_MERCADO'=>$_POST['idMercado']])->one();
                if($mercado['ID_MERCADO_TIPO']==1){
                    $parametro = Parametro::findOne(3);
                }else if($mercado['ID_MERCADO_TIPO']==2){
                    $parametro = Parametro::findOne(4);
                }
            }

            

            

            return [
                'success' => true,
                'productosMercadoPrecio' => $productosMercadoPrecio,
                'cantidadAlerta' => $parametro->NUM_VALOR
            ];
        }
    }


    public function actionGetListaProductosMercadoPrecioIndex(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productosMercadoPrecio = (new \yii\db\Query())
                ->select(["
                            TM_PRECIO.ID_PRECIO,
                            TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO,
                            TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO,
                            TG_PRODUCTO_MERCADO.ID_MERCADO,
                            TG_PRODUCTO.TXT_PRODUCTO,
                            TG_PRODUCTO.TXT_CODIGO_PRODUCTO,
                            TG_PRODUCTO.ID_UNIDAD_MEDIDA,
                            TG_ENVASE.NUM_EQUIVALENCIA,
                            TG_ENVASE.TXT_ENVASE,
                            TG_UNIDAD_MEDIDA.TXT_UNIDAD_MEDIDA,
                            TO_CHAR(TM_PRECIO.NUM_PRECIO_1,'9999.99') NUM_PRECIO_1,
                            TO_CHAR(TM_PRECIO.NUM_PRECIO_2,'9999.99') NUM_PRECIO_2,
                            TO_CHAR(TM_PRECIO.NUM_PRECIO_3,'9999.99') NUM_PRECIO_3,
                            TO_CHAR(TM_PRECIO.NUM_PRECIO_4,'9999.99') NUM_PRECIO_4,
                            TM_PRECIO.FEC_REGISTRO,
                            TG_UBIGEO.TXT_DEPARTAMENTO,
                            TG_MERCADO.TXT_MERCADO,
                            TG_PRODUCTO_GRUPO.TXT_CODIGO_GRUPO,
                            TG_PRODUCTO_GRUPO.TXT_PRODUCTO_GRUPO,
                            TG_PRODUCTO_GENERO.TXT_CODIGO_GENERO,
                            TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO,
                            TG_PRODUCTO.TXT_CODIGO_PRODUCTO,
                            TG_PRODUCTO.TXT_PRODUCTO
                        "])
                ->from('TM_PRECIO')
                ->innerJoin('TG_PRODUCTO_MERCADO',"TM_PRECIO.ID_PRODUCTO_MERCADO=TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO")
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->leftJoin('TG_ENVASE','TG_ENVASE.ID_ENVASE=TG_PRODUCTO_MERCADO.ID_ENVASE')
                ->leftJoin('TG_UNIDAD_MEDIDA','TG_UNIDAD_MEDIDA.ID_UNIDAD_MEDIDA=TG_ENVASE.ID_UNIDAD_MEDIDA')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->innerJoin('TG_UBIGEO','TG_UBIGEO.ID_UBIGEO=TG_MERCADO.ID_UBIGEO');
                
                $productosMercadoPrecio = $productosMercadoPrecio->andWhere(['in', "TM_PRECIO.FLG_HABILITADO",["1","2"]]);

                if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                    $productosMercadoPrecio = $productosMercadoPrecio->andWhere(['=', "TO_CHAR(TM_PRECIO.FEC_REGISTRO,'YYYY-MM-DD')",$_POST['fecRegistro']]);
                }

                if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                    $productosMercadoPrecio = $productosMercadoPrecio->andWhere(['=', 'TG_UBIGEO.ID_DEPARTAMENTO',$_POST['idRegion']]);
                }

                if(isset($_POST['idProductoGrupo']) && $_POST['idProductoGrupo']!=''){
                    $productosMercadoPrecio = $productosMercadoPrecio->andWhere(['=', 'TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO',$_POST['idProductoGrupo']]);
                }


                if(isset($_POST['idProductoGenero']) && $_POST['idProductoGenero']!=''){
                    $productosMercadoPrecio = $productosMercadoPrecio->andWhere(['=', 'TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO',$_POST['idProductoGenero']]);
                }
                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $productosMercadoPrecio = $productosMercadoPrecio->andWhere(['=', 'TG_PRODUCTO_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }
            
            $productosMercadoPrecio = $productosMercadoPrecio->all();
            
            return [
                'success' => true,
                'productosMercadoPrecio' => $productosMercadoPrecio
            ];
        }
    }


    public function actionEliminar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && !empty($_POST['listaIdsProductosMercados'])){
            $listaIdsProductosMercados = $_POST['listaIdsProductosMercados'];
            $fecRegistro = $_POST['fecRegistro'];
            //var_dump($listaIdsAbastecimientos);
            
            
            if(Precio::deleteAll(['and',['in', 'ID_PRODUCTO_MERCADO', $listaIdsProductosMercados],['=',"TO_CHAR(FEC_REGISTRO,'YYYY-MM-DD')",$fecRegistro]]) && TRPrecio::deleteAll(['and',['in', 'ID_PRODUCTO_MERCADO', $listaIdsProductosMercados],['=',"TO_CHAR(FEC_REGISTRO,'YYYY-MM-DD')",$fecRegistro]])){
                return [
                    'success' => true
                ];
            }else{
                return [
                    'success' => false
                ];
            }

            
        }
    }


}

