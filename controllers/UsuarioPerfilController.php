<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\UsuarioPerfil;
class UsuarioPerfilController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionAsignar(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new UsuarioPerfil();
        $model->titulo = 'Asignar perfil';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            } else {
                return $this->render('asignar', [
                    'model' => $model,
                ]);
            }
        }
    }

    /*
    public function actionCreate($idPefil){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new PerfilMenu();
        $model->titulo = 'Asignar permidos';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->ID_PERFIL = $idPefil;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = PerfilMenu::findOne($id);
        $model->titulo = 'Asignar permidos';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }*/

    public function actionGetListaUsuarioPerfil(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idUsuario = $_POST['idUsuario'];

            $perfiles = (new \yii\db\Query())
                ->select('TP_PERFIL.*,TC_USUARIO_PERFIL.ID_USUARIO_PERFIL')
                ->from('TP_PERFIL')
                ->leftJoin('TC_USUARIO_PERFIL','TC_USUARIO_PERFIL.ID_PERFIL=TP_PERFIL.ID_PERFIL and TC_USUARIO_PERFIL.ID_USUARIO='.$idUsuario.'')
                ->orderBy('TP_PERFIL.TXT_PERFIL asc')
                ->all();
            return ['success'=>true,'perfiles'=>$perfiles];
        }
    }

    public function actionAsignarPerfil(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idUsuario = $_POST['idUsuario'];
            $idPerfil = $_POST['idPerfil'];
            $flgActivo = $_POST['flgActivo'];

            if($flgActivo=="true"){
                $model = new UsuarioPerfil();
                $model->ID_PERFIL = $idPerfil;
                $model->ID_USUARIO = $idUsuario;
                $model->save();

            }else{
                UsuarioPerfil::find()->where('ID_PERFIL=:ID_PERFIL and ID_USUARIO=:ID_USUARIO',[':ID_PERFIL'=>$idPerfil,':ID_USUARIO'=>$idUsuario])->one()->delete();
            }

            return ['success'=>true];
        }
    }
}
