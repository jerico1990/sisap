<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;

class LoginController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'error') {
            $this->layout = 'login';
        }
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'login';
        $model = new LoginForm();

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['panel/index']);
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['panel/index']);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }


    public function actionOlvido()
    {
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new LoginForm();
        if ($request->isAjax) {
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $correo = $_POST['correo'];
                $usuario = Usuario::find()->where('TXT_EMAIL=:TXT_EMAIL and FLG_HABILITADO=1', [':TXT_EMAIL' => $correo])->all();
                if ($usuario != null) {
                    //$usuarioModel=Usuario::findOne($usuario[0]->ID_USUARIO);
                    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $guid= $this->generate($permitted_chars);
                    $usuario[0]->TXT_TRAMA=$guid;
                    if ($usuario[0]->save()) {
                        $body = Yii::$app->view->renderFile('@app/mail/layouts/plantillaOlvidoContrasena.php', [
                            'correo' => $correo, 'enlace' => 'http://localhost/PD-03-SISAP/web/recuperar/index?id=' . $guid
                        ]);
                        Yii::$app->mail->compose('@app/mail/layouts/html', ['content' => $body])
                            ->setFrom('cesar.gago.egocheaga@gmail.com')
                            ->setTo(strtolower($correo))
                            ->setSubject('Bienvenidos a la plataforma SISAP-MIDAGRI 2021')
                            ->send();
                        return ['success' => true];
                    } else {
                        return ['success' => false];
                    }   
                } else {
                    return ['success' => false];
                }
            } else {
                return $this->render('index', [
                    'model' => $model,
                ]);
            }
        }
    }

   

    function generate($input, $strength = 16)
    {
        
        $input_length = strlen($input);
        $random_string = '';
        for ($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }
}
