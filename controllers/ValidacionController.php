<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
class ValidacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($k,$llave){
        $descriptado = Yii::$app->getSecurity()->decryptByPassword($k,'ENA2021');
        $datos = explode("|", $descriptado);
        if($datos[0]==$llave){
            $this->layout='validacion';

            $username = $datos[0];
            $password = $datos[1];

            $model = new LoginForm();
            if (!Yii::$app->user->isGuest) {
                return $this->redirect(['panel/index']);
            }

            if ($model->load(Yii::$app->request->post())) {
                if($model->login()){
                    $connection = \Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $modelUsuario = Usuario::find()->where('TXT_USUARIO=:TXT_USUARIO and FLG_ACTIVO=1',[':TXT_USUARIO'=>$username])->one();
                        $modelUsuario->TXT_CODIGO_VALIDACION = null;
                        $modelUsuario->update();

                        $transaction->commit();
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        throw $e;
                    }

                    return $this->redirect(['panel/index']);
                }else{
                    $connection = \Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {

                        $modelUsuario = Usuario::find()->where('TXT_USUARIO=:TXT_USUARIO and FLG_ACTIVO=1',[':TXT_USUARIO'=>$username])->one();
                        $modelUsuario->TXT_CODIGO_VALIDACION = null;
                        $modelUsuario->update();

                        $transaction->commit();
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        throw $e;
                    }
                    
                    return $this->redirect(['login/index']);
                }
                
            }

            return $this->render('index',['username'=>$username,'us'=>$username,'ps'=>$password,'k'=>$k]);
        }else{
            echo "no permitido";
        }
    }

    public function actionGetEnviarCodigo(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && isset($_POST['username'])){
            $username = $_POST['username'];

            $codigoAleatorio = random_int(1000, 9999);

            //$usuario = (new \yii\db\Query())->select('TXT_CORREO_ELECTRONICO')->from('ap_tg_usuario')->where('TXT_USUARIO=:TXT_USUARIO',[':TXT_USUARIO'=>$username])->one();

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {

                $modelUsuario = Usuario::find()->where('TXT_USUARIO=:TXT_USUARIO and FLG_ACTIVO=1',[':TXT_USUARIO'=>$username])->one();
                $modelUsuario->TXT_CODIGO_VALIDACION = $codigoAleatorio;
                $modelUsuario->update();

                $transaction->commit();

                $body = Yii::$app->view->renderFile ( '@app/mail/layouts/plantilla.php' , [
                    'codigoAleatorio' => $codigoAleatorio 
                ] );

                Yii::$app->mail->compose('@app/mail/layouts/html',['content' => $body ])
                    ->setFrom('cesar.gago.egocheaga@gmail.com')
                    ->setTo(strtolower($modelUsuario->TXT_CORREO_ELECTRONICO))
                    ->setSubject('Bienvenidos a la plataforma SISAP-MIDAGRI 2021')
                    ->send();

                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
            return ['success'=>false];
        }
    }
}
