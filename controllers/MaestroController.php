<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Envase;
class MaestroController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionGetListaMaestros(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $maestros = (new \yii\db\Query())
                ->select('H.VALOR,H.TXT_DESCRIPCION')
                ->from('TG_MAESTRO H')
                ->innerJoin('TG_MAESTRO P','P.ID_MAESTRO=H.ID_PADRE');

                if(isset($_POST['idMaestro']) && $_POST['idMaestro']!=''){
                    $maestros = $maestros->andWhere(['=', 'P.ID_MAESTRO',$_POST['idMaestro']]);
                }
            $maestros = $maestros->all();

            return ['success'=>true,'maestros'=>$maestros];
        }
    }
}
