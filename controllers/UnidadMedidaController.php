<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\UnidadMedida;
class UnidadMedidaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new UnidadMedida();
        $model->titulo = 'Registrar unidad medida';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //$model->FLG_HABILITADO = 1 ;
                $unidadMedida = UnidadMedida::find()->where('TXT_CODIGO_UNIDAD_MEDIDA=:TXT_CODIGO_UNIDAD_MEDIDA',[':TXT_CODIGO_UNIDAD_MEDIDA'=>$model->TXT_CODIGO_UNIDAD_MEDIDA])->one();
                if($unidadMedida){
                    return ['success'=>false,'msg'=>1];
                }
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false,'msg'=>0];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = UnidadMedida::findOne($id);
        $model->titulo = 'Actualizar unidad medida';
        $txtCodigoUnidadMedida = $model->TXT_CODIGO_UNIDAD_MEDIDA;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($txtCodigoUnidadMedida!=$model->TXT_CODIGO_UNIDAD_MEDIDA){
                    $unidadMedida = UnidadMedida::find()->where('TXT_CODIGO_UNIDAD_MEDIDA=:TXT_CODIGO_UNIDAD_MEDIDA',[':TXT_CODIGO_UNIDAD_MEDIDA'=>$model->TXT_CODIGO_UNIDAD_MEDIDA])->one();
                    if($unidadMedida){
                        return ['success'=>false,'msg'=>1];
                    }
                }

                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false,'msg'=>0];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEliminar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && !empty($_POST['idUnidadMedida'])){
            $idUnidadMedida = $_POST['idUnidadMedida'];
            if(UnidadMedida::updateAll(['FLG_HABILITADO' => '2'], ['in', 'ID_UNIDAD_MEDIDA', $idUnidadMedida])){
                return [
                    'success' => true
                ];
            }else{
                return [
                    'success' => false
                ];
            }
        }
    }

    public function actionGetListaUnidadesMedidas(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $unidadesMedidas = (new \yii\db\Query())
                ->select('TG_UNIDAD_MEDIDA.*,TG_MAESTRO.TXT_DESCRIPCION as TXT_DESCRIPCION_HABILITADO')
                ->from('TG_UNIDAD_MEDIDA')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_UNIDAD_MEDIDA.FLG_HABILITADO')
                ->all();
            return ['success'=>true,'unidadesMedidas'=>$unidadesMedidas];
        }
    }
}
