<?php

namespace app\controllers;

use app\models\Clave;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Grupo;
use app\models\Usuario;

class RecuperarController extends Controller
{


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($id)
    {
        $this->layout = 'login';
        $request = Yii::$app->request;
        $model = new Clave();
        $model->titulo = 'Cambio de Clave';
        if ($request->isAjax) {
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $modelUsuario = Usuario::find()->where('TXT_TRAMA=:TXT_TRAMA', [':TXT_TRAMA' => $id])->one();
                $modelUsuario->TXT_CLAVE =  $model->TXT_CLAVE;
                $modelUsuario->TXT_TRAMA =  "";
                if ($modelUsuario->save()) {
                    return ['success' => true];
                } else {
                    return ['success' => false];
                }
            } else {
                return $this->render('index', [
                    'model' => $model,
                ]);
            }
        } else {
            $model = Clave::find()->where('TXT_TRAMA=:TXT_TRAMA', [':TXT_TRAMA' => $id])->one();
            if ($model != null) {
                $model->titulo = 'Cambio de Clave';
                $model->nombres = $model->TXT_APELLIDO_MATERNO . ' ' . $model->TXT_APELLIDO_PATERNO . ' ' . $model->TXT_NOMBRES;
                $model->TXT_CLAVE = "";
            }else{
                return $this->redirect(['login/index']);
            }
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
