<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Abastecimiento;
use app\models\Parametro;
use app\models\UsuarioPerfil;
use app\models\TemporalAbastecimiento;
use app\models\TRAbastecimiento;

use yii\web\UploadedFile;

class AbastecimientoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        
        return $this->render('index');
    }

    public function actionDashboard(){
        $this->layout='privado';
        
        return $this->render('dashboard');
    }

    public function actionTemporal(){
        $this->layout='privado';
        
        return $this->render('temporal');
    }

    public function actionAdjuntar(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new TemporalAbastecimiento;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->archivo = UploadedFile::getInstance($model, 'archivo');
                if($model->archivo){

                    TemporalAbastecimiento::deleteAll();

                    $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
                    
                    if(!empty($model->archivo->name) && in_array($model->archivo->type,$csvMimes)){
                        
                        if(is_uploaded_file($model->archivo->tempName)){
                            
                            //open uploaded csv file with read only mode
                            $csvFile = fopen($model->archivo->tempName, 'r');

                            $abastecimientoTemporal = [];
                            $fecRegistroValidacion = false;
                            $fecRegistroAnt = '';
                            $contador = 1 ;
                            while(($line = fgetcsv($csvFile,0, ",")) !== FALSE){

                                if($contador == 1 ){
                                    $fecRegistroAnt = trim($line[3]).'-'.trim($line[1]).'-'.trim($line[2]);
                                }

                                $idProductoMercado = trim($line[0]);
                                $idProcedencia = str_pad(trim($line[4]), 6, "0", STR_PAD_RIGHT);
                                $numVolumenProducto = trim($line[5]);
                                $fecRegistro = trim($line[3]).'-'.trim($line[1]).'-'.trim($line[2]);
                                
                                
                                if(trim($line[3]).'-'.trim($line[1]).'-'.trim($line[2]) != $fecRegistroAnt){
                                    return ['success'=>false,'msg'=>1];
                                }

                                $abastecimientoTemporal[] = [
                                                //utf8_encode($this->ClienteActual(trim($line[0]))),
                                                utf8_encode($idProductoMercado),
                                                utf8_encode($idProcedencia),
                                                utf8_encode($numVolumenProducto),
                                                utf8_encode($fecRegistro),
                                ];

                                $contador++;
                            }

                            Yii::$app->db->createCommand()->batchInsert('TT_ABASTECIMIENTO', ['TXT_CODIGO_PRODUCTO_MERCADO','ID_PROCEDENCIA', 'NUM_VOLUMEN_PRODUCTO','FEC_REGISTRO'],$abastecimientoTemporal)->execute();
                            return ['success'=>true];
                        }
                    }
                }
                return ['success'=>false,'msg'=>0];
            }
        }
    }

    public function actionProcesarTemporal(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            Yii::$app->db->createCommand('delete from TM_ABASTECIMIENTO where ID_PRODUCTO_MERCADO in (select TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO from TT_ABASTECIMIENTO inner join TG_PRODUCTO_MERCADO on TG_PRODUCTO_MERCADO.TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO=TT_ABASTECIMIENTO.TXT_CODIGO_PRODUCTO_MERCADO and ID_MERCADO=46) and TM_ABASTECIMIENTO.FEC_REGISTRO in (select distinct FEC_REGISTRO from TT_ABASTECIMIENTO)')->execute();

            Yii::$app->db->createCommand('insert into TM_ABASTECIMIENTO (ID_PRODUCTO_MERCADO,ID_PROCEDENCIA,NUM_VOLUMEN_PRODUCTO,FEC_REGISTRO,FLG_HABILITADO) select TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO,TT_ABASTECIMIENTO.ID_PROCEDENCIA,TT_ABASTECIMIENTO.NUM_VOLUMEN_PRODUCTO,TT_ABASTECIMIENTO.FEC_REGISTRO,1 FLG_HABILITADO from TT_ABASTECIMIENTO inner join TG_PRODUCTO_MERCADO on TG_PRODUCTO_MERCADO.TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO=TT_ABASTECIMIENTO.TXT_CODIGO_PRODUCTO_MERCADO and ID_MERCADO=46')->execute();
            Yii::$app->db->createCommand('delete from TT_ABASTECIMIENTO')->execute();
            return ['success'=>true];
        }
        return ['success'=>false];
    }

    public function actionCreate(){
        $this->layout = 'privado';
        $request = Yii::$app->request;

        $parametro = Parametro::findOne(2);
        $fechaActual = date('Y-m-d');
        if($parametro->FLG_HABILITADO==1){
            $fechaFin = date('Y-m-d');
            $fechaInicio = date("Y-m-d",strtotime($fechaFin."- ".$parametro->NUM_VALOR." days"));
        }else{
            $fechaFin = date('Y-m-d');
            $fechaInicio = "1900-01-01";
        }
        $perfiAdministrado = UsuarioPerfil::find()->where('ID_PERFIL=1 AND ID_USUARIO=:ID_USUARIO',[':ID_USUARIO'=>Yii::$app->user->id])->one();
        if($perfiAdministrado){
            $fechaActual = date('Y-m-d');
            $fechaFin = date('Y-m-d');
            $fechaInicio = "1900-01-01";
        }


        $model = new Abastecimiento();
        $model->titulo = 'Registrar abastecimiento';
        return $this->render('create', [
            'model' => $model,
            'fechaInicio' => $fechaInicio,
            'fechaFin' => $fechaFin,
            'fechaActual' => $fechaActual
        ]);
    }

    public function actionUpdate($idProductoMercado,$fecRegistro,$idAbastecimiento){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        if($idAbastecimiento=="null"){
            $idAbastecimiento=null;
        }
        //$model = Abastecimiento::findOne($id);

        $model = Abastecimiento::findOne($idAbastecimiento);

        //$model = Abastecimiento::find()->where("ID_PRODUCTO_MERCADO=:ID_PRODUCTO_MERCADO and FLG_HABILITADO in (1,2) and FEC_REGISTRO=TO_DATE(:FEC_REGISTRO,'YYYY-MM-DD')",[':ID_PRODUCTO_MERCADO'=>$idProductoMercado,':FEC_REGISTRO'=>$fecRegistro])->one();
        if(!$model){
            $model = new Abastecimiento;
        }


        $model->titulo = 'Actualizar abastecimiento';
        if($request->isAjax){
            if ($request->post()) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->ID_PRODUCTO_MERCADO = $idProductoMercado;

                $model->ID_PROCEDENCIA = (!empty($_POST['idProcedencia']))?$_POST['idProcedencia']:$model->ID_PROCEDENCIA;
                $model->NUM_CANTIDAD_PRODUCTO = (!empty($_POST['numCantidadProducto']))?str_replace('.',',',str_replace('Kg ', '', $_POST['numCantidadProducto'])):$model->NUM_CANTIDAD_PRODUCTO;

                $model->NUM_VOLUMEN_PRODUCTO = (isset($_POST['numVolumenProducto']))?str_replace('.',',',str_replace('Kg ', '', $_POST['numVolumenProducto'])):$model->NUM_VOLUMEN_PRODUCTO;

                $model->FEC_REGISTRO = (!empty($_POST['fecRegistro']))?date('d-m-Y',strtotime($_POST['fecRegistro'])):$model->FEC_REGISTRO;
                $model->ID_USUARIO_REGISTRO =  Yii::$app->user->id ;
                $model->FLG_HABILITADO=1;

                if($model->save()){
                    return ['success'=>true,'idAbastecimiento'=>$model->ID_ABASTECIMIENTO];
                }else{
                    return ['success'=>false];
                }
            }
        }
    }

    public function actionEliminar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && !empty($_POST['listaIdsAbastecimientos'])){
            $listaIdsAbastecimientos = $_POST['listaIdsAbastecimientos'];
            $fecRegistro = $_POST['fecRegistro'];
            //var_dump($listaIdsAbastecimientos);
            //if(Abastecimiento::deleteAll(['and',['in', 'ID_PRODUCTO_MERCADO', $listaIdsProductosMercado],['=',"TO_CHAR(FEC_REGISTRO,'YYYY-MM-DD')",$fecRegistro]]) && TRAbastecimiento::deleteAll(['and',['in', 'ID_PRODUCTO_MERCADO', $listaIdsProductosMercado],['=',"TO_CHAR(FEC_REGISTRO,'YYYY-MM-DD')",$fecRegistro]])){
            if(Abastecimiento::deleteAll(['and',['in', 'ID_ABASTECIMIENTO', $listaIdsAbastecimientos]])){
                TRAbastecimiento::deleteAll(['and',['in', 'ID_ABASTECIMIENTO', $listaIdsAbastecimientos]]);
                
                return [
                    'success' => true
                ];
            }else{
                return [
                    'success' => false
                ];
            }

            
        }
    }

    public function actionGetListaAbastecimientoTemporal(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productosMercadoAbastecimientoTemporal = (new \yii\db\Query())
                ->select("
                            TT_ABASTECIMIENTO.*,
                            TG_MERCADO.TXT_MERCADO,
                            TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO,
                            TG_PRODUCTO_GRUPO.TXT_CODIGO_GRUPO,
                            TG_PRODUCTO_GRUPO.TXT_PRODUCTO_GRUPO,
                            TG_PRODUCTO_GENERO.TXT_CODIGO_GENERO,
                            TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO,
                            TG_PRODUCTO.TXT_CODIGO_PRODUCTO,
                            TG_PRODUCTO.TXT_PRODUCTO,
                            TG_ENVASE.NUM_EQUIVALENCIA,
                            TG_ENVASE.TXT_ENVASE,
                            TG_UNIDAD_MEDIDA.TXT_UNIDAD_MEDIDA,
                            TG_PROCEDENCIA.TXT_PROCEDENCIA
                        ")
                ->from('TT_ABASTECIMIENTO')
                ->leftJoin('TG_PRODUCTO_MERCADO','TG_PRODUCTO_MERCADO.TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO=TT_ABASTECIMIENTO.TXT_CODIGO_PRODUCTO_MERCADO and ID_MERCADO=46')
                ->leftJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->leftJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO.ID_PRODUCTO_GENERO')
                ->leftJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->leftJoin('TG_ENVASE','TG_ENVASE.ID_ENVASE=TG_PRODUCTO_MERCADO.ID_ENVASE')
                ->leftJoin('TG_UNIDAD_MEDIDA','TG_UNIDAD_MEDIDA.ID_UNIDAD_MEDIDA=TG_ENVASE.ID_UNIDAD_MEDIDA')
                ->leftJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->leftJoin('TG_PROCEDENCIA','TG_PROCEDENCIA.ID_PROCEDENCIA=TT_ABASTECIMIENTO.ID_PROCEDENCIA');


            
            $productosMercadoAbastecimientoTemporal = $productosMercadoAbastecimientoTemporal->all();
            
            return [
                'success' => true,
                'productosMercadoAbastecimientoTemporal' => $productosMercadoAbastecimientoTemporal
            ];
        }
    }


    public function actionGetListaAbastecimiento(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productosMercadoAbastecimiento = (new \yii\db\Query())
                ->select(["
                            TM_ABASTECIMIENTO.ID_ABASTECIMIENTO,
                            TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO,
                            TG_PRODUCTO_GENERO.TXT_CODIGO_GENERO,
                            TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO,
                            TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO,
                            TG_PRODUCTO_MERCADO.ID_MERCADO,
                            TG_PRODUCTO.TXT_PRODUCTO,
                            TG_PRODUCTO.TXT_CODIGO_PRODUCTO,
                            TG_ENVASE.NUM_EQUIVALENCIA,
                            TG_ENVASE.TXT_ENVASE,
                            TG_UNIDAD_MEDIDA.TXT_UNIDAD_MEDIDA,
                            TM_ABASTECIMIENTO.ID_PROCEDENCIA,
                            TO_CHAR(TM_ABASTECIMIENTO.NUM_CANTIDAD_PRODUCTO,'999999.99') NUM_CANTIDAD_PRODUCTO,
                            TO_CHAR(TM_ABASTECIMIENTO.NUM_VOLUMEN_PRODUCTO,'999999.99') NUM_VOLUMEN_PRODUCTO,
                            TM_ABASTECIMIENTO.FEC_REGISTRO
                        "])
                ->from('TG_PRODUCTO_MERCADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->leftJoin('TG_ENVASE','TG_ENVASE.ID_ENVASE=TG_PRODUCTO_MERCADO.ID_ENVASE')
                ->leftJoin('TG_UNIDAD_MEDIDA','TG_UNIDAD_MEDIDA.ID_UNIDAD_MEDIDA=TG_ENVASE.ID_UNIDAD_MEDIDA');

                if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                    $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->leftJoin('TM_ABASTECIMIENTO',"TM_ABASTECIMIENTO.ID_PRODUCTO_MERCADO=TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO and TM_ABASTECIMIENTO.FLG_HABILITADO in (1,2) and TM_ABASTECIMIENTO.FEC_REGISTRO=TO_DATE('".$_POST['fecRegistro']."','YYYY-MM-DD')");
                }
                
                if(isset($_POST['idProductoGrupo']) && $_POST['idProductoGrupo']!=''){
                    $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->andWhere(['=', 'TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO',$_POST['idProductoGrupo']]);
                }

                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->andWhere(['=', 'TG_PRODUCTO_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }

            
            $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->all();
            
            return [
                'success' => true,
                'productosMercadoAbastecimiento' => $productosMercadoAbastecimiento
            ];
        }
    }

    public function actionGetListaAbastecimientoIndex(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productosMercadoAbastecimiento = (new \yii\db\Query())
                ->select(["
                            TG_UBIGEO.TXT_DEPARTAMENTO,
                            TG_MERCADO.TXT_MERCADO,
                            TG_PRODUCTO_GRUPO.TXT_CODIGO_GRUPO,
                            TG_PRODUCTO_GRUPO.TXT_PRODUCTO_GRUPO,
                            TG_PRODUCTO_GENERO.TXT_CODIGO_GENERO,
                            TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO,
                            TG_PRODUCTO.TXT_CODIGO_PRODUCTO,
                            TG_PRODUCTO.TXT_PRODUCTO,
                            TG_ENVASE.NUM_EQUIVALENCIA,
                            TG_ENVASE.TXT_ENVASE,
                            TG_UNIDAD_MEDIDA.TXT_UNIDAD_MEDIDA,
                            TO_CHAR(TM_ABASTECIMIENTO.NUM_CANTIDAD_PRODUCTO,'999999.99') NUM_CANTIDAD_PRODUCTO,
                            TO_CHAR(TM_ABASTECIMIENTO.NUM_VOLUMEN_PRODUCTO,'999999.99') NUM_VOLUMEN_PRODUCTO,
                            TM_ABASTECIMIENTO.FEC_REGISTRO,
                            TG_PROCEDENCIA.TXT_PROCEDENCIA
                        "])
                ->from('TM_ABASTECIMIENTO')
                ->innerJoin('TG_PRODUCTO_MERCADO','TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO=TM_ABASTECIMIENTO.ID_PRODUCTO_MERCADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->leftJoin('TG_ENVASE','TG_ENVASE.ID_ENVASE=TG_PRODUCTO_MERCADO.ID_ENVASE')
                ->leftJoin('TG_UNIDAD_MEDIDA','TG_UNIDAD_MEDIDA.ID_UNIDAD_MEDIDA=TG_ENVASE.ID_UNIDAD_MEDIDA')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->innerJoin('TG_UBIGEO','TG_UBIGEO.ID_UBIGEO=TG_MERCADO.ID_UBIGEO')
                ->leftJoin('TG_PROCEDENCIA','TG_PROCEDENCIA.ID_PROCEDENCIA=TM_ABASTECIMIENTO.ID_PROCEDENCIA');
                $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->andWhere(['in', "TM_ABASTECIMIENTO.FLG_HABILITADO",["1","2"]]);

                if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                    $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->andWhere(['=', "TO_CHAR(TM_ABASTECIMIENTO.FEC_REGISTRO,'YYYY-MM-DD')",$_POST['fecRegistro']]);
                }

                if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                    $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->andWhere(['=', 'TG_UBIGEO.ID_DEPARTAMENTO',$_POST['idRegion']]);
                }

                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->andWhere(['=', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }
                
                if(isset($_POST['idProductoGrupo']) && $_POST['idProductoGrupo']!=''){
                    $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->andWhere(['=', 'TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO',$_POST['idProductoGrupo']]);
                }

                if(isset($_POST['idProductoGenero']) && $_POST['idProductoGenero']!=''){
                    $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->andWhere(['=', 'TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO',$_POST['idProductoGenero']]);
                }

                if(isset($_POST['idProductoMercado']) && $_POST['idProductoMercado']!=''){
                    $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->andWhere(['=', 'TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO',$_POST['idProductoMercado']]);
                }

            
            $productosMercadoAbastecimiento = $productosMercadoAbastecimiento->all();
            
            return [
                'success' => true,
                'productosMercadoAbastecimiento' => $productosMercadoAbastecimiento
            ];
        }
    }

    public function actionGetListaRegionesPeso(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $regionesPesos = (new \yii\db\Query())
                ->select(["
                            TG_UBIGEO.ID_DEPARTAMENTO,
                            TG_UBIGEO.TXT_DEPARTAMENTO,
                            sum(TM_ABASTECIMIENTO.NUM_VOLUMEN_PRODUCTO) TOTAL
                        "])
                ->from('TM_ABASTECIMIENTO')
                ->innerJoin('TG_PRODUCTO_MERCADO','TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO=TM_ABASTECIMIENTO.ID_PRODUCTO_MERCADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->innerJoin('TG_UBIGEO','TG_UBIGEO.ID_UBIGEO=TG_MERCADO.ID_UBIGEO');
                $regionesPesos = $regionesPesos->andWhere(['in', "TM_ABASTECIMIENTO.FLG_HABILITADO",["1","2"]]);

                if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                    $regionesPesos = $regionesPesos->andWhere(['=', "TO_CHAR(TM_ABASTECIMIENTO.FEC_REGISTRO,'YYYY-MM-DD')",$_POST['fecRegistro']]);
                }
                 
                if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                    $regionesPesos = $regionesPesos->andWhere(['=', 'TG_UBIGEO.ID_DEPARTAMENTO',$_POST['idRegion']]);
                }
                
                
                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $regionesPesos = $regionesPesos->andWhere(['=', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }

            
            $regionesPesos = $regionesPesos->groupBy('TG_UBIGEO.ID_DEPARTAMENTO,TG_UBIGEO.TXT_DEPARTAMENTO')->all();
            
            return [
                'success' => true,
                'regionesPesos' => $regionesPesos
            ];
        }
    }

    public function actionGetListaGruposPeso(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $gruposPesos = (new \yii\db\Query())
                ->select(["
                            TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO,
                            TG_PRODUCTO_GRUPO.TXT_PRODUCTO_GRUPO,
                            sum(TM_ABASTECIMIENTO.NUM_VOLUMEN_PRODUCTO) TOTAL
                        "])
                ->from('TM_ABASTECIMIENTO')
                ->innerJoin('TG_PRODUCTO_MERCADO','TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO=TM_ABASTECIMIENTO.ID_PRODUCTO_MERCADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->innerJoin('TG_UBIGEO','TG_UBIGEO.ID_UBIGEO=TG_MERCADO.ID_UBIGEO');
                $gruposPesos = $gruposPesos->andWhere(['in', "TM_ABASTECIMIENTO.FLG_HABILITADO",["1","2"]]);

                if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                    $gruposPesos = $gruposPesos->andWhere(['=', "TO_CHAR(TM_ABASTECIMIENTO.FEC_REGISTRO,'YYYY-MM-DD')",$_POST['fecRegistro']]);
                }
                 
                if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                    $gruposPesos = $gruposPesos->andWhere(['=', 'TG_UBIGEO.ID_DEPARTAMENTO',$_POST['idRegion']]);
                }
                
                
                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $gruposPesos = $gruposPesos->andWhere(['=', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);

                }
            $gruposPesos = $gruposPesos->groupBy('TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO,TG_PRODUCTO_GRUPO.TXT_PRODUCTO_GRUPO')->all();
            
            return [
                'success' => true,
                'gruposPesos' => $gruposPesos
            ];
        }
    }

    public function actionGetListaGruposGeneroPeso(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $grupos = (new \yii\db\Query())
                ->select("
                            TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO,
                            TG_PRODUCTO_GRUPO.TXT_PRODUCTO_GRUPO
                        ")
                ->from('TM_ABASTECIMIENTO')
                ->innerJoin('TG_PRODUCTO_MERCADO','TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO=TM_ABASTECIMIENTO.ID_PRODUCTO_MERCADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->innerJoin('TG_UBIGEO','TG_UBIGEO.ID_UBIGEO=TG_MERCADO.ID_UBIGEO');
                $grupos = $grupos->andWhere(['in', "TM_ABASTECIMIENTO.FLG_HABILITADO",["1","2"]]);

                if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                    $grupos = $grupos->andWhere(['=', "TO_CHAR(TM_ABASTECIMIENTO.FEC_REGISTRO,'YYYY-MM-DD')",$_POST['fecRegistro']]);
                }
                 
                if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                    $grupos = $grupos->andWhere(['=', 'TG_UBIGEO.ID_DEPARTAMENTO',$_POST['idRegion']]);
                }
                
                
                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $grupos = $grupos->andWhere(['=', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }

            
            $grupos = $grupos->groupBy('TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO,TG_PRODUCTO_GRUPO.TXT_PRODUCTO_GRUPO')->all();


            $gruposGeneroPesos = (new \yii\db\Query())
                ->select(["
                            TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO,
                            TG_PRODUCTO_GRUPO.TXT_PRODUCTO_GRUPO,
                            TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO,
                            sum(TM_ABASTECIMIENTO.NUM_VOLUMEN_PRODUCTO) TOTAL
                        "])
                ->from('TM_ABASTECIMIENTO')
                ->innerJoin('TG_PRODUCTO_MERCADO','TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO=TM_ABASTECIMIENTO.ID_PRODUCTO_MERCADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->innerJoin('TG_UBIGEO','TG_UBIGEO.ID_UBIGEO=TG_MERCADO.ID_UBIGEO');
                $gruposGeneroPesos = $gruposGeneroPesos->andWhere(['in', "TM_ABASTECIMIENTO.FLG_HABILITADO",["1","2"]]);

                if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                    $gruposGeneroPesos = $gruposGeneroPesos->andWhere(['=', "TO_CHAR(TM_ABASTECIMIENTO.FEC_REGISTRO,'YYYY-MM-DD')",$_POST['fecRegistro']]);
                }
                 
                if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                    $gruposGeneroPesos = $gruposGeneroPesos->andWhere(['=', 'TG_UBIGEO.ID_DEPARTAMENTO',$_POST['idRegion']]);
                }
                
                
                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $gruposGeneroPesos = $gruposGeneroPesos->andWhere(['=', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }
            
            $gruposGeneroPesos = $gruposGeneroPesos->groupBy('TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO,TG_PRODUCTO_GRUPO.TXT_PRODUCTO_GRUPO,TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO')->all();
            
            return [
                'success' => true,
                'grupos' => $grupos,
                'gruposGeneroPesos' => $gruposGeneroPesos
            ];
        }
    }

    public function actionDuplicar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $model = new Abastecimiento;
            $model->ID_PRODUCTO_MERCADO = $_POST['idProductoMercado'];
            $model->FEC_REGISTRO = (!empty($_POST['fecRegistro']))?date('d-m-Y',strtotime($_POST['fecRegistro'])):$model->FEC_REGISTRO;
            $model->ID_USUARIO_REGISTRO =  Yii::$app->user->id ;
            $model->FLG_HABILITADO=1;

            if($model->save()){

                $productoMercadoAbastecimiento = (new \yii\db\Query())
                ->select(["
                            TM_ABASTECIMIENTO.ID_ABASTECIMIENTO,
                            TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO,
                            TG_PRODUCTO_GENERO.TXT_CODIGO_GENERO,
                            TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO,
                            TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO,
                            TG_PRODUCTO_MERCADO.ID_MERCADO,
                            TG_PRODUCTO.TXT_PRODUCTO,
                            TG_PRODUCTO.TXT_CODIGO_PRODUCTO,
                            TG_ENVASE.NUM_EQUIVALENCIA,
                            TG_ENVASE.TXT_ENVASE,
                            TG_UNIDAD_MEDIDA.TXT_UNIDAD_MEDIDA,
                            TM_ABASTECIMIENTO.ID_PROCEDENCIA,
                            TO_CHAR(TM_ABASTECIMIENTO.NUM_CANTIDAD_PRODUCTO,'999999.99') NUM_CANTIDAD_PRODUCTO,
                            TO_CHAR(TM_ABASTECIMIENTO.NUM_VOLUMEN_PRODUCTO,'999999.99') NUM_VOLUMEN_PRODUCTO,
                            TM_ABASTECIMIENTO.FEC_REGISTRO
                        "])
                ->from('TG_PRODUCTO_MERCADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->leftJoin('TG_ENVASE','TG_ENVASE.ID_ENVASE=TG_PRODUCTO_MERCADO.ID_ENVASE')
                ->leftJoin('TG_UNIDAD_MEDIDA','TG_UNIDAD_MEDIDA.ID_UNIDAD_MEDIDA=TG_ENVASE.ID_UNIDAD_MEDIDA')
                ->leftJoin('TM_ABASTECIMIENTO',"TM_ABASTECIMIENTO.ID_PRODUCTO_MERCADO=TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO and TM_ABASTECIMIENTO.FLG_HABILITADO in (1,2) and TM_ABASTECIMIENTO.FEC_REGISTRO=TO_DATE('".$_POST['fecRegistro']."','YYYY-MM-DD')")
                ->where('TM_ABASTECIMIENTO.ID_ABASTECIMIENTO=:ID_ABASTECIMIENTO',[':ID_ABASTECIMIENTO'=>$model->ID_ABASTECIMIENTO])
                ->all();


                return ['success'=>true,'productoMercadoAbastecimiento'=>$productoMercadoAbastecimiento];
            }else{
                return ['success'=>false];
            }
        }
    }

}

