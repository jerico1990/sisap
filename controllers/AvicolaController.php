<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Avicola;
class AvicolaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }


    public function actionCreate(){
        $this->layout = 'privado';
        $request = Yii::$app->request;
        $model = new Avicola();
        $model->titulo = 'Registrar avicola';
        $fechaActual = date('Y-m-d');
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->FEC_REGISTRO = (!empty($model->FEC_REGISTRO))?date('d-m-Y',strtotime($model->FEC_REGISTRO)):$model->FEC_REGISTRO;
                $model->FLG_HABILITADO = '1';
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } 
        } else {
            return $this->render('create', [
                'model' => $model,'fechaActual' => $fechaActual
            ]);
        }
    }



    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Avicola::findOne($id);
        $model->titulo = 'Actualizar avicola';
        if($request->isAjax){
            if ($request->post()) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $model->ID_MERCADO_UNIDAD_PRODUCCION = (!empty($_POST['idUnidadProduccion']))?$_POST['idUnidadProduccion']:$model->ID_MERCADO_UNIDAD_PRODUCCION;
                $model->NUM_CANTIDAD_PRODUCTO = (!empty($_POST['idCantidadUnidades']))?$_POST['idCantidadUnidades']:$model->NUM_CANTIDAD_PRODUCTO;
                $model->NUM_TOTAL = (!empty($_POST['idTotal']))?str_replace('.',',',str_replace('', '', $_POST['idTotal'])):$model->NUM_TOTAL;
                $model->NUM_PESO_PROMEDIO = (!empty($_POST['idPesoPromedio']))?str_replace('.',',',str_replace('', '', $_POST['idPesoPromedio'])):$model->NUM_PESO_PROMEDIO;
                $model->NUM_PRECIO_GRANJA = (!empty($_POST['idPrecioGranja']))?str_replace('.',',',str_replace('S/ ', '', $_POST['idPrecioGranja'])):$model->NUM_PRECIO_GRANJA;
                $model->NUM_PRECIO_CENTRO_ACOPIO = (!empty($_POST['idPrecioCentroAcopio']))?str_replace('.',',',str_replace('S/ ', '', $_POST['idPrecioCentroAcopio'])):$model->NUM_PRECIO_CENTRO_ACOPIO;

                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            }
        }
    }



    public function actionGetListaProductoAvicola(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productosMercadoAvicola = (new \yii\db\Query())
                ->select("*")
                ->from('VW_LISTA_AVICOLA');
        
            if(isset($_POST['idProductoMercado']) && $_POST['idProductoMercado']!=''){
                $productosMercadoAvicola = $productosMercadoAvicola->andWhere(['=', 'VW_LISTA_AVICOLA.ID_PRODUCTO_MERCADO',$_POST['idProductoMercado']]);
            }

            if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                $productosMercadoAvicola = $productosMercadoAvicola->andWhere("VW_LISTA_AVICOLA.FLG_HABILITADO=1 and VW_LISTA_AVICOLA.FEC_REGISTRO=TO_DATE('".$_POST['fecRegistro']."','YYYY-MM-DD')");
            }

            if(isset($_POST['idUnidadProduccion']) && $_POST['idUnidadProduccion']!=''){
                $productosMercadoAvicola = $productosMercadoAvicola->andWhere(['=', 'VW_LISTA_AVICOLA.ID_MERCADO_UNIDAD_PRODUCCION',$_POST['idUnidadProduccion']]);
            }
       
            $productosMercadoAvicola = $productosMercadoAvicola->all();
            
            return [
                'success' => true,
                'productosMercadoAvicola' => $productosMercadoAvicola
            ];
        }
    }

    
    public function actionEliminar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && !empty($_POST['listaIdsAvicola'])){
            $listaIdsAvicola = $_POST['listaIdsAvicola'];
            //var_dump($listaIdsAbastecimientos);
            if(Avicola::updateAll(['FLG_HABILITADO' => '2'], ['in', 'ID_AVICOLA', $listaIdsAvicola])){
                return [
                    'success' => true
                ];
            }else{
                return [
                    'success' => false
                ];
            }

            
        }
    }


    public function actionGetListaAvicolaIndex(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productosMercadoAvicola = (new \yii\db\Query())
                ->select(["
                            TG_UBIGEO.TXT_DEPARTAMENTO,
                            TG_MERCADO.TXT_MERCADO,
                            TG_PRODUCTO_GRUPO.TXT_CODIGO_GRUPO,
                            TG_PRODUCTO_GRUPO.TXT_PRODUCTO_GRUPO,
                            TG_PRODUCTO_GENERO.TXT_CODIGO_GENERO,
                            TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO,
                            TG_PRODUCTO.TXT_CODIGO_PRODUCTO,
                            TG_PRODUCTO.TXT_PRODUCTO,
                            TM_AVICOLA.NUM_CANTIDAD_PRODUCTO,
                            TO_CHAR(TM_AVICOLA.NUM_TOTAL,'99999.99') NUM_TOTAL,
                            TO_CHAR(TM_AVICOLA.NUM_PESO_PROMEDIO,'99999.99') NUM_PESO_PROMEDIO,
                            TO_CHAR(TM_AVICOLA.NUM_PRECIO_GRANJA,'99999.99') NUM_PRECIO_GRANJA,
                            TO_CHAR(TM_AVICOLA.NUM_PRECIO_CENTRO_ACOPIO,'99999.99') NUM_PRECIO_CENTRO_ACOPIO,
                            TM_AVICOLA.FEC_REGISTRO,
                            TM.TXT_MERCADO AS UNIDAD_PRODUCCION
                        "])
                ->from('TM_AVICOLA')
                ->innerJoin('TG_PRODUCTO_MERCADO',"TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO=TM_AVICOLA.ID_PRODUCTO_MERCADO")
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->innerJoin('TG_MERCADO AS TM','TM.ID_MERCADO=TM_AVICOLA.ID_MERCADO_UNIDAD_PRODUCCION')
                ->innerJoin('TG_UBIGEO','TG_UBIGEO.ID_UBIGEO=TG_MERCADO.ID_UBIGEO');
                $productosMercadoAvicola = $productosMercadoAvicola->andWhere(['=', "TM_AVICOLA.FLG_HABILITADO","1"]);

                if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                    $productosMercadoAvicola = $productosMercadoAvicola->andWhere(['=', "TO_CHAR(TM_AVICOLA.FEC_REGISTRO,'YYYY-MM-DD')",$_POST['fecRegistro']]);
                }

                if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                    $productosMercadoAvicola = $productosMercadoAvicola->andWhere(['=', 'TG_UBIGEO.ID_DEPARTAMENTO',$_POST['idRegion']]);
                }

                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $productosMercadoAvicola = $productosMercadoAvicola->andWhere(['=', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }
                
                if(isset($_POST['idProductoGrupo']) && $_POST['idProductoGrupo']!=''){
                    $productosMercadoAvicola = $productosMercadoAvicola->andWhere(['=', 'TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO',$_POST['idProductoGrupo']]);
                }

                if(isset($_POST['idProductoGenero']) && $_POST['idProductoGenero']!=''){
                    $productosMercadoAvicola = $productosMercadoAvicola->andWhere(['=', 'TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO',$_POST['idProductoGenero']]);
                }

                if(isset($_POST['idProductoMercado']) && $_POST['idProductoMercado']!=''){
                    $productosMercadoAvicola = $productosMercadoAvicola->andWhere(['=', 'TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO',$_POST['idProductoMercado']]);
                }
            
            $productosMercadoAvicola = $productosMercadoAvicola->all();
            
            return [
                'success' => true,
                'productosMercadoAvicola' => $productosMercadoAvicola
            ];
        }
    }


    public function actionGetUnidadProduccion(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $regiones = (new \yii\db\Query())
                ->select('ID_DEPARTAMENTO,TXT_DEPARTAMENTO')
                ->from('TG_UBIGEO')
                ->distinct()
                ->orderBy('TXT_DEPARTAMENTO asc')
                ->all();
            return ['success'=>true,'regiones'=>$regiones];
        }
    }




}

