<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\UsuarioMercado;
class UsuarioMercadoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionAsignar(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new UsuarioMercado();
        $model->titulo = 'Asignar mercado';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            } else {
                return $this->render('asignar', [
                    'model' => $model,
                ]);
            }
        }
    }

    /*
    public function actionCreate($idPefil){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new PerfilMenu();
        $model->titulo = 'Asignar permidos';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->ID_PERFIL = $idPefil;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = PerfilMenu::findOne($id);
        $model->titulo = 'Asignar permidos';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }*/

    public function actionGetListaUsuarioMercado(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idUsuario = $_POST['idUsuario'];

            $mercados = (new \yii\db\Query())
                ->select('TG_MERCADO.*,TM_USUARIO_MERCADO.ID_USUARIO_MERCADO,TG_MERCADO_TIPO.TXT_MERCADO_TIPO,TG_UBIGEO.TXT_DISTRITO,TG_UBIGEO.TXT_PROVINCIA,TG_UBIGEO.TXT_DEPARTAMENTO')
                ->from('TG_MERCADO')
                ->leftJoin('TM_USUARIO_MERCADO','TM_USUARIO_MERCADO.ID_MERCADO=TG_MERCADO.ID_MERCADO and TM_USUARIO_MERCADO.ID_USUARIO='.$idUsuario.'')
                ->leftJoin('TG_MERCADO_TIPO','TG_MERCADO_TIPO.ID_MERCADO_TIPO=TG_MERCADO.ID_MERCADO_TIPO')
                ->leftJoin('TG_UBIGEO','TG_UBIGEO.ID_UBIGEO=TG_MERCADO.ID_UBIGEO')
                ->orderBy('TG_MERCADO.TXT_MERCADO asc')
                ->all();
            return ['success'=>true,'mercados'=>$mercados];
        }
    }

    public function actionAsignarMercado(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idUsuario = $_POST['idUsuario'];
            $idMercado = $_POST['idMercado'];
            $flgActivo = $_POST['flgActivo'];

            if($flgActivo=="true"){
                $model = new UsuarioMercado();
                $model->ID_MERCADO = $idMercado;
                $model->ID_USUARIO = $idUsuario;
                $model->save();

            }else{
                UsuarioMercado::find()->where('ID_MERCADO=:ID_MERCADO and ID_USUARIO=:ID_USUARIO',[':ID_MERCADO'=>$idMercado,':ID_USUARIO'=>$idUsuario])->one()->delete();
            }

            return ['success'=>true];
        }
    }
}
