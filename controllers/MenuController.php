<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Menu;

class MenuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

/* 
    public function actionCreate($idPefil){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new PerfilMenu();
        $model->titulo = 'Asignar permidos';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->ID_PERFIL = $idPefil;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = PerfilMenu::findOne($id);
        $model->titulo = 'Asignar permidos';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    } */

    public function actionUsuarioMenu(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idUsuario = Yii::$app->user->identity->id;
            /*$connection = \Yii::$app->db;
            
            $listaModulos = 
                $connection->createCommand('SELECT VW_LISTA_MENU.* FROM VW_LISTA_MENU INNER JOIN (Select DISTINCT PM.ID_MENU 
                from TC_USUARIO_PERFIL UP 
                INNER JOIN TC_PERFIL_MENU PM ON PM.ID_PERFIL=UP.ID_PERFIL
                where UP.ID_USUARIO='.$idUsuario.') T on VW_LISTA_MENU.ID_MENU=T.ID_MENU')->queryAll();
            return ['success'=>true,'listaModulos'=>$listaModulos];*/
            //var_dump($menu);die;

            $idModulos = (new \yii\db\Query())
                ->select('VW_LISTA_MENU.ID_MODULO,VW_LISTA_MENU.TXT_MODULO')
                ->from('TP_USUARIO')
                ->innerJoin('TC_USUARIO_PERFIL','TC_USUARIO_PERFIL.ID_USUARIO=TP_USUARIO.ID_USUARIO')
                ->innerJoin('TC_PERFIL_MENU','TC_PERFIL_MENU.ID_PERFIL=TC_USUARIO_PERFIL.ID_PERFIL')
                ->innerJoin('VW_LISTA_MENU','VW_LISTA_MENU.ID_MENU=TC_PERFIL_MENU.ID_MENU')
                ->where('TP_USUARIO.ID_USUARIO=:ID_USUARIO',[':ID_USUARIO'=>$idUsuario])
                ->groupBy('VW_LISTA_MENU.ID_MODULO,VW_LISTA_MENU.TXT_MODULO')
                ->orderBy('VW_LISTA_MENU.ID_MODULO asc')
                ->all();
            $listaModulos = [];
            foreach($idModulos as $idModulo){
                $idCategorias = (new \yii\db\Query())
                    ->select('VW_LISTA_MENU.ID_CATEGORIA,VW_LISTA_MENU.TXT_CATEGORIA')
                    ->from('TP_USUARIO')
                    ->innerJoin('TC_USUARIO_PERFIL','TC_USUARIO_PERFIL.ID_USUARIO=TP_USUARIO.ID_USUARIO')
                    ->innerJoin('TC_PERFIL_MENU','TC_PERFIL_MENU.ID_PERFIL=TC_USUARIO_PERFIL.ID_PERFIL')
                    ->innerJoin('VW_LISTA_MENU','VW_LISTA_MENU.ID_MENU=TC_PERFIL_MENU.ID_MENU')
                    ->where('TP_USUARIO.ID_USUARIO=:ID_USUARIO and VW_LISTA_MENU.ID_MODULO=:ID_MODULO',[':ID_USUARIO'=>$idUsuario,':ID_MODULO'=>$idModulo['ID_MODULO']])
                    ->groupBy('VW_LISTA_MENU.ID_CATEGORIA,VW_LISTA_MENU.TXT_CATEGORIA')
                    ->orderBy('VW_LISTA_MENU.ID_CATEGORIA asc')
                    ->all();
                    $listaCategorias = [];

                    foreach($idCategorias as $idCategoria){
                        $listaMenus = (new \yii\db\Query())
                            ->select('VW_LISTA_MENU.ID_MENU as idMenu,VW_LISTA_MENU.TXT_MENU as txtMenu,VW_LISTA_MENU.TXT_LINK as txtLink')
                            ->from('TP_USUARIO')
                            ->innerJoin('TC_USUARIO_PERFIL','TC_USUARIO_PERFIL.ID_USUARIO=TP_USUARIO.ID_USUARIO')
                            ->innerJoin('TC_PERFIL_MENU','TC_PERFIL_MENU.ID_PERFIL=TC_USUARIO_PERFIL.ID_PERFIL')
                            ->innerJoin('VW_LISTA_MENU','VW_LISTA_MENU.ID_MENU=TC_PERFIL_MENU.ID_MENU')
                            ->where('TP_USUARIO.ID_USUARIO=:ID_USUARIO and VW_LISTA_MENU.ID_CATEGORIA=:ID_CATEGORIA',[':ID_USUARIO'=>$idUsuario,':ID_CATEGORIA'=>$idCategoria['ID_CATEGORIA']])
                            ->groupBy('VW_LISTA_MENU.ID_MENU,VW_LISTA_MENU.TXT_MENU,VW_LISTA_MENU.TXT_LINK')
                            ->all();
                        

                        array_push($listaCategorias, [
                            'idCategoria'=>$idCategoria['ID_CATEGORIA'],
                            'txtCategoria'=>$idCategoria['TXT_CATEGORIA'],
                            'listaMenus'=>$listaMenus
                        ]);
                    }
                array_push($listaModulos,  [
                    'idModulo'=> $idModulo['ID_MODULO'],
                    'txtModulo'=> $idModulo['TXT_MODULO'],
                    'listaCategorias'=>$listaCategorias
                ]);

                //var_dump($listaModulo);die;
                
            }
            return ['success'=>true,'listaModulos'=>$listaModulos];


            
        }
    }

    
}
