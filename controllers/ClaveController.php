<?php

namespace app\controllers;

use app\models\Clave;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Grupo;
use app\models\Usuario;

class ClaveController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        $this->layout = 'privado';
        $request = Yii::$app->request;
        $model = new Clave();

        if ($request->isAjax) {
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $idUsuario = Yii::$app->user->identity->id;
                $modelUsuario = Usuario::find()->where('ID_USUARIO=:ID_USUARIO', [':ID_USUARIO' => $idUsuario])->one();
                if ($modelUsuario->TXT_CLAVE == $_POST['claveanterior']) {
                    $modelUsuario->TXT_CLAVE =  $model->TXT_CLAVE;
                    if ($modelUsuario->save()) {
                        return ['success' => true];
                    } else {
                        return ['success' => false];
                    }
                } else {
                    return $this->render('index', ['success' => false, 'model' => $model]);
                }
            } else {
                return $this->render('index', [
                    'model' => $model,
                ]);
            }
        } else {
            $idUsuario = Yii::$app->user->identity->id;
            $model = Clave::find()->where('ID_USUARIO=:ID_USUARIO', [':ID_USUARIO' => $idUsuario])->one();
            $model->titulo = 'Cambio de Clave';
            $model->nombres = $model->TXT_APELLIDO_MATERNO . ' ' . $model->TXT_APELLIDO_PATERNO . ' ' . $model->TXT_NOMBRES;
            $model->TXT_CLAVE = "";
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
