<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Abastecimiento;
use app\models\Parametro;
use app\models\UsuarioPerfil;
use app\models\TemporalAbastecimiento;
use app\models\TRAbastecimiento;

use yii\web\UploadedFile;

class ReporteAvicolaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        
        return $this->render('index');
    }

    public function actionGetRegiones(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $regiones = (new \yii\db\Query())
                ->select('ID_DEPARTAMENTO,TXT_DEPARTAMENTO')
                ->from('TG_UBIGEO')
                ->distinct()
                ->orderBy('TXT_DEPARTAMENTO asc')
                ->all();
            return ['success'=>true,'regiones'=>$regiones];
        }
    }


    public function actionGetListaMercadosTipo(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $mercadosTipo = (new \yii\db\Query())
                ->select('TG_MERCADO_TIPO.*,TG_MAESTRO.TXT_DESCRIPCION as TXT_DESCRIPCION_HABILITADO')
                ->from('TG_MERCADO_TIPO')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_MERCADO_TIPO.FLG_HABILITADO')
                ->where('TG_MERCADO_TIPO.FLG_HABILITADO=1 and TG_MERCADO_TIPO.ID_MERCADO_TIPO in (4)')
                ->all();
            return ['success'=>true,'mercadosTipo'=>$mercadosTipo];
        }
    }


    public function actionGetListaOpcionesMercados(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $mercados = (new \yii\db\Query())
                ->select('TG_MERCADO.ID_MERCADO,TG_MERCADO.ID_UBIGEO,TG_MERCADO.TXT_MERCADO,TG_MERCADO_TIPO.TXT_MERCADO_TIPO')
                ->from('TG_MERCADO')
                ->innerJoin('TG_MERCADO_TIPO','TG_MERCADO_TIPO.ID_MERCADO_TIPO=TG_MERCADO.ID_MERCADO_TIPO');
            
            if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                $mercados = $mercados->andWhere(['in', 'SUBSTR(TG_MERCADO.ID_UBIGEO,1,2)',$_POST['idRegion']]);
            }

            if(isset($_POST['idMercadoTipo']) && $_POST['idMercadoTipo']!=''){
                $mercados = $mercados->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',$_POST['idMercadoTipo']]);
            }
            



            $mercados = $mercados->all();
            return ['success'=>true,'mercados'=>$mercados];
        }
    }

    public function actionGetListaAvicolas(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            
            
            if(isset($_POST['diario_inicio']) && $_POST['diario_inicio']!='' && isset($_POST['diario_fin']) && $_POST['diario_fin']!=''){
                $avicolas = (new \yii\db\Query())
                    ->select(["TXT_ANIO,TXT_MES,TXT_DIA,TXT_DEPARTAMENTO,TXT_MERCADO,TXT_PRODUCTO,NUM_CANTIDAD_PRODUCTO,TO_CHAR(NUM_TOTAL,'99999.99') asc NUM_TOTAL,TO_CHAR(NUM_PESO_PROMEDIO,'99999.99') NUM_PESO_PROMEDIO,TO_CHAR(NUM_PRECIO_GRANJA,'99999.99') NUM_PRECIO_GRANJA,TO_CHAR(NUM_PRECIO_CENTRO_ACOPIO,'99999.99') NUM_PRECIO_CENTRO_ACOPIO
                    "])
                    ->from('TR_AVICOLA');
            }

            if(isset($_POST['meses']) && $_POST['meses']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='2'){
                
                $avicolas = (new \yii\db\Query())
                ->select(["TXT_ANIO,TXT_MES,TXT_DEPARTAMENTO,TXT_MERCADO,TXT_PRODUCTO,NUM_CANTIDAD_PRODUCTO,TO_CHAR(NUM_TOTAL,'99999.99') NUM_TOTAL,TO_CHAR(NUM_PESO_PROMEDIO,'99999.99') NUM_PESO_PROMEDIO,TO_CHAR(NUM_PRECIO_GRANJA,'99999.99') NUM_PRECIO_GRANJA,TO_CHAR(NUM_PRECIO_CENTRO_ACOPIO,'99999.99') NUM_PRECIO_CENTRO_ACOPIO
                "])
                ->from('TR_AVICOLA');
            }

            if(isset($_POST['anios']) && $_POST['anios']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='3'){
                
                $avicolas = (new \yii\db\Query())
                ->select(["TXT_ANIO,TXT_DEPARTAMENTO,TXT_MERCADO,TXT_PRODUCTO,NUM_CANTIDAD_PRODUCTO,TO_CHAR(NUM_TOTAL,'99999.99') NUM_TOTAL,TO_CHAR(NUM_PESO_PROMEDIO,'99999.99') NUM_PESO_PROMEDIO,TO_CHAR(NUM_PRECIO_GRANJA,'99999.99') NUM_PRECIO_GRANJA,TO_CHAR(NUM_PRECIO_CENTRO_ACOPIO,'99999.99') NUM_PRECIO_CENTRO_ACOPIO
                "])
                ->from('TR_AVICOLA');
            }

            
            

            if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                $avicolas = $avicolas->andWhere(['=', "TO_CHAR(TM_PRECIO.FEC_REGISTRO,'YYYY-MM-DD')",$_POST['fecRegistro']]);
            }

            if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                $avicolas = $avicolas->andWhere(['in', 'ID_DEPARTAMENTO',$_POST['idRegion']]);
            }

            if(isset($_POST['idMercadoTipo']) && $_POST['idMercadoTipo']!=''){
                $avicolas = $avicolas->andWhere(['in', 'ID_MERCADO_TIPO',$_POST['idMercadoTipo']]);
            }


            if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                $avicolas = $avicolas->andWhere(['in', 'ID_MERCADO',$_POST['idMercado']]);
            }

            if(isset($_POST['variedades']) && $_POST['variedades']!=''){
                $avicolas = $avicolas->andWhere(['in', 'ID_PRODUCTO',$_POST['variedades']]);
            }

            if(isset($_POST['diario_inicio']) && $_POST['diario_inicio']!='' && isset($_POST['diario_fin']) && $_POST['diario_fin']!=''){
                /* $fechaListaInicio = explode("-", $_POST['diario_inicio']);
                $fechaListaFin = explode("-", $_POST['diario_fin']);

                $anioInicio = $fechaListaInicio[0];
                $mesInicio = $fechaListaInicio[1];
                $diaInicio = $fechaListaInicio[2];

                $anioFin = $fechaListaFin[0];
                $mesFin = $fechaListaFin[1];
                $diaFin = $fechaListaFin[2]; */

                /* $abastecimientos = $abastecimientos->andWhere(['=', 'TXT_ANIO',$anio]);
                $abastecimientos = $abastecimientos->andWhere(['=', 'TXT_MES',$mes]);
                $abastecimientos = $abastecimientos->andWhere(['=', 'TXT_DIA',$dia]); */

                $avicolas = $avicolas->andWhere(['between', "TO_CHAR(FEC_REGISTRO,'YYYY-MM-DD')",$_POST['diario_inicio'],$_POST['diario_fin']]);


            }

            if(isset($_POST['mensual']) && $_POST['mensual']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='2'){
                /* $fechaLista = explode("-", $_POST['mensual']);

                $anio = $fechaLista[0];
                $mes = $fechaLista[1]; */
                $meses = $_POST['meses'];
                $anios = $_POST['anios'];
                $avicolas = $avicolas->andWhere(['in', 'TXT_ANIO',$meses]);
                $avicolas = $avicolas->andWhere(['in', 'TXT_MES',$anios]);
                //$avicolas = $avicolas->andWhere(['=', 'ID_MERCADO',$_POST['idMercado']]);
            }

            if(isset($_POST['anios']) && $_POST['anios']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='3'){
                $anios = $_POST['anios'];
                $avicolas = $avicolas->andWhere(['in', 'TXT_ANIO',$anios]);
                //$avicolas = $avicolas->andWhere(['=', 'ID_MERCADO',$_POST['idMercado']]);
            }
            
            if(isset($_POST['diario_inicio']) && $_POST['diario_inicio']!='' && isset($_POST['diario_fin']) && $_POST['diario_fin']!=''){
                $avicolas = $avicolas->groupBy('TXT_ANIO,TXT_MES,TXT_DIA,TXT_DEPARTAMENTO,TXT_MERCADO,TXT_PRODUCTO,NUM_CANTIDAD_PRODUCTO,NUM_TOTAL,NUM_PESO_PROMEDIO,NUM_PRECIO_GRANJA,NUM_PRECIO_CENTRO_ACOPIO')->all();
            }
            
            if(isset($_POST['meses']) && $_POST['meses']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='2'){
                $avicolas = $avicolas->groupBy('TXT_ANIO,TXT_MES,TXT_DEPARTAMENTO,TXT_MERCADO,TXT_PRODUCTO,NUM_CANTIDAD_PRODUCTO,NUM_TOTAL,NUM_PESO_PROMEDIO,NUM_PRECIO_GRANJA,NUM_PRECIO_CENTRO_ACOPIO')->all();
            }

            if(isset($_POST['anios']) && $_POST['anios']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='3'){
                $avicolas = $avicolas->groupBy('TXT_ANIO,TXT_DEPARTAMENTO,TXT_MERCADO,TXT_PRODUCTO,NUM_CANTIDAD_PRODUCTO,NUM_TOTAL,NUM_PESO_PROMEDIO,NUM_PRECIO_GRANJA,NUM_PRECIO_CENTRO_ACOPIO')->all();
            }
            
            return [
                'success' => true,
                'avicolas' => $avicolas
            ];
        }
    }



    public function actionGetListaOpcionesGenerosVariedades(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $generos = (new \yii\db\Query())
                ->select('TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO,TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO')
                ->from('TG_PRODUCTO_MERCADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO')
                ->leftJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO');
                

            if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                $generos = $generos->andWhere(['in', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);
            }
            $generos = $generos
                        ->groupBy('TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO,TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO')
                        ->orderBy('TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO asc')
                        ->all();

            $listaGeneros = [];

            foreach($generos as $genero){
                $variedades = (new \yii\db\Query())
                    ->select('TG_PRODUCTO.ID_PRODUCTO idProducto,TG_PRODUCTO.TXT_PRODUCTO txtProducto')
                    ->from('TG_PRODUCTO_MERCADO')
                    ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                    ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO')
                    ->leftJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO');
                    
                    $variedades = $variedades->andWhere(['=', 'TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO',$genero['ID_PRODUCTO_GENERO']]);
                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $variedades = $variedades->andWhere(['in', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }

                /* if(isset($_POST['variedades']) && $_POST['variedades']!=''){
                    $variedades = $variedades->andWhere(['in', 'TG_PRODUCTO.ID_PRODUCTO',$_POST['variedades']]);
                } */
                $variedades = $variedades
                            ->groupBy('TG_PRODUCTO.ID_PRODUCTO,TG_PRODUCTO.TXT_PRODUCTO')
                            ->orderBy('TG_PRODUCTO.TXT_PRODUCTO asc')
                            ->all();


                array_push($listaGeneros, [
                    'idProductoGenero'=>$genero['ID_PRODUCTO_GENERO'],
                    'txtProductoGenero'=>$genero['TXT_PRODUCTO_GENERO'],
                    'variedades'=>$variedades
                ]);
            }

            return ['success'=>true,'generos'=>$listaGeneros];
        }
    }

}

