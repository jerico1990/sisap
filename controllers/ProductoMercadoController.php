<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ProductoMercado;
class ProductoMercadoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionCreate(){
        $this->layout = 'privado';
        $request = Yii::$app->request;
        $model = new ProductoMercado();
        $model->titulo = 'Registrar producto x mercado';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->FLG_HABILITADO=1;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } 
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = ProductoMercado::findOne($id);
        $model->titulo = 'Actualizar producto x mercado';
        if($request->isAjax){
            if ($request->post()) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                

                $idProducto = $model->ID_PRODUCTO;
                $idEnvase = $model->ID_ENVASE;
                //$numEquivalencia = $model->NUM_EQUIVALENCIA;
                
                $model->ID_PRODUCTO = (!empty($_POST['idProducto']))?$_POST['idProducto']:$model->ID_PRODUCTO;
                $model->ID_ENVASE = (!empty($_POST['idEnvase']))?$_POST['idEnvase']:$model->ID_ENVASE;
                //$model->NUM_EQUIVALENCIA = (!empty($_POST['numEquivalencia']))?$_POST['numEquivalencia']:$model->NUM_EQUIVALENCIA;
                $model->FLG_PUBLICADO = (!empty($_POST['flgHabilitado']))?$_POST['flgHabilitado']:$model->FLG_PUBLICADO;

                $model->TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO = (!empty($_POST['txtCodigoProductoMercadoAlternativo']))?$_POST['txtCodigoProductoMercadoAlternativo']:$model->TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO;


                $cantidadProductoMercado = ProductoMercado::find()->where('ID_MERCADO=:ID_MERCADO AND ID_PRODUCTO=:ID_PRODUCTO AND ID_ENVASE=:ID_ENVASE AND TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO=:TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO AND ID_PRODUCTO_MERCADO!=:ID_PRODUCTO_MERCADO',[
                    ':ID_MERCADO'=>$model->ID_MERCADO,
                    ':ID_PRODUCTO'=>$model->ID_PRODUCTO,
                    ':ID_ENVASE'=>$model->ID_ENVASE,
                    ':TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO'=> $model->TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO,
                    ':ID_PRODUCTO_MERCADO'=>$id
                ])->count();
                //var_dump($cantidadProductoMercado);die;
                if($cantidadProductoMercado==1){
                    $model->ID_PRODUCTO = (!empty($_POST['idProducto']))?NULL:$model->ID_PRODUCTO;
                    $model->ID_ENVASE = (!empty($_POST['idEnvase']))?NULL:$model->ID_ENVASE;
                    $model->TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO = (!empty($_POST['txtCodigoProductoMercadoAlternativo']))?NULL:$model->TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO;
                    //$model->NUM_EQUIVALENCIA = (!empty($_POST['numEquivalencia']))?NULL:$model->NUM_EQUIVALENCIA;
                    $model->save();

                    return ['success'=>false,'msg'=>1];
                }

                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false,'msg'=>0];
                }
            }
        }
    }


    public function actionGetListaProductos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productosMercado = (new \yii\db\Query())
                ->select("
                        TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO,
                        TG_PRODUCTO.TXT_CODIGO_PRODUCTO,
                        TG_PRODUCTO.TXT_PRODUCTO,
                        TG_ENVASE.TXT_ENVASE
                        ")
                ->from('TG_PRODUCTO_MERCADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->leftJoin('TG_ENVASE','TG_ENVASE.ID_ENVASE=TG_PRODUCTO_MERCADO.ID_ENVASE');
            
            $productosMercado = $productosMercado->andWhere(['=', "TG_PRODUCTO_MERCADO.FLG_HABILITADO","1"]);

            if(isset($_POST['idProductoGenero']) && $_POST['idProductoGenero']!=''){
                $productosMercado = $productosMercado->andWhere(['=', 'TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO',$_POST['idProductoGenero']]);
            }
            if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                $productosMercado = $productosMercado->andWhere(['=', 'TG_PRODUCTO_MERCADO.ID_MERCADO',$_POST['idMercado']]);
            }

            $productosMercado = $productosMercado->all();
            
            return [
                'success' => true,
                'productosMercado' => $productosMercado
            ];
        }
    }

    public function actionGetListaProductosMercado(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productosMercado = (new \yii\db\Query())
                ->select("
                        TG_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO AS DT_RowId,
                        TG_PRODUCTO_MERCADO.*,
                        TG_MERCADO_TIPO.TXT_MERCADO_TIPO,
                        TG_MERCADO.TXT_MERCADO,
                        TG_UBIGEO.TXT_DEPARTAMENTO,
                        TG_PRODUCTO_GRUPO.TXT_CODIGO_GRUPO,
                        TG_PRODUCTO_GRUPO.TXT_PRODUCTO_GRUPO,
                        TG_PRODUCTO_GENERO.TXT_CODIGO_GENERO,
                        TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO,
                        TG_PRODUCTO.TXT_CODIGO_PRODUCTO,
                        TG_PRODUCTO.TXT_PRODUCTO,
                        TG_ENVASE.NUM_EQUIVALENCIA,
                        TG_ENVASE.TXT_ENVASE,
                        TG_UNIDAD_MEDIDA.TXT_UNIDAD_MEDIDA,
                        TG_MAESTRO.TXT_DESCRIPCION TXT_PUBLICADO
                        ")
                ->from('TG_PRODUCTO_MERCADO')
                ->leftJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                ->leftJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->leftJoin('TG_MERCADO_TIPO','TG_MERCADO_TIPO.ID_MERCADO_TIPO=TG_MERCADO.ID_MERCADO_TIPO')
                ->leftJoin('TG_UBIGEO','TG_UBIGEO.ID_UBIGEO=TG_MERCADO.ID_UBIGEO')
                ->leftJoin('TG_ENVASE','TG_ENVASE.ID_ENVASE=TG_PRODUCTO_MERCADO.ID_ENVASE')
                ->leftJoin('TG_UNIDAD_MEDIDA','TG_UNIDAD_MEDIDA.ID_UNIDAD_MEDIDA=TG_ENVASE.ID_UNIDAD_MEDIDA')
                ->leftJoin('TG_MAESTRO','TG_MAESTRO.VALOR=TG_PRODUCTO_MERCADO.FLG_PUBLICADO AND TG_MAESTRO.ID_PADRE=1');
            
                $productosMercado = $productosMercado->andWhere(['=', "TG_PRODUCTO_MERCADO.FLG_HABILITADO","1"]);
            if(isset($_POST['idProductoGenero']) && $_POST['idProductoGenero']!=''){
                $productosMercado = $productosMercado->andWhere(['=', 'TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO',$_POST['idProductoGenero']]);
            }
            if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                $productosMercado = $productosMercado->andWhere(['=', 'TG_PRODUCTO_MERCADO.ID_MERCADO',$_POST['idMercado']]);
            }
            if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                $productosMercado = $productosMercado->andWhere(['=', 'TG_UBIGEO.ID_DEPARTAMENTO',$_POST['idRegion']]);
            }
            if(isset($_POST['idProductoGrupo']) && $_POST['idProductoGrupo']!=''){
                $productosMercado = $productosMercado->andWhere(['=', 'TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO',$_POST['idProductoGrupo']]);
            }

            $productosMercado = $productosMercado->all();
            
            return [
                'success' => true,
                'productosMercado' => $productosMercado
            ];
        }
    }

    public function actionEliminar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && !empty($_POST['listaIdsProductosMercado'])){
            $listaIdsProductosMercado = $_POST['listaIdsProductosMercado'];
            //var_dump($listaIdsAbastecimientos);
            if(ProductoMercado::updateAll(['FLG_HABILITADO' => '2','FLG_PUBLICADO'=>'0'], ['in', 'ID_PRODUCTO_MERCADO', $listaIdsProductosMercado])){
                return [
                    'success' => true
                ];
            }else{
                return [
                    'success' => false
                ];
            }

            
        }
    }

}