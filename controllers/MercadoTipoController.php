<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\MercadoTipo;


class MercadoTipoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }


    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new MercadoTipo();
        $model->titulo = 'Registrar mercado tipo';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //$model->FLG_HABILITADO=1;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    //return ActiveForm::validate($model);
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = MercadoTipo::findOne($id);
        $model->titulo = 'Actualizar mercado tipo';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEliminar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && !empty($_POST['idMercadoTipo'])){
            $idMercadoTipo = $_POST['idMercadoTipo'];
            //var_dump($listaIdsAbastecimientos);
            if(MercadoTipo::updateAll(['FLG_HABILITADO' => '2'], ['in', 'ID_MERCADO_TIPO', $idMercadoTipo])){
                return [
                    'success' => true
                ];
            }else{
                return [
                    'success' => false
                ];
            }

            
        }
    }


    public function actionGetListaMercadosTipo(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $mercadosTipo = (new \yii\db\Query())
                ->select('TG_MERCADO_TIPO.*,TG_MAESTRO.TXT_DESCRIPCION as TXT_DESCRIPCION_HABILITADO')
                ->from('TG_MERCADO_TIPO')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_MERCADO_TIPO.FLG_HABILITADO')
                ->where('TG_MERCADO_TIPO.FLG_HABILITADO=1')
                ->all();
            return ['success'=>true,'mercadosTipo'=>$mercadosTipo];
        }
    }
}
