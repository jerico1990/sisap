<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Mercado;

class MercadoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Mercado();
        $model->titulo = 'Registrar mercado';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //$model->FLG_HABILITADO = 1 ;
                //var_dump($this->codigoNuevo($model->cod_ubigeo));die;
                $model->TXT_CODIGO_MERCADO = $this->codigoNuevo($model->ID_UBIGEO);
                //var_dump($model->cod_mercado);die;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Mercado::findOne($id);
        $model->titulo = 'Actualizar mercado';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEliminar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && !empty($_POST['idMercado'])){
            $idMercado = $_POST['idMercado'];
            if(Mercado::updateAll(['FLG_HABILITADO' => '2'], ['in', 'ID_MERCADO', $idMercado])){
                return [
                    'success' => true
                ];
            }else{
                return [
                    'success' => false
                ];
            }
        }
    }

    public function actionGetListaMercados(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $mercados = (new \yii\db\Query())
                ->select(['*'])
                ->from('VW_LISTA_MERCADO');

            if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                $mercados = $mercados->andWhere(['=', 'SUBSTR(VW_LISTA_MERCADO.ID_UBIGEO,1,2)',$_POST['idRegion']]);
            }

            $mercados = $mercados->all();
            return ['success'=>true,'mercados'=>$mercados];
        }
    }

    public function actionGetListaOpcionesMercados(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $mercados = (new \yii\db\Query())
                ->select('TG_MERCADO.ID_MERCADO,TG_MERCADO.ID_UBIGEO,TG_MERCADO.TXT_MERCADO,TG_MERCADO_TIPO.TXT_MERCADO_TIPO')
                ->from('TG_MERCADO')
                ->innerJoin('TG_MERCADO_TIPO','TG_MERCADO_TIPO.ID_MERCADO_TIPO=TG_MERCADO.ID_MERCADO_TIPO');

            if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                $mercados = $mercados->andWhere(['=', 'SUBSTR(TG_MERCADO.ID_UBIGEO,1,2)',$_POST['idRegion']]);
            }

            if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='precio'){
                $mercados = $mercados->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[1,2]]);
            }

            $mercados = $mercados->all();
            return ['success'=>true,'mercados'=>$mercados];
        }
    }

    public function actionGetListaOpcionesMercadosUsuario(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $mercados = (new \yii\db\Query())
                ->select('TG_MERCADO.ID_MERCADO,TG_MERCADO.ID_UBIGEO,TG_MERCADO.TXT_MERCADO,TG_MERCADO_TIPO.TXT_MERCADO_TIPO')
                ->from('TG_MERCADO')
                ->innerJoin('TG_MERCADO_TIPO','TG_MERCADO_TIPO.ID_MERCADO_TIPO=TG_MERCADO.ID_MERCADO_TIPO')
                ->innerJoin('TM_USUARIO_MERCADO','TM_USUARIO_MERCADO.ID_MERCADO=TG_MERCADO.ID_MERCADO');
            $mercados = $mercados->andWhere(['=', 'TM_USUARIO_MERCADO.ID_USUARIO',Yii::$app->user->id]);
            if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                $mercados = $mercados->andWhere(['=', 'SUBSTR(TG_MERCADO.ID_UBIGEO,1,2)',$_POST['idRegion']]);
            }

            if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='precio'){
                $mercados = $mercados->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[1,2]]);
            }

            if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='abastecimiento'){
                $mercados = $mercados->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[1,2]]);
            }

            if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='avicola'){
                $mercados = $mercados->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[4]]);
            }


            $mercados = $mercados->all();
            return ['success'=>true,'mercados'=>$mercados];
        }
    }

    public function codigoNuevo($codUbigeo){
        $codigo = (new \yii\db\Query())->select(['max(TXT_CODIGO_MERCADO) as codigoNuevo'])->from('TG_MERCADO')->where('ID_UBIGEO=:ID_UBIGEO',[':ID_UBIGEO'=>$codUbigeo])->one();
        if($codigo["codigoNuevo"]){
            return $codigo["codigoNuevo"] + 1;
        }
        return $codUbigeo."01";
    }

    public function actionGetListaMercadosUnidadProduccion(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $mercados = (new \yii\db\Query())
                ->select(['*'])
                ->from('VW_LISTA_MERCADO');
            $mercados = $mercados->andWhere(['=', 'VW_LISTA_MERCADO.ID_MERCADO_TIPO',41]);

            $mercados = $mercados->all();
            return ['success'=>true,'mercados'=>$mercados];
        }
    }
}
