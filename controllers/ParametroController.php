<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Parametro;
class ParametroController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Parametro::findOne($id);
        $model->titulo = 'Actualizar parametro';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false,'msg'=>0];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }


    public function actionGetListaParametros(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $parametros = (new \yii\db\Query())
                ->select('TG_PARAMETRO.*,TG_MAESTRO.TXT_DESCRIPCION as TXT_DESCRIPCION_HABILITADO')
                ->from('TG_PARAMETRO')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_PARAMETRO.FLG_HABILITADO')
                ->all();
            return ['success'=>true,'parametros'=>$parametros];
        }
    }
}
