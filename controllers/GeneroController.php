<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Genero;
class GeneroController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }


    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Genero();
        $model->titulo = 'Registrar genero';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //$model->FLG_HABILITADO = 1 ;
                $genero = Genero::find()->where('TXT_CODIGO_GENERO=:TXT_CODIGO_GENERO',[':TXT_CODIGO_GENERO'=>$model->TXT_CODIGO_GENERO])->one();
                if($genero){
                    return ['success'=>false,'msg'=>1];
                }
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false,'msg'=>0];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Genero::findOne($id);
        $model->titulo = 'Actualizar genero';
        $txtCodigoGenero = $model->TXT_CODIGO_GENERO;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($txtCodigoGenero!=$model->TXT_CODIGO_GENERO){
                    $genero = Genero::find()->where('TXT_CODIGO_GENERO=:TXT_CODIGO_GENERO',[':TXT_CODIGO_GENERO'=>$model->TXT_CODIGO_GENERO])->one();
                    if($genero){
                        return ['success'=>false,'msg'=>1];
                    }
                }

                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false,'msg'=>0];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEliminar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && !empty($_POST['idProductoGenero'])){
            $idProductoGenero = $_POST['idProductoGenero'];
            if(Genero::updateAll(['FLG_HABILITADO' => '2'], ['in', 'ID_PRODUCTO_GENERO', $idProductoGenero])){
                return [
                    'success' => true
                ];
            }else{
                return [
                    'success' => false
                ];
            }
        }
    }

    public function actionGetListaGeneros(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $generos = (new \yii\db\Query())
                ->select('*')
                ->from('VW_LISTA_GENERO');
                //$generos = $generos->andWhere(['=', 'VW_LISTA_GENERO.FLG_HABILITADO',1]);
                if(isset($_POST['idProductoGrupo']) && $_POST['idProductoGrupo']!=''){
                    $generos = $generos->andWhere(['=', 'VW_LISTA_GENERO.ID_PRODUCTO_GRUPO',$_POST['idProductoGrupo']]);
                }

                $generos = $generos->all();
            return ['success'=>true,'generos'=>$generos];
        }
    }

    public function actionGetListaOpcionesGenerosUsuario(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $generos = (new \yii\db\Query())
                ->select('TG_PRODUCTO_GENERO.*,TG_MAESTRO.TXT_DESCRIPCION as TXT_DESCRIPCION_HABILITADO')
                ->from('TG_PRODUCTO_GENERO')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_PRODUCTO_GENERO.FLG_HABILITADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO_GENERO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_MERCADO','TG_PRODUCTO_MERCADO.ID_PRODUCTO=TG_PRODUCTO.ID_PRODUCTO')
                ->innerJoin('TM_USUARIO_MERCADO','TM_USUARIO_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TM_USUARIO_MERCADO.ID_MERCADO')
                ->where('TG_PRODUCTO_GENERO.FLG_HABILITADO=1 AND TM_USUARIO_MERCADO.ID_USUARIO=:ID_USUARIO',[':ID_USUARIO'=>Yii::$app->user->id]);

            if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='precio'){
                $generos = $generos->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[1,2]]);
            }

            if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='abastecimiento'){
                $generos = $generos->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[1,2]]);
            }

            if(isset($_POST['idProductoGrupo']) && $_POST['idProductoGrupo']!=''){
                $generos = $generos->andWhere(['=', 'TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO',$_POST['idProductoGrupo']]);
            }

            $generos = $generos->distinct()->orderBy('TG_PRODUCTO_GENERO.TXT_CODIGO_GENERO asc')->all();



            return ['success'=>true,'generos'=>$generos];
        }
    }


    public function actionGetListaOpcionesGeneros(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $generos = (new \yii\db\Query())
                ->select('*')
                ->from('VW_LISTA_GENERO');
                $generos = $generos->andWhere(['=', 'VW_LISTA_GENERO.FLG_HABILITADO',1]);
                if(isset($_POST['idProductoGrupo']) && $_POST['idProductoGrupo']!=''){
                    $generos = $generos->andWhere(['=', 'VW_LISTA_GENERO.ID_PRODUCTO_GRUPO',$_POST['idProductoGrupo']]);
                }

                $generos = $generos->all();
            return ['success'=>true,'generos'=>$generos];
        }
    }

    
    public function actionGetListaOpcionesGenerosTodos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $generos = (new \yii\db\Query())
                ->select('TG_PRODUCTO_GENERO.*,TG_MAESTRO.TXT_DESCRIPCION as TXT_DESCRIPCION_HABILITADO')
                ->from('TG_PRODUCTO_GENERO')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_PRODUCTO_GENERO.FLG_HABILITADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO_GENERO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_MERCADO','TG_PRODUCTO_MERCADO.ID_PRODUCTO=TG_PRODUCTO.ID_PRODUCTO')
                //->innerJoin('TM_USUARIO_MERCADO','TM_USUARIO_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->where('TG_PRODUCTO_GENERO.FLG_HABILITADO=1');

            if(isset($_POST['idProductoGrupo']) && $_POST['idProductoGrupo']!=''){
                $generos = $generos->andWhere(['=', 'TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO',$_POST['idProductoGrupo']]);
            }
            if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                $generos = $generos->andWhere(['=', 'TG_PRODUCTO_MERCADO.ID_MERCADO',$_POST['idMercado']]);
            }

            $generos = $generos->distinct()->orderBy('TG_PRODUCTO_GENERO.TXT_CODIGO_GENERO asc')->all();



            return ['success'=>true,'generos'=>$generos];
        }
    }
}
