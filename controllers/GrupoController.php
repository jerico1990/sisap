<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Grupo;
class GrupoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }


    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Grupo();
        $model->titulo = 'Registrar grupo';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //$model->FLG_HABILITADO = 1 ;
                $grupo = Grupo::find()->where('TXT_CODIGO_GRUPO=:TXT_CODIGO_GRUPO',[':TXT_CODIGO_GRUPO'=>$model->TXT_CODIGO_GRUPO])->one();
                if($grupo){
                    return ['success'=>false,'msg'=>1];
                }
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false,'msg'=>0];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Grupo::findOne($id);
        $model->titulo = 'Actualizar grupo';
        $txtCodigoGrupo = $model->TXT_CODIGO_GRUPO;

        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($txtCodigoGrupo!=$model->TXT_CODIGO_GRUPO){
                    $grupo = Grupo::find()->where('TXT_CODIGO_GRUPO=:TXT_CODIGO_GRUPO',[':TXT_CODIGO_GRUPO'=>$model->TXT_CODIGO_GRUPO])->one();
                    if($grupo){
                        return ['success'=>false,'msg'=>1];
                    }
                }

                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false,'msg'=>0];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEliminar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && !empty($_POST['idProductoGrupo'])){
            $idProductoGrupo = $_POST['idProductoGrupo'];
            if(Grupo::updateAll(['FLG_HABILITADO' => '2'], ['in', 'ID_PRODUCTO_GRUPO', $idProductoGrupo])){
                return [
                    'success' => true
                ];
            }else{
                return [
                    'success' => false
                ];
            }
        }
    }


    public function actionGetListaGrupos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $grupos = (new \yii\db\Query())
                ->select('TG_PRODUCTO_GRUPO.*,TG_MAESTRO.TXT_DESCRIPCION as TXT_DESCRIPCION_HABILITADO')
                ->from('TG_PRODUCTO_GRUPO')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_PRODUCTO_GRUPO.FLG_HABILITADO')
                ->orderBy('TG_PRODUCTO_GRUPO.TXT_CODIGO_GRUPO asc')
                ->all();
            return ['success'=>true,'grupos'=>$grupos];
        }
    }

    public function actionGetListaOpcionesGruposUsuario(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $grupos = (new \yii\db\Query())
                ->select('TG_PRODUCTO_GRUPO.*,TG_MAESTRO.TXT_DESCRIPCION as TXT_DESCRIPCION_HABILITADO')
                ->from('TG_PRODUCTO_GRUPO')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_PRODUCTO_GRUPO.FLG_HABILITADO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO_GENERO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_MERCADO','TG_PRODUCTO_MERCADO.ID_PRODUCTO=TG_PRODUCTO.ID_PRODUCTO')
                ->innerJoin('TM_USUARIO_MERCADO','TM_USUARIO_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TM_USUARIO_MERCADO.ID_MERCADO')
                ->where('TG_PRODUCTO_GRUPO.FLG_HABILITADO=1 AND TM_USUARIO_MERCADO.ID_USUARIO=:ID_USUARIO',[':ID_USUARIO'=>Yii::$app->user->id]);

                if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='precio'){
                    $grupos = $grupos->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[1,2]]);
                }

                if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='abastecimiento'){
                    $grupos = $grupos->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[1,2]]);
                }

                if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='avicola'){
                    $grupos = $grupos->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[4]]);
                }

                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $grupos = $grupos->andWhere(['=', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }

                

                $grupos = $grupos->distinct()->orderBy('TG_PRODUCTO_GRUPO.TXT_CODIGO_GRUPO asc')->all();
            return ['success'=>true,'grupos'=>$grupos];
        }
    }

    public function actionGetListaOpcionesGrupos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $grupos = (new \yii\db\Query())
                ->select('TG_PRODUCTO_GRUPO.*,TG_MAESTRO.TXT_DESCRIPCION as TXT_DESCRIPCION_HABILITADO')
                ->from('TG_PRODUCTO_GRUPO')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_PRODUCTO_GRUPO.FLG_HABILITADO')
                ->where('TG_PRODUCTO_GRUPO.FLG_HABILITADO=1')
                ->orderBy('TG_PRODUCTO_GRUPO.TXT_CODIGO_GRUPO asc')
                ->all();
            return ['success'=>true,'grupos'=>$grupos];
        }
    }


    public function actionGetListaOpcionesGruposTodos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $grupos = (new \yii\db\Query())
                ->select('TG_PRODUCTO_GRUPO.*')
                ->from('TG_PRODUCTO_GRUPO')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_PRODUCTO_GRUPO.FLG_HABILITADO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO_GENERO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO')
                ->innerJoin('TG_PRODUCTO_MERCADO','TG_PRODUCTO_MERCADO.ID_PRODUCTO=TG_PRODUCTO.ID_PRODUCTO')
                //->innerJoin('TM_USUARIO_MERCADO','TM_USUARIO_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO')
                ->where('TG_PRODUCTO_GRUPO.FLG_HABILITADO=1');


                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $grupos = $grupos->andWhere(['=', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }

                

                $grupos = $grupos->distinct()->orderBy('TG_PRODUCTO_GRUPO.TXT_CODIGO_GRUPO asc')->all();
            return ['success'=>true,'grupos'=>$grupos];
        }
    }
}
