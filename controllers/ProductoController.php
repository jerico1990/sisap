<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Producto;

class ProductoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }


    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Producto();
        $model->titulo = 'Registrar producto';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //$model->int_estado = 1 ;
                //$model->cod_rubro = $model->cod_grupo;
                $model->TXT_CODIGO_PRODUCTO = $this->codigoNuevo($model->ID_PRODUCTO_GENERO);
                //var_dump($model->TXT_CODIGO_PRODUCTO);die;
                //$model->cod_variedad = substr($model->cod_producto, 4, 2);
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Producto::findOne($id);
        $model->titulo = 'Actualizar producto';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEliminar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && !empty($_POST['idProducto'])){
            $idProducto = $_POST['idProducto'];
            if(Producto::updateAll(['FLG_HABILITADO' => '2'], ['in', 'ID_PRODUCTO', $idProducto])){
                return [
                    'success' => true
                ];
            }else{
                return [
                    'success' => false
                ];
            }
        }
    }

    public function actionGetListaProductos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productos = (new \yii\db\Query())
                ->select('*')
                ->from('VW_LISTA_PRODUCTO')
                ->all();
            return ['success'=>true,'productos'=>$productos];
        }
    }

    public function actionGetListaOpcionesProductos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productos = (new \yii\db\Query())
                ->select('TG_PRODUCTO.ID_PRODUCTO,TG_PRODUCTO.TXT_PRODUCTO,TG_PRODUCTO.TXT_CODIGO_PRODUCTO')
                ->from('TG_PRODUCTO');
            $productos = $productos->andWhere(['=', 'TG_PRODUCTO.FLG_HABILITADO',1]);
            if(isset($_POST['idProductoGenero']) && $_POST['idProductoGenero']!=''){
                $productos = $productos->andWhere(['=', 'TG_PRODUCTO.ID_PRODUCTO_GENERO',$_POST['idProductoGenero']]);
            }
            $productos = $productos->orderBy('TXT_PRODUCTO asc')->all();
            //$productos = json_encode(['ID_PRODUCTO' => $productos]);
            //'options' =>json_decode($productos)
            return ['success'=>true,'productos' =>$productos];
        }
    }


    public function actionGetListaOpcionesProductosMercado(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $productos = (new \yii\db\Query())
                ->select('VW_LISTA_PRODUCTO_MERCADO.ID_PRODUCTO_MERCADO,VW_LISTA_PRODUCTO_MERCADO.TXT_PRODUCTO')
                ->from('VW_LISTA_PRODUCTO_MERCADO');
            
            $productos = $productos->andWhere(['=', 'VW_LISTA_PRODUCTO_MERCADO.FLG_HABILITADO',1]);
            if(isset($_POST['idProductoGenero']) && $_POST['idProductoGenero']!=''){
                $productos = $productos->andWhere(['=', 'VW_LISTA_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO',$_POST['idProductoGenero']]);
            }
            if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                $productos = $productos->andWhere(['=', 'VW_LISTA_PRODUCTO_MERCADO.ID_MERCADO',$_POST['idMercado']]);
            }
            $productos = $productos->orderBy('TXT_PRODUCTO asc')->all();
            //$productos = json_encode(['ID_PRODUCTO' => $productos]);
            //'options' =>json_decode($productos)
            return ['success'=>true,'productos' =>$productos];
        }
    }

    public function codigoNuevo($idProductoGenero){
        $codigosProductoGenero = (new \yii\db\Query())->select('TG_PRODUCTO_GENERO.TXT_CODIGO_GENERO,TG_PRODUCTO_GRUPO.TXT_CODIGO_GRUPO')
                                ->from('TG_PRODUCTO_GENERO')
                                ->innerJoin('TG_PRODUCTO_GRUPO','TG_PRODUCTO_GRUPO.ID_PRODUCTO_GRUPO=TG_PRODUCTO_GENERO.ID_PRODUCTO_GRUPO')
                                ->where('ID_PRODUCTO_GENERO=:ID_PRODUCTO_GENERO',[':ID_PRODUCTO_GENERO'=>$idProductoGenero])
                                ->one();
        //var_dump($codigosProductoGenero);die;
        $max = (new \yii\db\Query())->select(['max(TXT_CODIGO_PRODUCTO) as TXT_CODIGO_PRODUCTO'])->from('TG_PRODUCTO')->where('ID_PRODUCTO_GENERO=:ID_PRODUCTO_GENERO',[':ID_PRODUCTO_GENERO'=>$idProductoGenero])->one();

        $codigo = (new \yii\db\Query())->select(['max(TXT_CODIGO_PRODUCTO) as codigoNuevo'])->from('TG_PRODUCTO')->where('ID_PRODUCTO_GENERO=:ID_PRODUCTO_GENERO AND TXT_CODIGO_PRODUCTO!=:TXT_CODIGO_PRODUCTO',[':ID_PRODUCTO_GENERO'=>$idProductoGenero,':TXT_CODIGO_PRODUCTO'=>$max["TXT_CODIGO_PRODUCTO"]])->one();
        //var_dump($codigo);die;
        if($codigo["codigoNuevo"]){
            return str_pad(($codigo["codigoNuevo"] + 1), 6, "0", STR_PAD_LEFT);
        }
        return $codigosProductoGenero['TXT_CODIGO_GRUPO'].$codigosProductoGenero['TXT_CODIGO_GENERO']."01";
    }
}
