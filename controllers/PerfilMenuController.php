<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PerfilMenu;
class PerfilMenuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionAsignar(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new PerfilMenu();
        $model->titulo = 'Asignar menú';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            } else {
                return $this->render('asignar', [
                    'model' => $model,
                ]);
            }
        }
    }

    /*
    public function actionCreate($idPefil){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new PerfilMenu();
        $model->titulo = 'Asignar permidos';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->ID_PERFIL = $idPefil;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = PerfilMenu::findOne($id);
        $model->titulo = 'Asignar permidos';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }*/
    

    public function actionGetListaPerfilMenu(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idPerfil = $_POST['idPerfil'];

            $menus = (new \yii\db\Query())
                ->select('VW_LISTA_MENU.*,TC_PERFIL_MENU.ID_PERFIL_MENU')
                ->from('VW_LISTA_MENU')
                ->leftJoin('TC_PERFIL_MENU','TC_PERFIL_MENU.ID_MENU=VW_LISTA_MENU.ID_MENU and TC_PERFIL_MENU.ID_PERFIL='.$idPerfil.'')
                ->orderBy('VW_LISTA_MENU.ID_MENU_PADRE,VW_LISTA_MENU.INT_ORDEN asc')
                ->all();
            return ['success'=>true,'menus'=>$menus];
        }
    }
    
    

    public function actionAsignarMenu(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idMenu = $_POST['idMenu'];
            $idPerfil = $_POST['idPerfil'];
            $flgActivo = $_POST['flgActivo'];

            if($flgActivo=="true"){
                $model = new PerfilMenu();
                $model->ID_PERFIL = $idPerfil;
                $model->ID_MENU = $idMenu;
                $model->save();

            }else{
                PerfilMenu::find()->where('ID_PERFIL=:ID_PERFIL and ID_MENU=:ID_MENU',[':ID_PERFIL'=>$idPerfil,':ID_MENU'=>$idMenu])->one()->delete();
            }

            return ['success'=>true];
        }
    }
}
