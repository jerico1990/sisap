<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Procedencia;
class ProcedenciaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }


    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Procedencia();
        $model->titulo = 'Registrar procedencia';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->cod_ubigeo=$model->cod_ubigeo.'00';

                $validarRegistro = (new \yii\db\Query())
                ->select('*')
                ->from('ap_tg_producto_proc')
                ->where('cod_producto=:cod_producto and cod_ubigeo=:cod_ubigeo',[':cod_producto'=>$model->cod_producto,':cod_ubigeo'=>$model->cod_ubigeo])
                ->one();

                if($validarRegistro){
                    return ['success'=>true,'msg'=>'Ya se encuentra registrado'];
                }
                //$model->int_estado = 1 ;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Procedencia::findOne($id);
        $model->titulo = 'Actualizar procedencia';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->cod_ubigeo=$model->cod_ubigeo.'00';
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionGetListaProcedencias(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $procedencias = (new \yii\db\Query())
                ->select('ap_tg_producto_proc.*,ap_tg_producto_variedad.txt_dscvariedad,ap_tg_ubigeo.txt_provincia,ap_tg_ubigeo.txt_departamento')
                ->from('ap_tg_producto_proc')
                ->innerJoin("ap_tg_producto_variedad","ap_tg_producto_variedad.cod_producto=ap_tg_producto_proc.cod_producto")
                ->innerJoin("(select distinct cod_prov_ubigeo,txt_provincia,txt_departamento from ap_tg_ubigeo) ap_tg_ubigeo","ap_tg_ubigeo.cod_prov_ubigeo=ap_tg_producto_proc.cod_ubigeo")
                ->orderBy('ap_tg_producto_proc.cod_ubigeo asc,ap_tg_producto_variedad.txt_dscvariedad asc')
                ->all();
            return ['success'=>true,'procedencias'=>$procedencias];
        }
    }
}
