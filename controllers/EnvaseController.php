<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Envase;
class EnvaseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Envase();
        $model->titulo = 'Registrar envase';
        

        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $envase = Envase::find()->where('TXT_ENVASE=:TXT_ENVASE and ID_UNIDAD_MEDIDA=:ID_UNIDAD_MEDIDA and NUM_EQUIVALENCIA=:NUM_EQUIVALENCIA',[':TXT_ENVASE'=>trim($model->TXT_ENVASE),':ID_UNIDAD_MEDIDA'=>$model->ID_UNIDAD_MEDIDA,':NUM_EQUIVALENCIA'=>trim($model->NUM_EQUIVALENCIA),])->one();
                if($envase){
                    return ['success'=>false,'msg'=>1];
                }

                //$model->FLG_HABILITADO = 1 ;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false,'msg'=>0];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Envase::findOne($id);
        $model->titulo = 'Actualizar envase';
        $txtEnvase = $model->TXT_ENVASE;
        $idUnidadMedida = $model->ID_UNIDAD_MEDIDA;
        $numEquivalencia = $model->NUM_EQUIVALENCIA;

        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($txtEnvase!=trim($model->TXT_ENVASE) || $idUnidadMedida!=$model->ID_UNIDAD_MEDIDA || trim($numEquivalencia!=$model->NUM_EQUIVALENCIA)){
                    $envase = Envase::find()->where('TXT_ENVASE=:TXT_ENVASE and ID_UNIDAD_MEDIDA=:ID_UNIDAD_MEDIDA and NUM_EQUIVALENCIA=:NUM_EQUIVALENCIA',[':TXT_ENVASE'=>$model->TXT_ENVASE,':ID_UNIDAD_MEDIDA'=>$model->ID_UNIDAD_MEDIDA,':NUM_EQUIVALENCIA'=>$model->NUM_EQUIVALENCIA,])->one();
                    if($envase){
                        return ['success'=>false,'msg'=>1];
                    }
                }

                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false,'msg'=>0];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEliminar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && !empty($_POST['idEnvase'])){
            $idEnvase = $_POST['idEnvase'];
            if(Envase::updateAll(['FLG_HABILITADO' => '2'], ['in', 'ID_ENVASE', $idEnvase])){
                return [
                    'success' => true
                ];
            }else{
                return [
                    'success' => false
                ];
            }
        }
    }


    public function actionGetListaEnvases(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $envases = (new \yii\db\Query())
                ->select('TG_ENVASE.*,TG_MAESTRO.TXT_DESCRIPCION as TXT_DESCRIPCION_HABILITADO,TG_UNIDAD_MEDIDA.TXT_UNIDAD_MEDIDA')
                ->from('TG_ENVASE')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_ENVASE.FLG_HABILITADO')
                ->leftJoin('TG_UNIDAD_MEDIDA','TG_UNIDAD_MEDIDA.ID_UNIDAD_MEDIDA=TG_ENVASE.ID_UNIDAD_MEDIDA')
                ->all();
            return ['success'=>true,'envases'=>$envases];
        }
    }


    public function actionGetListaOpcionesEnvases(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $envases = (new \yii\db\Query())
                ->select('TG_ENVASE.ID_ENVASE, TG_ENVASE.TXT_ENVASE,TG_UNIDAD_MEDIDA.TXT_UNIDAD_MEDIDA,TG_ENVASE.NUM_EQUIVALENCIA')
                ->from('TG_ENVASE')
                ->leftJoin('TG_UNIDAD_MEDIDA','TG_UNIDAD_MEDIDA.ID_UNIDAD_MEDIDA=TG_ENVASE.ID_UNIDAD_MEDIDA')
                ->where('TG_ENVASE.FLG_HABILITADO=1')
                ->all();
            return ['success'=>true,'envases'=>$envases];
        }
    }
}
