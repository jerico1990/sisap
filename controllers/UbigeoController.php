<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Usuario;
class UbigeoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo "No permitido";
    }

    public function actionGetRegiones(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $regiones = (new \yii\db\Query())
                ->select('ID_DEPARTAMENTO,TXT_DEPARTAMENTO')
                ->from('TG_UBIGEO')
                ->distinct()
                ->orderBy('TXT_DEPARTAMENTO asc')
                ->all();
            return ['success'=>true,'regiones'=>$regiones];
        }
    }

    public function actionGetRegionesUsuario(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $regiones = (new \yii\db\Query())
                ->select('ID_DEPARTAMENTO,TXT_DEPARTAMENTO')
                ->from('TG_UBIGEO')
                ->innerJoin('TG_MERCADO','TG_MERCADO.ID_UBIGEO=TG_UBIGEO.ID_UBIGEO')
                ->innerJoin('TM_USUARIO_MERCADO','TM_USUARIO_MERCADO.ID_MERCADO=TG_MERCADO.ID_MERCADO')
                ->where('ID_USUARIO=:ID_USUARIO',[':ID_USUARIO'=>Yii::$app->user->id]);
                

            if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='precio'){
                $regiones = $regiones->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[1,2]]);
            }

            if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='abastecimiento'){
                $regiones = $regiones->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[1,2]]);
            }

            if(isset($_POST['proceso']) && $_POST['proceso']!='' && $_POST['proceso']=='avicola'){
                $regiones = $regiones->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[4]]);
            }

            $regiones = $regiones->distinct()->orderBy('TXT_DEPARTAMENTO asc')->all();

            return ['success'=>true,'regiones'=>$regiones];
        }
    }

    public function actionGetProvincias(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idRegion = $_POST['idRegion'];

            $provincias = (new \yii\db\Query())
                ->select('ID_PROVINCIA,TXT_PROVINCIA')
                ->from('TG_UBIGEO')
                ->where('ID_DEPARTAMENTO=:ID_DEPARTAMENTO',[':ID_DEPARTAMENTO'=>$idRegion])
                ->distinct()
                ->orderBy('TXT_PROVINCIA asc')
                ->all();
            return ['success'=>true,'provincias'=>$provincias];
        }
    }

    public function actionGetDistritos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idProvincia = $_POST['idProvincia'];

            $distritos = (new \yii\db\Query())
                ->select('ID_UBIGEO,TXT_DISTRITO')
                ->from('TG_UBIGEO')
                ->where('ID_PROVINCIA=:ID_PROVINCIA',[':ID_PROVINCIA'=>$idProvincia])
                ->distinct()
                ->orderBy('TXT_DISTRITO asc')
                ->all();
            return ['success'=>true,'distritos'=>$distritos];
        }
    }

    public function actionGetListaProcedencias(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $procedencias = (new \yii\db\Query())
                ->select('ID_PROCEDENCIA,TXT_PROCEDENCIA')
                ->from('TG_PROCEDENCIA')
                ->orderBy('ID_PROCEDENCIA asc')
                ->all();
            return ['success'=>true,'procedencias'=>$procedencias];
        }
    }

    public function actionGetUbigeos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $regiones = (new \yii\db\Query())
                ->select('*')
                ->from('TG_UBIGEO')
                ->distinct()
                ->all();
            return ['success'=>true,'regiones'=>$regiones];
        }
    }
    

}
