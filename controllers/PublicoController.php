<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use PDO;

class PublicoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['?','@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionGetRegiones(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $regiones = (new \yii\db\Query())
                ->select('ID_DEPARTAMENTO,TXT_DEPARTAMENTO')
                ->from('TG_UBIGEO')
                ->distinct()
                ->orderBy('TXT_DEPARTAMENTO asc')
                ->all();
            return ['success'=>true,'regiones'=>$regiones];
        }
    }


    public function actionGetListaMercadosTipo(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $mercadosTipo = (new \yii\db\Query())
                ->select('TG_MERCADO_TIPO.*,TG_MAESTRO.TXT_DESCRIPCION as TXT_DESCRIPCION_HABILITADO')
                ->from('TG_MERCADO_TIPO')
                ->innerJoin('TG_MAESTRO','TG_MAESTRO.ID_PADRE=4 and TG_MAESTRO.VALOR=TG_MERCADO_TIPO.FLG_HABILITADO')
                ->where('TG_MERCADO_TIPO.FLG_HABILITADO=1 and TG_MERCADO_TIPO.ID_MERCADO_TIPO in (1,2)')
                ->all();
            return ['success'=>true,'mercadosTipo'=>$mercadosTipo];
        }
    }


    public function actionGetListaOpcionesMercados(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $mercados = (new \yii\db\Query())
                ->select('TG_MERCADO.ID_MERCADO,TG_MERCADO.ID_UBIGEO,TG_MERCADO.TXT_MERCADO,TG_MERCADO_TIPO.TXT_MERCADO_TIPO')
                ->from('TG_MERCADO')
                ->innerJoin('TG_MERCADO_TIPO','TG_MERCADO_TIPO.ID_MERCADO_TIPO=TG_MERCADO.ID_MERCADO_TIPO');
            
            if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                $mercados = $mercados->andWhere(['=', 'SUBSTR(TG_MERCADO.ID_UBIGEO,1,2)',$_POST['idRegion']]);
            }

            $mercados = $mercados->andWhere(['in', 'TG_MERCADO.ID_MERCADO_TIPO',[1,2]]);



            $mercados = $mercados->all();
            return ['success'=>true,'mercados'=>$mercados];
        }
    }

    public function actionGetListaPrecios(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            
            
            if(isset($_POST['diario']) && $_POST['diario']!=''){
                
                $precios = (new \yii\db\Query())
                    ->select(["TXT_ANIO,TXT_MES,TXT_DIA,TXT_DEPARTAMENTO,TXT_CODIGO_PRODUCTO,TXT_PRODUCTO,TXT_MERCADO,NUM_EQUIVALENCIA,
                    AVG(NUM_PRECIO_PROMEDIO) NUM_PRECIO_PROMEDIO,AVG(NUM_PRECIO_MINIMO) NUM_PRECIO_MINIMO,AVG(NUM_PRECIO_MAXIMO) NUM_PRECIO_MAXIMO"])
                    ->from('TR_PRECIO');

            }

            if(isset($_POST['meses']) && $_POST['meses']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='2'){
                
                $precios = (new \yii\db\Query())
                    ->select(["TXT_ANIO,TXT_MES,TXT_DEPARTAMENTO,TXT_CODIGO_PRODUCTO,TXT_PRODUCTO,TXT_MERCADO,NUM_EQUIVALENCIA,
                    AVG(NUM_PRECIO_PROMEDIO) NUM_PRECIO_PROMEDIO,AVG(NUM_PRECIO_MINIMO) NUM_PRECIO_MINIMO,AVG(NUM_PRECIO_MAXIMO) NUM_PRECIO_MAXIMO"])
                    ->from('TR_PRECIO');
            }

            if(isset($_POST['anios']) && $_POST['anios']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='3'){
                
                $precios = (new \yii\db\Query())
                    ->select(["TXT_ANIO,TXT_DEPARTAMENTO,TXT_CODIGO_PRODUCTO,TXT_PRODUCTO,TXT_MERCADO,NUM_EQUIVALENCIA,
                    AVG(NUM_PRECIO_PROMEDIO) NUM_PRECIO_PROMEDIO,AVG(NUM_PRECIO_MINIMO) NUM_PRECIO_MINIMO,AVG(NUM_PRECIO_MAXIMO) NUM_PRECIO_MAXIMO"])
                    ->from('TR_PRECIO');
            }

            
            

            if(isset($_POST['fecRegistro']) && $_POST['fecRegistro']!=''){
                $precios = $precios->andWhere(['=', "TO_CHAR(TM_PRECIO.FEC_REGISTRO,'YYYY-MM-DD')",$_POST['fecRegistro']]);
            }

            if(isset($_POST['idRegion']) && $_POST['idRegion']!=''){
                $precios = $precios->andWhere(['=', 'ID_DEPARTAMENTO',$_POST['idRegion']]);
            }

            if(isset($_POST['idMercadoTipo']) && $_POST['idMercadoTipo']!=''){
                $precios = $precios->andWhere(['=', 'ID_MERCADO_TIPO',$_POST['idMercadoTipo']]);
            }


            if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                $precios = $precios->andWhere(['=', 'ID_MERCADO',$_POST['idMercado']]);
            }

            if(isset($_POST['variedades']) && $_POST['variedades']!=''){
                $precios = $precios->andWhere(['in', 'ID_PRODUCTO',$_POST['variedades']]);
            }

            if(isset($_POST['diario']) && $_POST['diario']!=''){
                $fechaLista = explode("-", $_POST['diario']);

                $anio = $fechaLista[0];
                $mes = $fechaLista[1];
                $dia = $fechaLista[2];

                $precios = $precios->andWhere(['=', 'TXT_ANIO',$anio]);
                $precios = $precios->andWhere(['=', 'TXT_MES',$mes]);
                $precios = $precios->andWhere(['=', 'TXT_DIA',$dia]);
            }

            if(isset($_POST['mensual']) && $_POST['mensual']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='2'){
                /* $fechaLista = explode("-", $_POST['mensual']);

                $anio = $fechaLista[0];
                $mes = $fechaLista[1]; */
                $meses = $_POST['meses'];
                $anios = $_POST['anios'];
                $precios = $precios->andWhere(['in', 'TXT_ANIO',$meses]);
                $precios = $precios->andWhere(['in', 'TXT_MES',$anios]);
                //$precios = $precios->andWhere(['=', 'ID_MERCADO',$_POST['idMercado']]);
            }

            if(isset($_POST['anios']) && $_POST['anios']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='3'){
                $anios = $_POST['anios'];
                $precios = $precios->andWhere(['in', 'TXT_ANIO',$anios]);
                //$precios = $precios->andWhere(['=', 'ID_MERCADO',$_POST['idMercado']]);
            }
            
            if(isset($_POST['diario']) && $_POST['diario']!=''){
                $precios = $precios->groupBy('TXT_ANIO,TXT_MES,TXT_DIA,TXT_DEPARTAMENTO,TXT_CODIGO_PRODUCTO,TXT_PRODUCTO,TXT_MERCADO,NUM_EQUIVALENCIA')->all();
            }
            
            if(isset($_POST['meses']) && $_POST['meses']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='2'){
                $precios = $precios->groupBy('TXT_ANIO,TXT_MES,TXT_DEPARTAMENTO,TXT_CODIGO_PRODUCTO,TXT_PRODUCTO,TXT_MERCADO,NUM_EQUIVALENCIA')->all();
            }

            if(isset($_POST['anios']) && $_POST['anios']!='' && isset($_POST['temporalidad']) && $_POST['temporalidad']!='' && $_POST['temporalidad']=='3'){
                $precios = $precios->groupBy('TXT_ANIO,TXT_DEPARTAMENTO,TXT_CODIGO_PRODUCTO,TXT_PRODUCTO,TXT_MERCADO,NUM_EQUIVALENCIA')->all();
            }
            
            
            
            return [
                'success' => true,
                'precios' => $precios
            ];
        }
    }

    public function actionGetListaOpcionesGenerosVariedades(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $generos = (new \yii\db\Query())
                ->select('TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO,TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO')
                ->from('TG_PRODUCTO_MERCADO')
                ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO')
                ->leftJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO');
                

            if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                $generos = $generos->andWhere(['=', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);
            }
            $generos = $generos
                        ->groupBy('TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO,TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO')
                        ->orderBy('TG_PRODUCTO_GENERO.TXT_PRODUCTO_GENERO asc')
                        ->all();

            $listaGeneros = [];

            foreach($generos as $genero){
                $variedades = (new \yii\db\Query())
                    ->select('TG_PRODUCTO.ID_PRODUCTO idProducto,TG_PRODUCTO.TXT_PRODUCTO txtProducto')
                    ->from('TG_PRODUCTO_MERCADO')
                    ->innerJoin('TG_PRODUCTO','TG_PRODUCTO.ID_PRODUCTO=TG_PRODUCTO_MERCADO.ID_PRODUCTO')
                    ->innerJoin('TG_PRODUCTO_GENERO','TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO=TG_PRODUCTO_MERCADO.ID_PRODUCTO_GENERO')
                    ->leftJoin('TG_MERCADO','TG_MERCADO.ID_MERCADO=TG_PRODUCTO_MERCADO.ID_MERCADO');
                    
                    $variedades = $variedades->andWhere(['=', 'TG_PRODUCTO_GENERO.ID_PRODUCTO_GENERO',$genero['ID_PRODUCTO_GENERO']]);
                if(isset($_POST['idMercado']) && $_POST['idMercado']!=''){
                    $variedades = $variedades->andWhere(['=', 'TG_MERCADO.ID_MERCADO',$_POST['idMercado']]);
                }

                /* if(isset($_POST['variedades']) && $_POST['variedades']!=''){
                    $variedades = $variedades->andWhere(['in', 'TG_PRODUCTO.ID_PRODUCTO',$_POST['variedades']]);
                } */
                $variedades = $variedades
                            ->groupBy('TG_PRODUCTO.ID_PRODUCTO,TG_PRODUCTO.TXT_PRODUCTO')
                            ->orderBy('TG_PRODUCTO.TXT_PRODUCTO asc')
                            ->all();


                array_push($listaGeneros, [
                    'idProductoGenero'=>$genero['ID_PRODUCTO_GENERO'],
                    'txtProductoGenero'=>$genero['TXT_PRODUCTO_GENERO'],
                    'variedades'=>$variedades
                ]);
            }

            return ['success'=>true,'generos'=>$listaGeneros];
        }
    }


}

