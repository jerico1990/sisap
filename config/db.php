<?php

/* return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=sisap_dev',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
]; */


return [
    'class' => 'yii\db\Connection',
    'dsn' => 'oci:dbname=//10.50.74.11:1521/DEVELOPER',
    'username' => 'SISAP',
    'password' => 'D3v3l0p3r$',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 3600,
    'schemaCache' => 'cache',
];