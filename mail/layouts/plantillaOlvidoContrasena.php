
<p>
    Estimado usuario <?= $correo ?> ,<br>
    ¡Hubo una solicitud para cambiar su contraseña!<br>
    Si no realizó esta solicitud, ignore este correo electrónico.<br>
    De lo contrario, haga clic en este enlace para cambiar su contraseña: <br>
    <?= $enlace ?><br>
    <br><br>
    Plataforma SISP-MIDAGRI 2021
</p>