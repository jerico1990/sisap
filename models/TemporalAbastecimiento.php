<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TT_ABASTECIMIENTO".
 *
 * @property float $ID_PRODUCTO_MERCADO
 * @property string|null $ID_PROCEDENCIA
 * @property float|null $NUM_VOLUMEN_PRODUCTO
 * @property string|null $FEC_REGISTRO
 */
class TemporalAbastecimiento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $tmp;
    public $archivo;
    public static function tableName()
    {
        return 'TT_ABASTECIMIENTO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['ID_PRODUCTO_MERCADO'], 'required'],
            [['NUM_VOLUMEN_PRODUCTO'], 'number'],
            [['TXT_CODIGO_PRODUCTO_MERCADO'], 'string', 'max' => 4],
            [['ID_PROCEDENCIA'], 'string', 'max' => 6],
            [['FEC_REGISTRO'], 'string', 'max' => 7],
            [['TXT_CODIGO_PRODUCTO_MERCADO'], 'unique'],
            [['tmp','archivo'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TXT_CODIGO_PRODUCTO_MERCADO' => 'Id  Producto  Mercado',
            'ID_PROCEDENCIA' => 'Id  Procedencia',
            'NUM_VOLUMEN_PRODUCTO' => 'Num  Volumen  Producto',
            'FEC_REGISTRO' => 'Fec  Registro',
        ];
    }
}
