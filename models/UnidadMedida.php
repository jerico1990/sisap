<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TG_UNIDAD_MEDIDA".
 *
 * @property float $ID_UNIDAD_MEDIDA
 * @property string|null $TXT_CODIGO_UNIDAD_MEDIDA
 * @property string|null $TXT_UNIDAD_MEDIDA
 * @property float|null $FLG_HABILITADO
 *
 * @property TGPRODUCTO[] $tGPRODUCTOs
 */
class UnidadMedida extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TG_UNIDAD_MEDIDA';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_UNIDAD_MEDIDA', 'FLG_HABILITADO'], 'number'],
            [['TXT_CODIGO_UNIDAD_MEDIDA'], 'string', 'max' => 50],
            [['TXT_UNIDAD_MEDIDA'], 'string', 'max' => 150],
            [['ID_UNIDAD_MEDIDA'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_UNIDAD_MEDIDA' => 'Id  Unidad  Medida',
            'TXT_CODIGO_UNIDAD_MEDIDA' => 'Txt  Codigo  Unidad  Medida',
            'TXT_UNIDAD_MEDIDA' => 'Txt  Unidad  Medida',
            'FLG_HABILITADO' => 'Flg  Habilitado',
        ];
    }

    /**
     * Gets query for [[TGPRODUCTOs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTGPRODUCTOs()
    {
        return $this->hasMany(TGPRODUCTO::className(), ['ID_UNIDAD_MEDIDA' => 'ID_UNIDAD_MEDIDA']);
    }
}
