<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TG_MERCADO_TIPO".
 *
 * @property float $ID_MERCADO_TIPO
 * @property string|null $TXT_MERCADO_TIPO
 * @property float|null $FLG_HABILITADO
 */
class MercadoTipo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TG_MERCADO_TIPO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['TXT_MERCADO_TIPO'], 'required'],
            [['FLG_HABILITADO'], 'number'],
            [['TXT_MERCADO_TIPO'], 'string', 'max' => 150],
            //[['ID_MERCADO_TIPO'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_MERCADO_TIPO' => 'Id  Mercado  Tipo',
            'TXT_MERCADO_TIPO' => 'Txt  Mercado  Tipo',
            'FLG_HABILITADO' => 'Flg  Habilitado',
        ];
    }
}
