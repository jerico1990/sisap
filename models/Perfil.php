<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TP_PERFIL".
 *
 * @property float $ID_PERFIL
 * @property string|null $TXT_PERFIL
 * @property float|null $FLG_HABILITADO
 *
 * @property TCPERFILMENU[] $tCPERFILMENUs
 */
class Perfil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TP_PERFIL';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_PERFIL', 'FLG_HABILITADO'], 'number'],
            [['TXT_PERFIL'], 'string', 'max' => 50],
            [['ID_PERFIL'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PERFIL' => 'Id  Perfil',
            'TXT_PERFIL' => 'Txt  Perfil',
            'FLG_HABILITADO' => 'Flg  Habilitado',
        ];
    }

    /**
     * Gets query for [[TCPERFILMENUs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTCPERFILMENUs()
    {
        return $this->hasMany(TCPERFILMENU::className(), ['ID_PERFIL' => 'ID_PERFIL']);
    }
}
