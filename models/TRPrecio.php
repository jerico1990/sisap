<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TR_PRECIO".
 *
 * @property string|null $ID_DEPARTAMENTO Codigo de Departamento
 * @property string|null $TXT_DEPARTAMENTO Descripcion de Departamento
 * @property string|null $ID_PROVINCIA Codigo de Provincia
 * @property string|null $TXT_PROVINCIA Descripcion de Provincia
 * @property string|null $ID_DISTRITO Codigo de Distrito
 * @property string|null $TXT_DISTRITO Descripcion de Distrito
 * @property float|null $ID_MERCADO Codigo de Mercado
 * @property string|null $TXT_MERCADO Descripcion de Mercado
 * @property float|null $ID_GRUPO Codigo de Grupo
 * @property string|null $TXT_GRUPO Descripcion de Grupo
 * @property float|null $ID_GENERO Codigo de Genero
 * @property string|null $TXT_GENERO Descripcion de Genero
 * @property float|null $ID_PRODUCTO Codigo de Producto
 * @property string|null $TXT_PRODUCTO Descripcion de Producto
 * @property float|null $ID_ENVASE Codigo de Envase
 * @property string|null $TXT_ENVASE Descripcion de Envase
 * @property float|null $ID_MERCADO_TIPO Codigo de Tipo de Mercado
 * @property string|null $TXT_MERCADO_TIPO Descripcion de Tipo de Mercado
 * @property string|null $TXT_ANIO Anio
 * @property string|null $TXT_MES Mes
 * @property string|null $TXT_DIA Dia
 * @property float|null $NUM_PRECIO_MINIMO Precio Minimo
 * @property float|null $NUM_PRECIO_MAXIMO Precio Maximo
 * @property float|null $NUM_PRECIO_PROMEDIO Precio Promedio
 * @property string|null $FEC_REGISTRO Fecha de registro
 * @property string|null $TXT_ORIGEN Origen
 * @property float|null $ID_PRODUCTO_MERCADO
 * @property string|null $FEC_PROCESO
 */
class TRPrecio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TR_PRECIO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_MERCADO', 'ID_GRUPO', 'ID_GENERO', 'ID_PRODUCTO', 'ID_ENVASE', 'ID_MERCADO_TIPO', 'NUM_PRECIO_MINIMO', 'NUM_PRECIO_MAXIMO', 'NUM_PRECIO_PROMEDIO', 'ID_PRODUCTO_MERCADO'], 'number'],
            [['ID_DEPARTAMENTO', 'TXT_MES', 'TXT_DIA'], 'string', 'max' => 2],
            [['TXT_DEPARTAMENTO', 'TXT_PROVINCIA', 'TXT_DISTRITO', 'TXT_MERCADO', 'TXT_GRUPO', 'TXT_GENERO', 'TXT_PRODUCTO', 'TXT_ENVASE', 'TXT_MERCADO_TIPO', 'TXT_ORIGEN'], 'string', 'max' => 150],
            [['ID_PROVINCIA', 'TXT_ANIO'], 'string', 'max' => 4],
            [['ID_DISTRITO'], 'string', 'max' => 6],
            [['FEC_REGISTRO', 'FEC_PROCESO'], 'string', 'max' => 7],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_DEPARTAMENTO' => 'Id  Departamento',
            'TXT_DEPARTAMENTO' => 'Txt  Departamento',
            'ID_PROVINCIA' => 'Id  Provincia',
            'TXT_PROVINCIA' => 'Txt  Provincia',
            'ID_DISTRITO' => 'Id  Distrito',
            'TXT_DISTRITO' => 'Txt  Distrito',
            'ID_MERCADO' => 'Id  Mercado',
            'TXT_MERCADO' => 'Txt  Mercado',
            'ID_GRUPO' => 'Id  Grupo',
            'TXT_GRUPO' => 'Txt  Grupo',
            'ID_GENERO' => 'Id  Genero',
            'TXT_GENERO' => 'Txt  Genero',
            'ID_PRODUCTO' => 'Id  Producto',
            'TXT_PRODUCTO' => 'Txt  Producto',
            'ID_ENVASE' => 'Id  Envase',
            'TXT_ENVASE' => 'Txt  Envase',
            'ID_MERCADO_TIPO' => 'Id  Mercado  Tipo',
            'TXT_MERCADO_TIPO' => 'Txt  Mercado  Tipo',
            'TXT_ANIO' => 'Txt  Anio',
            'TXT_MES' => 'Txt  Mes',
            'TXT_DIA' => 'Txt  Dia',
            'NUM_PRECIO_MINIMO' => 'Num  Precio  Minimo',
            'NUM_PRECIO_MAXIMO' => 'Num  Precio  Maximo',
            'NUM_PRECIO_PROMEDIO' => 'Num  Precio  Promedio',
            'FEC_REGISTRO' => 'Fec  Registro',
            'TXT_ORIGEN' => 'Txt  Origen',
            'ID_PRODUCTO_MERCADO' => 'Id  Producto  Mercado',
            'FEC_PROCESO' => 'Fec  Proceso',
        ];
    }
}
