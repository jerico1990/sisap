<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TC_USUARIO_PERFIL".
 *
 * @property float $ID_USUARIO_PERFIL Autogenerado
 * @property float|null $ID_PERFIL Identificador de Perfil
 * @property float|null $ID_USUARIO Identificador de Usuario
 * @property string|null $FEC_INICIO Fecha de Inicio
 * @property string|null $FEC_FIN Fecha de Fin
 * @property int|null $FLG_REGISTRO Estado
 * @property string|null $TXT_USER_REGISTRA Usuario que registro
 * @property string|null $FEC_REGISTRA Fecha de registro
 * @property string|null $TXT_USER_ACTUALIZA Usuario que actualizo
 * @property string|null $FEC_ACTUALIZA Fecha de actualizacion
 * @property float|null $FLG_HABILITADO Estado del registro
 *
 * @property TPUSUARIO $uSUARIO
 */
class UsuarioPerfil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TC_USUARIO_PERFIL';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_USUARIO_PERFIL', 'ID_PERFIL', 'ID_USUARIO', 'FLG_HABILITADO'], 'number'],
            [['FLG_REGISTRO'], 'integer'],
            [['FEC_INICIO', 'FEC_FIN', 'FEC_REGISTRA', 'FEC_ACTUALIZA'], 'string', 'max' => 7],
            [['TXT_USER_REGISTRA', 'TXT_USER_ACTUALIZA'], 'string', 'max' => 20],
            [['ID_USUARIO_PERFIL'], 'unique'],
            //[['ID_USUARIO'], 'exist', 'skipOnError' => true, 'targetClass' => TPUSUARIO::className(), 'targetAttribute' => ['ID_USUARIO' => 'ID_USUARIO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_USUARIO_PERFIL' => 'Id  Usuario  Perfil',
            'ID_PERFIL' => 'Id  Perfil',
            'ID_USUARIO' => 'Id  Usuario',
            'FEC_INICIO' => 'Fec  Inicio',
            'FEC_FIN' => 'Fec  Fin',
            'FLG_REGISTRO' => 'Flg  Registro',
            'TXT_USER_REGISTRA' => 'Txt  User  Registra',
            'FEC_REGISTRA' => 'Fec  Registra',
            'TXT_USER_ACTUALIZA' => 'Txt  User  Actualiza',
            'FEC_ACTUALIZA' => 'Fec  Actualiza',
            'FLG_HABILITADO' => 'Flg  Habilitado',
        ];
    }

    /**
     * Gets query for [[USUARIO]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUSUARIO()
    {
        return $this->hasOne(TPUSUARIO::className(), ['ID_USUARIO' => 'ID_USUARIO']);
    }
}
