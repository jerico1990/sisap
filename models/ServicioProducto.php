<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TG_PRODUCTO_MERCADO".
 *
 * @property float $ID_PRODUCTO_MERCADO Identificador del Producto por mercado
 * @property float|null $ID_PRODUCTO Codigo de Producto
 * @property float|null $ID_MERCADO Codigo de Mercado
 * @property float|null $ID_ENVASE Codigo de Envase
 * @property float|null $NUM_EQUIVALENCIA Numero de Equivalencia
 * @property float|null $FLG_HABILTADO Estado
 * @property float|null $FLG_PUBLICADO Estado de publicacion del producto en el mercado
 * @property float|null $ID_USUARIO_REGISTRO Usuario que registro
 *
 * @property TGPRODUCTO $pRODUCTO
 */
class ServicioProducto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TG_SERVICIO_PRODUCTO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['ID_SERVICIO_PRODUCTO', 'ID_SERVICIO', 'ID_PRODUCTO_MERCADO', 'FLG_HABILITADO'], 'number'],
            [['ID_SERVICIO_PRODUCTO'], 'unique'],
            //[['ID_PRODUCTO'], 'exist', 'skipOnError' => true, 'targetClass' => TGPRODUCTO::className(), 'targetAttribute' => ['ID_PRODUCTO' => 'ID_PRODUCTO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PRODUCTO_MERCADO' => 'Id  Producto  Mercado',
            'ID_SERVICIO' => 'Id  Producto',
            'ID_SERVICIO_PRODUCTO' => 'Id  Mercado',
            'FLG_HABILITADO' => 'Flg  Habiltado',
        ];
    }

    /**
     * Gets query for [[PRODUCTO]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUCTO()
    {
        return $this->hasOne(TGPRODUCTO::className(), ['ID_PRODUCTO' => 'ID_PRODUCTO']);
    }
}
