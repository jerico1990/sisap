<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TM_PRECIO".
 *
 * @property float $ID_PRECIO Identificador de Precio
 * @property float|null $ID_PRODUCTO_MERCADO Codigo de Producto por Mercado
 * @property float|null $NUM_PRECIO_1 Precio de puesto 1
 * @property float|null $NUM_PRECIO_2 Precio de puesto 2
 * @property float|null $NUM_PRECIO_3 Precio de puesto 3
 * @property float|null $NUM_PRECIO_4 Precio de puesto 4
 * @property string|null $FEC_REGISTRO Fecha de registro
 * @property float|null $FLG_HABILITADO Estado
 * @property float|null $ID_USUARIO_REGISTRO Usuario que registro
 *
 * @property TPUSUARIO $uSUARIOREGISTRO
 */
class Precio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TM_PRECIO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_PRECIO', 'ID_PRODUCTO_MERCADO', 'FLG_HABILITADO', 'ID_USUARIO_REGISTRO'], 'number'],
            [['NUM_PRECIO_1','NUM_PRECIO_2', 'NUM_PRECIO_3', 'NUM_PRECIO_4','FEC_REGISTRO'], 'safe'],
            //[['ID_PRECIO'], 'unique'],
            //[['ID_USUARIO_REGISTRO'], 'exist', 'skipOnError' => true, 'targetClass' => TPUSUARIO::className(), 'targetAttribute' => ['ID_USUARIO_REGISTRO' => 'ID_USUARIO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PRECIO' => 'Id  Precio',
            'ID_PRODUCTO_MERCADO' => 'Id  Producto  Mercado',
            'NUM_PRECIO_1' => 'Num  Precio 1',
            'NUM_PRECIO_2' => 'Num  Precio 2',
            'NUM_PRECIO_3' => 'Num  Precio 3',
            'NUM_PRECIO_4' => 'Num  Precio 4',
            'FEC_REGISTRO' => 'Fec  Registro',
            'FLG_HABILITADO' => 'Flg  Habilitado',
            'ID_USUARIO_REGISTRO' => 'Id  Usuario  Registro',
        ];
    }

    /**
     * Gets query for [[USUARIOREGISTRO]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUSUARIOREGISTRO()
    {
        return $this->hasOne(TPUSUARIO::className(), ['ID_USUARIO' => 'ID_USUARIO_REGISTRO']);
    }
}
