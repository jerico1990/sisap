<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $usuario
 * @property string $clave
 * @property int $estado
 */
class Clave extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $titulo;
    public $nombres;
    public $claveanterior;
    public $idusuario;
    public static function tableName()
    {
        //return 'ap_tg_usuario';
        return 'TP_USUARIO';
    }

    public function rules()
    {
        return [
            [['ID_USUARIO','TXT_USUARIO','TXT_CLAVE','TXT_NOMBRES','TXT_APELLIDO_PATERNO','TXT_APELLIDO_MATERNO','FLG_ACTIVO'], 'safe'],
            //[['ID_UBIGEO'], 'exist', 'skipOnError' => true, 'targetClass' => TGUBIGEO::className(), 'targetAttribute' => ['ID_UBIGEO' => 'ID_UBIGEO']],
        ];
    }


}
