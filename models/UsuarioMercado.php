<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TM_USUARIO_MERCADO".
 *
 * @property float $ID_USUARIO_MERCADO Identificador de Usuario por Mercado
 * @property float|null $ID_USUARIO Codigo de Usuario
 * @property float|null $ID_MERCADO Codigo de Mercado
 * @property string|null $FEC_INICIO Fecha de Inicio
 * @property string|null $FEC_FIN Fecha de Fin
 * @property float|null $FLG_HABILITADO Estado
 *
 * @property TPUSUARIO $uSUARIO
 */
class UsuarioMercado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TM_USUARIO_MERCADO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_USUARIO_MERCADO', 'ID_USUARIO', 'ID_MERCADO', 'FLG_HABILITADO'], 'number'],
            [['FEC_INICIO', 'FEC_FIN'], 'string', 'max' => 7],
            [['ID_USUARIO_MERCADO'], 'unique'],
            //[['ID_USUARIO'], 'exist', 'skipOnError' => true, 'targetClass' => TPUSUARIO::className(), 'targetAttribute' => ['ID_USUARIO' => 'ID_USUARIO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_USUARIO_MERCADO' => 'Id  Usuario  Mercado',
            'ID_USUARIO' => 'Id  Usuario',
            'ID_MERCADO' => 'Id  Mercado',
            'FEC_INICIO' => 'Fec  Inicio',
            'FEC_FIN' => 'Fec  Fin',
            'FLG_HABILITADO' => 'Flg  Habilitado',
        ];
    }

    /**
     * Gets query for [[USUARIO]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUSUARIO()
    {
        return $this->hasOne(TPUSUARIO::className(), ['ID_USUARIO' => 'ID_USUARIO']);
    }
}
