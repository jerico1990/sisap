<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TG_PARAMETRO".
 *
 * @property float|null $ID_PARAMETRO
 * @property string|null $TXT_PARAMETRO
 * @property float|null $NUM_VALOR
 * @property float|null $FLG_HABILITADO
 */
class Parametro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TG_PARAMETRO';
    }

    /**
     * {@inheritdoc}
     */
    public $titulo;
    public function rules()
    {
        return [
            [['ID_PARAMETRO', 'NUM_VALOR', 'FLG_HABILITADO'], 'number'],
            [['TXT_PARAMETRO'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PARAMETRO' => 'Id  Parametro',
            'TXT_PARAMETRO' => 'Txt  Parametro',
            'NUM_VALOR' => 'Num  Valor',
            'FLG_HABILITADO' => 'Flg  Habilitado',
        ];
    }
}
