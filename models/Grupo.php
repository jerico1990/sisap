<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TG_PRODUCTO_GRUPO".
 *
 * @property float $ID_PRODUCTO_GRUPO
 * @property string|null $TXT_PRODUCTO_GRUPO
 * @property float|null $FLG_HABILITADO
 *
 * @property TGPRODUCTOGENERO[] $tGPRODUCTOGENEROs
 */
class Grupo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TG_PRODUCTO_GRUPO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_PRODUCTO_GRUPO', 'FLG_HABILITADO'], 'number'],
            [['TXT_PRODUCTO_GRUPO'], 'string', 'max' => 150],
            [['ID_PRODUCTO_GRUPO'], 'unique'],
            [['TXT_CODIGO_GRUPO'], 'string', 'max' => 6],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PRODUCTO_GRUPO' => 'Id  Producto  Grupo',
            'TXT_PRODUCTO_GRUPO' => 'Txt  Producto  Grupo',
            'FLG_HABILITADO' => 'Flg  Habilitado',
        ];
    }

    /**
     * Gets query for [[TGPRODUCTOGENEROs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTGPRODUCTOGENEROs()
    {
        return $this->hasMany(TGPRODUCTOGENERO::className(), ['ID_PRODUCTO_GRUPO' => 'ID_PRODUCTO_GRUPO']);
    }
}
