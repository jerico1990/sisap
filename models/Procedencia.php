<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ap_tg_producto_proc".
 *
 * @property string $cod_producto
 * @property string $cod_ubigeo
 * @property string $flg_activo
 *
 * @property ApTgProductoVariedad $cODPRODUCTO
 */
class Procedencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'ap_tg_producto_proc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_producto', 'cod_ubigeo', 'flg_activo'], 'required'],
            [['cod_producto', 'cod_ubigeo'], 'string', 'max' => 6],
            [['flg_activo'], 'string', 'max' => 1],
            [['cod_producto', 'cod_ubigeo'], 'unique', 'targetAttribute' => ['cod_producto', 'cod_ubigeo']],
            [['COD_PRODUCTO'], 'exist', 'skipOnError' => true, 'targetClass' => ApTgProductoVariedad::className(), 'targetAttribute' => ['COD_PRODUCTO' => 'COD_PRODUCTO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_producto' => 'Cod Producto',
            'cod_ubigeo' => 'Cod Ubigeo',
            'flg_activo' => 'Flg Activo',
        ];
    }

    /**
     * Gets query for [[CODPRODUCTO]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCODPRODUCTO()
    {
        return $this->hasOne(ApTgProductoVariedad::className(), ['COD_PRODUCTO' => 'COD_PRODUCTO']);
    }
}
