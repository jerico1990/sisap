<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TM_ABASTECIMIENTO".
 *
 * @property float $ID_ABASTECIMIENTO Identificador de Abastecimiento
 * @property float|null $ID_PRODUCTO_MERCADO Codigo de Producto por Mercado
 * @property string|null $ID_UBIGEO_PROCEDENCIA Ubigeo de Procedencia
 * @property float|null $NUM_CANTIDAD_PRODUCTO Cantidad del  producto
 * @property float|null $NUM_VOLUMEN_PRODUCTO Volumen del  producto
 * @property string|null $FEC_REGISTRO Fecha de registro
 * @property float|null $FLG_HABILITADO Estado
 * @property float|null $ID_USUARIO_REGISTRO Usuario que registro
 *
 * @property TPUSUARIO $uSUARIOREGISTRO
 */
class Abastecimiento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TM_ABASTECIMIENTO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_ABASTECIMIENTO', 'ID_PRODUCTO_MERCADO', 'FLG_HABILITADO', 'ID_USUARIO_REGISTRO'], 'number'],
            [['ID_PROCEDENCIA'], 'string', 'max' => 6],
            [['FEC_REGISTRO', 'NUM_CANTIDAD_PRODUCTO', 'NUM_VOLUMEN_PRODUCTO'], 'safe'],
            [['ID_ABASTECIMIENTO'], 'unique'],
           // [['ID_USUARIO_REGISTRO'], 'exist', 'skipOnError' => true, 'targetClass' => TPUSUARIO::className(), 'targetAttribute' => ['ID_USUARIO_REGISTRO' => 'ID_USUARIO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_ABASTECIMIENTO' => 'Id  Abastecimiento',
            'ID_PRODUCTO_MERCADO' => 'Id  Producto  Mercado',
            'ID_PROCEDENCIA' => 'Id  Ubigeo  Procedencia',
            'NUM_CANTIDAD_PRODUCTO' => 'Num  Cantidad  Producto',
            'NUM_VOLUMEN_PRODUCTO' => 'Num  Volumen  Producto',
            'FEC_REGISTRO' => 'Fec  Registro',
            'FLG_HABILITADO' => 'Flg  Habilitado',
            'ID_USUARIO_REGISTRO' => 'Id  Usuario  Registro',
        ];
    }

    /**
     * Gets query for [[USUARIOREGISTRO]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUSUARIOREGISTRO()
    {
        return $this->hasOne(TPUSUARIO::className(), ['ID_USUARIO' => 'ID_USUARIO_REGISTRO']);
    }
}
