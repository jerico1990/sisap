<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TG_PRODUCTO_MERCADO".
 *
 * @property float $ID_PRODUCTO_MERCADO Identificador del Producto por mercado
 * @property float|null $ID_PRODUCTO Codigo de Producto
 * @property float|null $ID_MERCADO Codigo de Mercado
 * @property float|null $ID_ENVASE Codigo de Envase
 * @property float|null $NUM_EQUIVALENCIA Numero de Equivalencia
 * @property float|null $FLG_HABILTADO Estado
 * @property float|null $FLG_PUBLICADO Estado de publicacion del producto en el mercado
 * @property float|null $ID_USUARIO_REGISTRO Usuario que registro
 *
 * @property TGPRODUCTO $pRODUCTO
 */
class ProductoMercado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TG_PRODUCTO_MERCADO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['ID_PRODUCTO_MERCADO', 'ID_PRODUCTO', 'ID_MERCADO', 'ID_ENVASE', 'FLG_HABILITADO', 'FLG_PUBLICADO', 'ID_USUARIO_REGISTRO','ID_PRODUCTO_GENERO'], 'number'],
            [['ID_PRODUCTO_MERCADO'], 'unique'],
            //[['ID_PRODUCTO'], 'exist', 'skipOnError' => true, 'targetClass' => TGPRODUCTO::className(), 'targetAttribute' => ['ID_PRODUCTO' => 'ID_PRODUCTO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PRODUCTO_MERCADO' => 'Id  Producto  Mercado',
            'ID_PRODUCTO' => 'Id  Producto',
            'ID_MERCADO' => 'Id  Mercado',
            'ID_ENVASE' => 'Id  Envase',
            'FLG_HABILITADO' => 'Flg  Habiltado',
            'FLG_PUBLICADO' => 'Flg  Publicado',
            'ID_USUARIO_REGISTRO' => 'Id  Usuario  Registro',
        ];
    }

    /**
     * Gets query for [[PRODUCTO]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUCTO()
    {
        return $this->hasOne(TGPRODUCTO::className(), ['ID_PRODUCTO' => 'ID_PRODUCTO']);
    }
}
