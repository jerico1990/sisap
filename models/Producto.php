<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TG_PRODUCTO".
 *
 * @property float $ID_PRODUCTO Identificador de Producto
 * @property float|null $ID_PRODUCTO_GENERO Codigo de Genero de producto
 * @property float|null $ID_UNIDAD_MEDIDA Codigo de Unidad de medida
 * @property string $TXT_CODIGO_PRODUCTO Codigo de Producto
 * @property string|null $TXT_PRODUCTO Descripcion de Producto
 * @property float|null $FLG_HABILITADO Estado
 *
 * @property TGPRODUCTOMERCADO[] $tGPRODUCTOMERCADOs
 * @property TGUNIDADMEDIDA $uNIDADMEDIDA
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public $ID_PRODUCTO_GRUPO;
    public static function tableName()
    {
        return 'TG_PRODUCTO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_PRODUCTO_GENERO', 'ID_UNIDAD_MEDIDA', 'FLG_HABILITADO','ID_PRODUCTO_GRUPO'], 'number'],
            //[['TXT_CODIGO_PRODUCTO'], 'required'],
            [['TXT_CODIGO_PRODUCTO'], 'string', 'max' => 6],
            [['TXT_PRODUCTO'], 'string', 'max' => 150],
            //[['ID_PRODUCTO'], 'unique'],
            //[['ID_UNIDAD_MEDIDA'], 'exist', 'skipOnError' => true, 'targetClass' => TGUNIDADMEDIDA::className(), 'targetAttribute' => ['ID_UNIDAD_MEDIDA' => 'ID_UNIDAD_MEDIDA']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PRODUCTO' => 'Id  Producto',
            'ID_PRODUCTO_GENERO' => 'Id  Producto  Genero',
            'ID_UNIDAD_MEDIDA' => 'Id  Unidad  Medida',
            'TXT_CODIGO_PRODUCTO' => 'Txt  Codigo  Producto',
            'TXT_PRODUCTO' => 'Txt  Producto',
            'FLG_HABILITADO' => 'Flg  Habilitado',
        ];
    }

    /**
     * Gets query for [[TGPRODUCTOMERCADOs]].
     *
     * @return \yii\db\ActiveQuery
     */
   /*  public function getTGPRODUCTOMERCADOs()
    {
        return $this->hasMany(TGPRODUCTOMERCADO::className(), ['ID_PRODUCTO' => 'ID_PRODUCTO']);
    } */

    /**
     * Gets query for [[UNIDADMEDIDA]].
     *
     * @return \yii\db\ActiveQuery
     */
    /* public function getUNIDADMEDIDA()
    {
        return $this->hasOne(TGUNIDADMEDIDA::className(), ['ID_UNIDAD_MEDIDA' => 'ID_UNIDAD_MEDIDA']);
    } */
}
