<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $usuario
 * @property string $clave
 * @property int $estado
 */
class Usuario extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public $auth_key;
    public $titulo;
    public $ID_REGION;
    public $ID_PROVINCIA;
    public $ID_PERFILES;

    public static function tableName()
    {
        //return 'ap_tg_usuario';
        return 'TP_USUARIO';
    }

    public function rules()
    {
        return [
            [['ID_REGION', 'ID_PROVINCIA','ID_PERFILES','TXT_USUARIO','TXT_CLAVE','TXT_NOMBRES','TXT_APELLIDO_PATERNO','TXT_APELLIDO_MATERNO','TXT_EMAIL','TXT_NUMERO_CONTACTO','ID_UBIGEO','FLG_HABILITADO','TXT_TRAMA'], 'safe'],
            //[['ID_UBIGEO'], 'exist', 'skipOnError' => true, 'targetClass' => TGUBIGEO::className(), 'targetAttribute' => ['ID_UBIGEO' => 'ID_UBIGEO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    

    /**
     * {@inheritdoc}
     */
    

    public static function findIdentity($id){
        return static::findOne($id);
    }

    public function getId(){
        return $this->getPrimaryKey();
    }

    public function getUsername(){
        return $this->TXT_NOMBRES.' '.$this->TXT_APELLIDO_PATERNO;
    }

    public static function findByUsername($password,$username)
    {
      return static::find()->where('TXT_USUARIO=:TXT_USUARIO and FLG_HABILITADO=1',[':TXT_USUARIO' => $username])->one();
    }

    public function validatePassword($password,$username){

        $model=static::find()->where('TXT_USUARIO=:TXT_USUARIO and FLG_HABILITADO=1',[':TXT_USUARIO' => $username])->one();
        if ($password == $model->TXT_CLAVE) {
            return $model;
        }
        return false;
    }

    public static function findIdentityByAccessToken($token, $type = null){
        if ($user['accessToken'] === $token) {
           return new static($user);
        }
        return null;
    }

    public function getAuthKey(){
        return $this->auth_key;
    }

    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }
}
