<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TR_ABASTECIMIENTO".
 *
 * @property string|null $ID_DEPARTAMENTO Codigo de Departamento
 * @property string|null $TXT_DEPARTAMENTO Descripcion de Departamento
 * @property string|null $ID_PROVINCIA Codigo de Provincia
 * @property string|null $TXT_PROVINCIA Descripcion de Provincia
 * @property string|null $ID_DISTRITO Codigo de Distrito
 * @property string|null $TXT_DISTRITO Descripcion de Distrito
 * @property string|null $ID_PAIS_PROCEDENCIA Codigo de Pais de procedencia
 * @property string|null $TXT_PAIS_PROCEDENCIA Descripcion de Pais de procedencia
 * @property string|null $ID_DEPARTAMENTO_PROCEDENCIA Codigo de Departamento de procedencia
 * @property string|null $TXT_DEPARTAMENTO_PROCEDENCIA Descripcion de Departamento de procedencia
 * @property string|null $ID_PROVINCIA_PROCEDENCIA Codigo de Provincia de procedencia
 * @property string|null $TXT_PROVINCIA_PROCEDENCIA Descripcion de Provincia de procedencia
 * @property string|null $ID_DISTRITO_PROCEDENCIA Codigo de Distrito de procedencia
 * @property string|null $TXT_DISTRITO_PROCEDENCIA Descripcion de Distrito de procedencia
 * @property float|null $ID_MERCADO Codigo de Mercado
 * @property string|null $TXT_MERCADO Descripcion de Mercado
 * @property float|null $ID_GRUPO Codigo de Grupo de producto
 * @property string|null $TXT_GRUPO Descripcion de Grupo de producto
 * @property float|null $ID_GENERO Codigo de Genero de producto
 * @property string|null $TXT_GENERO Descripcion de Genero de producto
 * @property float|null $ID_PRODUCTO Codigo de Producto
 * @property string|null $TXT_PRODUCTO Descripcion de Producto
 * @property float|null $ID_ENVASE Codigo de Envase
 * @property string|null $TXT_ENVASE Descripcion de Envase
 * @property float|null $ID_MERCADO_TIPO Codigo de Tipo de Mercado
 * @property string|null $TXT_MERCADO_TIPO Descripcion de Tipo de Mercado
 * @property string|null $TXT_ANIO Anio
 * @property string|null $TXT_MES Mes
 * @property string|null $TXT_DIA Dia
 * @property float|null $NUM_VOLUMEN Volumen
 * @property string|null $FEC_REGISTRO Fecha de registro
 * @property string|null $TXT_ORIGEN Origen
 */
class TRAbastecimiento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TR_ABASTECIMIENTO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_MERCADO', 'ID_GRUPO', 'ID_GENERO', 'ID_PRODUCTO', 'ID_ENVASE', 'ID_MERCADO_TIPO', 'NUM_VOLUMEN'], 'number'],
            [['ID_DEPARTAMENTO', 'ID_DEPARTAMENTO_PROCEDENCIA', 'TXT_MES', 'TXT_DIA'], 'string', 'max' => 2],
            [['TXT_DEPARTAMENTO', 'TXT_PROVINCIA', 'TXT_DISTRITO', 'TXT_PAIS_PROCEDENCIA', 'TXT_DEPARTAMENTO_PROCEDENCIA', 'TXT_PROVINCIA_PROCEDENCIA', 'TXT_DISTRITO_PROCEDENCIA', 'TXT_MERCADO', 'TXT_GRUPO', 'TXT_GENERO', 'TXT_PRODUCTO', 'TXT_ENVASE', 'TXT_MERCADO_TIPO', 'TXT_ORIGEN'], 'string', 'max' => 150],
            [['ID_PROVINCIA', 'ID_PROVINCIA_PROCEDENCIA', 'TXT_ANIO'], 'string', 'max' => 4],
            [['ID_DISTRITO', 'ID_PAIS_PROCEDENCIA', 'ID_DISTRITO_PROCEDENCIA'], 'string', 'max' => 6],
            [['FEC_REGISTRO'], 'string', 'max' => 7],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_DEPARTAMENTO' => 'Id  Departamento',
            'TXT_DEPARTAMENTO' => 'Txt  Departamento',
            'ID_PROVINCIA' => 'Id  Provincia',
            'TXT_PROVINCIA' => 'Txt  Provincia',
            'ID_DISTRITO' => 'Id  Distrito',
            'TXT_DISTRITO' => 'Txt  Distrito',
            'ID_PAIS_PROCEDENCIA' => 'Id  Pais  Procedencia',
            'TXT_PAIS_PROCEDENCIA' => 'Txt  Pais  Procedencia',
            'ID_DEPARTAMENTO_PROCEDENCIA' => 'Id  Departamento  Procedencia',
            'TXT_DEPARTAMENTO_PROCEDENCIA' => 'Txt  Departamento  Procedencia',
            'ID_PROVINCIA_PROCEDENCIA' => 'Id  Provincia  Procedencia',
            'TXT_PROVINCIA_PROCEDENCIA' => 'Txt  Provincia  Procedencia',
            'ID_DISTRITO_PROCEDENCIA' => 'Id  Distrito  Procedencia',
            'TXT_DISTRITO_PROCEDENCIA' => 'Txt  Distrito  Procedencia',
            'ID_MERCADO' => 'Id  Mercado',
            'TXT_MERCADO' => 'Txt  Mercado',
            'ID_GRUPO' => 'Id  Grupo',
            'TXT_GRUPO' => 'Txt  Grupo',
            'ID_GENERO' => 'Id  Genero',
            'TXT_GENERO' => 'Txt  Genero',
            'ID_PRODUCTO' => 'Id  Producto',
            'TXT_PRODUCTO' => 'Txt  Producto',
            'ID_ENVASE' => 'Id  Envase',
            'TXT_ENVASE' => 'Txt  Envase',
            'ID_MERCADO_TIPO' => 'Id  Mercado  Tipo',
            'TXT_MERCADO_TIPO' => 'Txt  Mercado  Tipo',
            'TXT_ANIO' => 'Txt  Anio',
            'TXT_MES' => 'Txt  Mes',
            'TXT_DIA' => 'Txt  Dia',
            'NUM_VOLUMEN' => 'Num  Volumen',
            'FEC_REGISTRO' => 'Fec  Registro',
            'TXT_ORIGEN' => 'Txt  Origen',
        ];
    }
}
