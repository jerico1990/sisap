<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TG_MERCADO".
 *
 * @property float $ID_MERCADO
 * @property string|null $ID_UBIGEO
 * @property string|null $TXT_MERCADO
 * @property float|null $ID_MERCADO_TIPO
 * @property string|null $TXT_DIRECCION
 * @property string|null $TXT_LATITUD
 * @property string|null $TXT_LONGITUD
 * @property float|null $FLG_HABILITADO
 *
 * @property TGUBIGEO $uBIGEO
 */
class Mercado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public $ID_REGION;
    public $ID_PROVINCIA;
    public static function tableName()
    {
        return 'TG_MERCADO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_MERCADO', 'ID_MERCADO_TIPO', 'FLG_HABILITADO'], 'number'],
            [['ID_UBIGEO','ID_REGION','ID_PROVINCIA'], 'string', 'max' => 6],
            [['TXT_MERCADO'], 'string', 'max' => 150],
            [['TXT_DIRECCION'], 'string', 'max' => 200],
            [['TXT_LATITUD', 'TXT_LONGITUD'], 'string', 'max' => 255],
            [['ID_MERCADO'], 'unique'],
            [['NUM_DESVIACION_ESTANDAR'],'safe']
            //[['ID_UBIGEO'], 'exist', 'skipOnError' => true, 'targetClass' => TGUBIGEO::className(), 'targetAttribute' => ['ID_UBIGEO' => 'ID_UBIGEO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_MERCADO' => 'Id  Mercado',
            'ID_UBIGEO' => 'Id  Ubigeo',
            'TXT_MERCADO' => 'Txt  Mercado',
            'ID_MERCADO_TIPO' => 'Id  Mercado  Tipo',
            'TXT_DIRECCION' => 'Txt  Direccion',
            'TXT_LATITUD' => 'Txt  Latitud',
            'TXT_LONGITUD' => 'Txt  Longitud',
            'FLG_HABILITADO' => 'Flg  Habilitado',
        ];
    }

    /**
     * Gets query for [[UBIGEO]].
     *
     * @return \yii\db\ActiveQuery
     */
    /* public function getUBIGEO()
    {
        return $this->hasOne(TGUBIGEO::className(), ['ID_UBIGEO' => 'ID_UBIGEO']);
    } */
}
