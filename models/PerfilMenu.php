<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TC_PERFIL_MENU".
 *
 * @property float $ID_PERFIL_MENU
 * @property float|null $ID_PERFIL
 * @property float|null $ID_MENU
 *
 * @property TPPERFIL $pERFIL
 */
class PerfilMenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TC_PERFIL_MENU';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_PERFIL_MENU', 'ID_PERFIL', 'ID_MENU'], 'number'],
            [['ID_PERFIL_MENU'], 'unique'],
            //[['ID_PERFIL'], 'exist', 'skipOnError' => true, 'targetClass' => TPPERFIL::className(), 'targetAttribute' => ['ID_PERFIL' => 'ID_PERFIL']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PERFIL_MENU' => 'Id  Perfil  Menu',
            'ID_PERFIL' => 'Id  Perfil',
            'ID_MENU' => 'Id  Menu',
        ];
    }

    /**
     * Gets query for [[PERFIL]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPERFIL()
    {
        return $this->hasOne(TPPERFIL::className(), ['ID_PERFIL' => 'ID_PERFIL']);
    }
}
