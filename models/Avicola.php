<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TM_AVICOLA".
 *
 * @property float $ID_AVICOLA Identificador de Avicola
 * @property float|null $ID_PRODUCTO_MERCADO Codigo de Producto por Mercado
 * @property string|null $ID_UBIGEO_PROCEDENCIA Ubigeo de Procedencia
 * @property float|null $ID_MERCADO_UNIDAD_PRODUCCION Unidad de produccion del mercado
 * @property float|null $NUM_TOTAL Total
 * @property float|null $NUM_CANTIDAD_PRODUCTO Cantidad de productos
 * @property float|null $NUM_PESO_PROMEDIO Peso promedio
 * @property float|null $NUM_PRECIO_GRANJA Precio en Granja
 * @property float|null $NUM_PRECIO_CENTRO_ACOPIO Precio en Centro de Acopio
 * @property string|null $FEC_REGISTRO Fecha de registro
 * @property float|null $FLG_HABILITADO Estado
 * @property float|null $ID_USUARIO_REGISTRO Usuario que registro
 *
 * @property TPUSUARIO $uSUARIOREGISTRO
 */
class Avicola extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TM_AVICOLA';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_AVICOLA', 'ID_PRODUCTO_MERCADO', 'ID_MERCADO_UNIDAD_PRODUCCION', 'NUM_CANTIDAD_PRODUCTO', 'FLG_HABILITADO', 'ID_USUARIO_REGISTRO'], 'number'],
            [['NUM_PESO_PROMEDIO', 'NUM_PRECIO_GRANJA', 'NUM_PRECIO_CENTRO_ACOPIO'], 'safe'],
            [['ID_UBIGEO_PROCEDENCIA'], 'string', 'max' => 6],
            [['FEC_REGISTRO'], 'safe'],
            [['ID_AVICOLA'], 'unique'],
            [['NUM_TOTAL'], 'safe'],
            //[['ID_USUARIO_REGISTRO'], 'exist', 'skipOnError' => true, 'targetClass' => TPUSUARIO::className(), 'targetAttribute' => ['ID_USUARIO_REGISTRO' => 'ID_USUARIO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {

        return [
            'ID_AVICOLA' => 'Id  Avicola',
            'ID_PRODUCTO_MERCADO' => 'Id  Producto  Mercado',
            'ID_UBIGEO_PROCEDENCIA' => 'Id  Ubigeo  Procedencia',
            'ID_MERCADO_UNIDAD_PRODUCCION' => 'Id  Mercado  Unidad  Produccion',
            'NUM_CANTIDAD_PRODUCTO' => 'Num  Cantidad  Producto',
            'NUM_TOTAL' =>'Total',
            'NUM_PESO_PROMEDIO' => 'Num  Peso  Promedio',
            'NUM_PRECIO_GRANJA' => 'Num  Precio  Granja',
            'NUM_PRECIO_CENTRO_ACOPIO' => 'Num  Precio  Centro  Acopio',
            'FEC_REGISTRO' => 'Fec  Registro',
            'FLG_HABILITADO' => 'Flg  Habilitado',
            'ID_USUARIO_REGISTRO' => 'Id  Usuario  Registro',
        ];
    }

    /**
     * Gets query for [[USUARIOREGISTRO]].
     *
     * @return \yii\db\ActiveQuery
     */
    /*public function getUSUARIOREGISTRO()
    {
        return $this->hasOne(TPUSUARIO::className(), ['ID_USUARIO' => 'ID_USUARIO_REGISTRO']);
        
    }*/
}
