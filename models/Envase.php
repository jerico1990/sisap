<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TG_ENVASE".
 *
 * @property float $ID_ENVASE
 * @property string|null $TXT_ENVASE
 * @property float|null $FLG_HABILITADO
 */
class Envase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TG_ENVASE';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_ENVASE', 'FLG_HABILITADO','ID_UNIDAD_MEDIDA','NUM_EQUIVALENCIA'], 'number'],
            [['TXT_ENVASE'], 'string', 'max' => 150],
            [['ID_ENVASE'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_ENVASE' => 'Id  Envase',
            'TXT_ENVASE' => 'Txt  Envase',
            'FLG_HABILITADO' => 'Flg  Habilitado',
        ];
    }
}
