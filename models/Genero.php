<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TG_PRODUCTO_GENERO".
 *
 * @property float $ID_PRODUCTO_GENERO
 * @property float|null $ID_PRODUCTO_GRUPO
 * @property string|null $TXT_PRODUCTO_GENERO
 * @property float|null $FLG_HABILITADO
 *
 * @property TGPRODUCTOGRUPO $pRODUCTOGRUPO
 */
class Genero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'TG_PRODUCTO_GENERO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_PRODUCTO_GENERO', 'ID_PRODUCTO_GRUPO', 'FLG_HABILITADO'], 'number'],
            [['TXT_PRODUCTO_GENERO'], 'string', 'max' => 150],
            [['ID_PRODUCTO_GENERO'], 'unique'],
            [['TXT_CODIGO_GENERO'], 'string', 'max' => 6],
            
            //[['ID_PRODUCTO_GRUPO'], 'exist', 'skipOnError' => true, 'targetClass' => TGPRODUCTOGRUPO::className(), 'targetAttribute' => ['ID_PRODUCTO_GRUPO' => 'ID_PRODUCTO_GRUPO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PRODUCTO_GENERO' => 'Id  Producto  Genero',
            'ID_PRODUCTO_GRUPO' => 'Id  Producto  Grupo',
            'TXT_PRODUCTO_GENERO' => 'Txt  Producto  Genero',
            'FLG_HABILITADO' => 'Flg  Habilitado',
        ];
    }

    /**
     * Gets query for [[PRODUCTOGRUPO]].
     *
     * @return \yii\db\ActiveQuery
     */
    /* public function getPRODUCTOGRUPO()
    {
        return $this->hasOne(TGPRODUCTOGRUPO::className(), ['ID_PRODUCTO_GRUPO' => 'ID_PRODUCTO_GRUPO']);
    } */
}
