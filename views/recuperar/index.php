<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formClave', 'class' => 'form-horizontal']]); ?>

<div class="modal fade" style="z-index:1060" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog text-center" role="document">
            Cargando <br>
            <div class="spinner-border text-primary m-1" role="status"></div>
        </div>
    </div>

<div class="modal-content" style="margin: 0 auto;width: 500px;text-align: center;">
    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
    </div>
    <div class="modal-body">
    <?= Html::hiddenInput('idusuario', $model->ID_USUARIO); ?>
        <div class="row">
            <div class="col-md-12">
                <label for="">Apellidos y Nombres:</label>
                <label for=""><?= Html::encode($model->nombres) ?></label>
                
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'TXT_CLAVE')->label('Nueva Clave')->passwordInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label for="claveNueva">Repetir Nueva Clave</label>
                <input name="claveNueva" type="password" id="claveNueva" class="form-control">
            </div>
        </div>

    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" onclick="irLogin()" data-dismiss="modal">Ir Login</button>
        <button type="button" id="btngrabar" class="btn btn-success btn-grabar-clave" onclick="grabarClave()">Grabar</button>
    </div>
</div>
<?php ActiveForm::end(); ?>

<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');


    function grabarClave() {
        debugger;
        var form = $('#formClave');
        var formData = $('#formClave').serializeArray();

        var error = "";
        var txtClave = $.trim($("[name=\"Clave[TXT_CLAVE]\"]").val());
        var claveNueva = $.trim($("[name=\"claveNueva\"]").val());

        if (!txtClave) {
            $('#clave-txt_clave').addClass('alert-danger');
            $('#clave-txt_clave').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#clave-txt_clave').removeClass('alert-danger');
            $('#clave-txt_clave').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!claveNueva) {
            $('#claveNueva').addClass('alert-danger');
            $('#claveNueva').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#claveNueva').removeClass('alert-danger');
            $('#claveNueva').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if(txtClave!=claveNueva){
            error = error + "error 1";
            alert("La clave no coincide.");
        }

        if (error != "") {
            return false;
        }


        if (form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                //$('.sidebar-mini').LoadingOverlay("hide", true);
            },
            beforeSend: function() {
                $('#staticBackdrop').modal("show")
            },
            success: function(results) {
                if (results.success) {
                    alert("Clave actualizada correctamente.");
                    $("#claveNueva").val("");
                    $("#clave-txt_clave").val("");
                    $("#btngrabar").hide();
                } else {
                    alert("Clave anterior no es valida.");
                }
                $('#staticBackdrop').modal("hide")
            },
        });
    };


    function irLogin(){
        $(location).attr('href','/PD-03-SISAP/web/login/index');
    }

</script>