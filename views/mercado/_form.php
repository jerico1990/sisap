<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formMercado','class' => 'form-horizontal']]); ?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'TXT_MERCADO')->label('Mercado') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'ID_MERCADO_TIPO')->dropDownList([], ['prompt' => 'Seleccionar' ])->label('Tipo de mercado'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'NUM_DESVIACION_ESTANDAR')->label('% Desviación Estandar') ?>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'TXT_DIRECCION')->label('Dirección') ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'ID_REGION')->dropDownList([], ['prompt' => 'Seleccionar' ])->label('Región'); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'ID_PROVINCIA')->dropDownList([], ['prompt' => 'Seleccionar' ])->label('Provincia'); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'ID_UBIGEO')->dropDownList([], ['prompt' => 'Seleccionar' ])->label('Distrito'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
            <?= $form->field($model, 'FLG_HABILITADO')->dropDownList([1=>'Habilitado',2=>'Deshabilitado'], ['prompt' => 'Seleccionar' ])->label('Estado'); ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12" >
                <div class="form-group">
                    <?= $form->field($model, 'TXT_LATITUD')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'TXT_LONGITUD')->hiddenInput()->label(false) ?>
                    
                    <label for="ubicacion">Ubica tu mercado, dandole clic al icono <i class="fas fa-map-marker-alt"></i></label>
                    <div  id="map" class="map sidebar-map" style="height:450px"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success btn-grabar-mercado">Grabar</button>
    </div>

<?php ActiveForm::end(); ?>