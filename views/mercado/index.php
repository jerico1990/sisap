<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet@1.5.1/dist/leaflet.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.fullscreen/1.4.2/Control.FullScreen.min.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/leaflet@1.5.1/dist/leaflet.js" crossorigin=""></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.fullscreen/1.4.2/Control.FullScreen.min.js"></script>
<script src="https://unpkg.com/esri-leaflet@2.3.3/dist/esri-leaflet.js" integrity="sha512-cMQ5e58BDuu1pr9BQ/eGRn6HaR6Olh0ofcHFWe5XesdCITVuSBiBZZbhCijBe5ya238f/zMMRYIMIIg1jxv4sQ==" crossorigin=""></script>
<script src="https://consbio.github.io/Leaflet.Basemaps/L.Control.Basemaps.js" crossorigin="anonymous"></script>

<script src="<?= \Yii::$app->request->BaseUrl ?>/leaflet-Basemaps/L.Control.Basemaps.js"></script>
<link rel="<?= \Yii::$app->request->BaseUrl ?>/leaflet-Basemaps/L.Control.Basemaps.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.js"></script>
<script src="<?= \Yii::$app->request->BaseUrl ?>/leaflet-turf/turf.min.js"></script>
<!-- <script src="<?= \Yii::$app->request->BaseUrl ?>/regiones.js"></script>
<script src="<?= \Yii::$app->request->BaseUrl ?>/provincias.js"></script> -->

<style>
    #lista-mercados_filter {
        display: none
    }
</style>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Mercado</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de mercados</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sel_departamento">Región</label>
                            <select class="form-control" name="sel_departamento" id="sel_departamento">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sel_provincia">Provincia</label>
                            <select class="form-control" name="sel_provincia" id="sel_provincia">
                                <option value>Seleccionar</option>
                            </select>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sel_distrito">Distrito</label>
                            <select class="form-control" name="sel_distrito" id="sel_distrito">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sel_tipo">Tipo de mercado</label>
                            <select class="form-control" name="sel_tipo" id="sel_tipo">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_mercado">Mercado</label>
                            <input type="text" id="txt_mercado" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-agregar-mercado">Agregar</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <label for="">&nbsp&nbsp</label> <br>
                        <button style="float: right;" class="btn btn-primary btn-buscar-mercado">Buscar</button>
                    </div>
                </div>

                <table id="lista-mercados" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Código</th>
                        <th>Descripción</th>
                        <th>% Desviacón Estandar</th>
                        <th>Tipo de mercado</th>
                        <th>Región</th>
                        <th>Provincia</th>
                        <th>Distrito</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="modal fade demo" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>


<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');
    var distritoPoligono;
    var distritoArray;

    Regiones("", "sel_departamento");
    MercadosTipo("", "sel_tipo");
    $('#lista-mercados').DataTable();

    Mercados();
    async function Mercados() {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-mercados',
            method: 'POST',
            data: {
                _csrf: csrf
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var mercados = "";
                    $('#lista-mercados').DataTable().destroy();

                    $.each(results.mercados, function(index, value) {
                        mercados = mercados + "<tr>";
                        mercados = mercados + "<td> " + ((value.TXT_CODIGO_MERCADO)?value.TXT_CODIGO_MERCADO:"") + "</td>";
                        mercados = mercados + "<td> " + ((value.TXT_MERCADO)?value.TXT_MERCADO:"") + "</td>";
                        mercados = mercados + "<td> " + ((value.NUM_DESVIACION_ESTANDAR)?value.NUM_DESVIACION_ESTANDAR:"") + "</td>";
                        mercados = mercados + "<td> " + ((value.TXT_MERCADO_TIPO)?value.TXT_MERCADO_TIPO:"") + "</td>";
                        mercados = mercados + "<td> " + ((value.TXT_DEPARTAMENTO)?value.TXT_DEPARTAMENTO:"") + "</td>";
                        mercados = mercados + "<td> " + ((value.TXT_PROVINCIA)?value.TXT_PROVINCIA:"") + "</td>";
                        mercados = mercados + "<td> " + ((value.TXT_DISTRITO)?value.TXT_DISTRITO:"") + "</td>";
                        mercados = mercados + "<td> " + ((value.TXT_DESCRIPCION_HABILITADO)?value.TXT_DESCRIPCION_HABILITADO:"") + "</td>";
                        mercados = mercados + "<td>";
                        mercados = mercados + '<button data-id="' + value.ID_MERCADO + '" data-idTipoMercado="' + value.ID_MERCADO_TIPO + '" data-idRegion="' + value.ID_REGION + '" data-idProvincia="' + value.ID_PROVINCIA + '" data-idDistrito="' + value.ID_UBIGEO + '" data-txtLatitud= "' + value.TXT_LATITUD + '" data-txtLongitud= "' + value.TXT_LONGITUD + '" class="btn btn-info btn-sm btn-modificar-mercado" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                        mercados = mercados + '<button data-id="' + value.ID_MERCADO + '" class="btn btn-danger btn-sm btn-eliminar-mercado" href="#"><i class="fas fa-trash"></i></button>';
                        mercados = mercados + "</td>";
                        mercados = mercados + "</tr>";
                    });

                    $('#lista-mercados tbody').html(mercados);
                    $('#lista-mercados').DataTable({
                        "dom": "Bfrtip",
                        buttons: [
                            'excelHtml5',
                            'csvHtml5',
                        ],
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 10,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });
                    
                    setTimeout(function() {
                        loading.hide();
                    }, 1000);
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    $("body").on("change", "#sel_departamento", function(e) {
        var idRegion = $(this).val();
        Provincias(idRegion, "", "sel_provincia");
        $("#sel_distrito").html("<option value>Seleccionar</option>");

    });

    $("body").on("change", "#sel_provincia", function(e) {
        var idProvincia = $(this).val();
        Distritos(idProvincia, "", "sel_distrito");
    });
    
    //buscar
    $('body').on('click', '.btn-buscar-mercado', function(e) {
        e.preventDefault();
        var mercado=$("#txt_mercado").val();
        var region=$("#sel_departamento option:selected").text();
        var provincia=$("#sel_provincia option:selected").text();
        var distrito=$("#sel_distrito option:selected").text();
        var tipo=$("#sel_tipo option:selected").text();
              
        $('#lista-mercados').DataTable().column(1).search(mercado).draw();
            
         if(region!="Seleccionar" && region!=""){
            $('#lista-mercados').DataTable().column(4).search(region).draw();
         }else{
            $('#lista-mercados').DataTable().column(4).search("").draw();
         }

         if(provincia!="Seleccionar" && provincia!=""){
            $('#lista-mercados').DataTable().column(5).search(provincia).draw();
         }else{
            $('#lista-mercados').DataTable().column(5).search("").draw();
         }

         if(distrito!="Seleccionar" && distrito!=""){
            $('#lista-mercados').DataTable().column(6).search(distrito).draw();
         }else{
            $('#lista-mercados').DataTable().column(6).search("").draw();
         }

         if(tipo!="Seleccionar" && tipo!=""){
            $('#lista-mercados').DataTable().column(3).search(tipo).draw();
         }else{
            $('#lista-mercados').DataTable().column(3).search("").draw();
         }

       
    });



    //agregar

    $('body').on('click', '.btn-agregar-mercado', function(e) {
        e.preventDefault();
        var demo=1;
        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/mercado/create', function(response, status, xhr) {
            Regiones("", "mercado-id_region");
            MercadosTipo("", "mercado-id_mercado_tipo");
            Mapa();
            RegionesG();
           
        },false);
        $('#modal').modal('show');
    });

    //modificar
    $('body').on('click', '.btn-modificar-mercado', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var idTipoMercado = $(this).attr('data-idTipoMercado');
        var idRegion = $(this).attr('data-idRegion');
        var idProvincia = $(this).attr('data-idProvincia');
        var idDistrito = $(this).attr('data-idDistrito');
        var txtLatitud = $(this).attr('data-txtLatitud');
        var txtLongitud = $(this).attr('data-txtLongitud');
        var demo2=1;
        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/mercado/update?id=' + id, function(response, status, xhr) {
            console.log(status);
            MercadosTipo(idTipoMercado, "mercado-id_mercado_tipo");
            Regiones(idRegion, "mercado-id_region");
            Provincias(idRegion, idProvincia, "mercado-id_provincia");
            Distritos(idProvincia, idDistrito, "mercado-id_ubigeo");
            Mapa();
            DistritosG(idDistrito, txtLatitud, txtLongitud);
        },false);

        
        $('#modal').modal('show');
    });

    //eliminar
    $('body').on('click', '.btn-eliminar-mercado', function (e) {
        e.preventDefault();
        idMercado = $(this).attr('data-id');
        Swal.fire({
            title: '¿Está seguro de deshabilitar el registro?',
            //text: "Una vez eliminado el registro ya no se podrá visualizar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, deshabilitar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url : '<?= \Yii::$app->request->BaseUrl ?>/mercado/eliminar',
                    method: "POST",
                    data:{_csrf:csrf,idMercado:idMercado},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            Mercados();
                            toastr.success('Registro deshabilitado');
                        }
                    },
                    error:function(){
                        alert("Error al realizar el proceso.");
                    }
                });
                
            }
        });
    });


    //grabar

    $('body').on('click', '.btn-grabar-mercado', function(e) {

        e.preventDefault();
        var form = $('#formMercado');
        var formData = $('#formMercado').serializeArray();

        var error = "";
        var txtMercado = $.trim($("[name=\"Mercado[TXT_MERCADO]\"]").val());
        var idMercadoTipo = $.trim($("[name=\"Mercado[ID_MERCADO_TIPO]\"]").val());
        var flgHabilitado = $.trim($("[name=\"Mercado[FLG_HABILITADO]\"]").val());
        var numDesviacionEstandar = $.trim($("[name=\"Mercado[NUM_DESVIACION_ESTANDAR]\"]").val());


        if (!txtMercado) {
            $('#mercado-txt_mercado').addClass('alert-danger');
            $('#mercado-txt_mercado').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#mercado-txt_mercado').removeClass('alert-danger');
            $('#mercado-txt_mercado').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!idMercadoTipo) {
            $('#mercado-id_mercado_tipo').addClass('alert-danger');
            $('#mercado-id_mercado_tipo').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 2";
        } else {
            $('#mercado-id_mercado_tipo').removeClass('alert-danger');
            $('#mercado-id_mercado_tipo').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!flgHabilitado) {
            $('#mercado-flg_habilitado').addClass('alert-danger');
            $('#mercado-flg_habilitado').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#mercado-flg_habilitado').removeClass('alert-danger');
            $('#mercado-flg_habilitado').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!numDesviacionEstandar) {
            $('#mercado-num_desviacion_estandar').addClass('alert-danger');
            $('#mercado-num_desviacion_estandar').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 2";
        } else {
            $('#mercado-num_desviacion_estandar').removeClass('alert-danger');
            $('#mercado-num_desviacion_estandar').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }


        if (error != "") {
            return false;
        }


        if (form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                //$('.sidebar-mini').LoadingOverlay("hide", true);
                toastr.error('Error al realizar el proceso.');
            },
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results.success) {
                    /* setTimeout(function() {
                        loading.hide();
                    }, 1000); */
                    Mercados();
                    $('#modal').modal('hide');
                    toastr.success('Regitro grabado');
                }else{
                    toastr.warning('No se pudo grabar el registro, realizarlo nuevamente');
                }
            },
        });
    });

    $("body").on("change", "#mercado-id_region", function(e) {
        e.preventDefault();
        drawnItems.clearLayers();
        $("#mercado-id_provincia").html("<option value>Seleccionar</option>");
        $("#mercado-id_ubigeo").html("<option value>Seleccionar</option>");
        if ($(this).val()) {
            var idRegion = $(this).val();

            if(boundsNacional.isValid()){
                map.fitBounds(boundsNacional);
            }

            //map.fitBounds(boundsNacional);

            if (regionesGis != undefined) {
                map.removeLayer(regionesGis);
            }

            if (regionGis != undefined) {
                map.removeLayer(regionGis);
            }


            if (provinciasGis != undefined) {
                map.removeLayer(provinciasGis);
                if (distritosGis) {
                    map.removeLayer(distritosGis);
                }
            }

            regionGis = new L.esri.featureLayer({
                url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
                where: "iddpto='" + idRegion + "'",
                style: function(feature) {
                    return {
                        color: 'white',
                        weight: 2
                    };
                },
            }).addTo(map);

            regionGis.once('load', async function(evt) {
                // create a new empty Leaflet bounds object
                boundsRegion = L.latLngBounds([]);
                // loop through the features returned by the server
                regionGis.eachFeature(async function(layer) {
                    // get the bounds of an individual feature
                    var layerBounds = layer.getBounds();
                    // extend the bounds of the collection to fit the bounds of the new feature
                    await boundsRegion.extend(layerBounds);
                });

                // once we've looped through all the features, zoom the map to the extent of the collection
                if(boundsRegion.isValid()){
                    await map.fitBounds(boundsRegion);
                }
               
            });

            provinciasGis = new L.esri.featureLayer({
                url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
                where: "iddpto='" + idRegion + "'",
                style: function(feature) {
                    return {
                        color: 'white',
                        weight: 2
                    };
                },
            }).addTo(map);

            Provincias(idRegion, "", "mercado-id_provincia");
        } else {

            if(boundsNacional.isValid()){
                map.fitBounds(boundsNacional);
            }

            //map.fitBounds(boundsNacional);

            if (regionGis != undefined) {
                map.removeLayer(regionGis);
            }

            if (provinciasGis != undefined) {
                map.removeLayer(provinciasGis);
            }

            if (distritosGis != undefined) {
                map.removeLayer(distritosGis);
            }

            RegionesG();

            //regionesGis.addTo(map);
        }
    });

    $("body").on("change", "#mercado-id_provincia", function(e) {
        e.preventDefault();
        drawnItems.clearLayers();
        var idRegion = $("#mercado-id_region").val();
        $("#mercado-id_ubigeo").html("<option value>Seleccionar</option>");
        if ($(this).val()) {
            idProvincia = $(this).val();

            if(boundsRegion){
                if(boundsRegion.isValid()){
                    map.fitBounds(boundsRegion);
                }
                
            }else{
                if(boundsNacional.isValid()){
                    map.fitBounds(boundsNacional);
                }
                //map.fitBounds(boundsNacional);
            }

            if (regionGis != undefined) {
                map.removeLayer(regionGis);
            }

            if (provinciasGis != undefined) {
                map.removeLayer(provinciasGis);
            }

            if (distritosGis != undefined) {
                map.removeLayer(distritosGis);
            }

            provinciasGis = new L.esri.featureLayer({
                url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
                where: "idprov='" + idProvincia + "'",
                style: function(feature) {
                    return {
                        color: 'white',
                        weight: 2
                    };
                },
            }).addTo(map);

            provinciasGis.once('load', async function(evt) {
                // create a new empty Leaflet bounds object
                boundsProvincia = L.latLngBounds([]);
                // loop through the features returned by the server
                provinciasGis.eachFeature(async function(layer) {
                    // get the bounds of an individual feature
                    var layerBounds = layer.getBounds();
                    // extend the bounds of the collection to fit the bounds of the new feature
                    await boundsProvincia.extend(layerBounds);
                });

                // once we've looped through all the features, zoom the map to the extent of the collection
                if(boundsProvincia.isValid()){
                    await map.fitBounds(boundsProvincia);
                }
                
            });

            distritosGis = new L.esri.featureLayer({
                url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
                where: "idprov='" + idProvincia + "'",
                style: function(feature) {
                    return {
                        color: 'white',
                        weight: 2
                    };
                },
            }).addTo(map);


            Distritos(idProvincia, "", "mercado-id_ubigeo");
        } else {
            if(boundsRegion){
                if(boundsRegion.isValid()){
                    map.fitBounds(boundsRegion);
                }
            }else{
                if(boundsNacional.isValid()){
                    map.fitBounds(boundsNacional);
                }
            }

            if (regionGis != undefined) {
                map.removeLayer(regionGis);
            }

            if (provinciasGis != undefined) {
                map.removeLayer(provinciasGis);
            }

            if (distritosGis != undefined) {
                map.removeLayer(distritosGis);
            }

            /* quitando tabla de agricultores */
            //map.removeControl(lista_agricultores)

            regionGis = new L.esri.featureLayer({
                url: 'https://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
                where: "iddpto='" + idRegion + "'",
                style: function(feature) {
                    return {
                        color: 'white',
                        weight: 2
                    };
                },
            });
            regionGis.addTo(map);

            regionGis.once('load',async function(evt) {
                // create a new empty Leaflet bounds object
                boundsRegion = L.latLngBounds([]);
                // loop through the features returned by the server
                regionGis.eachFeature(async function(layer) {
                    // get the bounds of an individual feature
                    var layerBounds = layer.getBounds();
                    // extend the bounds of the collection to fit the bounds of the new feature
                    await boundsRegion.extend(layerBounds);
                });
                // once we've looped through all the features, zoom the map to the extent of the collection
                await map.fitBounds(boundsRegion);
            });

            provinciasGis = new L.esri.featureLayer({
                url: 'https://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
                where: "iddpto='" + idRegion + "'",
                style: function(feature) {
                    return {
                        color: 'white',
                        weight: 2
                    };
                },
            });
            provinciasGis.addTo(map);

        }
    });

    $("body").on("change", "#mercado-id_ubigeo", function(e) {
        e.preventDefault();
        drawnItems.clearLayers();
        var idProvincia = $("#mercado-id_provincia").val();
        if ($(this).val()) {
            idUbigeo = $(this).val();
            console.log(boundsProvincia);
            if(boundsProvincia!= undefined){
                map.fitBounds(boundsProvincia);
            }else{
                if(boundsNacional.isValid()){
                    map.fitBounds(boundsNacional);
                }
            } 
            
            if(provinciasGis != undefined){
                map.removeLayer(provinciasGis);
            }
            
            if(distritosGis != undefined){
                map.removeLayer(distritosGis);
            }

            /* 
            map.fitBounds(boundsProvincia);

            map.removeLayer(provinciasGis);
            map.removeLayer(distritosGis); */
            

            distritosGis = new L.esri.featureLayer({
                url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
                where: "iddist='" + idUbigeo + "'",
                style: function(feature) {
                    return {
                        color: 'white',
                        weight: 2
                    };
                },
            });

            distritosGis.addTo(map);

            distritosGis.once('load', async function(evt) {
                // create a new empty Leaflet bounds object
                var bounds = L.latLngBounds([]);
                // loop through the features returned by the server
                distritosGis.eachFeature(async function(layer) {
                    // get the bounds of an individual feature
                    //console.log(layer);
                    if(layer.feature.geometry.coordinates.length>=1){
                        distritoArray = layer.feature.geometry.coordinates;
                    }
                    
                    var layerBounds = layer.getBounds();
                    // extend the bounds of the collection to fit the bounds of the new feature
                    //if(bounds.isValid()){
                        await bounds.extend(layerBounds);
                    //}
                    
                });

                // once we've looped through all the features, zoom the map to the extent of the collection
                if(bounds.isValid()){
                    await map.fitBounds(bounds);
                }
            });

            
            
        } else {
            if(boundsProvincia){
                map.fitBounds(boundsProvincia);
            }else{
                if(boundsNacional.isValid()){
                    map.fitBounds(boundsNacional);
                }
            } 

            //map.removeLayer(region);
            if (provinciasGis != undefined) {
                map.removeLayer(provinciasGis);
            }


            if (distritosGis != undefined) {
                map.removeLayer(distritosGis);
                //iddist='';
            }


            provinciasGis = new L.esri.featureLayer({
                url: 'https://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
                where: "idprov='" + idProvincia + "'",
                style: function(feature) {
                    return {
                        color: 'white',
                        weight: 2
                    };
                },
            });
            provinciasGis.addTo(map);

            provinciasGis.once('load', async function(evt) {
                // create a new empty Leaflet bounds object
                boundsProvincia = L.latLngBounds([]);
                // loop through the features returned by the server
                provinciasGis.eachFeature(async function(layer) {
                    // get the bounds of an individual feature
                    var layerBounds = layer.getBounds();
                    // extend the bounds of the collection to fit the bounds of the new feature
                    await boundsProvincia.extend(layerBounds);
                });

                // once we've looped through all the features, zoom the map to the extent of the collection
                if(boundsProvincia.isValid()){
                    await map.fitBounds(boundsProvincia);
                }
            });

            distritosGis = new L.esri.featureLayer({
                url: 'https://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
                where: "idprov='" + idProvincia + "'",
                style: function(feature) {
                    return {
                        color: 'white',
                        weight: 2
                    };
                },
            });
            distritosGis.addTo(map);
        }
    });

    /* Carga Maestras */

    async function MercadosTipo(idTipoMercado, idCampoTipoMercado) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado-tipo/get-lista-mercados-tipo",
            method: "POST",
            data: {
                _csrf: csrf
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsTipoMercado = "<option value>Seleccionar</option>";

                    $.each(results.mercadosTipo, function(index, value) {
                        optionsTipoMercado = optionsTipoMercado + `<option value='${value.ID_MERCADO_TIPO}'>${value.TXT_MERCADO_TIPO}</option>`;
                    });

                    $("#" + idCampoTipoMercado).html(optionsTipoMercado);

                    if (idTipoMercado) {
                        $("#" + idCampoTipoMercado).val(idTipoMercado);
                    }
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    var map;
    var boundsNacional = L.latLngBounds([L.latLng(0.6591651462894632, -58.22753906250001),L.latLng(-18.77111506233701, -91.8017578125)]);
    var boundsRegion;
    var boundsProvincia;

    var regionesGis;
    var provinciasGis;
    var distritosGis;
    var regionGis;
    var drawnItems = new L.FeatureGroup();

    function Mapa() {
        loading.show();
        if(map){
            //map.gestureHandling.disable();
            console.log("aaaaa");
            map.remove();
            map=null;
            $('#map').html('');
            $("#map").removeClass();
            $('#map').addClass('map sidebar-map');
            $("#map").removeAttr("style");
            $("#map").css({"height":"450px"});
            $("#map").removeAttr("tabindex");
        }
        map = L.map('map', {
            zoomControl: false,
            scrollWheelZoom: true,
            fullscreenControl: false,
            attributionControl: false,
            dragging: true,
            layers: [],
            minZoom: 3,
            maxNativeZoom: 17,
            maxZoom: 17,
            zoom: 5,
            center: [-9.1899672, -75.015152],
        });

        /* Mapa Base */

        L.esri.basemapLayer('Imagery').addTo(map);

        setTimeout(() => {
            map.invalidateSize(true)
        }, 1000);

        // Controles de dibujo

        map.addLayer(drawnItems);
        var options = {
            position: 'topleft',
            draw: {
                marker: true,
                polyline: false,
                polygon: false,
                circle: false, // Turns off this drawing tool
                rectangle: false,
            },
            edit: {
                featureGroup: drawnItems, //REQUIRED!!
                remove: true
            }
        };

        var lat_lng;
        var drawControl = new L.Control.Draw(options);
        map.addControl(drawControl);

        map.on('draw:created', function(e) {
            var type = e.layerType,
                layer = e.layer;
            layer.options.showMeasurements = true;
            if (type === 'marker') {
                // Do marker specific actions

                //console.log(layer.getLatLng());
                //alert(layer.getLatLng());
                drawnItems.clearLayers();
                //lat_lng = layer.getLatLng();

                var point = turf.point([layer.getLatLng().lng, layer.getLatLng().lat]);

                var banderaPoint=0;

                if(distritoArray.length>1){

                    $.each(distritoArray, function(i, item) {
                        distritoPoligono = turf.multiPolygon([item]);
                        if (!turf.booleanPointInPolygon(point, distritoPoligono)) {
                            banderaPoint++;
                        }
                    });
                    if(banderaPoint==distritoArray.length){
                        alert("Su marcado no se encuentra dentro de la zona del distrito");
                        return false;
                    }else{
                        $('#mercado-txt_latitud').val(layer.getLatLng().lat);
                        $('#mercado-txt_longitud').val(layer.getLatLng().lng);
                        drawnItems.addLayer(layer);
                    }
                }else if(distritoArray.length==1){

                    distritoPoligono = turf.multiPolygon([distritoArray]);

                    if (!turf.booleanPointInPolygon(point, distritoPoligono)) {
                        alert("Su marcado no se encuentra dentro de la zona del distrito");
                        return false;
                    }

                    if(distritoPoligono){
                        $('#mercado-txt_latitud').val(layer.getLatLng().lat);
                        $('#mercado-txt_longitud').val(layer.getLatLng().lng);
                        drawnItems.addLayer(layer);
                    }
                }
            } 
            
        });
    }

    function RegionesG() {
        regionesGis = new L.esri.featureLayer({
            url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
            minZoom: 5,
            style: function(feature) {
                return {
                    color: 'white',
                    weight: 2
                };
            },
        });

        regionesGis.addTo(map);

        /* Cargando vista*/
        regionesGis.on("load", async function(evt) {

            boundsNacional = L.latLngBounds([]);
            // loop through the features returned by the server
            regionesGis.eachFeature(async function(layer) {
                // get the bounds of an individual feature
                var layerBounds = layer.getBounds();
                // extend the bounds of the collection to fit the bounds of the new feature
                await boundsNacional.extend(layerBounds);
            });
            setTimeout(function() {
                loading.hide();
            }, 1000);
            // once we've looped through all the features, zoom the map to the extent of the collection
            if(boundsNacional.isValid()){
                map.fitBounds(boundsNacional);
            }
            //map.fitBounds(boundsRegiones);

        });
    }
    var latlng;
    function DistritosG(idUbigeo, txtLatitud, txtLongitud) {
        drawnItems.clearLayers();
        boundsRegion=null;
        boundsProvincia=null;

        distritosGis = new L.esri.featureLayer({
            url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
            where: "iddist='" + idUbigeo + "'",
            style: function(feature) {
                return {
                    color: 'white',
                    weight: 2
                };
            },
        });
        distritosGis.addTo(map);
        var boundsBandera = 1;

        distritosGis.once('load', async function(evt) {
            // create a new empty Leaflet bounds object
            var bounds = L.latLngBounds([]);
            // loop through the features returned by the server
            var layerBoundsBandera = 1;
            distritosGis.eachFeature(async function(layer) {
                // get the bounds of an individual feature
                //console.log(layer);

                if(layer.feature.geometry.coordinates.length>=1){
                    distritoArray = layer.feature.geometry.coordinates;
                }
                var layerBounds = layer.getBounds();
                if(layerBounds){
                    if(layerBoundsBandera==1){
                        await bounds.extend(layerBounds);
                    }
                    layerBoundsBandera++;
                }
                
                //distritoPoligono = turf.multiPolygon([layer.feature.geometry.coordinates]);
                
            });

            // once we've looped through all the features, zoom the map to the extent of the collection111
            if(bounds.isValid()){
                await map.fitBounds(bounds);
            }
        });
        
        console.log(txtLatitud,txtLongitud,IsvalidLatLong(txtLatitud,txtLongitud));
        if(IsvalidLatLong(txtLatitud,txtLongitud)){
            latlng = L.latLng(txtLatitud, txtLongitud);
            pointGeo = L.marker(latlng);
            drawnItems.addLayer(pointGeo);
        }
        

    }

    async function Regiones(idRegion, idCampoDepartamento) {

        var optionsDepartamentos = "<option value>Seleccionar</option>";
        $.each(JSON.parse(localStorage.getItem('region')), function(index, value) {
            optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
        });

        $("#" + idCampoDepartamento).html(optionsDepartamentos);

        if (idRegion) {
            $("#" + idCampoDepartamento).val(idRegion);
        }

        /*
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones",
            method: "POST",
            data: {
                _csrf: csrf
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsDepartamentos = "<option value>Seleccionar</option>";

                    $.each(results.regiones, function(index, value) {
                        optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                    });

                    $("#" + idCampoDepartamento).html(optionsDepartamentos);

                    if (idRegion) {
                        $("#" + idCampoDepartamento).val(idRegion);
                    }
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });*/
    }

    function Provincias(idRegion, idProvincia, idCampoProvincia) {


        var provinciaLista = $.grep(JSON.parse(localStorage.getItem('ubigeo')).regiones, function(v) {
            return v.ID_DEPARTAMENTO === idRegion;
        });

        var provincia = [];
        $.each(provinciaLista, function(index, event) {

            var events = $.grep(provincia, function(e) {
                return event.TXT_PROVINCIA === e.TXT_PROVINCIA;
            });
            if (events.length === 0) {
                provincia.push(event);
            }
        });

        var optionsProvincias = "<option value>Seleccionar</option>";
        $.each(provincia.sort((a, b) => (a.TXT_PROVINCIA > b.TXT_PROVINCIA) ? 1 : -1), function(index, value) {
            optionsProvincias = optionsProvincias + `<option value='${value.ID_PROVINCIA}'>${value.TXT_PROVINCIA}</option>`;
        });
        $("#" + idCampoProvincia).html(optionsProvincias);


        if (idProvincia) {
            $("#" + idCampoProvincia).val(idProvincia);
        }

        setTimeout(function() {
            loading.hide();
        }, 1000);


        /*$.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-provincias",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsProvincias = "<option value>Seleccionar</option>";
                    $.each(results.provincias, function(index, value) {
                        optionsProvincias = optionsProvincias + `<option value='${value.ID_PROVINCIA}'>${value.TXT_PROVINCIA}</option>`;
                    });
                    $("#" + idCampoProvincia).html(optionsProvincias);

                    if (idProvincia) {
                        $("#" + idCampoProvincia).val(idProvincia);
                    }

                    setTimeout(function() {
                        loading.hide();
                    }, 1000);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });*/
    }

    function Distritos(idProvincia, idDistrito, idCampoDistrito) {


        var distritoLista = $.grep(JSON.parse(localStorage.getItem('ubigeo')).regiones, function(v) {
            return v.ID_PROVINCIA === idProvincia;
        });

        var distrito = [];
        $.each(distritoLista, function(index, event) {

            var events = $.grep(distrito, function(e) {
                return event.TXT_DISTRITO === e.TXT_DISTRITO;
            });
            if (events.length === 0) {
                distrito.push(event);
            }
        });

        var optionsDistritos = "<option value>Seleccionar</option>";
        $.each(distrito.sort((a, b) => (a.TXT_DISTRITO > b.TXT_DISTRITO) ? 1 : -1), function(index, value) {
            optionsDistritos = optionsDistritos + `<option value='${value.ID_UBIGEO}'>${value.TXT_DISTRITO}</option>`;
        });
        $("#" + idCampoDistrito).html(optionsDistritos);

        if (idDistrito) {
            $("#" + idCampoDistrito).val(idDistrito);
        }

        setTimeout(function() {
            loading.hide();
        }, 1000);

        /*$.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-distritos",
            method: "POST",
            data: {
                _csrf: csrf,
                idProvincia: idProvincia
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsDistritos = "<option value>Seleccionar</option>";
                    $.each(results.distritos, function(index, value) {
                        optionsDistritos = optionsDistritos + `<option value='${value.ID_UBIGEO}'>${value.TXT_DISTRITO}</option>`;
                    });
                    $("#" + idCampoDistrito).html(optionsDistritos);

                    if (idDistrito) {
                        $("#" + idCampoDistrito).val(idDistrito);
                    }

                    setTimeout(function() {
                        loading.hide();
                    }, 1000);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });*/
    }


    function IsvalidLatLong(lat,long)
    {
        const regex = new RegExp('^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}'); 
        return regex.test(lat) && regex.test(long);
    }
</script>