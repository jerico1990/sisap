<!-- hero section start -->
<section class="section " id="home">
        
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for>Región</label>
                    <select class="form-control" id="id_region">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for>Tipo de mercado</label>
                    <select class="form-control" id="id_tipo_mercado">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for>Mercado</label>
                    <select class="form-control" id="id_mercado">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for>Temporalidad</label>
                    <select class="form-control" id="temporalidad">
                        <option value>Seleccionar</option>
                        <option value="1">Diario</option>
                        <option value="2">Mensual</option>
                        <option value="3">Anual</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 diario" style="display:none">
                <div class="form-group">
                    <label for>Día</label>
                    <input type="date" class="form-control" id="diario">
                </div>
            </div>
            <div class="col-md-4 mensual" style="display:none">
                <div class="form-group">
                    <label for>Mes</label>
                    <input type="month" class="form-control" id="mensual">
                </div>
            </div>
            <div class="col-md-4 anual" style="display:none">
                <div class="form-group">
                    <label for>Anual</label>
                    <select id="anual" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    
                    <button class="btn btn-success btn-consultar">Consultar</button>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="lista-productos" class="table table-bordered dt-responsive ">
                            <thead>
                                <th>Año</th>
                                <th>Región</th>
                                <!-- <th>Tipo de Mercado</th>
                                <th>Mercado</th> -->
                                <th>Variedad</th>
                                <th>Envase</th>
                                <th>Precio Promedio</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div>
    <!-- end container -->
</section>
<!-- hero section end -->

<script>
var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
var loading = $('.staticBackdrop');
var idRegion;
var idMercadoTipo;
var temporalidad;
var idMercado;
var diario;
var mensual;
var anual;

$('body').on('change', '#temporalidad', function (e) {
    e.preventDefault();
    temporalidad = $(this).val();
    $('.diario').hide();
    $('.mensual').hide();
    $('.anual').hide();

    $('#diario').val('');
    $('#mensual').val('');
    $('#anual').val('');

    if(temporalidad=="1"){
        $('.diario').show();
    }else if(temporalidad=="2"){
        $('.mensual').show();
    }else if(temporalidad=="3"){
        $('.anual').show();
    }
});


function ListaPrecios(){
    
    $('#lista-productos').DataTable().destroy();
    $('#lista-productos tbody').html("");
    $('#lista-productos').DataTable({
                            "dom": "Bfrtip",
                            buttons: [
                                'excelHtml5',
                                'csvHtml5',
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });
}


async function Precios(){
        
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/publico/get-lista-precios',
                method: 'POST',
                data:{_csrf:csrf,diario:diario,mensual:mensual,anual:anual,temporalidad:temporalidad,idRegion:idRegion,idMercadoTipo:idMercadoTipo,idMercado:idMercado},
                dataType:'Json',
                beforeSend:function(){
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var precios ="";
                        $('#lista-productos').DataTable().destroy();

                        $.each(results.precios, function( index, value ) {
                            txtUnidadMedida= ((value.TXT_UNIDAD_MEDIDA_ENVASE)?value.TXT_UNIDAD_MEDIDA_ENVASE:"");
                            numEquivalencia= ((value.NUM_EQUIVALENCIA)?value.NUM_EQUIVALENCIA:"");
                            fecha="";
                            if(temporalidad == "1"){
                                fecha = value.TXT_ANIO + "-" + value.TXT_MES + "-" + value.TXT_DIA;
                            }else if(temporalidad == "2"){
                                fecha = value.TXT_ANIO + "-" + value.TXT_MES;
                            }else if(temporalidad == "3"){
                                fecha = value.TXT_ANIO;
                            }


                            precios = precios + "<tr>";
                                precios = precios + "<td> " + ((fecha)?fecha:"") + "</td>";
                                precios = precios + "<td> " + ((value.TXT_DEPARTAMENTO)?value.TXT_DEPARTAMENTO:"") + "</td>";
                                precios = precios + "<td> " + ((value.TXT_PRODUCTO)?value.TXT_CODIGO_PRODUCTO + "-" +value.TXT_PRODUCTO:"") + "</td>";
                                precios = precios + "<td> " + ((value.TXT_ENVASE)?value.TXT_ENVASE + "-" + numEquivalencia + "-" + txtUnidadMedida:"") + "</td>";
                                precios = precios + "<td> " + ((value.NUM_PRECIO)?parseFloat(value.NUM_PRECIO).toFixed(2):"") + "</td>";
                            precios = precios + "</tr>";
                        });
                        
                        $('#lista-productos tbody').html(precios);
                        $('#lista-productos').DataTable({
                            "dom": "Bfrtip",
                            buttons: [
                                'excelHtml5',
                                'csvHtml5',
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.hide(); }, 500);
                    }
                },
                error:function(){
                    toastr.error('Error al realizar el proceso.');
                }
            });

}


async function Regiones() {

    await   $.ajax({
                    url: "<?= \Yii::$app->request->BaseUrl ?>/publico/get-regiones",
                    method: "POST",
                    data:{_csrf:csrf},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            var optionsDepartamentos = "<option value>Seleccionar</option>";

                            $.each(results.regiones, function( index, value ) {
                                optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                            });

                            $("#id_region").html(optionsDepartamentos);

                        }
                    },
                    error:function(){
                        alert("Error al realizar el proceso.");
                    }
                });
            
}

async function MercadosTipo() {
    await $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/publico/get-lista-mercados-tipo",
        method: "POST",
        data: {
            _csrf: csrf
        },
        dataType: "Json",
        beforeSend: function(xhr, settings) {
            //loading.show();
        },
        success: function(results) {
            if (results && results.success) {
                var optionsTipoMercado = "<option value>Seleccionar</option>";

                $.each(results.mercadosTipo, function(index, value) {
                    optionsTipoMercado = optionsTipoMercado + `<option value='${value.ID_MERCADO_TIPO}'>${value.TXT_MERCADO_TIPO}</option>`;
                });

                $("#id_tipo_mercado").html(optionsTipoMercado);
            }
        },
        error: function() {
            alert("Error al realizar el proceso.");
        }
    });
}

async function Mercados(idRegion,idMercadoTipo) {
    await $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/publico/get-lista-opciones-mercados",
        method: "POST",
        data: {
            _csrf: csrf,
            idRegion: idRegion,
            idMercadoTipo:idMercadoTipo
        },
        dataType: "Json",
        beforeSend: function(xhr, settings) {
            //loading.show();
        },
        success: function(results) {
            if (results && results.success) {
                var optionsMercados = "<option value>Seleccionar</option>";

                $.each(results.mercados, function(index, value) {
                    optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                });

                $("#id_mercado").html(optionsMercados);

            }
        },
        error: function() {
            toastr.error('Error al realizar el proceso.');
        }
    });
}

$('body').on('change', '#id_region', function(e) {
    e.preventDefault();
    idRegion = $(this).val();

    if(idRegion && idMercadoTipo){
        Mercados(idRegion,idMercadoTipo);
    }

});

$('body').on('change', '#id_tipo_mercado', function(e) {
    e.preventDefault();
    idMercadoTipo = $(this).val();
    if(idRegion && idMercadoTipo){
        Mercados(idRegion,idMercadoTipo);
    }
});


$('body').on('click', '.btn-consultar', function(e) {
    e.preventDefault();
    diario = $("#diario").val();
    mensual = $("#mensual").val();
    anual = $("#anual").val();
    
    if(diario || mensual || anual){
        Precios();
    }else{
        alert("Debe seleccionar una temporalidad y llenar la información")
    }
});


function init() {
    ListaPrecios();
    Regiones();
    MercadosTipo();
}

init();

</script>