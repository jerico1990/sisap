
<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="<?= \Yii::$app->request->BaseUrl ?>/bootstrap-multiselect-master/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/bootstrap-multiselect-master/css/bootstrap-multiselect.css" type="text/css"/>
<style>
.multiselect-native-select .btn-group,.multiselect-native-select .btn-group .multiselect {
    width: 100% !important;
}

.dropdown-menu.show{
    width:100%;
}
</style>

<div class="container-fluid">
    <div class="row">
        <!-- <div class="col-md-2" >
            <img src="img/sisap_icono.png" alt="" style="width:100%">
        </div> -->
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for>Región</label>
                                <select class="form-control" id="id_region">
                                    <option value>Seleccionar</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for>Tipo de mercado</label>
                                <select class="form-control" id="id_tipo_mercado">
                                    <option value>Seleccionar</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for>Mercado</label>
                                <select class="form-control" id="id_mercado">
                                    <option value>Seleccionar</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for>Temporalidad</label>
                                <select class="form-control" id="temporalidad">
                                    <option value>Seleccionar</option>
                                    <option value="1">Diario</option>
                                    <option value="2">Mensual</option>
                                    <option value="3">Anual</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 diario" style="display:none">
                            <div class="form-group">
                                <label for>Día</label>
                                <input type="date" class="form-control" id="diario">
                            </div>
                        </div>
                        <div class="col-md-4 meses" style="display:none">
                            <div class="form-group">
                                <label for>Meses</label>
                                <select multiple="multiple" class="form-control" id="meses" >
                                    <option value="01">Enero</option>
                                    <option value="02">Febrero</option>
                                    <option value="03">Marzo</option>
                                    <option value="04">Abril</option>
                                    <option value="05">Mayo</option>
                                    <option value="06">Junio</option>
                                    <option value="07">Julio</option>
                                    <option value="08">Agosto</option>
                                    <option value="09">Septiembre</option>
                                    <option value="10">Octubre</option>
                                    <option value="11">Noviembre</option>
                                    <option value="12">Diciembre</option>
                                </select>
                            </div>
                            
                        </div>
                        <div class="col-md-4 anios" style="display:none">
                            <div class="form-group">
                                <label for="">Años</label>
                                <select id="anios" class="form-control" multiple="multiple" >
                                    <?php for($i=1997;$i<=date("Y");$i++){ ?>
                                        <option value="<?= $i ?>"><?= $i ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for>Producto/Variedad</label>
                                <select id="variedades" multiple="multiple" class="form-control">
                                    <option value>Seleccionar</option>
                                </select>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        
                        <button class="btn btn-success btn-consultar">Consultar</button>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="lista-productos" class="table table-bordered dt-responsive ">
                                <thead>
                                    <th>Fecha</th>
                                    <th>Región</th>
                                    <!-- <th>Tipo de Mercado</th>-->
                                    <th>Mercado</th> 
                                    <th>Variedad</th>
                                    <th>Precio Promedio (S/. x Kg., S/. x Lt. o S/. x Unid.)</th>
                                    <th>Precio mínimo</th>
                                    <th>Precio máximo</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
    </div>
</div>
<!-- end container -->

<div id="modalInformativo" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">
                    <!-- Informativo -->
                    <!-- SISTEMA DE ABASTECIMIENTO Y PRECIOS - SISAP -->
                    Bienvenidos al Módulo de Mercados
                </h5>
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <h2 class="text-left"></h2>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-2 text-center">
                        <img src="img/sisap_icono.png" alt="" style="width:100%">
                    </div>
                    <div class="col-md-10 text-justify">
                        <p>
                        El SISAP (Sistema de Información de Abastecimiento y Precios) es un innovador servicio de consultas que el Ministerio de Desarrollo Agrario y Riego pone a disposición para que acceda en tiempo real a información referida a volúmenes, precios y procedencias de los principales productos agropecuarios y agroindustriales.
                        </p>
                        <p>
                        Gracias a este sistema usted podrá obtener información diaria, semanal, mensual y anual de diversos productos.
                        </p>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- <div class="modal fade" id="modalInformativo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">SISTEMA DE ABASTECIMIENTO Y PRECIOS - SISAP</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <h2>Bienvenidos al Módulo de Mercados Mayoristas</h2>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
</div> -->

<script>
$("#modalInformativo").modal('show');

var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
var loading = $('.staticBackdrop');
var idRegion;
var idMercadoTipo;
var temporalidad;
var idMercado;
var variedades;
var diario;
var meses;
var anios;

$('body').on('change', '#temporalidad', function (e) {
    e.preventDefault();
    temporalidad = $(this).val();
    $('.diario').hide();
    $('.meses').hide();
    $('.anios').hide();

    $('#diario').val('');
    $('#meses').val('');
    $('#anios').val('');

    if(temporalidad=="1"){
        $('.diario').show();
    }else if(temporalidad=="2"){
        $('.meses').show();
        $('.anios').show();
    }else if(temporalidad=="3"){
        $('.anios').show();
    }
});


function ListaPrecios(){
    
    $('#lista-productos').DataTable().destroy();
    $('#lista-productos tbody').html("");
    $('#lista-productos').DataTable({
                            "dom": "Bfrtip",
                            buttons: [
                                {extend:'excelHtml5',className: 'btn-success',},
                                {extend:'csvHtml5',className: 'btn-success',},
                                {text: 'Consultar',className: 'btn-info btn-consultar',}
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });
}


async function Precios(){
        
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/publico/get-lista-precios',
                method: 'POST',
                data:{_csrf:csrf,diario:diario,meses:meses,anios:anios,temporalidad:temporalidad,idRegion:idRegion,idMercadoTipo:idMercadoTipo,idMercado:idMercado,variedades:variedades},
                dataType:'Json',
                beforeSend:function(){
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var precios ="";
                        $('#lista-productos').DataTable().destroy();

                        $.each(results.precios, function( index, value ) {
                            fecha="";
                            if(temporalidad == "1"){
                                fecha = value.TXT_ANIO + "-" + value.TXT_MES + "-" + value.TXT_DIA;
                            }else if(temporalidad == "2"){
                                fecha = value.TXT_ANIO + "-" + value.TXT_MES;
                            }else if(temporalidad == "3"){
                                fecha = value.TXT_ANIO;
                            }


                            precios = precios + "<tr>";
                                precios = precios + "<td> " + ((fecha)?fecha:"") + "</td>";
                                precios = precios + "<td> " + ((value.TXT_DEPARTAMENTO)?value.TXT_DEPARTAMENTO:"") + "</td>";
                                precios = precios + "<td> " + ((value.TXT_MERCADO)?value.TXT_MERCADO :"") + "</td>";
                                precios = precios + "<td> " + ((value.TXT_PRODUCTO)?value.TXT_CODIGO_PRODUCTO + "-" +value.TXT_PRODUCTO:"") + "</td>";
                                precios = precios + "<td> " + ((value.NUM_PRECIO_PROMEDIO)?parseFloat(value.NUM_PRECIO_PROMEDIO).toFixed(2):"") + "</td>";
                                precios = precios + "<td> " + ((value.NUM_PRECIO_MINIMO)?parseFloat(value.NUM_PRECIO_MINIMO).toFixed(2):"") + "</td>";
                                precios = precios + "<td> " + ((value.NUM_PRECIO_MAXIMO)?parseFloat(value.NUM_PRECIO_MAXIMO).toFixed(2):"") + "</td>";
                                
                            precios = precios + "</tr>";
                        });
                        
                        $('#lista-productos tbody').html(precios);
                        $('#lista-productos').DataTable({
                            "dom": "Bfrtip",
                            buttons: [
                                {extend:'excelHtml5',className: 'btn-success',},
                                {extend:'csvHtml5',className: 'btn-success',},
                                {text: 'Consultar',className: 'btn-info btn-consultar',}
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.hide(); }, 500);
                    }
                },
                error:function(){
                    toastr.error('Error al realizar el proceso.');
                }
            });

}


async function Regiones() {

    await   $.ajax({
                    url: "<?= \Yii::$app->request->BaseUrl ?>/publico/get-regiones",
                    method: "POST",
                    data:{_csrf:csrf},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            var optionsDepartamentos = "<option value>Seleccionar</option>";

                            $.each(results.regiones, function( index, value ) {
                                optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                            });

                            $("#id_region").html(optionsDepartamentos);

                        }
                    },
                    error:function(){
                        alert("Error al realizar el proceso.");
                    }
                });
            
}

async function MercadosTipo() {
    await $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/publico/get-lista-mercados-tipo",
        method: "POST",
        data: {
            _csrf: csrf
        },
        dataType: "Json",
        beforeSend: function(xhr, settings) {
            //loading.show();
        },
        success: function(results) {
            if (results && results.success) {
                var optionsTipoMercado = "<option value>Seleccionar</option>";

                $.each(results.mercadosTipo, function(index, value) {
                    optionsTipoMercado = optionsTipoMercado + `<option value='${value.ID_MERCADO_TIPO}'>${value.TXT_MERCADO_TIPO}</option>`;
                });

                $("#id_tipo_mercado").html(optionsTipoMercado);
            }
        },
        error: function() {
            alert("Error al realizar el proceso.");
        }
    });
}

async function Mercados(idRegion,idMercadoTipo) {
    await $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/publico/get-lista-opciones-mercados",
        method: "POST",
        data: {
            _csrf: csrf,
            idRegion: idRegion,
            idMercadoTipo:idMercadoTipo
        },
        dataType: "Json",
        beforeSend: function(xhr, settings) {
            //loading.show();
        },
        success: function(results) {
            if (results && results.success) {
                var optionsMercados = "<option value>Seleccionar</option>";

                $.each(results.mercados, function(index, value) {
                    optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                });

                $("#id_mercado").html(optionsMercados);

            }
        },
        error: function() {
            toastr.error('Error al realizar el proceso.');
        }
    });
}

async function Variedades(idMercado) {

    await $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/publico/get-lista-opciones-generos-variedades",
        method: "POST",
        data: {
            _csrf: csrf,
            idMercado: idMercado
        },
        dataType: "Json",
        beforeSend: function(xhr, settings) {
            loading.show();
        },
        success: function(results) {
            if (results && results.success) {
                $('#variedades').multiselect('destroy');
                var optionsGeneros = "";
                $.each(results.generos, function(index, value) {
                    optionsGeneros = optionsGeneros + `<optgroup label="${value.txtProductoGenero}">`;
                        $.each(value.variedades, function(index2, value2) {
                            optionsGeneros = optionsGeneros + `<option value="${value2.idProducto}">${value2.txtProducto}</option>`;
                        });
                    optionsGeneros = optionsGeneros + `</optgroup>`;
                });

                $("#variedades").html(optionsGeneros);

                
                $('#variedades').multiselect({
                    enableClickableOptGroups: true,
                    enableCollapsibleOptGroups: true,
                    enableFiltering: true,
                    includeSelectAllOption: false,
                    filterPlaceholder: 'Búsqueda',
                    buttonClass : "col-md-12 form-control",
                    maxHeight: 200,
                    onInitialized: function(select, container) {
                        
                    },
                    buttonText: function(options) {
                        if (options.length === 0) {
                            return 'Ningún seleccionado';
                        }
                        else if (options.length > 3) {
                            return options.length + ' selected';
                        }
                        else {
                            var selected = [];
                            options.each(function() {
                                selected.push([$(this).text(), $(this).data('order')]);
                            });

                            selected.sort(function(a, b) {
                                return a[1] - b[1];
                            });

                            var text = '';
                            for (var i = 0; i < selected.length; i++) {
                                text += selected[i][0] + ', ';
                            }

                            return text.substr(0, text.length -2);
                        }
                    }
                });
                setTimeout(function() {
                        loading.hide();
                    }, 1000);
            }
        },
        error: function() {
            toastr.error('Error al realizar el proceso.');
        }
    });
}

$('body').on('change', '#id_region', function(e) {
    e.preventDefault();
    idRegion = $(this).val();

    if(idRegion && idMercadoTipo){
        Mercados(idRegion,idMercadoTipo);
    }

});

$('body').on('change', '#id_tipo_mercado', function(e) {
    e.preventDefault();
    idMercadoTipo = $(this).val();
    if(idRegion && idMercadoTipo){
        Mercados(idRegion,idMercadoTipo);
    }
});

$('body').on('change', '#id_mercado', function(e) {
    e.preventDefault();
    idMercado = $(this).val();
    if(idMercado){
        Variedades(idMercado);
    }
});

$('body').on('change', '#variedades', function(e) {
    e.preventDefault();
    variedades = $(this).val();
});


$('body').on('click', '.btn-consultar', function(e) {
    e.preventDefault();
    diario = $("#diario").val();
    meses = $("#meses").val();
    anios = $("#anios").val();
    
    if(diario!='' || meses!='' || anios!=''){
        Precios();
    }else{
        alert("Debe seleccionar una temporalidad y llenar la información")
    }
});


$('#variedades').multiselect({
    enableClickableOptGroups: true,
    enableCollapsibleOptGroups: true,
    enableFiltering: true,
    includeSelectAllOption: false,
    filterPlaceholder: 'Búsqueda',
    buttonClass : "col-md-12 form-control",
    maxHeight: 200,
    buttonText: function(options) {
        if (options.length === 0) {
            return 'Ningún seleccionado';
        }
        else if (options.length > 3) {
            return options.length + ' selected';
        }
        else {
            var selected = [];
            options.each(function() {
                selected.push([$(this).text(), $(this).data('order')]);
            });

            selected.sort(function(a, b) {
                return a[1] - b[1];
            });

            var text = '';
            for (var i = 0; i < selected.length; i++) {
                text += selected[i][0] + ', ';
            }

            return text.substr(0, text.length -2);
        }
    }
});

$('#meses').multiselect({
    enableClickableOptGroups: true,
    enableCollapsibleOptGroups: true,
    enableFiltering: true,
    includeSelectAllOption: false,
    filterPlaceholder: 'Búsqueda',
    buttonClass : "col-md-12 form-control",
    maxHeight: 200,
    buttonText: function(options) {
        if (options.length === 0) {
            return 'Ningún seleccionado';
        }
        else if (options.length > 3) {
            return options.length + ' selected';
        }
        else {
            var selected = [];
            options.each(function() {
                selected.push([$(this).text(), $(this).data('order')]);
            });

            selected.sort(function(a, b) {
                return a[1] - b[1];
            });

            var text = '';
            for (var i = 0; i < selected.length; i++) {
                text += selected[i][0] + ', ';
            }

            return text.substr(0, text.length -2);
        }
    }
});

$('#anios').multiselect({
    enableClickableOptGroups: true,
    enableCollapsibleOptGroups: true,
    enableFiltering: true,
    includeSelectAllOption: false,
    filterPlaceholder: 'Búsqueda',
    buttonClass : "col-md-12 form-control",
    maxHeight: 200,
    buttonText: function(options) {
        if (options.length === 0) {
            return 'Ningún seleccionado';
        }
        else if (options.length > 3) {
            return options.length + ' selected';
        }
        else {
            var selected = [];
            options.each(function() {
                selected.push([$(this).text(), $(this).data('order')]);
            });

            selected.sort(function(a, b) {
                return a[1] - b[1];
            });

            var text = '';
            for (var i = 0; i < selected.length; i++) {
                text += selected[i][0] + ', ';
            }

            return text.substr(0, text.length -2);
        }
    }
});

function init() {
    ListaPrecios();
    Regiones();
    MercadosTipo();
}

init();

</script>