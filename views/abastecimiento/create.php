<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Producto x Mercado x Abastecimiento</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de productos x mercado x abastecimiento</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formProductoMercadoAbastecimiento', 'class' => 'form-horizontal']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Región</label>
                            <select id="abastecimiento-id_region" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Mercado</label>
                            <select id="abastecimiento-id_mercado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha</label>
                            <input type="date" id="abastecimiento-fec_registro" name="Abastecimiento[FEC_REGISTRO]" class="form-control" value="<?= $fechaActual ?>" min="<?= $fechaInicio ?>" max="<?= $fechaFin ?>" onkeydown="return false">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Grupo</label>
                            <select id="abastecimiento-id_producto_grupo" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Genero</label>
                            <select id="abastecimiento-id_producto_genero" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Producto con envase</label>
                            <select id="abastecimiento-id_producto_mercado" name="Abastecimiento[ID_PRODUCTO_MERCADO]" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div> -->
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <!-- <button type="button" class="btn btn-success btn-agregar-producto-abastecimiento">Agregar</button> -->
                            <button type="button" class="btn btn-danger btn-eliminar-producto-abastecimiento">Eliminar</button>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/temporal" class="btn btn-success">Subir archivo</a>
                        </div>
                    </div>
                </div>
                <table id="lista-productos-mercado-abastecimiento" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Acciones</th>
                        <th>Producto</th>
                        <th>Variedad</th>
                        <th>Cantidad</th>
                        <th>Envase</th>
                        <th>Peso</th>
                        <th>Procedencia</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');
    var listaAbastecimiento = [];
    function ListaProductosMercadoAbastecimiento() {

        $('#lista-productos-mercado-abastecimiento').DataTable().destroy();
        $('#lista-productos-mercado-abastecimiento tbody').html("");
        $('#lista-productos-mercado-abastecimiento').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "pageLength": 10,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningun dato disponible en esta lista",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });

        setTimeout(function() {
            loading.hide();
        }, 500);
    }



    async function ProductosMercadoAbastecimiento(idProductoGrupo,idMercado, fecRegistro) {

        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/get-lista-abastecimiento',
            method: 'POST',
            data: {
                _csrf: csrf,
                idProductoGrupo: idProductoGrupo,
                idMercado:idMercado,
                fecRegistro: fecRegistro
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    
                    listaAbastecimiento = results.productosMercadoAbastecimiento;

                    var productosMercadoAbastecimiento = "";
                    $('#lista-productos-mercado-abastecimiento').DataTable().destroy();

                    $.each(listaAbastecimiento, function(index, value) {
                        txtUnidadMedida= ((value.TXT_UNIDAD_MEDIDA)?value.TXT_UNIDAD_MEDIDA:"");
                        numEquivalencia= ((value.NUM_EQUIVALENCIA)?value.NUM_EQUIVALENCIA:0.00);

                        var optionsProcedencias = "<option value>Seleccionar</option>";
                        productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<tr>";
                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td class='text-center'> ";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + '<div class="form-check mb-3" ' + ((value.ID_ABASTECIMIENTO)?'':'style="display:none"') + ' ><input class="form-check-input check-abastecimiento" data-idAbastecimiento="' + value.ID_ABASTECIMIENTO + '"  type="checkbox" data-id="' + value.ID_PRODUCTO_MERCADO + '" ></div>';
                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</td>";

                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PRODUCTO_GENERO) ? value.TXT_CODIGO_GENERO + "-" + value.TXT_PRODUCTO_GENERO : "") + "</td>";
                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PRODUCTO) ? value.TXT_CODIGO_PRODUCTO + "-" + value.TXT_PRODUCTO : "") + "</td>";

                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td>";
                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<input type='text' data-idAbastecimiento='" + value.ID_ABASTECIMIENTO + "' data-numEquivalencia='" + numEquivalencia + "' data-inputmask=\"'alias': 'numeric', 'groupSeparator': ' ', 'digits': 2, 'digitsOptional': false\" class='form-control input-mask btn-modificar-num_cantidad_producto' data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' value='" + ((value.NUM_CANTIDAD_PRODUCTO) ? value.NUM_CANTIDAD_PRODUCTO : "") + "'>";
                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</td>";

                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + txtUnidadMedida + "-" + numEquivalencia : "") + "</td>";
                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td class='num_volumen_producto'> " + ((value.NUM_VOLUMEN_PRODUCTO) ? parseFloat(value.NUM_VOLUMEN_PRODUCTO).toFixed(2) : "") + "</td>";

                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> ";

                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<select data-idAbastecimiento='" + value.ID_ABASTECIMIENTO + "' data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' class='form-control btn-modificar-procedencia select2'> ";

                                $.each(listaProcedencias, function(index, value2) {
                                    if (value2.ID_PROCEDENCIA == value.ID_PROCEDENCIA) {
                                        optionsProcedencias = optionsProcedencias + `<option value='${value2.ID_PROCEDENCIA}' selected>${value2.ID_PROCEDENCIA}-${value2.TXT_PROCEDENCIA}</option>`;
                                    } else {
                                        optionsProcedencias = optionsProcedencias + `<option value='${value2.ID_PROCEDENCIA}' >${value2.ID_PROCEDENCIA}-${value2.TXT_PROCEDENCIA}</option>`;
                                    }
                                });
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + optionsProcedencias;
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</select> ";
                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</td>";

                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> ";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + '<button ' + ((value.ID_ABASTECIMIENTO)?'':'style="display:none"') + '  class="btn btn-success btn-duplicar-abastecimiento" data-idAbastecimiento="' + value.ID_ABASTECIMIENTO + '" data-id="' + value.ID_PRODUCTO_MERCADO + '">Duplicar</button>';
                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</td>";

                        
                        productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</tr>";
                    });


                    $('#lista-productos-mercado-abastecimiento tbody').html(productosMercadoAbastecimiento);
                    $(".input-mask").inputmask();
                    $(".select2").select2({
                        language: {
                            inputTooShort: function() {
                                return 'Por favor ingresa 3 o mas carácteres';
                            }
                        },
                        width: "100%",
                        minimumInputLength:3,
                        minimumResultsForSearch:3
                    });

                    $('.form-control').on("keypress", function(e) {
                        if (e.keyCode == 13) {
                            var that = $(this);
                            setTimeout(function () { that.parent().next().find('.form-control').focus(); }, 50);
                        }
                    });


                    $('#lista-productos-mercado-abastecimiento').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 10,
                        "order": [[ 2, "asc" ],[4,"asc"]],
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });


                    //ListarProductosMercados(results.productosMercadoAbastecimiento);

                    setTimeout(function() {
                        loading.hide();
                    }, 500);
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });

    }

    

    function NuevaFila(ListarProductosMercadoAbastecimiento){
        var productosMercadoAbastecimiento = "";
        console.log(ListarProductosMercadoAbastecimiento);
        $.each(ListarProductosMercadoAbastecimiento, function(index, value) {
            console.log(value);
            txtUnidadMedida= ((value.TXT_UNIDAD_MEDIDA)?value.TXT_UNIDAD_MEDIDA:"");
            numEquivalencia= ((value.NUM_EQUIVALENCIA)?value.NUM_EQUIVALENCIA:0.00);

            var optionsProcedencias = "<option value>Seleccionar</option>";
            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<tr>";
                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td class='text-center'> ";
                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + '<div class="form-check mb-3" ' + ((value.ID_ABASTECIMIENTO)?'':'style="display:none"') + ' ><input class="form-check-input check-abastecimiento" data-idAbastecimiento="' + value.ID_ABASTECIMIENTO + '"  type="checkbox" data-id="' + value.ID_PRODUCTO_MERCADO + '" ></div>';
                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</td>";

                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PRODUCTO_GENERO) ? value.TXT_CODIGO_GENERO + "-" + value.TXT_PRODUCTO_GENERO : "") + "</td>";
                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PRODUCTO) ? value.TXT_CODIGO_PRODUCTO + "-" + value.TXT_PRODUCTO : "") + "</td>";

                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td>";
                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<input type='text' data-idAbastecimiento='" + value.ID_ABASTECIMIENTO + "' data-numEquivalencia='" + numEquivalencia + "' data-inputmask=\"'alias': 'numeric', 'groupSeparator': ' ', 'digits': 2, 'digitsOptional': false\" class='form-control input-mask btn-modificar-num_cantidad_producto' data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' value='" + ((value.NUM_CANTIDAD_PRODUCTO) ? value.NUM_CANTIDAD_PRODUCTO : "") + "'>";
                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</td>";

                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + txtUnidadMedida + "-" + numEquivalencia : "") + "</td>";
                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td class='num_volumen_producto'> " + ((value.NUM_VOLUMEN_PRODUCTO) ? parseFloat(value.NUM_VOLUMEN_PRODUCTO).toFixed(2) : "") + "</td>";

                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> ";

                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<select data-idAbastecimiento='" + value.ID_ABASTECIMIENTO + "' data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' class='form-control btn-modificar-procedencia select2'> ";

                    $.each(listaProcedencias, function(index, value2) {
                        if (value2.ID_PROCEDENCIA == value.ID_PROCEDENCIA) {
                            optionsProcedencias = optionsProcedencias + `<option value='${value2.ID_PROCEDENCIA}' selected>${value2.ID_PROCEDENCIA}-${value2.TXT_PROCEDENCIA}</option>`;
                        } else {
                            optionsProcedencias = optionsProcedencias + `<option value='${value2.ID_PROCEDENCIA}' >${value2.ID_PROCEDENCIA}-${value2.TXT_PROCEDENCIA}</option>`;
                        }
                    });
                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + optionsProcedencias;
                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</select> ";
                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</td>";

                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> ";
                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + '<button ' + ((value.ID_ABASTECIMIENTO)?'':'style="display:none"') + '  class="btn btn-success btn-duplicar-abastecimiento" data-idAbastecimiento="' + value.ID_ABASTECIMIENTO + '" data-id="' + value.ID_PRODUCTO_MERCADO + '">Duplicar</button>';
                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</td>";

            
            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</tr>";
        });
        tableListaProductos = $('#lista-productos-mercado-abastecimiento').DataTable();
        //$('#lista-productos-mercado-abastecimiento').DataTable().order();
        tableListaProductos.row.add($(productosMercadoAbastecimiento));
        tableListaProductos.order( [ 2, 'asc' ], [ 4, 'asc' ] );
        tableListaProductos.draw(false);
        /* 
        $('#lista-productos-mercado-abastecimiento').DataTable().row.add($(productosMercadoAbastecimiento)).draw(false);
        $('#lista-productos-mercado-abastecimiento').DataTable().order([[ 2, "asc" ]]);
 */
        $(".input-mask").inputmask();
        $(".select2").select2({
            language: {
                inputTooShort: function() {
                    return 'Por favor ingresa 3 o mas carácteres';
                }
            },
            width: "100%",
            minimumInputLength:3,
            minimumResultsForSearch:3
        });

        $('.form-control').on("keypress", function(e) {
            if (e.keyCode == 13) {
                var that = $(this);
                setTimeout(function () { that.parent().next().find('.form-control').focus(); }, 50);
            }
        });

    }



    async function RegionesAbastecimiento() {

        await   $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones-usuario",
            method: "POST",
            data:{_csrf:csrf,proceso:'abastecimiento'},
            dataType:"Json",
            beforeSend:function(xhr, settings)
            {
                //loading.show();
            },
            success:function(results)
            {   
                if(results && results.success){
                    var optionsDepartamentos = "<option value>Seleccionar</option>";

                    $.each(results.regiones, function( index, value ) {
                        optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                    });

                    $("#abastecimiento-id_region").html(optionsDepartamentos);

                }
            },
            error:function(){
                alert("Error al realizar el proceso.");
            }
        });

    }

    async function Mercados(idRegion) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-opciones-mercados-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion,
                proceso:'precio'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsMercados = "<option value>Seleccionar</option>";

                    $.each(results.mercados, function(index, value) {
                        optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                    });

                    $("#abastecimiento-id_mercado").html(optionsMercados);

                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    async function GruposAbastecimiento(idMercado) {

        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-opciones-grupos-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idMercado:idMercado,
                proceso:'abastecimiento'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsGrupo = "<option value>Seleccionar</option>";

                    $.each(results.grupos, function(index, value) {
                        optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
                    });

                    $("#abastecimiento-id_producto_grupo").html(optionsGrupo);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });

    }


    var listaProcedencias = [];
    async function Procedencias() {
        await $.ajax({
            async: false,
            url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-procedencias",
            method: "POST",
            data: {
                _csrf: csrf
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    $.each(results.procedencias, function(index, value) {
                        listaProcedencias.push(value);
                    });
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }


    async function MercadoProductos(idMercado, idProductoGenero) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/get-lista-productos",
            method: "POST",
            data: {
                _csrf: csrf,
                idMercado: idMercado,
                idProductoGenero: idProductoGenero
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsProducto = "<option value>Seleccionar</option>";
                    $.each(results.productosMercado, function(index, value) {
                        let TXT_ENVASE = (value.TXT_ENVASE) ? value.TXT_ENVASE : '';
                        optionsProducto = optionsProducto + `<option value='${value.ID_PRODUCTO_MERCADO}'>${value.TXT_CODIGO_PRODUCTO}-${value.TXT_PRODUCTO}-${TXT_ENVASE}</option>`;
                    });

                    $("#abastecimiento-id_producto_mercado").html(optionsProducto);
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    

    $('body').on('change', '#abastecimiento-id_region', function(e) {
        e.preventDefault();
        idRegion = $(this).val();
        $("#abastecimiento-id_producto_grupo").html('<option value>Seleccionar</option>');
        //$("#abastecimiento-id_mercado").val('<option value>Seleccionar</option>');
        ListaProductosMercadoAbastecimiento();
        Mercados(idRegion);
    });

    /* Evento de los 3 campos */

    $('body').on('change', '#abastecimiento-id_producto_grupo', function(e) {
        e.preventDefault();
        idProductoGrupo = $(this).val();
        fecRegistro = $('#abastecimiento-fec_registro').val();
        idMercado = $('#abastecimiento-id_mercado').val();

        if (!idProductoGrupo) {
            ListaProductosMercadoAbastecimiento();
        }

        if (idProductoGrupo && fecRegistro && idMercado) {
            ProductosMercadoAbastecimiento(idProductoGrupo,idMercado,fecRegistro);
        }
    });

    $('body').on('change', '#abastecimiento-id_mercado', function(e) {
        e.preventDefault();
        idMercado = $(this).val();
        fecRegistro = $('#abastecimiento-fec_registro').val();
        idProductoGrupo = $('#abastecimiento-id_producto_grupo').val();
        //idProductoGenero = $('#abastecimiento-id_producto_genero').val();
        GruposAbastecimiento(idMercado);
        if (!idMercado) {
            ListaProductosMercadoAbastecimiento();
        }

        if (idProductoGrupo && fecRegistro && idMercado) {
            ProductosMercadoAbastecimiento(idProductoGrupo,idMercado,fecRegistro);
        }
    });

    $('body').on('change', '#abastecimiento-fec_registro', function(e) {
        e.preventDefault();

        let fechaInicio = "<?= $fechaInicio ?>";
        let fechaFin = "<?= $fechaFin ?>";
        let fecha = $(this).val();

        fechaInicioArray = fechaInicio.split("-");
        fechaFinArray = fechaFin.split("-");
        fechaArray = fecha.split("-");
        
        var fechaInicioJs = new Date(fechaInicioArray[0], parseInt(fechaInicioArray[1]) - 1, fechaInicioArray[2]);
        var fechaFinJs = new Date(fechaFinArray[0], parseInt(fechaFinArray[1]) - 1, fechaFinArray[2]);
        var fechaJs = new Date(fechaArray[0], parseInt(fechaArray[1]) - 1, fechaArray[2]);

        if (!(fechaJs >= fechaInicioJs && fechaJs <= fechaFinJs)) {
            toastr.warning('La fecha indicada no pertenece al rango habilitado por el sistema');
            $(this).val('')
            return false;
        }


        fecRegistro = $(this).val();
        idMercado = $('#abastecimiento-id_mercado').val();
        idProductoGrupo = $('#abastecimiento-id_producto_grupo').val();

        if (!fecRegistro) {
            ListaProductosMercadoAbastecimiento();
        }

        if (idProductoGrupo && fecRegistro && idMercado) {
            ProductosMercadoAbastecimiento(idProductoGrupo,idMercado,fecRegistro);
        }
    });


    $('body').on('change', '.btn-modificar-procedencia', function(e) {
        e.preventDefault();
        let idProcedencia = $(this).val();
        let idProductoMercado = $(this).attr('data-idProductoMercado');
        let fecRegistro = $('#abastecimiento-fec_registro').val();
        let idAbastecimiento = $(this).attr('data-idAbastecimiento');
        var element = $(this);
        $.ajax({
            //url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/update?id=' + idAbastecimiento,
            //url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/update?id=' + idAbastecimiento,
            url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/update?idProductoMercado=' + idProductoMercado + '&fecRegistro=' + fecRegistro + '&idAbastecimiento=' + idAbastecimiento,
            method: "POST",
            data: {
                _csrf: csrf,
                idProcedencia: idProcedencia,
                fecRegistro:fecRegistro
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    element.closest('tr').find('.btn-duplicar-abastecimiento').show();
                    element.closest('tr').find('.form-check').show();
                    element.attr('data-idAbastecimiento',results.idAbastecimiento);
                    
                    element.closest('tr').find('.btn-modificar-num_cantidad_producto').attr('data-idAbastecimiento',results.idAbastecimiento);
                    element.closest('tr').find('.btn-duplicar-abastecimiento').attr('data-idAbastecimiento',results.idAbastecimiento);
                    element.closest('tr').find('.check-abastecimiento').attr('data-idAbastecimiento',results.idAbastecimiento);
                    toastr.success('Procedencia grabada');
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
        /* $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/update?id=" + idAbastecimiento,
            method: "POST",
            data: {
                _csrf: csrf,
                idProcedencia: idProcedencia
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Procedencia grabada');
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        }); */
    });

    $('body').on('blur', '.btn-modificar-num_cantidad_producto', function(e) {
        e.preventDefault();
        let numCantidadProducto = parseFloat($(this).val());
        var element = $(this);
        let idProductoMercado = $(this).attr('data-idProductoMercado');
        let numEquivalencia = $(this).attr('data-numEquivalencia');
        let fecRegistro = $('#abastecimiento-fec_registro').val();
        let idAbastecimiento = $(this).attr('data-idAbastecimiento');

        if(isNaN(numCantidadProducto)){
            toastr.info('No es un número');
            return false;   
        }
        
        //console.log(element.closest('tr').find('.btn-duplicar-abastecimiento'));
        //element.attr('data-aaaa','aaa')
        //return false;

        let numVolumenProducto = numEquivalencia*numCantidadProducto;
        element.closest('tr').find('.num_volumen_producto').html(parseFloat(numVolumenProducto).toFixed(2));
        //$('.num_volumen_producto').html(parseFloat(numVolumenProducto).toFixed(2));
        
        $.ajax({
            //url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/update?id=' + idAbastecimiento,
            //url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/update?id=' + idAbastecimiento,
            url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/update?idProductoMercado=' + idProductoMercado + '&fecRegistro=' + fecRegistro + '&idAbastecimiento=' + idAbastecimiento,
            method: "POST",
            data: {
                _csrf: csrf,
                numCantidadProducto: numCantidadProducto,
                fecRegistro:fecRegistro,
                numVolumenProducto:numVolumenProducto
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    element.closest('tr').find('.btn-duplicar-abastecimiento').show();
                    element.closest('tr').find('.form-check').show();


                    


                    element.attr('data-idAbastecimiento',results.idAbastecimiento);
                    element.closest('tr').find('.btn-modificar-procedencia').attr('data-idAbastecimiento',results.idAbastecimiento);
                    element.closest('tr').find('.btn-duplicar-abastecimiento').attr('data-idAbastecimiento',results.idAbastecimiento);

                    element.closest('tr').find('.check-abastecimiento').attr('data-idAbastecimiento',results.idAbastecimiento);

                    toastr.success('Cantidad grabada');
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });
    //ID_PRODUCTO_MERCADO
    var listaIdsAbastecimientos = [];
    //var listaIdsProductosMercado = [];
    $('body').on('change', '.check-abastecimiento', function(e) {
        e.preventDefault();
        let idAbastecimiento = $(this).attr('data-idAbastecimiento');
        let bAbastecimiento = $(this).is(":checked");

        if (bAbastecimiento) {
            listaIdsAbastecimientos.push(idAbastecimiento);
        } else {
            listaIdsAbastecimientos.splice($.inArray(idAbastecimiento, listaIdsAbastecimientos), 1);
        }

    });


    $('body').on('click', '.btn-eliminar-producto-abastecimiento', function(e) {
        e.preventDefault();
        fecRegistro = $('#abastecimiento-fec_registro').val();
        Swal.fire({
            title: '¿Está seguro de eliminar los registros seleccionados?',
            text: "Una vez eliminado los registros ya no se podrá visualizar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/eliminar',
                    method: "POST",
                    data: {
                        _csrf: csrf,
                        listaIdsAbastecimientos: listaIdsAbastecimientos,
                        fecRegistro:fecRegistro
                    },
                    dataType: "Json",
                    beforeSend: function(xhr, settings) {
                        //loading.show();
                    },
                    success: function(results) {
                        if (results && results.success) {
                            idProductoGrupo = $('#abastecimiento-id_producto_grupo').val();
                            idMercado = $('#abastecimiento-id_mercado').val();
                            //fecRegistro = $('#abastecimiento-fec_registro').val();
                            ProductosMercadoAbastecimiento(idProductoGrupo,idMercado,fecRegistro);
                            toastr.success('Registro eliminado');
                        }
                    },
                    error: function() {
                        toastr.error('Error al realizar el proceso.');
                    }
                });

            }
        })

    });

    $('body').on('click', '.btn-duplicar-abastecimiento', function(e) {
        e.preventDefault();
        var idProductoMercado = $(this).attr('data-id');
        fecRegistro = $('#abastecimiento-fec_registro').val();
        $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/duplicar',
            method: "POST",
            data: {
                _csrf: csrf,
                idProductoMercado: idProductoMercado,
                fecRegistro:fecRegistro
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    idProductoGrupo = $('#abastecimiento-id_producto_grupo').val();
                    idMercado = $('#abastecimiento-id_mercado').val();
                    fecRegistro = $('#abastecimiento-fec_registro').val();
                    //ProductosMercadoAbastecimiento(idProductoGrupo,idMercado,fecRegistro);
                    /* listaAbastecimiento.push(results.productoMercadoAbastecimiento);
                    ListarProductosMercados(listaAbastecimiento); */
                    //$('#lista-productos-mercado-abastecimiento').html('asdsad')
                    //$('#lista-productos-mercado-abastecimiento').DataTable();
                    //$('#lista-productos-mercado-abastecimiento').DataTable().destroy();
                    //$('#lista-productos-mercado-abastecimiento').DataTable().row.add(results.productoMercadoAbastecimiento).draw();
                    
                    NuevaFila(results.productoMercadoAbastecimiento)
                    toastr.success('Registro duplicado');
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });

    });

    

    function init() {
        Procedencias();
        ListaProductosMercadoAbastecimiento();
        RegionesAbastecimiento();
    }

    init();
</script>