<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
$this->title='de';
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Producto x Mercado x Abastecimiento</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de productos x mercado x abastecimiento</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formProductoMercadoAbastecimiento', 'class' => 'form-horizontal']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Región</label>
                            <select id="abastecimiento-id_region" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Mercado</label>
                            <select id="abastecimiento-id_mercado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha</label>
                            <input type="date" id="abastecimiento-fec_registro" name="Abastecimiento[FEC_REGISTRO]" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Grupo</label>
                            <select id="abastecimiento-id_producto_grupo" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Producto</label>
                            <select id="abastecimiento-id_producto_genero" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Producto con envase</label>
                            <select id="abastecimiento-id_producto_mercado" name="Abastecimiento[ID_PRODUCTO_MERCADO]" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div> -->
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="lista-productos-mercado-abastecimiento" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Fecha Registro</th>
                        <th>Región</th>
                        <th>Mercado</th>
                        <th>Grupo</th>
                        <th>Producto</th>
                        <th>Variedad</th>
                        <th>Envase</th>
                        <th>Cantidad</th>
                        <th>Peso</th>
                        <th>Procedencia</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');
    var idRegion;
    var idMercado;
    var fecRegistro;
    var idProductoGrupo;
    var idProductoGenero;
    var idProductoMercado;

    function ListaProductosMercadoAbastecimiento() {

        $('#lista-productos-mercado-abastecimiento').DataTable().destroy();
        $('#lista-productos-mercado-abastecimiento tbody').html("");
        $('#lista-productos-mercado-abastecimiento').DataTable({
            "dom": "Bfrtip",
            buttons: [
                'excelHtml5',
                'csvHtml5',
            ],
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "pageLength": 10,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningun dato disponible en esta lista",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });
        setTimeout(function(){ loading.hide(); }, 500);
    }



    async function ProductosMercadoAbastecimiento(){
        
        await   $.ajax({
                    url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/get-lista-abastecimiento-index',
                    method: 'POST',
                    data:{_csrf:csrf,fecRegistro:fecRegistro,idRegion:idRegion,idMercado:idMercado,idProductoGrupo:idProductoGrupo,idProductoGenero:idProductoGenero,idProductoMercado:idProductoMercado},
                    dataType:'Json',
                    beforeSend:function(){
                        loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            var productosMercadoAbastecimiento ="";
                            $('#lista-productos-mercado-abastecimiento').DataTable().destroy();

                            $.each(results.productosMercadoAbastecimiento, function( index, value ) {
                                txtUnidadMedida= ((value.TXT_UNIDAD_MEDIDA)?value.TXT_UNIDAD_MEDIDA:"");
                                numEquivalencia= ((value.NUM_EQUIVALENCIA)?value.NUM_EQUIVALENCIA:"");

                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<tr>";
                                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.FEC_REGISTRO)?value.FEC_REGISTRO:"") + "</td>";
                                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_DEPARTAMENTO)?value.TXT_DEPARTAMENTO:"") + "</td>";
                                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_MERCADO)?value.TXT_MERCADO:"") + "</td>";
                                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PRODUCTO_GRUPO)? value.TXT_CODIGO_GRUPO + "-" + value.TXT_PRODUCTO_GRUPO:"") + "</td>";
                                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PRODUCTO_GENERO)? value.TXT_CODIGO_GENERO + "-" + value.TXT_PRODUCTO_GENERO:"") + "</td>";
                                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PRODUCTO)? value.TXT_CODIGO_PRODUCTO + "-" + value.TXT_PRODUCTO:"") + "</td>";
                                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + numEquivalencia + "-" + txtUnidadMedida : "") + "</td>";
                                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.NUM_CANTIDAD_PRODUCTO)?parseFloat(value.NUM_CANTIDAD_PRODUCTO).toFixed(2):"") + "</td>";
                                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.NUM_VOLUMEN_PRODUCTO)?parseFloat(value.NUM_VOLUMEN_PRODUCTO).toFixed(2):"") + "</td>";
                                    productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PROCEDENCIA)?value.TXT_PROCEDENCIA:"") + "</td>";
                                    
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</tr>";
                            });
                            
                            $('#lista-productos-mercado-abastecimiento tbody').html(productosMercadoAbastecimiento);
                            $('#lista-productos-mercado-abastecimiento').DataTable({
                                "dom": "Bfrtip",
                                buttons: [
                                    'excelHtml5',
                                    'csvHtml5',
                                ],
                                "paging": true,
                                "lengthChange": true,
                                "searching": true,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "pageLength" : 10,
                                "language": {
                                    "sProcessing":    "Procesando...",
                                    "sLengthMenu":    "Mostrar _MENU_ registros",
                                    "sZeroRecords":   "No se encontraron resultados",
                                    "sEmptyTable":    "Ningun dato disponible en esta lista",
                                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":   "",
                                    "sSearch":        "Buscar:",
                                    "sUrl":           "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":    "Último",
                                        "sNext":    "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    }
                                },
                            });

                            setTimeout(function(){ loading.hide(); }, 500);
                        }
                    },
                    error:function(){
                        toastr.error('Error al realizar el proceso.');
                    }
                });

    }


    async function RegionesAbastecimiento() {
        await   $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones-usuario",
            method: "POST",
            data:{_csrf:csrf,proceso:'abastecimiento'},
            dataType:"Json",
            beforeSend:function(xhr, settings)
            {
                //loading.show();
            },
            success:function(results)
            {   
                if(results && results.success){
                    var optionsDepartamentos = "<option value>Seleccionar</option>";

                    $.each(results.regiones, function( index, value ) {
                        optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                    });

                    $("#abastecimiento-id_region").html(optionsDepartamentos);

                }
            },
            error:function(){
                alert("Error al realizar el proceso.");
            }
        });

    }

    async function Mercados(idRegion) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-opciones-mercados-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion,
                proceso:'precio'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsMercados = "<option value>Seleccionar</option>";

                    $.each(results.mercados, function(index, value) {
                        optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                    });

                    $("#abastecimiento-id_mercado").html(optionsMercados);

                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    async function GruposAbastecimiento() {

        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-opciones-grupos-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idMercado:idMercado,
                proceso:'abastecimiento'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsGrupo = "<option value>Seleccionar</option>";

                    $.each(results.grupos, function(index, value) {
                        optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
                    });

                    $("#abastecimiento-id_producto_grupo").html(optionsGrupo);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    /*
    async function MercadoProductos(idMercado,idProductoGenero){
        await   $.ajax({
                    url: "<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/get-lista-productos",
                    method: "POST",
                    data:{_csrf:csrf,idMercado:idMercado,idProductoGenero:idProductoGenero},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            var optionsProducto = "<option value>Seleccionar</option>";

                            $.each(results.productosMercado, function( index, value ) {
                                let TXT_ENVASE = (value.TXT_ENVASE)?value.TXT_ENVASE:'';
                                optionsProducto = optionsProducto + `<option value='${value.ID_PRODUCTO_MERCADO}'>${value.TXT_CODIGO_PRODUCTO}-${value.TXT_PRODUCTO}-${TXT_ENVASE}</option>`;
                            });
                            
                            $("#abastecimiento-id_producto_mercado").html(optionsProducto);
                        }
                    },
                    error:function(){
                        toastr.error('Error al realizar el proceso.');
                    }
                });
    }*/

    async function GenerosAbastecimiento(idProductoGrupo) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/genero/get-lista-opciones-generos-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idProductoGrupo: idProductoGrupo,
                proceso:'abastecimiento'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsGenero = "<option value>Seleccionar</option>";

                    $.each(results.generos, function(index, value) {
                        optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_PRODUCTO_GENERO}</option>`;
                    });

                    $("#abastecimiento-id_producto_genero").html(optionsGenero);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    async function MercadoProductos(idMercado, idProductoGenero) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/get-lista-productos",
            method: "POST",
            data: {
                _csrf: csrf,
                idMercado: idMercado,
                idProductoGenero: idProductoGenero
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsProducto = "<option value>Seleccionar</option>";

                    $.each(results.productosMercado, function(index, value) {
                        optionsProducto = optionsProducto + `<option value='${value.ID_PRODUCTO_MERCADO}'>${value.TXT_CODIGO_PRODUCTO}-${value.TXT_PRODUCTO}-${value.TXT_ENVASE}</option>`;
                    });

                    $("#abastecimiento-id_producto_mercado").html(optionsProducto);
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }


    $('body').on('change', '#abastecimiento-fec_registro', function(e) {
        e.preventDefault();
        fecRegistro = $(this).val();

        if (!fecRegistro) {
            ListaProductosMercadoAbastecimiento();
        }

        if(fecRegistro){
            ProductosMercadoAbastecimiento();
        }
    });


    $('body').on('change', '#abastecimiento-id_region', function (e) {
        e.preventDefault();
        idRegion = $(this).val();
        Mercados(idRegion);

        if(!idRegion){
            ListaProductosMercadoAbastecimiento();
        }

        if(fecRegistro){
            ProductosMercadoAbastecimiento();
        }
    });

    $('body').on('change', '#abastecimiento-id_mercado', function (e) {
        e.preventDefault();
        idMercado = $(this).val();
        idProductoGenero = $('#abastecimiento-id_producto_genero').val();
        GruposAbastecimiento(idMercado);
        if(!idMercado){
            ListaProductosMercadoAbastecimiento();
        }

        if(idMercado && idProductoGenero){
            MercadoProductos(idMercado,idProductoGenero);
        }

        if(fecRegistro){
            ProductosMercadoAbastecimiento();
        }
    });



    $('body').on('change', '#abastecimiento-id_producto_grupo', function (e) {
        e.preventDefault();
        idProductoGrupo = $(this).val();
        GenerosAbastecimiento(idProductoGrupo);

        if(fecRegistro){
            ProductosMercadoAbastecimiento();
        }
    });




    $('body').on('change', '#abastecimiento-id_producto_genero', function (e) {
        e.preventDefault();
        idProductoGenero = $(this).val();
        idMercado = $('#abastecimiento-id_mercado').val();
        
        /* if(idMercado && idProductoGenero){
            MercadoProductos(idMercado,idProductoGenero);
        } */

        if(fecRegistro){
            ProductosMercadoAbastecimiento();
        }

    });






    $('body').on('change', '#abastecimiento-id_producto_mercado', function (e) {
        e.preventDefault();
        idProductoMercado = $(this).val();
        fecRegistro = $('#abastecimiento-fec_registro').val();

        if(!idProductoMercado){
            ListaProductosMercadoAbastecimiento();
        }

        if(idProductoMercado && fecRegistro){
            ProductosMercadoAbastecimiento(idProductoMercado,fecRegistro);
        }

        if(fecRegistro){
            ProductosMercadoAbastecimiento();
        }
    });





    function init() {
        ListaProductosMercadoAbastecimiento();
        RegionesAbastecimiento();
        //GruposAbastecimiento();
    }

    init();
</script>