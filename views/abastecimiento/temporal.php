<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
$this->title='de';
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Adjuntar x Abastecimiento</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de información temporal de abastecimiento</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['action'=>'adjuntar','options' => ['id' => 'formAbastecimiento', 'class' => 'form-horizontal']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Archivo</label>
                            <input type="file" id="temporal-abastecimiento-archivo" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-success btn-grabar-abastecimiento">Subir</button>
                        <button type="button" class="btn btn-success btn-procesar-abastecimiento">Procesar</button>
                    </div>
                </div>
            </div>
            
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="lista-temporal-abastecimiento" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Mercado</th>
                        <th>Grupo</th>
                        <th>Producto</th>
                        <th>Codigo externo</th>
                        <th>Variedad</th>
                        <th>Envase</th>
                        <th>Peso</th>
                        <th>Procedencia</th>
                        <th>Fecha</th>
                        <th>Relacionado</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');
    $('body').on('click', '.btn-grabar-abastecimiento', function (e) {

        e.preventDefault();
        var form = $('#formAbastecimiento');
        var formData = new FormData();
        formData.append("_csrf", csrf);
        formData.append("TemporalAbastecimiento[archivo]", document.getElementById('temporal-abastecimiento-archivo').files[0]);
        formData.append("TemporalAbastecimiento[tmp]", 'carga');


        $.ajax({
            url:form.attr("action"),
            type: form.attr("method"),
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            dataType: 'json',
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                //$('.sidebar-mini').LoadingOverlay("hide", true);
            },
            beforeSend:function()
            {
                loading.show();
            },
            success: function (results) {
                if(results.success){
                    ListaTemporalAbastecimiento();
                }else{
                    if(results.msg==0){
                        toastr.warning('No se pudo grabar los registro, realizarlo nuevamente'); 
                    }else if(results.msg==1){
                        toastr.warning('El archivo contiene 2 fechas diferentes');
                    }
                    ListaTemporalAbastecimiento();
                }
            },
        });
    });

    async function ListaTemporalAbastecimiento(){
        await   $.ajax({
                    url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/get-lista-abastecimiento-temporal',
                    method: 'POST',
                    data:{_csrf:csrf},
                    dataType:'Json',
                    beforeSend:function(){
                        loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            var productosMercadoAbastecimientoTemporal ="";
                            $('#lista-temporal-abastecimiento').DataTable().destroy();
                            
                            $.each(results.productosMercadoAbastecimientoTemporal, function( index, value ) {
                                //fecRegistroLista.push(value.FEC_REGISTRO);
                                txtUnidadMedida= ((value.TXT_UNIDAD_MEDIDA)?value.TXT_UNIDAD_MEDIDA:"");
                                numEquivalencia= ((value.NUM_EQUIVALENCIA)?value.NUM_EQUIVALENCIA:"");

                                productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "<tr>";
                                    productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "<td> " + ((value.TXT_MERCADO)?value.TXT_MERCADO:"") + "</td>";
                                    productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "<td> " + ((value.TXT_PRODUCTO_GRUPO)? value.TXT_CODIGO_GRUPO + "-" + value.TXT_PRODUCTO_GRUPO:"") + "</td>";
                                    productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "<td> " + ((value.TXT_PRODUCTO_GENERO)? value.TXT_CODIGO_GENERO + "-" + value.TXT_PRODUCTO_GENERO:"") + "</td>";
                                    productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "<td> " + ((value.TXT_CODIGO_PRODUCTO_MERCADO)?value.TXT_CODIGO_PRODUCTO_MERCADO:"") + "</td>";
                                    productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "<td> " + ((value.TXT_PRODUCTO)? value.TXT_CODIGO_PRODUCTO + "-" + value.TXT_PRODUCTO:"") + "</td>";
                                    productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "<td> " + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + txtUnidadMedida + "-" + numEquivalencia : "") + "</td>";
                                    productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "<td> " + ((value.NUM_VOLUMEN_PRODUCTO)?value.NUM_VOLUMEN_PRODUCTO:"") + "</td>";
                                    productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "<td> " + ((value.TXT_PROCEDENCIA)?value.TXT_PROCEDENCIA:"") + "</td>";
                                    productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "<td> " + ((value.FEC_REGISTRO)?value.FEC_REGISTRO:"") + "</td>";
                                    productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "<td> " + ((value.ID_PRODUCTO_MERCADO)?"Si se encontro":"El producto relacionado no se encuentra anexado al mercado") + "</td>";
                                    
                                productosMercadoAbastecimientoTemporal = productosMercadoAbastecimientoTemporal + "</tr>";
                            });
                            
                            
                            $('#lista-temporal-abastecimiento tbody').html(productosMercadoAbastecimientoTemporal);
                            $('#lista-temporal-abastecimiento').DataTable({
                                "dom": "Bfrtip",
                                buttons: [
                                    'excelHtml5',
                                    'csvHtml5',
                                ],
                                "paging": true,
                                "lengthChange": true,
                                "searching": true,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "pageLength" : 10,
                                "language": {
                                    "sProcessing":    "Procesando...",
                                    "sLengthMenu":    "Mostrar _MENU_ registros",
                                    "sZeroRecords":   "No se encontraron resultados",
                                    "sEmptyTable":    "Ningun dato disponible en esta lista",
                                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":   "",
                                    "sSearch":        "Buscar:",
                                    "sUrl":           "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":    "Último",
                                        "sNext":    "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    }
                                },
                            });

                            setTimeout(function(){ loading.hide(); }, 1000);
                        }
                    },
                    error:function(){
                        toastr.error('Error al realizar el proceso.');
                    }
                });

    }
    ListaTemporalAbastecimiento();




    $('body').on('click', '.btn-procesar-abastecimiento', function(e) {
        e.preventDefault();
        Swal.fire({
            title: '¿Está seguro de procesar los registros que si estan anexados?',
            text: "Una vez procesados se eliminara la información en la tabla temporal y en caso exista registros en abastecimiento se reemplazara en base a la fecha",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, procesar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/procesar-temporal',
                    method: "POST",
                    data: {
                        _csrf: csrf
                    },
                    dataType: "Json",
                    beforeSend: function(xhr, settings) {
                        //loading.show();
                    },
                    success: function(results) {
                        if (results && results.success) {
                            ListaTemporalAbastecimiento();
                        }
                    },
                    error: function() {
                        toastr.error('Error al realizar el proceso.');
                    }
                });

            }
        })

    });
    
</script>