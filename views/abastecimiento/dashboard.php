<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
$this->title='de';
?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Dashboard Abastecimiento</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Dashboard abastecimiento</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formProductoMercadoAbastecimiento', 'class' => 'form-horizontal']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Región</label>
                            <select id="abastecimiento-id_region" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Mercado</label>
                            <select id="abastecimiento-id_mercado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha</label>
                            <input type="date" id="abastecimiento-fec_registro" name="Abastecimiento[FEC_REGISTRO]" class="form-control">
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-6">
        <div class="card">
            <div class="card-body">
                <div id="grafico-barras-region-peso" class="grafico-barras-region-peso"></div>
            </div>
        </div>
    </div> <!-- end col -->
    <div class="col-6">
        <div class="card">
            <div class="card-body">
                <div id="grafico-pie-grupo-peso" class="grafico-pie-grupo-peso"></div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div id="grafico-bubble-grupo-producto-peso" class="grafico-bubble-grupo-producto-peso"></div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->


<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');
    var idRegion;
    var idMercado;
    var fecRegistro;
    var idProductoGrupo;
    var idProductoGenero;
    var idProductoMercado;

    


async function ProductosMercadoAbastecimiento(){
    
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/get-lista-abastecimiento-index',
                method: 'POST',
                data:{_csrf:csrf,fecRegistro:fecRegistro,idRegion:idRegion,idMercado:idMercado,idProductoGrupo:idProductoGrupo,idProductoGenero:idProductoGenero,idProductoMercado:idProductoMercado},
                dataType:'Json',
                beforeSend:function(){
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var productosMercadoAbastecimiento ="";
                        $('#lista-productos-mercado-abastecimiento').DataTable().destroy();

                        $.each(results.productosMercadoAbastecimiento, function( index, value ) {
                            txtUnidadMedida= ((value.TXT_UNIDAD_MEDIDA)?value.TXT_UNIDAD_MEDIDA:"");
                            numEquivalencia= ((value.NUM_EQUIVALENCIA)?value.NUM_EQUIVALENCIA:"");

                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<tr>";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.FEC_REGISTRO)?value.FEC_REGISTRO:"") + "</td>";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_DEPARTAMENTO)?value.TXT_DEPARTAMENTO:"") + "</td>";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_MERCADO)?value.TXT_MERCADO:"") + "</td>";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PRODUCTO_GRUPO)? value.TXT_CODIGO_GRUPO + "-" + value.TXT_PRODUCTO_GRUPO:"") + "</td>";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PRODUCTO_GENERO)? value.TXT_CODIGO_GENERO + "-" + value.TXT_PRODUCTO_GENERO:"") + "</td>";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PRODUCTO)? value.TXT_CODIGO_PRODUCTO + "-" + value.TXT_PRODUCTO:"") + "</td>";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + txtUnidadMedida + "-" + numEquivalencia : "") + "</td>";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.NUM_CANTIDAD_PRODUCTO)?value.NUM_CANTIDAD_PRODUCTO:"") + "</td>";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.NUM_VOLUMEN_PRODUCTO)?value.NUM_VOLUMEN_PRODUCTO:"") + "</td>";
                                productosMercadoAbastecimiento = productosMercadoAbastecimiento + "<td> " + ((value.TXT_PROCEDENCIA)?value.TXT_PROCEDENCIA:"") + "</td>";
                                
                            productosMercadoAbastecimiento = productosMercadoAbastecimiento + "</tr>";
                        });
                        
                        $('#lista-productos-mercado-abastecimiento tbody').html(productosMercadoAbastecimiento);
                        $('#lista-productos-mercado-abastecimiento').DataTable({
                            "dom": "Bfrtip",
                            buttons: [
                                'excelHtml5',
                                'csvHtml5',
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": true,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.hide(); }, 500);
                    }
                },
                error:function(){
                    toastr.error('Error al realizar el proceso.');
                }
            });

        }


    async function RegionesAbastecimiento() {
        await   $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones-usuario",
            method: "POST",
            data:{_csrf:csrf,proceso:'abastecimiento'},
            dataType:"Json",
            beforeSend:function(xhr, settings)
            {
                //loading.show();
            },
            success:function(results)
            {   
                if(results && results.success){
                    var optionsDepartamentos = "<option value>Seleccionar</option>";

                    $.each(results.regiones, function( index, value ) {
                        optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                    });

                    $("#abastecimiento-id_region").html(optionsDepartamentos);
                    setTimeout(function(){ loading.hide(); }, 500);
                }
            },
            error:function(){
                alert("Error al realizar el proceso.");
            }
        });

    }

    async function Mercados(idRegion) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-opciones-mercados-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion,
                proceso:'precio'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsMercados = "<option value>Seleccionar</option>";

                    $.each(results.mercados, function(index, value) {
                        optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                    });

                    $("#abastecimiento-id_mercado").html(optionsMercados);

                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    async function RegionesPeso() {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/get-lista-regiones-peso",
            method: "POST",
            data: {
                _csrf: csrf,
                fecRegistro: fecRegistro,
                idRegion:idRegion,
                idMercado:idMercado
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var categorias = [];
                    var series = [];
                    var data =[];
                    $.each(results.regionesPesos, function(index, value) {
                        let total = (value.TOTAL)?parseFloat(value.TOTAL):0.00;
                        if(total>0.00){
                            categorias.push(value.TXT_DEPARTAMENTO);
                            data.push(total);
                        }
                        
                    });

                    series.push({
                        name: 'Pesos',
                        data: data
                    });

                    graficoBarras('grafico-barras-region-peso','Región por Peso','',categorias,series,'Kilogramos');
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    async function GruposPeso() {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/get-lista-grupos-peso",
            method: "POST",
            data: {
                _csrf: csrf,
                fecRegistro: fecRegistro,
                idRegion:idRegion,
                idMercado:idMercado
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var categorias = [];
                    var data =[];
                    $.each(results.gruposPesos, function(index, value) {
                        let total = (value.TOTAL)?parseFloat(value.TOTAL):0.00;
                        if(total>0.00){
                            categorias.push(value.TXT_PRODUCTO_GRUPO);
                            data.push({
                                y: total,
                                name: value.TXT_PRODUCTO_GRUPO,
                            });
                        }
                        
                    });

                    graficoPie('grafico-pie-grupo-peso','Grupo por Peso','',categorias,data);
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    async function GruposGeneroPeso() {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/abastecimiento/get-lista-grupos-genero-peso",
            method: "POST",
            data: {
                _csrf: csrf,
                fecRegistro: fecRegistro,
                idRegion:idRegion,
                idMercado:idMercado
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var series = [];
                    $.each(results.grupos, function(index, value) {
                        var data = [];
                        $.each(results.gruposGeneroPesos, function(index2, value2) {

                            if(value.ID_PRODUCTO_GRUPO==value2.ID_PRODUCTO_GRUPO){
                                let total = (value2.TOTAL)?parseFloat(value2.TOTAL):0.00;
                                if(total>0.00){

                                    data.push({
                                        value: total,
                                        name: value2.TXT_PRODUCTO_GENERO,
                                    });
                                }
                            }
                        });
                        series.push({
                            name: value.TXT_PRODUCTO_GRUPO,
                            data : data
                        })
                    });
                    console.log(series);
                    


                    graficoBubble('grafico-bubble-grupo-producto-peso','Grupo por producto y Peso',series);
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }



    $('body').on('change', '#abastecimiento-fec_registro', function(e) {
        e.preventDefault();
        fecRegistro = $(this).val();


        if(fecRegistro){
            //ProductosMercadoAbastecimiento();
            RegionesPeso();
            GruposPeso();
            GruposGeneroPeso();
        }
    });


    $('body').on('change', '#abastecimiento-id_region', function (e) {
        e.preventDefault();
        idRegion = $(this).val();
        Mercados(idRegion);


        if(fecRegistro){
            RegionesPeso();
            GruposPeso();
            GruposGeneroPeso();
        }
    });

    $('body').on('change', '#abastecimiento-id_mercado', function (e) {
        e.preventDefault();
        idMercado = $(this).val();
        idProductoGenero = $('#abastecimiento-id_producto_genero').val();
        //GruposAbastecimiento(idMercado);

        if(idMercado && idProductoGenero){
            MercadoProductos(idMercado,idProductoGenero);
        }

        if(fecRegistro){
            RegionesPeso();
            GruposPeso();
            GruposGeneroPeso();
        }
    });



    function graficoBarras(identificador,titulo,subtitulo,categorias,series,medida){
        Highcharts.chart(identificador, {
            credits: {
                enabled: false
            },
            chart: {
                type: 'column'
            },
            title: {
                text: titulo
            },
            subtitle: {
                text: subtitulo
            },
            xAxis: {
                categories: categorias,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: '(' + medida + ')'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} ' + medida +'</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}'
                    }
                },
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: series
        });
    }

    function graficoPie(identificador,titulo,subtitulo,categorias,data){
        Highcharts.chart(identificador, {
            credits: {
                enabled: false
            },
            chart: {
                type: 'pie'
            },
            title: {
                text: titulo
            },
            subtitle: {
                text: subtitulo
            },
            xAxis: {
                categories: categorias
            },
            series: [{
                type: 'pie',
                allowPointSelect: true,
                data: data,
                showInLegend: true
            }]
        });
    }

    function graficoBubble(identificador,titulo,series){
        Highcharts.chart(identificador, {
            chart: {
                type: 'packedbubble',
                height: '100%'
            },
            title: {
                text: titulo
            },
            tooltip: {
                useHTML: true,
                pointFormat: '<b>{point.name}:</b> {point.value}Kg'
            },
            plotOptions: {
                packedbubble: {
                    minSize: '20%',
                    maxSize: '100%',
                    zMin: 0,
                    zMax: 10000000,
                    layoutAlgorithm: {
                        gravitationalConstant: 0.05,
                        splitSeries: true,
                        seriesInteraction: false,
                        dragBetweenSeries: true,
                        parentNodeLimit: true
                    },
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}',
                        filter: {
                            property: 'y',
                            operator: '>',
                            value: 10000
                        },
                        style: {
                            color: 'black',
                            textOutline: 'none',
                            fontWeight: 'normal'
                        }
                    }
                }
            },
            series: series
        });
    }


    function init() {
        RegionesAbastecimiento();
        //GruposAbastecimiento();
    }

    init();
</script>