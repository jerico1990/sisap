<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Producto x Mercado x Precio</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de productos x mercado x precio</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formProductoMercadoPrecio', 'class' => 'form-horizontal']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Región</label>
                            <select id="precio-id_region" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Mercado</label>
                            <select name="Precio[ID_MERCADO]" id="precio-id_mercado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha</label>
                            <input type="date" name="Precio[FEC_REGISTRO]" id="precio-fec_registro" class="form-control" value="<?= $fechaActual ?>" min="<?= $fechaInicio ?>" max="<?= $fechaFin ?>" onkeydown="return false">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Grupo</label>
                            <select id="precio-id_producto_grupo" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Genero</label>
                            <select name="Precio[ID_PRODUCTO_GENERO]" id="precio-id_producto_genero" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div> -->
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button type="button" class="btn btn-danger btn-eliminar-precio">Eliminar</button>
                        </div>
                    </div>
                </div>
                <table id="lista-productos-mercado-precio" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Acciones</th>
                        <!-- <th>Región</th>
                        <th>Mercado</th> -->
                        <th>Producto</th>
                        <th>Variedad</th>
                        <th>Envase</th>
                        <th>Precio 1</th>
                        <th>Precio 2</th>
                        <th>Precio 3</th>
                        <th>Precio 4</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');

    function ListaProductosMercadoPrecio() {
        console.log("ingreso");

        $('#lista-productos-mercado-precio').DataTable().destroy();
        $('#lista-productos-mercado-precio tbody').html("");
        $('#lista-productos-mercado-precio').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "pageLength": 10,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningun dato disponible en esta lista",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });

        setTimeout(function() {
                        loading.hide();
                    }, 500);
    }



    async function ProductosMercadoPrecio(idProductoGrupo, idMercado, fecRegistro) {

        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/precio/get-lista-productos-mercado-precio',
            method: 'POST',
            data: {
                _csrf: csrf,
                idProductoGrupo: idProductoGrupo,
                idMercado: idMercado,
                fecRegistro: fecRegistro
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var productosMercadoPrecio = "";
                    $('#lista-productos-mercado-precio').DataTable().destroy();
                    //listaProductos

                    $.each(results.productosMercadoPrecio, function(index, value) {
                        /* var optionsProductos = "<option value>Seleccionar</option>";
                        var optionsEnvases = "<option value>Seleccionar</option>";
                        var optionsPublicados = "<option value>Seleccionar</option>"; */
                        txtUnidadMedida= ((value.TXT_UNIDAD_MEDIDA)?value.TXT_UNIDAD_MEDIDA:"");
                        numEquivalencia= ((value.NUM_EQUIVALENCIA)?value.NUM_EQUIVALENCIA:"");

                        productosMercadoPrecio = productosMercadoPrecio + "<tr>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td class='text-center'> ";
                                    productosMercadoPrecio = productosMercadoPrecio + '<div class="form-check mb-3"><input class="form-check-input check-precio" type="checkbox" data-id="' + value.ID_PRODUCTO_MERCADO + '" ></div>';
                            productosMercadoPrecio = productosMercadoPrecio + "</td>";

                            //productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.ID_PRODUCTO_MERCADO) ? value.ID_PRODUCTO_MERCADO : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.TXT_PRODUCTO_GENERO) ? value.TXT_CODIGO_GENERO + "-" + value.TXT_PRODUCTO_GENERO : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.TXT_PRODUCTO) ? value.TXT_CODIGO_PRODUCTO + "-" + value.TXT_PRODUCTO : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + txtUnidadMedida + "-" + numEquivalencia : "") + "</td>";
                            //productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.NUM_EQUIVALENCIA) ? value.NUM_EQUIVALENCIA : "") + "</td>";

                            productosMercadoPrecio = productosMercadoPrecio + "<td>";
                                productosMercadoPrecio = productosMercadoPrecio + "<input type='text' data-inputmask=\"'alias': 'numeric', 'groupSeparator': ' ', 'digits': 2, 'digitsOptional': false, 'prefix': 'S/ ', 'placeholder': '0'\" class='form-control input-mask btn-modificar-num_precio num_precio_" + value.ID_PRODUCTO_MERCADO + "_1 " + ((value.ID_PRECIO && value.NUM_PRECIO_1==null && 1<=results.cantidadAlerta) ? "alert-danger" : "") + "' style='" + ((value.ID_PRECIO && value.NUM_PRECIO_1==null && 1<=results.cantidadAlerta) ? "border:1px solid #DA1414" : "") + "' data-cantidadAlerta='" + results.cantidadAlerta + "' data-numPrecioPosicion='1' data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' value='" + ((value.NUM_PRECIO_1) ? value.NUM_PRECIO_1 : "") + "'>";
                            productosMercadoPrecio = productosMercadoPrecio + "</td>";

                            productosMercadoPrecio = productosMercadoPrecio + "<td>";
                                productosMercadoPrecio = productosMercadoPrecio + "<input type='text' data-inputmask=\"'alias': 'numeric', 'groupSeparator': ' ', 'digits': 2, 'digitsOptional': false, 'prefix': 'S/ ', 'placeholder': '0'\" class='form-control input-mask btn-modificar-num_precio num_precio_" + value.ID_PRODUCTO_MERCADO + "_2 " + ((value.ID_PRECIO && value.NUM_PRECIO_2==null && 2<=results.cantidadAlerta) ? "alert-danger" : "") + "' style='" + ((value.ID_PRECIO && value.NUM_PRECIO_2==null && 2<=results.cantidadAlerta) ? "border:1px solid #DA1414" : "") + "' data-cantidadAlerta='" + results.cantidadAlerta + "' data-numPrecioPosicion='2' data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' value='" + ((value.NUM_PRECIO_2) ? value.NUM_PRECIO_2 : "") + "'>";
                            productosMercadoPrecio = productosMercadoPrecio + "</td>";

                            productosMercadoPrecio = productosMercadoPrecio + "<td>";
                                productosMercadoPrecio = productosMercadoPrecio + "<input type='text' data-inputmask=\"'alias': 'numeric', 'groupSeparator': ' ', 'digits': 2, 'digitsOptional': false, 'prefix': 'S/ ', 'placeholder': '0'\" class='form-control input-mask btn-modificar-num_precio num_precio_" + value.ID_PRODUCTO_MERCADO + "_3 " + ((value.ID_PRECIO && value.NUM_PRECIO_3==null && 3<=results.cantidadAlerta) ? "alert-danger" : "") + "' style='" + ((value.ID_PRECIO && value.NUM_PRECIO_3==null && 3<=results.cantidadAlerta) ? "border:1px solid #DA1414" : "") + "' data-cantidadAlerta='" + results.cantidadAlerta + "' data-numPrecioPosicion='3' data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' value='" + ((value.NUM_PRECIO_3) ? value.NUM_PRECIO_3 : "") + "'>";
                            productosMercadoPrecio = productosMercadoPrecio + "</td>";

                            productosMercadoPrecio = productosMercadoPrecio + "<td>";
                                productosMercadoPrecio = productosMercadoPrecio + "<input type='text' data-inputmask=\"'alias': 'numeric', 'groupSeparator': ' ', 'digits': 2, 'digitsOptional': false, 'prefix': 'S/ ', 'placeholder': '0'\" class='form-control input-mask btn-modificar-num_precio num_precio_" + value.ID_PRODUCTO_MERCADO + "_4 " + ((value.ID_PRECIO && value.NUM_PRECIO_4==null && 4<=results.cantidadAlerta) ? "alert-danger" : "") + "' style='" + ((value.ID_PRECIO && value.NUM_PRECIO_4==null && 4<=results.cantidadAlerta) ? "border:1px solid #DA1414" : "") + "' data-cantidadAlerta='" + results.cantidadAlerta + "' data-numPrecioPosicion='4' data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' value='" + ((value.NUM_PRECIO_4) ? value.NUM_PRECIO_4 : "") + "'>";
                            productosMercadoPrecio = productosMercadoPrecio + "</td>";

                        productosMercadoPrecio = productosMercadoPrecio + "</tr>";
                    });

                    $('#lista-productos-mercado-precio tbody').html(productosMercadoPrecio);

                    $(".input-mask").inputmask();

                    $('.form-control').on("keypress", function(e) {
                        if (e.keyCode == 13) {
                            var that = $(this);
                            setTimeout(function () { that.parent().next().find('.form-control').focus(); }, 50);
                        }
                    });


                    $('#lista-productos-mercado-precio').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 10,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });

                    setTimeout(function() {
                        loading.hide();
                    }, 500);
                }
            },
            error: function() {
                alert('Error al realizar el proceso.');
            }
        });

    }

    async function RegionesPrecio() {


        /* var optionsDepartamentos = "<option value>Seleccionar</option>";

        $.each(JSON.parse(localStorage.getItem('region')), function(index, value) {
            optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
        });

        $("#precio-id_region").html(optionsDepartamentos); */




        await   $.ajax({
                    url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones-usuario",
                    method: "POST",
                    data:{_csrf:csrf,proceso:'precio'},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            var optionsDepartamentos = "<option value>Seleccionar</option>";

                            $.each(results.regiones, function( index, value ) {
                                optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                            });

                            $("#precio-id_region").html(optionsDepartamentos);

                        }
                    },
                    error:function(){
                        alert("Error al realizar el proceso.");
                    }
                });
                
    }

    async function Mercados(idRegion) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-opciones-mercados-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion,
                proceso:'precio'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsMercados = "<option value>Seleccionar</option>";

                    $.each(results.mercados, function(index, value) {
                        optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO_TIPO}-${value.TXT_MERCADO}</option>`;
                    });

                    $("#precio-id_mercado").html(optionsMercados);

                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    async function GruposPrecio(idMercado) {


        /* var optionsGrupo = "<option value>Seleccionar</option>";

        $.each(JSON.parse(localStorage.getItem('grupo')).grupos, function(index, value) {
            optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
        });

        $("#precio-id_producto_grupo").html(optionsGrupo); */

        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-opciones-grupos-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idMercado:idMercado,
                proceso:'precio'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsGrupo = "<option value>Seleccionar</option>";

                    $.each(results.grupos, function(index, value) {
                        optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
                    });

                    $("#precio-id_producto_grupo").html(optionsGrupo);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }


    var listaIdsProductosMercados = [];
    $('body').on('change', '.check-precio', function (e) {
        e.preventDefault();
        let idProductoMercado = $(this).attr('data-id');
        let bProductoMercado =  $(this).is(":checked");

        if(bProductoMercado){
            listaIdsProductosMercados.push(idProductoMercado);
        }else{
            listaIdsProductosMercados.splice($.inArray(idProductoMercado, listaIdsProductosMercados),1);
        }
        

    });

    $('body').on('click', '.btn-eliminar-precio', function (e) {
        e.preventDefault();
        let fecRegistro = $('#precio-fec_registro').val();
        Swal.fire({
            title: '¿Está seguro de eliminar los registros seleccionados?',
            text: "Una vez eliminado los registros ya no se podrá visualizar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url : '<?= \Yii::$app->request->BaseUrl ?>/precio/eliminar',
                    method: "POST",
                    data:{_csrf:csrf,listaIdsProductosMercados:listaIdsProductosMercados,fecRegistro:fecRegistro},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            fecRegistro = $('#precio-fec_registro').val();
                            idProductoGenero = $('#precio-id_producto_genero').val();
                            idMercado = $('#precio-id_mercado').val();

                            ProductosMercadoPrecio(idProductoGenero,idMercado,fecRegistro);
                            toastr.success('Registros eliminados');
                        }
                    },
                    error:function(){
                        toastr.error('Error al realizar el proceso.');
                    }
                });
                
            }
        });

        
        
    });



    var listaEstadosPublicados = [];
    async function EstadosPublicados() {
        await $.ajax({
            async: false,
            url: "<?= \Yii::$app->request->BaseUrl ?>/maestro/get-lista-maestros",
            method: "POST",
            data: {
                _csrf: csrf,
                idMaestro: 1
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    $.each(results.maestros, function(index, value) {
                        listaEstadosPublicados.push(value);
                    });
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }



    $('body').on('change', '#precio-id_region', function(e) {
        e.preventDefault();
        idRegion = $(this).val();
        Mercados(idRegion);
    });

    


    /* Evento de los 3 campos */
    
    $('body').on('change', '#precio-id_producto_grupo', function(e) {
        e.preventDefault();
        idProductoGrupo = $(this).val();
        idMercado = $('#precio-id_mercado').val();
        fecRegistro = $('#precio-fec_registro').val();

        if (!idProductoGrupo) {
            ListaProductosMercadoPrecio();
        }


        if (idMercado && fecRegistro && idProductoGrupo) {
            ProductosMercadoPrecio(idProductoGrupo, idMercado, fecRegistro);
        }
    });

    $('body').on('change', '#precio-id_mercado', function(e) {
        e.preventDefault();
        idMercado = $(this).val();
        GruposPrecio(idMercado);
        idProductoGrupo = $('#precio-id_producto_grupo').val();
        fecRegistro = $('#precio-fec_registro').val();
        if (!idMercado) {
            console.log("id_mercado");
            ListaProductosMercadoPrecio();
        }

        if (idMercado && fecRegistro && idProductoGrupo) {
            ProductosMercadoPrecio(idProductoGrupo, idMercado, fecRegistro);
        }
    });

    $('body').on('change', '#precio-fec_registro', function(e) {
        e.preventDefault();
        let fechaInicio = "<?= $fechaInicio ?>";
        let fechaFin = "<?= $fechaFin ?>";
        let fecha = $(this).val();

        fechaInicioArray = fechaInicio.split("-");
        fechaFinArray = fechaFin.split("-");
        fechaArray = fecha.split("-");
        
        var fechaInicioJs = new Date(fechaInicioArray[0], parseInt(fechaInicioArray[1]) - 1, fechaInicioArray[2]);
        var fechaFinJs = new Date(fechaFinArray[0], parseInt(fechaFinArray[1]) - 1, fechaFinArray[2]);
        var fechaJs = new Date(fechaArray[0], parseInt(fechaArray[1]) - 1, fechaArray[2]);

        if (!(fechaJs >= fechaInicioJs && fechaJs <= fechaFinJs)) {
            toastr.warning('La fecha indicada no pertenece al rango habilitado por el sistema');
            $(this).val('')
            return false;
        }


        
            

        fecRegistro = $(this).val();
        idProductoGrupo = $('#precio-id_producto_grupo').val();
        idMercado = $('#precio-id_mercado').val();

        if (!fecRegistro) {
            console.log("fec_registro");
            ListaProductosMercadoPrecio();
        }

        if (idMercado && fecRegistro && idProductoGrupo) {
            ProductosMercadoPrecio(idProductoGrupo, idMercado, fecRegistro);
        }
    });


    $('body').on('blur', '.btn-modificar-num_precio', function(e) {
        e.preventDefault();
        let numPrecio = $.trim($(this).val());
        let numPrecioPosicion = $(this).attr('data-numPrecioPosicion');
        let idProductoMercado = $(this).attr('data-idProductoMercado');
        var cantidadAlerta = parseInt($(this).attr('data-cantidadAlerta'));

        let fecRegistro = $('#precio-fec_registro').val();
        
        if(numPrecio){
            $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/precio/update?idProductoMercado=' + idProductoMercado + '&fecRegistro=' + fecRegistro,
                method: "POST",
                data: {
                    _csrf: csrf,
                    numPrecioPosicion: numPrecioPosicion,
                    numPrecio: numPrecio,
                    fecRegistro: fecRegistro
                },
                dataType: "Json",
                beforeSend: function(xhr, settings) {
                    //loading.show();
                },
                success: function(results) {
                    if (results && results.success && results.valor >= 1) {
                        toastr.success('Precio ' + numPrecioPosicion + ' grabado');
                        $(".num_precio_" + idProductoMercado + "_" + numPrecioPosicion).removeClass('alert-danger');
                        $(".num_precio_" + idProductoMercado + "_" + numPrecioPosicion).css('border', '');

                        for (let i = 1; i <= cantidadAlerta; i++) {
                            if ($.trim($(".num_precio_" + idProductoMercado + "_" + i).val()) == "") {
                                $(".num_precio_" + idProductoMercado + "_" + i).addClass('alert-danger');
                                $(".num_precio_" + idProductoMercado + "_" + i).css('border', '1px solid #DA1414');
                            }
                        }
                    } else {
                        toastr.error('Precio '+numPrecioPosicion + ' grabado ,Fuera de rango.<br>'+results.msg);
                        $(".num_precio_" + idProductoMercado + "_" + numPrecioPosicion).addClass('alert-danger');
                        $(".num_precio_" + idProductoMercado + "_" + numPrecioPosicion).css('border', '1px solid #DA1414');
                    }
                },
                error: function() {
                    toastr.error('Error al realizar el proceso.');
                }
            });
        }
        
    });


    function init() {
        ListaProductosMercadoPrecio();
        RegionesPrecio();
        //GruposPrecio();
        EstadosPublicados();
        //Productos();
        //ProductosMercado();
    }

    init();
</script>