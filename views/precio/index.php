<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Productos por Mercado por Precio</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de Productos por Mercado por Precio</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Región</label>
                            <select id="precio-id_region" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Mercado</label>
                            <select name="Precio[ID_MERCADO]" id="precio-id_mercado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha</label>
                            <input type="date" name="Precio[FEC_REGISTRO]" id="precio-fec_registro" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Grupo</label>
                            <select id="precio-id_producto_grupo" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Producto</label>
                            <select name="Precio[ID_PRODUCTO_GENERO]" id="precio-id_producto_genero" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/precio/create" class="btn btn-success">Agregar</a>
                        </div>
                    </div>
                </div> -->

                <table id="lista-productos-mercado-precio" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Fecha Registro</th>
                        <th>Región</th>
                        <th>Mercado</th>
                        <th>Grupo</th>
                        <th>Producto</th>
                        <th>Variedad</th>
                        <th>Envase</th>
                        <th>Precio 1</th>
                        <th>Precio 2</th>
                        <th>Precio 3</th>
                        <th>Precio 4</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');
    var idRegion;
    var idMercado;
    var idProductoGrupo;
    var idProductoGenero;
    var fecRegistro;

    $('#lista-productos-mercado-precio').DataTable();

    async function ProductosMercadoPrecio() {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/precio/get-lista-productos-mercado-precio-index',
            method: 'POST',
            data: {
                _csrf: csrf,
                idRegion: idRegion,
                idMercado: idMercado,
                idProductoGrupo: idProductoGrupo,
                idProductoGenero: idProductoGenero,
                fecRegistro: fecRegistro
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var productosMercadoPrecio = "";
                    $('#lista-productos-mercado-precio').DataTable().destroy();

                    $.each(results.productosMercadoPrecio, function(index, value) {
                        txtUnidadMedida= ((value.TXT_UNIDAD_MEDIDA)?value.TXT_UNIDAD_MEDIDA:"");
                        numEquivalencia= ((value.NUM_EQUIVALENCIA)?value.NUM_EQUIVALENCIA:"");
                        productosMercadoPrecio = productosMercadoPrecio + "<tr>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.FEC_REGISTRO) ? value.FEC_REGISTRO : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.TXT_DEPARTAMENTO) ? value.TXT_DEPARTAMENTO : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.TXT_MERCADO) ? value.TXT_MERCADO : "") + "</td>";
                            /* productosMercadoPrecio = productosMercadoPrecio + "<td> " + value.TXT_CODIGO_PRODUCTO + "</td>"; */
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.TXT_PRODUCTO_GRUPO) ? value.TXT_CODIGO_GRUPO + "-" + value.TXT_PRODUCTO_GRUPO : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.TXT_PRODUCTO_GENERO) ? value.TXT_CODIGO_GENERO + "-" + value.TXT_PRODUCTO_GENERO : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.TXT_PRODUCTO) ? value.TXT_CODIGO_PRODUCTO + "-" + value.TXT_PRODUCTO : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + txtUnidadMedida + "-" + numEquivalencia : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.NUM_PRECIO_1) ? value.NUM_PRECIO_1 : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.NUM_PRECIO_2) ? value.NUM_PRECIO_2 : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.NUM_PRECIO_3) ? value.NUM_PRECIO_3 : "") + "</td>";
                            productosMercadoPrecio = productosMercadoPrecio + "<td> " + ((value.NUM_PRECIO_4) ? value.NUM_PRECIO_4 : "") + "</td>";
                        productosMercadoPrecio = productosMercadoPrecio + "</tr>";
                    });

                    $('#lista-productos-mercado-precio tbody').html(productosMercadoPrecio);
                    $('#lista-productos-mercado-precio').DataTable({
                        "dom": "Bfrtip",
                        buttons: [
                            'excelHtml5',
                            'csvHtml5',
                        ],
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 10,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });

                    setTimeout(function() {
                        loading.hide();
                    }, 500);
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }


    function ListaProductosMercadoPrecio(){
    
        $('#lista-productos-mercado-precio').DataTable().destroy();
        $('#lista-productos-mercado-precio tbody').html("");
        $('#lista-productos-mercado-precio').DataTable({
                                "dom": "Bfrtip",
                                buttons: [
                                    'excelHtml5',
                                    'csvHtml5',
                                ],
                                "paging": true,
                                "lengthChange": true,
                                "searching": false,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "pageLength" : 10,
                                "language": {
                                    "sProcessing":    "Procesando...",
                                    "sLengthMenu":    "Mostrar _MENU_ registros",
                                    "sZeroRecords":   "No se encontraron resultados",
                                    "sEmptyTable":    "Ningun dato disponible en esta lista",
                                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":   "",
                                    "sSearch":        "Buscar:",
                                    "sUrl":           "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":    "Último",
                                        "sNext":    "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    }
                                },
                            });

                            setTimeout(function() {
                        loading.hide();
                    }, 500);
    }


    async function RegionesPrecio() {


        /* var optionsDepartamentos = "<option value>Seleccionar</option>";

        $.each(JSON.parse(localStorage.getItem('region')), function(index, value) {
            optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
        });

        $("#precio-id_region").html(optionsDepartamentos); */




        await   $.ajax({
                     url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones-usuario",
                     method: "POST",
                     data:{_csrf:csrf,proceso:'precio'},
                     dataType:"Json",
                     beforeSend:function(xhr, settings)
                     {
                         //loading.show();
                     },
                     success:function(results)
                     {   
                         if(results && results.success){
                             var optionsDepartamentos = "<option value>Seleccionar</option>";

                             $.each(results.regiones, function( index, value ) {
                                 optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                             });

                             $("#precio-id_region").html(optionsDepartamentos);

                         }
                     },
                     error:function(){
                         alert("Error al realizar el proceso.");
                     }
                 });
                
    }

    async function Mercados(idRegion) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-opciones-mercados-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion,
                proceso:'precio'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsMercados = "<option value>Seleccionar</option>";

                    $.each(results.mercados, function(index, value) {
                        optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                    });

                    $("#precio-id_mercado").html(optionsMercados);

                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    async function GruposPrecio(idMercado) {


        /* var optionsGrupo = "<option value>Seleccionar</option>";

        $.each(JSON.parse(localStorage.getItem('grupo')).grupos, function(index, value) {
            optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
        });

        $("#precio-id_producto_grupo").html(optionsGrupo); */

        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-opciones-grupos-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idMercado:idMercado,
                proceso:'precio'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsGrupo = "<option value>Seleccionar</option>";

                    $.each(results.grupos, function(index, value) {
                        optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
                    });

                    $("#precio-id_producto_grupo").html(optionsGrupo);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    async function GenerosPrecio(idProductoGrupo) {
        

        await $.ajax({
             url: "<?= \Yii::$app->request->BaseUrl ?>/genero/get-lista-opciones-generos-usuario",
             method: "POST",
             data: {
                 _csrf: csrf,
                 idProductoGrupo: idProductoGrupo,
                proceso:'precio'
             },
             dataType: "Json",
             beforeSend: function(xhr, settings) {
                 //loading.show();
             },
             success: function(results) {
                 if (results && results.success) {
                     var optionsGenero = "<option value>Seleccionar</option>";

                     $.each(results.generos, function(index, value) {
                         optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_PRODUCTO_GENERO}</option>`;
                     });

                     $("#precio-id_producto_genero").html(optionsGenero);
                 }
             },
             error: function() {
                 alert("Error al realizar el proceso.");
             }
         });
    }



    

    $('body').on('change', '#precio-id_region', function(e) {
        e.preventDefault();
        idRegion = $(this).val();
        Mercados(idRegion);
        if(fecRegistro){
            ProductosMercadoPrecio();
        }
    });

    $('body').on('change', '#precio-id_mercado', function(e) {
        e.preventDefault();
        idMercado = $(this).val();
        GruposPrecio(idMercado);
        if(fecRegistro){
            ProductosMercadoPrecio();
        }
    });

    $('body').on('change', '#precio-id_producto_grupo', function (e) {
        e.preventDefault();
        idProductoGrupo = $(this).val();
        GenerosPrecio(idProductoGrupo);
        if(fecRegistro){
            ProductosMercadoPrecio();
        }
    });

    $('body').on('change', '#precio-id_producto_genero', function(e) {
        e.preventDefault();
        idProductoGenero = $(this).val();
        if(fecRegistro){
            ProductosMercadoPrecio();
        }
    });

    $('body').on('change', '#precio-fec_registro', function(e) {
        e.preventDefault();
        fecRegistro = $(this).val();
        ProductosMercadoPrecio();
    });


    function init() {
        ListaProductosMercadoPrecio();
        RegionesPrecio();
        //GruposPrecio();

        setTimeout(function() {
                        loading.hide();
                    }, 500);
    }

    init();
</script>
