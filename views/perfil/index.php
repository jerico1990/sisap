<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Perfil</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de perfiles</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_descripcion">Descripción</label>
                            <input type="text" id="txt_descripcion" class="form-control">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-agregar-perfil">Agregar</button>
                        </div>
                        
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <label for="">&nbsp&nbsp</label> <br>
                        <button style="float: right;" class="btn btn-primary btn-buscar-perfil">Buscar</button>
                    </div>
                </div>

                <table id="lista-perfiles" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Descripción</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('.staticBackdrop');

$('#lista-perfiles').DataTable();

Perfiles();
async function Perfiles(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/perfil/get-lista-perfiles',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var perfiles ="";
                        $('#lista-perfiles').DataTable().destroy();
                        
                        $.each(results.perfiles, function( index, value ) {
                            perfiles = perfiles + "<tr>";
                                perfiles = perfiles + "<td> " + value.TXT_PERFIL + "</td>";
                                perfiles = perfiles + "<td>" ;
                                    perfiles = perfiles + '<button data-id="' + value.ID_PERFIL + '" class="btn btn-info btn-sm btn-modificar-perfil" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    perfiles = perfiles + '<button data-id="' + value.ID_PERFIL + '" class="btn btn-info btn-sm btn-asignar-perfil-menu" href="#"><i class="fas fa-list-alt"></i></button> ';
                                    perfiles = perfiles + '<button data-id="' + value.ID_PERFIL + '" class="btn btn-danger btn-sm btn-eliminar-perfil" href="#"><i class="fas fa-trash"></i></button>';
                                perfiles = perfiles +"</td>";
                            perfiles = perfiles + "</tr>";
                        });
                        
                        $('#lista-perfiles tbody').html(perfiles);
                        $('#lista-perfiles').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": true,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.hide(); }, 1000);
                        $(".dataTables_filter").hide();
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function PerfilMenu(idPerfil){
    //debugger;
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/perfil-menu/get-lista-perfil-menu',
                method: 'POST',
                data:{_csrf:csrf,idPerfil:idPerfil},
                dataType:'Json',
                beforeSend:function(){
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var menus ="";
                        $('#lista-perfil-menu').DataTable().destroy();
                        
                        $.each(results.menus, function( index, value ) {
                            menus = menus + "<tr>";
                                menus = menus + "<td> " + value.TXT_MODULO + "</td>";
                                menus = menus + "<td> " + value.TXT_CATEGORIA + "</td>";
                                menus = menus + "<td> " + value.TXT_MENU + "</td>";
                                menus = menus + "<td>" ;
                                    if(value.ID_PERFIL_MENU){
                                        menus = menus + '<div class="custom-control custom-checkbox mb-3"> <input type="checkbox" data-id="' + value.ID_MENU + '" data-idPerfil="' + idPerfil + '" class="custom-control-input asignar_menu" id="perfil_menu_' + value.ID_MENU + '" checked><label class="custom-control-label" for="perfil_menu_' + value.ID_MENU + '"></label></div>';
                                    }else{
                                        menus = menus + '<div class="custom-control custom-checkbox mb-3"> <input type="checkbox" data-id="' + value.ID_MENU + '" data-idPerfil="' + idPerfil + '" class="custom-control-input asignar_menu" id="perfil_menu_' + value.ID_MENU + '"><label class="custom-control-label" for="perfil_menu_' + value.ID_MENU + '"></label></div>';
                                    }
                                    

                                    //menu = menu + '<button data-id="' + value.ID_PERFIL + '" class="btn btn-info btn-sm btn-modificar-perfil" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                menus = menus +"</td>";
                            menus = menus + "</tr>";
                        });
                        
                        $('#lista-perfil-menu tbody').html(menus);
                        $('#lista-perfil-menu').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.hide(); }, 1000);
                       
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

    //Buscar
    $('body').on('click', '.btn-buscar-perfil', function(e) {
        e.preventDefault();
        var descripcion=$("#txt_descripcion").val();
        $('#lista-perfiles').DataTable().column(0).search(descripcion).draw();
              
    });

//agregar

$('body').on('click', '.btn-agregar-perfil', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/perfil/create');
    $('#modal').modal('show');
});

//modificar
$('body').on('click', '.btn-modificar-perfil', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/perfil/update?id=' + id);
    $('#modal').modal('show');
});

//agregar perfil menu 
$('body').on('click', '.btn-asignar-perfil-menu', function (e) {
    e.preventDefault();
    var idPerfil = $(this).attr('data-id');

    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/perfil-menu/asignar',function(){
        PerfilMenu(idPerfil);
    });
    $('#modal').modal('show');
});

//modificar perfil menu
/* $('body').on('click', '.btn-modificar-perfil-menu', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/perfil/update?id=' + id);
    $('#modal').modal('show');
}); */

//grabar
$('body').on('click', '.btn-grabar-perfil', function (e) {

    e.preventDefault();
    var form = $('#formPerfil');
    var formData = $('#formPerfil').serializeArray();

    var error = "";
    var txtPerfil = $.trim($("[name=\"Perfil[TXT_PERFIL]\"]").val());
    var flgHabilitado = $.trim($("[name=\"Perfil[FLG_HABILITADO]\"]").val());


    if (!txtPerfil) {
        $('#perfil-txt_perfil').addClass('alert-danger');
        $('#perfil-txt_perfil').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#perfil-txt_perfil').removeClass('alert-danger');
        $('#perfil-txt_perfil').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (!flgHabilitado) {
        $('#perfil-flg_habilitado').addClass('alert-danger');
        $('#perfil-flg_habilitado').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 2";
    } else {
        $('#perfil-flg_habilitado').removeClass('alert-danger');
        $('#perfil-flg_habilitado').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (error != "") {
        return false;
    }


    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.show();
        },
        success: function (results) {
            if(results.success){
                //setTimeout(function(){ loading.hide(); }, 1000);
                Perfiles();
                $('#modal').modal('hide');
            }
        },
    });
});

$("body").on("change", ".asignar_menu", function (e) {
    e.preventDefault();
    idMenu = $(this).attr('data-id');
    idPerfil = $(this).attr('data-idPerfil');

    if($("[id=\"perfil_menu_" + idMenu +"\"]:checked").val()){
        asignarPermiso(idMenu,idPerfil,true);
    }else{
        asignarPermiso(idMenu,idPerfil,false);
    }
});

async function asignarPermiso(idMenu,idPerfil,flgActivo){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/perfil-menu/asignar-menu",
                method: "POST",
                data:{_csrf:csrf,idMenu:idMenu,idPerfil:idPerfil,flgActivo:flgActivo},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        setTimeout(function(){ loading.hide(); }, 1000);
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}





</script>
