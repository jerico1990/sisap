<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Parametro</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de parametros</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <table id="lista-parametros" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Descripción</th>
                        <th>Valor</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('.staticBackdrop');

$('#lista-parametros').DataTable();

Parametros();
async function Parametros(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/parametro/get-lista-parametros',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var parametros ="";
                        $('#lista-parametros').DataTable().destroy();
                        
                        $.each(results.parametros, function( index, value ) {
                            parametros = parametros + "<tr>";
                                parametros = parametros + "<td> " + value.TXT_PARAMETRO + "</td>";
                                parametros = parametros + "<td> " + value.NUM_VALOR + "</td>";
                                parametros = parametros + "<td> " + value.TXT_DESCRIPCION_HABILITADO + "</td>";
                                parametros = parametros + "<td>" ;
                                    parametros = parametros + '<button data-id="' + value.ID_PARAMETRO + '" class="btn btn-info btn-sm btn-modificar-parametro" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                parametros = parametros +"</td>";
                            parametros = parametros + "</tr>";
                        });
                        
                        $('#lista-parametros tbody').html(parametros);
                        $('#lista-parametros').DataTable({
                            "dom": "Bfrtip",
                            buttons: [
                                'excelHtml5',
                                'csvHtml5',
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": true,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.hide(); }, 1000);
                        $(".dataTables_filter").hide();
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}



//modificar
$('body').on('click', '.btn-modificar-parametro', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/parametro/update?id='+id);
    $('#modal').modal('show');
});

//grabar

$('body').on('click', '.btn-grabar-parametro', function (e) {

    e.preventDefault();
    var form = $('#formParametro');
    var formData = $('#formParametro').serializeArray();

    var error = "";
    var txtParametro = $.trim($("[name=\"Parametro[TXT_PARAMETRO]\"]").val());
    var numValor = $.trim($("[name=\"Parametro[NUM_VALOR]\"]").val());
    var flgHabilitado = $.trim($("[name=\"Parametro[FLG_HABILITADO]\"]").val());

    if (!txtParametro) {
        $('#parametro-txt_parametro').addClass('alert-danger');
        $('#parametro-txt_parametro').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#parametro-txt_parametro').removeClass('alert-danger');
        $('#parametro-txt_parametro').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (!numValor) {
        $('#parametro-num_valor').addClass('alert-danger');
        $('#parametro-num_valor').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#parametro-num_valor').removeClass('alert-danger');
        $('#parametro-num_valor').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (!flgHabilitado) {
        $('#parametro-flg_habilitado').addClass('alert-danger');
        $('#parametro-flg_habilitado').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#parametro-flg_habilitado').removeClass('alert-danger');
        $('#parametro-flg_habilitado').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (error != "") {
        return false;
    }

    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            toastr.error('Error al realizar el proceso.');
        },
        beforeSend:function()
        {
            loading.show();
        },
        success: function (results) {
            if(results.success){
                //setTimeout(function(){ loading.hide(); }, 1000);
                Parametros();
                $('#modal').modal('hide');
                toastr.success('Registro grabado');
            }else{
                    if(results.msg==0){
                        toastr.warning('No se pudo grabar el registro, realizarlo nuevamente'); 
                    }else if(results.msg==1){
                        setTimeout(function() {
                            loading.hide();
                        }, 1000);
                        toastr.warning('La abreviatura para la unidad de medida ya existe'); 
                    }
                    
                }
        },
    });
});




</script>
