<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formProcedencia','class' => 'form-horizontal']]); ?>
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Producto</label>
            <select id="procedencia-cod_producto" class="form-control" name="Procedencia[cod_producto]">
                <option value>Seleccionar</option>
            </select>
        </div>
        <div class="form-group">
            <label>Región</label>
            <select id="procedencia-cod_region" class="form-control" name="Procedencia[cod_region]">
                <option value>Seleccionar</option>
            </select>
        </div>
        <div class="form-group">
            <label>Provincia</label>
            <select id="procedencia-cod_ubigeo" class="form-control" name="Procedencia[cod_ubigeo]">
                <option value>Seleccionar</option>
            </select>
        </div>
        <div class="form-group">
            <label>Estado</label>
            <select id="procedencia-flg_activo" class="form-control" name="Procedencia[flg_activo]">
                <option value>Seleccionar</option>
                <option value="1" <?= (($model->flg_activo=="1")?"selected":"") ?> >Activo</option>
                <option value="2" <?= (($model->flg_activo=="2")?"selected":"") ?> >Desactivo</option>
            </select>
        </div>
    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success btn-grabar-procedencia">Grabar</button>
    </div>
</div>
<?php ActiveForm::end(); ?>