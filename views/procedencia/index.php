<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Procedencia</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de procedencias</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-agregar-procedencia">Agregar</button>
                        </div>
                    </div>
                </div>

                <table id="lista-procedencias" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Código ubigeo</th>
                        <th>Ubigeo</th>
                        <th>Producto</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');

$('#lista-procedencias').DataTable();

Procedencias();
async function Procedencias(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/procedencia/get-lista-procedencias',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var procedencias ="";
                        $('#lista-procedencias').DataTable().destroy();
                        
                        $.each(results.procedencias, function( index, value ) {
                            procedencias = procedencias + "<tr>";
                                procedencias = procedencias + "<td> " + value.cod_ubigeo + "</td>";
                                procedencias = procedencias + "<td> " + value.txt_departamento + "/" +  value.txt_provincia + "</td>";
                                procedencias = procedencias + "<td> " + value.txt_dscvariedad + "</td>";
                                procedencias = procedencias + "<td>" ;
                                    procedencias = procedencias + '<button data-codProducto="' + value.cod_producto + '" data-codUbigeo="' + value.codUbigeo + '" class="btn btn-info btn-sm btn-modificar-procedencia" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    procedencias = procedencias + '<button data-codProducto="' + value.cod_producto + '" class="btn btn-danger btn-sm btn-eliminar-procedencia" href="#"><i class="fas fa-trash"></i></button>';
                                procedencias = procedencias +"</td>";
                            procedencias = procedencias + "</tr>";
                        });
                        
                        $('#lista-procedencias tbody').html(procedencias);
                        $('#lista-procedencias').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function Productos(codProducto){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/variedad/get-lista-variedades",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsProductos = "<option value>Seleccionar</option>";

                        $.each(results.variedades, function( index, value ) {
                            optionsProductos = optionsProductos + `<option value='${value.cod_producto}'>${value.txt_dscvariedad}</option>`;
                        });

                        $("#procedencia-cod_producto").html(optionsProductos);

                        if(codProducto){
                            $("#procedencia-cod_producto").val(codProducto);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

async function Regiones(codRegion){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsDepartamentos = "<option value>Seleccionar</option>";

                        $.each(results.regiones, function( index, value ) {
                            optionsDepartamentos = optionsDepartamentos + `<option value='${value.cod_dep}'>${value.txt_departamento}</option>`;
                        });

                        $("#procedencia-cod_region").html(optionsDepartamentos);

                        if(codRegion){
                            $("#procedencia-cod_region").val(codRegion);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

function Provincias(codRegion,codProvincia){
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-provincias",
        method: "POST",
        data:{_csrf:csrf,codRegion:codRegion},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                var optionsProvincias = "<option value>Seleccionar</option>";
                $.each(results.provincias, function( index, value ) {
                    optionsProvincias = optionsProvincias + `<option value='${value.cod_prov}'>${value.txt_provincia}</option>`;
                });
                $("#procedencia-cod_ubigeo").html(optionsProvincias);
                

                if(codProvincia){
                    $("#procedencia-cod_ubigeo").val(codProvincia);
                }
                
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
}

$("body").on("change", "#procedencia-cod_region", function (e) {
    e.preventDefault();
    $("#procedencia-cod_ubigeo").html("<option value>Seleccionar</option>");
    if($(this).val()){
        codRegion = $(this).val();
        Provincias(codRegion);
    }

});


//agregar

$('body').on('click', '.btn-agregar-procedencia', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/procedencia/create',function(){
        Regiones();
        Productos();
    });
    $('#modal').modal('show');
});

//modificar
$('body').on('click', '.btn-modificar-procedencia', function (e) {
    e.preventDefault();
    var codProducto = $(this).attr('data-codProducto');
    var codUbigeo = $(this).attr('data-codUbigeo');
    
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/procedencia/update?codProducto='+codProducto+'&codUbigeo='+codUbigeo);
    $('#modal').modal('show');
});


//grabar

$('body').on('click', '.btn-grabar-procedencia', function (e) {

    e.preventDefault();
    var form = $('#formProcedencia');
    var formData = $('#formProcedencia').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                //setTimeout(function(){ loading.modal("hide"); }, 2000);
                Procedencias();
                $('#modal').modal('hide');
            }
        },
    });
});




</script>
