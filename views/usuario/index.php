<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Usuario</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de usuarios</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_dni">DNI</label>
                            <input type="text" id="txt_dni" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_nombres">Nombres y Apellidos</label>
                            <input type="text" id="txt_nombres" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sel_perfil">Perfil</label>
                            <select class="form-control" name="sel_perfil" id="sel_perfil">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sel_departamento">Región</label>
                            <select class="form-control" name="sel_departamento" id="sel_departamento">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sel_provincia">Provincia</label>
                            <select class="form-control" name="sel_provincia" id="sel_provincia">
                            </select>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sel_distrito">Distrito</label>
                            <select class="form-control" name="sel_distrito" id="sel_distrito">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-agregar-usuario">Agregar</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <label for="">&nbsp&nbsp</label> <br>
                        <button style="float: right;" class="btn btn-primary btn-buscar-usuario">Buscar</button>
                    </div>
                </div>

                <table id="lista-usuarios" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Usuario</th>
                        <th>Nombres y apellidos</th>
                        <th>Ubigeo</th>
                        <th>Perfil</th>
                        <!-- <th>Telefono</th> -->
                        <th>Estado</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');
    Perfiles();
    Regiones("", "sel_departamento");
    $('#lista-usuarios').DataTable();

    Usuarios();
    async function Usuarios() {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/usuario/get-lista-usuarios',
            method: 'POST',
            data: {
                _csrf: csrf
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var usuarios = "";
                    $('#lista-usuarios').DataTable().destroy();

                    $.each(results.usuarios, function(index, value) {

                        usuarios = usuarios + "<tr>";
                        usuarios = usuarios + "<td> " + ((value.TXT_USUARIO) ? value.TXT_USUARIO : "") + "</td>";
                        usuarios = usuarios + "<td> " + ((value.TXT_NOMBRES) ? value.TXT_NOMBRES : "") + "</td>";
                        usuarios = usuarios + "<td> " + ((value.ID_UBIGEO) ? value.TXT_DEPARTAMENTO + "/" + value.TXT_PROVINCIA + "/" + value.TXT_DISTRITO : "") + "</td>";
                        usuarios = usuarios + "<td> " + ((value.TXT_PERFILES) ? value.TXT_PERFILES : "") + "</td>";
                        //usuarios = usuarios + "<td> " + ((value.TXT_NUMERO_CONTACTO) ? value.TXT_NUMERO_CONTACTO : "") + "</td>";
                        usuarios = usuarios + "<td> " + ((value.FLG_HABILITADO) ? ((value.FLG_HABILITADO == "1") ? "Activo" : "Desactivo") : "") + "</td>";
                        usuarios = usuarios + "<td>";
                        usuarios = usuarios + '<button data-id="' + value.ID_USUARIO + '" data-idUbigeo="' + value.ID_UBIGEO + '" class="btn btn-info btn-sm btn-modificar-usuario" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                        usuarios = usuarios + '<button data-id="' + value.ID_USUARIO + '" class="btn btn-info btn-sm btn-asignar-usuario-perfil" href="#"><i class="fas fa-tasks"></i></button> ';
                        usuarios = usuarios + '<button data-id="' + value.ID_USUARIO + '" class="btn btn-info btn-sm btn-asignar-usuario-mercado" href="#"><i class="fas fa-columns"></i></button> ';

                        usuarios = usuarios + '<button data-id="' + value.ID_USUARIO + '" class="btn btn-danger btn-sm btn-eliminar-usuario" href="#"><i class="fas fa-trash"></i></button>';
                        usuarios = usuarios + "</td>";
                        usuarios = usuarios + "</tr>";
                    });

                    $('#lista-usuarios tbody').html(usuarios);
                    $('#lista-usuarios').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": false,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 10,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });

                    setTimeout(function() {
                        loading.hide();
                    }, 2000);
                    $(".dataTables_filter").hide();
                }
            },
            error: function() {
                alert('Error al realizar el proceso.');
            }
        });
    }

    async function UsuarioPerfil(idUsuario) {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/usuario-perfil/get-lista-usuario-perfil',
            method: 'POST',
            data: {
                _csrf: csrf,
                idUsuario: idUsuario
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var perfiles = "";
                    $('#lista-usuario-perfiles').DataTable().destroy();

                    $.each(results.perfiles, function(index, value) {
                        perfiles = perfiles + "<tr>";
                        perfiles = perfiles + "<td> " + value.TXT_PERFIL + "</td>";
                        perfiles = perfiles + "<td>";
                        if (value.ID_USUARIO_PERFIL) {
                            perfiles = perfiles + '<div class="custom-control custom-checkbox mb-3"> <input type="checkbox" data-id="' + value.ID_PERFIL + '" data-idUsuario="' + idUsuario + '" class="custom-control-input asignar_perfil" id="usuario_perfil_' + value.ID_PERFIL + '" checked><label class="custom-control-label" for="usuario_perfil_' + value.ID_PERFIL + '"></label></div>';
                        } else {
                            perfiles = perfiles + '<div class="custom-control custom-checkbox mb-3"> <input type="checkbox" data-id="' + value.ID_PERFIL + '" data-idUsuario="' + idUsuario + '" class="custom-control-input asignar_perfil" id="usuario_perfil_' + value.ID_PERFIL + '"><label class="custom-control-label" for="usuario_perfil_' + value.ID_PERFIL + '"></label></div>';
                        }


                        //menu = menu + '<button data-id="' + value.ID_PERFIL + '" class="btn btn-info btn-sm btn-modificar-perfil" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                        perfiles = perfiles + "</td>";
                        perfiles = perfiles + "</tr>";
                    });

                    $('#lista-usuario-perfiles tbody').html(perfiles);
                    $('#lista-usuario-perfiles').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": false,
                        "ordering": false,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 5,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });


                    setTimeout(function() {
                        loading.hide();
                    }, 2000);

                }
            },
            error: function() {
                alert('Error al realizar el proceso.');
            }
        });
    }

    $("body").on("change", "#sel_departamento", function(e) {
        var idRegion = $(this).val();
        Provincias(idRegion, "", "sel_provincia");
        $("#sel_distrito").html("<option value>Seleccionar</option>");

    });

    $("body").on("change", "#sel_provincia", function(e) {
        var idProvincia = $(this).val();
        Distritos(idProvincia, "", "sel_distrito");
    });

    //Buscar
    $('body').on('click', '.btn-buscar-usuario', function(e) {
        e.preventDefault();
        var dni = $("#txt_dni").val();
        var nombres = $("#txt_nombres").val();
        var perfil = $("#sel_perfil option:selected").text();
        //debugger;
        var region = $("#sel_departamento option:selected").text();
        var provincia = $("#sel_provincia option:selected").text();
        var distrito = $("#sel_distrito option:selected").text();

        $('#lista-usuarios').DataTable().column(0).search(dni).draw();
        $('#lista-usuarios').DataTable().column(1).search(nombres).draw();


        if (region != "Seleccionar" && region != "" && provincia != "Seleccionar" && provincia != "" && distrito != "Seleccionar" && distrito != "") {
            $('#lista-usuarios').DataTable().column(2).search(region + "/" + provincia + "/" + distrito).draw();
        } else if (region != "Seleccionar" && region != "" && provincia != "Seleccionar" && provincia != "") {
            $('#lista-usuarios').DataTable().column(2).search(region + "/" + provincia + "/").draw();
        } else if (region != "Seleccionar" && region != "") {
            $('#lista-usuarios').DataTable().column(2).search(region + "/").draw();
        } else {
            $('#lista-usuarios').DataTable().column(2).search("").draw();
        }


        if (perfil != "Seleccionar" && perfil != "") {
            $('#lista-usuarios').DataTable().column(3).search(perfil).draw();
        } else {
            $('#lista-usuarios').DataTable().column(3).search("").draw();
        }


    });

    async function UsuarioMercado(idUsuario) {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/usuario-mercado/get-lista-usuario-mercado',
            method: 'POST',
            data: {
                _csrf: csrf,
                idUsuario: idUsuario
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var mercados = "";
                    $('#lista-usuario-mercados').DataTable().destroy();

                    $.each(results.mercados, function(index, value) {
                        mercados = mercados + "<tr>";
                        mercados = mercados + "<td> " + value.TXT_DEPARTAMENTO + "</td>";
                        mercados = mercados + "<td> " + value.TXT_PROVINCIA + "</td>";
                        mercados = mercados + "<td> " + value.TXT_DISTRITO + "</td>";
                        mercados = mercados + "<td> " + value.TXT_MERCADO + "</td>";
                        mercados = mercados + "<td>";
                        if (value.ID_USUARIO_MERCADO) {
                            mercados = mercados + '<div class="custom-control custom-checkbox mb-3"> <input type="checkbox" data-id="' + value.ID_MERCADO + '" data-idUsuario="' + idUsuario + '" class="custom-control-input asignar_mercado" id="usuario_mercado_' + value.ID_MERCADO + '" checked><label class="custom-control-label" for="usuario_mercado_' + value.ID_MERCADO + '"></label></div>';
                        } else {
                            mercados = mercados + '<div class="custom-control custom-checkbox mb-3"> <input type="checkbox" data-id="' + value.ID_MERCADO + '" data-idUsuario="' + idUsuario + '" class="custom-control-input asignar_mercado" id="usuario_mercado_' + value.ID_MERCADO + '"><label class="custom-control-label" for="usuario_mercado_' + value.ID_MERCADO + '"></label></div>';
                        }


                        //menu = menu + '<button data-id="' + value.ID_PERFIL + '" class="btn btn-info btn-sm btn-modificar-perfil" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                        mercados = mercados + "</td>";
                        mercados = mercados + "</tr>";
                    });

                    $('#lista-usuario-mercados tbody').html(mercados);
                    $('#lista-usuario-mercados').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": false,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 5,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });

                    setTimeout(function() {
                        loading.hide();
                    }, 2000);
                }
            },
            error: function() {
                alert('Error al realizar el proceso.');
            }
        });
    }

    //agregar

    $('body').on('click', '.btn-agregar-usuario', function(e) {
        e.preventDefault();
        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/usuario/create', function() {
            Regiones("", "usuario-id_region");
            //Perfil();
        });
        $('#modal').modal('show');
    });

    //modificar
    $('body').on('click', '.btn-modificar-usuario', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var idPerfiles = $(this).attr('data-idPerfiles');
        var idDistrito = ($(this).attr('data-idUbigeo') != "null") ? $(this).attr('data-idUbigeo') : "";
        var idProvincia = (idDistrito != "null") ? idDistrito.substr(0, 4) : "";
        var idRegion = (idDistrito != "null") ? idDistrito.substr(0, 2) : "";


        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/usuario/update?id=' + id, function() {
            Regiones(idRegion, "usuario-id_region");
            Provincias(idRegion, idProvincia, "usuario-id_provincia");
            Distritos(idProvincia, idDistrito, "usuario-id_ubigeo");
            //Perfil(idPerfiles);
        });
        $('#modal').modal('show');
    });

    //agregar perfil menu 
    $('body').on('click', '.btn-asignar-usuario-perfil', function(e) {
        e.preventDefault();
        var idUsuario = $(this).attr('data-id');

        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/usuario-perfil/asignar', function() {
            UsuarioPerfil(idUsuario);
        });
        $('#modal').modal('show');
    });

    //agregar usuario mercado 
    $('body').on('click', '.btn-asignar-usuario-mercado', function(e) {
        e.preventDefault();
        var idUsuario = $(this).attr('data-id');

        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/usuario-mercado/asignar', function() {
            UsuarioMercado(idUsuario);
        });
        $('#modal').modal('show');
    });


    //grabar

    $('body').on('click', '.btn-grabar-usuario', function(e) {

        e.preventDefault();
        var form = $('#formUsuario');
        var formData = $('#formUsuario').serializeArray();

        var error = "";
        var txtUsuario = $.trim($("[name=\"Usuario[TXT_USUARIO]\"]").val());
        var txtClave = $.trim($("[name=\"Usuario[TXT_CLAVE]\"]").val());
        var txtNombres = $.trim($("[name=\"Usuario[TXT_NOMBRES]\"]").val());
        var txtApellidoPaterno = $.trim($("[name=\"Usuario[TXT_APELLIDO_PATERNO]\"]").val());
        var txtApellidoMaterno = $.trim($("[name=\"Usuario[TXT_APELLIDO_MATERNO]\"]").val());
        var txtEmail = $.trim($("[name=\"Usuario[TXT_EMAIL]\"]").val());
        var txtNumeroContacto = $.trim($("[name=\"Usuario[TXT_NUMERO_CONTACTO]\"]").val());
        var idRegion = $.trim($("[name=\"Usuario[ID_REGION]\"]").val());
        var idProvincia = $.trim($("[name=\"Usuario[ID_PROVINCIA]\"]").val());
        var idUbigeo = $.trim($("[name=\"Usuario[ID_UBIGEO]\"]").val());
        var flgHabilitado = $.trim($("[name=\"Usuario[FLG_HABILITADO]\"]").val());

        if (!txtUsuario) {
            $('#usuario-txt_usuario').addClass('alert-danger');
            $('#usuario-txt_usuario').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#usuario-txt_usuario').removeClass('alert-danger');
            $('#usuario-txt_usuario').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!txtClave) {
            $('#usuario-txt_clave').addClass('alert-danger');
            $('#usuario-txt_clave').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 2";
        } else {
            $('#usuario-txt_clave').removeClass('alert-danger');
            $('#usuario-txt_clave').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!txtNombres) {
            $('#usuario-txt_nombres').addClass('alert-danger');
            $('#usuario-txt_nombres').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 3";
        } else {
            $('#usuario-txt_nombres').removeClass('alert-danger');
            $('#usuario-txt_nombres').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!txtApellidoPaterno) {
            $('#usuario-txt_apellido_paterno').addClass('alert-danger');
            $('#usuario-txt_apellido_paterno').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 4";
        } else {
            $('#usuario-txt_apellido_paterno').removeClass('alert-danger');
            $('#usuario-txt_apellido_paterno').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!txtApellidoMaterno) {
            $('#usuario-txt_apellido_materno').addClass('alert-danger');
            $('#usuario-txt_apellido_materno').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 5";
        } else {
            $('#usuario-txt_apellido_materno').removeClass('alert-danger');
            $('#usuario-txt_apellido_materno').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!txtEmail) {
            $('#usuario-txt_email').addClass('alert-danger');
            $('#usuario-txt_email').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 5";
        } else {
            $('#usuario-txt_email').removeClass('alert-danger');
            $('#usuario-txt_email').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!txtNumeroContacto) {
            $('#usuario-txt_numero_contacto').addClass('alert-danger');
            $('#usuario-txt_numero_contacto').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 5";
        } else {
            $('#usuario-txt_numero_contacto').removeClass('alert-danger');
            $('#usuario-txt_numero_contacto').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!idRegion) {
            $('#usuario-id_region').addClass('alert-danger');
            $('#usuario-id_region').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 5";
        } else {
            $('#usuario-id_region').removeClass('alert-danger');
            $('#usuario-id_region').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!idProvincia) {
            $('#usuario-id_provincia').addClass('alert-danger');
            $('#usuario-id_provincia').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 5";
        } else {
            $('#usuario-id_provincia').removeClass('alert-danger');
            $('#usuario-id_provincia').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!idUbigeo) {
            $('#usuario-id_ubigeo').addClass('alert-danger');
            $('#usuario-id_ubigeo').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 5";
        } else {
            $('#usuario-id_ubigeo').removeClass('alert-danger');
            $('#usuario-id_ubigeo').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!flgHabilitado) {
            $('#usuario-flg_habilitado').addClass('alert-danger');
            $('#usuario-flg_habilitado').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 5";
        } else {
            $('#usuario-flg_habilitado').removeClass('alert-danger');
            $('#usuario-flg_habilitado').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (error != "") {
            return false;
        }


        if (form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                //$('.sidebar-mini').LoadingOverlay("hide", true);
            },
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results.success) {
                    //setTimeout(function(){ loading.hide(); }, 2000);
                    Usuarios();
                    $('#modal').modal('hide');
                }
            },
        });
    });

    $("body").on("change", "#usuario-id_region", function(e) {
        e.preventDefault();
        $("#usuario-id_provincia").html("<option value>Seleccionar</option>");
        $("#usuario-id_ubigeo").html("<option value>Seleccionar</option>");
        if ($(this).val()) {
            idRegion = $(this).val();
            Provincias(idRegion, "", "usuario-id_provincia");
        }

    });

    $("body").on("change", "#usuario-id_provincia", function(e) {
        e.preventDefault();
        $("#usuario-id_ubigeo").html("<option value>Seleccionar</option>");
        if ($(this).val()) {
            idProvincia = $(this).val();
            Distritos(idProvincia, "", "usuario-id_ubigeo");
        }
    });


    /* Carga Maestras */



    async function Regiones(idRegion, idCampoDepartamento) {

        var optionsDepartamentos = "<option value>Seleccionar</option>";
        $.each(JSON.parse(localStorage.getItem('region')), function(index, value) {
            optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
        });

        $("#" + idCampoDepartamento).html(optionsDepartamentos);

        if (idRegion) {
            $("#" + idCampoDepartamento).val(idRegion);
        }



        /*await   $.ajax({
                    url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones",
                    method: "POST",
                    data:{_csrf:csrf},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            var optionsDepartamentos = "<option value>Seleccionar</option>";

                            $.each(results.regiones, function( index, value ) {
                                optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                            });

                            $("#"+idCampoDepartamento).html(optionsDepartamentos);

                            if(idRegion){
                                
                                $("#"+idCampoDepartamento).val(idRegion);
                            }
                        }
                    },
                    error:function(){
                        alert("Error al realizar el proceso.");
                    }
                });*/
    }

    function Provincias(idRegion, idProvincia, idCampoProvincia) {


        var provinciaLista = $.grep(JSON.parse(localStorage.getItem('ubigeo')).regiones, function(v) {
            return v.ID_DEPARTAMENTO === idRegion;
        });

        var provincia = [];
        $.each(provinciaLista, function(index, event) {

            var events = $.grep(provincia, function(e) {
                return event.TXT_PROVINCIA === e.TXT_PROVINCIA;
            });
            if (events.length === 0) {
                provincia.push(event);
            }
        });

        var optionsProvincias = "<option value>Seleccionar</option>";
        $.each(provincia.sort((a, b) => (a.TXT_PROVINCIA > b.TXT_PROVINCIA) ? 1 : -1), function(index, value) {
            optionsProvincias = optionsProvincias + `<option value='${value.ID_PROVINCIA}'>${value.TXT_PROVINCIA}</option>`;
        });
        $("#" + idCampoProvincia).html(optionsProvincias);


        if (idProvincia) {
            $("#" + idCampoProvincia).val(idProvincia);
        }

        setTimeout(function() {
            loading.hide();
        }, 2000);


        /*
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-provincias",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsProvincias = "<option value>Seleccionar</option>";
                    $.each(results.provincias, function(index, value) {
                        optionsProvincias = optionsProvincias + `<option value='${value.ID_PROVINCIA}'>${value.TXT_PROVINCIA}</option>`;
                    });
                    $("#" + idCampoProvincia).html(optionsProvincias);


                    if (idProvincia) {
                        $("#" + idCampoProvincia).val(idProvincia);
                    }

                    setTimeout(function() {
                        loading.hide();
                    }, 2000);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });*/
    }

    function Distritos(idProvincia, idDistrito, idCampoDistrito) {


        var distritoLista = $.grep(JSON.parse(localStorage.getItem('ubigeo')).regiones, function(v) {
            return v.ID_PROVINCIA === idProvincia;
        });

        var distrito = [];
        $.each(distritoLista, function(index, event) {

            var events = $.grep(distrito, function(e) {
                return event.TXT_DISTRITO === e.TXT_DISTRITO;
            });
            if (events.length === 0) {
                distrito.push(event);
            }
        });

        var optionsDistritos = "<option value>Seleccionar</option>";
        $.each(distrito.sort((a, b) => (a.TXT_DISTRITO > b.TXT_DISTRITO) ? 1 : -1), function(index, value) {
            optionsDistritos = optionsDistritos + `<option value='${value.ID_UBIGEO}'>${value.TXT_DISTRITO}</option>`;
        });
        $("#" + idCampoDistrito).html(optionsDistritos);

        if (idDistrito) {
            $("#" + idCampoDistrito).val(idDistrito);
        }

        setTimeout(function() {
            loading.hide();
        }, 2000);

        /*
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-distritos",
            method: "POST",
            data: {
                _csrf: csrf,
                idProvincia: idProvincia
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsDistritos = "<option value>Seleccionar</option>";
                    $.each(results.distritos, function(index, value) {
                        optionsDistritos = optionsDistritos + `<option value='${value.ID_UBIGEO}'>${value.TXT_DISTRITO}</option>`;
                    });
                    $("#" + idCampoDistrito).html(optionsDistritos);

                    if (idDistrito) {
                        $("#" + idCampoDistrito).val(idDistrito);
                    }

                    setTimeout(function() {
                        loading.hide();
                    }, 2000);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });*/
    }


    $("body").on("change", ".asignar_perfil", function(e) {
        e.preventDefault();
        idPerfil = $(this).attr('data-id');
        idUsuario = $(this).attr('data-idUsuario');

        if ($("[id=\"usuario_perfil_" + idPerfil + "\"]:checked").val()) {
            asignarPerfil(idUsuario, idPerfil, true);
        } else {
            asignarPerfil(idUsuario, idPerfil, false);
        }
    });

    $("body").on("change", ".asignar_mercado", function(e) {
        e.preventDefault();
        idMercado = $(this).attr('data-id');
        idUsuario = $(this).attr('data-idUsuario');

        if ($("[id=\"usuario_mercado_" + idMercado + "\"]:checked").val()) {
            asignarMercado(idUsuario, idMercado, true);
        } else {
            asignarMercado(idUsuario, idMercado, false);
        }
    });

    async function asignarPerfil(idUsuario, idPerfil, flgActivo) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/usuario-perfil/asignar-perfil",
            method: "POST",
            data: {
                _csrf: csrf,
                idUsuario: idUsuario,
                idPerfil: idPerfil,
                flgActivo: flgActivo
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    setTimeout(function() {
                        loading.hide();
                    }, 1000);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }


    async function Perfiles() {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/perfil/get-lista-perfiles",
            method: "POST",
            data: {
                _csrf: csrf
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsPerfiles = "<option value>Seleccionar</option>";

                    $.each(results.perfiles, function(index, value) {
                        optionsPerfiles = optionsPerfiles + `<option value='${value.ID_PERFIL}'>${value.TXT_PERFIL}</option>`;
                    });

                    $("#sel_perfil").html(optionsPerfiles);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    async function asignarMercado(idMenu, idMercado, flgActivo) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/usuario-mercado/asignar-mercado",
            method: "POST",
            data: {
                _csrf: csrf,
                idUsuario: idUsuario,
                idMercado: idMercado,
                flgActivo: flgActivo
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    setTimeout(function() {
                        loading.hide();
                    }, 1000);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }
</script>