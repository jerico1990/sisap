<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formUsuario','class' => 'form-horizontal']]); ?>

<div class="modal-header">
    <h4 class="modal-title"><?= $model->titulo ?></h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">×</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'TXT_USUARIO')->label('Usuario') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'TXT_CLAVE')->label('Clave') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'TXT_NOMBRES')->label('Nombres') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'TXT_APELLIDO_PATERNO')->label('Apellido paterno') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'TXT_APELLIDO_MATERNO')->label('Apellido materno') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'TXT_EMAIL')->label('Correo electrónico') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'TXT_NUMERO_CONTACTO')->label('Teléfono') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'ID_REGION')->dropDownList([], ['prompt' => 'Seleccionar' ])->label('Región'); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'ID_PROVINCIA')->dropDownList([], ['prompt' => 'Seleccionar' ])->label('Provincia'); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'ID_UBIGEO')->dropDownList([], ['prompt' => 'Seleccionar' ])->label('Distrito'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'FLG_HABILITADO')->dropDownList([1=>'Activo',2=>'Desactivo'], ['prompt' => 'Seleccionar' ])->label('Estado'); ?>
        </div>
    </div>
</div>
<div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
    <button type="button" class="btn btn-success btn-grabar-usuario">Grabar</button>
</div>
<?php ActiveForm::end(); ?>