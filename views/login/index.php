<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
?>

<div class="modal fade" style="z-index:1060" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog text-center" role="document">
            Cargando <br>
            <div class="spinner-border text-primary m-1" role="status"></div>
        </div>
    </div>

<div class="account-pages my-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card overflow-hidden">
                    <div class="bg-success">
                        <div class="row">
                            <div class="col-9">
                                <div class="text-white p-4">
                                    <h5 class="text-white">MIDAGRI - SISAP</h5>
                                    <p>Ingrese su sesión</p>
                                </div>
                            </div>
                            <div class="col-3 align-self-end">
                                <!-- <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/profile-img.png" alt="" class="img-fluid"> -->
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div>
                            <a href="#">
                                <div class="avatar-md profile-user-wid mb-4">
                                    <span class="avatar-title rounded-circle bg-success">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/img/midagri_login.jpg" alt="" class="rounded-circle" height="54">
                                    </span>
                                </div>
                            </a>
                        </div>
                        <div class="p-2">
                            <?php $form = ActiveForm::begin(); ?>

                            <div class="form-group">
                                <label for="username">Usuario</label>
                                <input type="text" class="form-control" id="username" name="LoginForm[username]" placeholder="Ingresa usuario">
                            </div>

                            <div class="form-group">
                                <label for="userpassword">Clave</label>
                                <input type="password" class="form-control" id="password" name="LoginForm[password]" placeholder="Ingresa clave">
                            </div>

                            <div class="form-group">
                                <div class="slidercaptcha card">
                                    <div class="card-header">
                                        <span>Captcha</span>
                                    </div>
                                    <div class="card-body">
                                        <div id="captcha"></div>
                                    </div>
                                </div>
                                <!-- <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Ingresar</button> -->
                            </div>
                            <div class="form-group">
                                <button type="button" onclick="mostrarFormulario()" class="btn btn-info">Olvide Contraseña</button>
                            </div>

                            <div class="form-group" id="divFormulario" style="display: none;">
                                <label for="correo">Correo Electrónico</label>
                                <input type="email" class="form-control" id="correo" name="correo" placeholder="Ingresa correo electrónico">

                                <button type="button" onclick="enviarFormulario()" class="btn btn-primary btn-block">Enviar</button>
                            </div>



                            <?php ActiveForm::end(); ?>
                        </div>

                    </div>
                </div>
                <div class="mt-5 text-center">
                    <div>
                        <p>© 2021 - SISAP - MIDAGRI | PERÚ. </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    var captcha = sliderCaptcha({
        id: 'captcha',
        setSrc: function() {
            return '<?= \Yii::$app->request->BaseUrl ?>/captcha/images/Pic' + Math.round(Math.random() * 4) + '.jpg';
        },
        onSuccess: function() {
            localStorage.removeItem('menu');
            localStorage.removeItem('ubigeo');
            localStorage.removeItem('region');
            localStorage.removeItem("grupo");
            localStorage.removeItem("genero");

            $("#w0").submit();
            /* var handler = setTimeout(function () {
                window.clearTimeout(handler);
                captcha.reset();
            }, 500); */

        }
    });

    function mostrarFormulario() {
        $("#divFormulario").show(1000);
    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function enviarFormulario() {
        var form = $('#w0');
        var error = "";
        var correo = $.trim($("#correo").val());
        if(!isEmail(correo)) {
            $('#correo').addClass('alert-danger');
            $('#correo').css('border', '1px solid #DA1414');
            error = error + "error 1";
        } else {
            $('#correo').removeClass('alert-danger');
            $('#correo').css('border', '');
        }

        if (error != "") {
            return false;
        }
        if (form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: "/PD-03-SISAP/web/login/olvido",
            type: "post",
            data: $('#w0').serializeArray(),
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                //$('.sidebar-mini').LoadingOverlay("hide", true);
                $('#staticBackdrop').modal("hide")
            },
            beforeSend: function() {
                $('#staticBackdrop').modal("show")
            },
            success: function(results) {
                $('#staticBackdrop').modal("hide")
                if (results.success) {
                    alert("correo enviado correctamente. verificar su bandeja de correo.");
                }else{
                    alert("correo no existe.");
                }
            },
        });

    }
</script>