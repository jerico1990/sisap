<style>
    h2 {
        color: #4E7720;
        margin-left: 30px;
        margin-bottom: 7px;
        padding: 0px;
        font: bold 11pt/14pt "Verdana", arial;
        text-align: center;
    }

    h3 {
        color: #4E7720;
        margin: 5px 0px 5px 36px;
        padding: 0px;
        font: bold 12pt/15pt "Trebuchet MS", arial;
        border-bottom: 1px solid #4E7720;
        text-align: center;
    }

    .normalp,
    .centerp {
        font: 9pt/11pt "Verdana", arial;
        margin-left: 30px;
        border: none;
        background: #fff;
        color: #000;
        font-weight: normal;
        text-align: justify;
        height: 100% !important;
    }

    div.normalp {
        clear: left;
        padding: 0px 5px;
        margin: 8px 0px 0 30;
        background: none;
        color: #000;
        font: 9pt/11pt "Verdana", arial;
        font-weight: normal;
        text-align: justify;
    }

    .Estilo1 {
        color: #4A7521;
    }
</style>
<!-- 
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div id="contenido">
        <h2 style="text-align: center;">Dirección General de Seguimiento y Evaluación de Políticas</h2>
        <h2>Dirección de Estadística Agraria</h2>
        <br>
        <h2>OBJETIVOS DEL SISTEMA:</h2>
        <h3>Generales:</h3>
        <div class="normalp">Permitira investigar el volumen del abastecimiento por garitas y por mercados. Asi como, los precios de las transacciones mayorista, minorista a nivel nacional de los principales productos agropecuarios y agroindustriales, para tomar decisiones e informar a traves de los diferentes medios de difusión masiva hasta el nivel del Productor Agrario.</div>
        <h3>Específicos:</h3>
        <ol>
            <li class="normalp">Mejorar la calidad de la Información optimizando los procesos de recolección y procesamiento de datos.</li>
            <li class="normalp">Retroalimentar la información en forma agregada y Útil para la toma de decisiones a los agentes que nos proveen de información.</li>
            <li class="normalp">Poner a disposición de los usuarios internos y externos del MINAG la información y herramientas necesarias.</li>
            <li class="normalp">Optimizar el uso del sistema vía Internet.</li>
            <br><br>
            <p class="centerp Estilo1">Desarrollado por:</p>
            <p class="centerp Estilo1">Dirección General de Seguimiento y Evaluación de Políticas</p>
            <p class="centerp Estilo1">Descargar el manual <a class="normalp" href="Manual_de_Usuario.pdf" target="_blank">aquí</a></p>
            <br>
            
           
        </ol>
    </div>
</body>

</html> -->




<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');
    setTimeout(function(){ loading.hide(); }, 500);
</script>