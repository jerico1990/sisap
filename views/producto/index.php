<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Producto</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de productos</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_codigo">Código</label>
                            <input type="text" id="txt_codigo" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_descripcion">Descripción</label>
                            <input type="text" id="txt_descripcion" class="form-control">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-agregar-producto">Agregar</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <label for="">&nbsp&nbsp</label> <br>
                        <button style="float: right;" class="btn btn-primary btn-buscar-producto">Buscar</button>
                    </div>
                </div>

                <table id="lista-productos" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Grupo</th>
                        <th>Genero</th>
                        <th>Código</th>
                        <th>Producto</th>
                        <th>Unidad medida</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');

    $('#lista-productos').DataTable();

    Productos();
    async function Productos() {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/producto/get-lista-productos',
            method: 'POST',
            data: {
                _csrf: csrf
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var productos = "";
                    $('#lista-productos').DataTable().destroy();

                    $.each(results.productos, function(index, value) {

                        productos = productos + "<tr>";
                            productos = productos + "<td> " + value.TXT_CODIGO_GRUPO + "-" + value.TXT_PRODUCTO_GRUPO + "</td>";
                            productos = productos + "<td> " + value.TXT_CODIGO_GENERO + "-" +  value.TXT_PRODUCTO_GENERO + "</td>";
                            productos = productos + "<td> " + value.TXT_CODIGO_PRODUCTO + "</td>";
                            productos = productos + "<td> " + value.TXT_PRODUCTO + "</td>";
                            productos = productos + "<td> " + value.TXT_UNIDAD_MEDIDA + "</td>";
                            productos = productos + "<td> " + value.TXT_DESCRIPCION_HABILITADO + "</td>";

                            productos = productos + "<td>";
                                productos = productos + '<button data-id="' + value.ID_PRODUCTO + '" data-idProductoGrupo="' + value.ID_PRODUCTO_GRUPO + '" data-idProductoGenero="' + value.ID_PRODUCTO_GENERO + '" data-idUnidadMedida="' + value.ID_UNIDAD_MEDIDA + '" class="btn btn-info btn-sm btn-modificar-producto" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                productos = productos + '<button data-id="' + value.ID_PRODUCTO + '" class="btn btn-danger btn-sm btn-eliminar-producto" href="#"><i class="fas fa-trash"></i></button>';
                            productos = productos + "</td>";
                        productos = productos + "</tr>";
                    });

                    $('#lista-productos tbody').html(productos);
                    $('#lista-productos').DataTable({
                        
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 10,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });

                    setTimeout(function() {
                        loading.hide();
                    }, 1000);
                    //$(".dataTables_filter").hide();
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    /* Carga Maestras */

  function GruposProducto(idProductoGrupo) {
        //debugger;
        var optionsGrupo = "<option value>Seleccionar</option>";

        $.each(JSON.parse(localStorage.getItem('grupo')).grupos, function(index, value) {
            optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
        });

        $("#producto-id_producto_grupo").html(optionsGrupo);

        if (idProductoGrupo) {
            $("#producto-id_producto_grupo").val(idProductoGrupo);
        }

        /*await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-grupos",
            method: "POST",
            data: {
                _csrf: csrf
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsGrupo = "<option value>Seleccionar</option>";

                    $.each(results.grupos, function(index, value) {
                        optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_PRODUCTO_GRUPO}</option>`;
                    });

                    $("#producto-id_producto_grupo").html(optionsGrupo);

                    if (idProductoGrupo) {
                        $("#producto-id_producto_grupo").val(idProductoGrupo);
                    }
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });*/
    }

    async function GenerosProducto(idProductoGrupo, idProductoGenero) {
        var generoLista = $.grep(JSON.parse(localStorage.getItem('genero')).generos, function(v) {
            return v.ID_PRODUCTO_GRUPO === idProductoGrupo;
        });

        var optionsGenero = "<option value>Seleccionar</option>";

        $.each(generoLista, function(index, value) {
            optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_CODIGO_GENERO}-${value.TXT_PRODUCTO_GENERO}</option>`;
        });

        $("#producto-id_producto_genero").html(optionsGenero);

        if (idProductoGenero) {
            $("#producto-id_producto_genero").val(idProductoGenero);
        }


        /*await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/genero/get-lista-generos",
            method: "POST",
            data: {
                _csrf: csrf,
                idProductoGrupo: idProductoGrupo
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsGenero = "<option value>Seleccionar</option>";

                    $.each(results.generos, function(index, value) {
                        optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_PRODUCTO_GENERO}</option>`;
                    });

                    $("#producto-id_producto_genero").html(optionsGenero);

                    if (idProductoGenero) {
                        $("#producto-id_producto_genero").val(idProductoGenero);
                    }
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });*/
    }

    async function UnidadesMedidas(idUnidadMedida) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/get-lista-unidades-medidas",
            method: "POST",
            data: {
                _csrf: csrf
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsUnidadMedida = "<option value>Seleccionar</option>";

                    $.each(results.unidadesMedidas, function(index, value) {
                        optionsUnidadMedida = optionsUnidadMedida + `<option value='${value.ID_UNIDAD_MEDIDA}'>${value.TXT_UNIDAD_MEDIDA}</option>`;
                    });

                    $("#producto-id_unidad_medida").html(optionsUnidadMedida);

                    if (idUnidadMedida) {
                        $("#producto-id_unidad_medida").val(idUnidadMedida);
                    }
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    $('body').on('change', '#producto-id_producto_grupo', function(e) {
        e.preventDefault();
        idProductoGrupo = $(this).val();
        GenerosProducto(idProductoGrupo);
    });

    //Buscar
    $('body').on('click', '.btn-buscar-producto', function(e) {
        e.preventDefault();
        var codigo = $("#txt_codigo").val();
        var descripcion = $("#txt_descripcion").val();

        $('#lista-productos').DataTable().column(2).search(codigo).draw();
        $('#lista-productos').DataTable().column(3).search(descripcion).draw();

    });


    //agregar

    $('body').on('click', '.btn-agregar-producto', function(e) {
        e.preventDefault();
        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/producto/create', function() {
            GruposProducto();
            UnidadesMedidas();
        });
        $('#modal').modal('show');
    });

    //modificar
    $('body').on('click', '.btn-modificar-producto', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var idProductoGrupo = $(this).attr('data-idProductoGrupo');
        var idProductoGenero = $(this).attr('data-idProductoGenero');
        var idUnidadMedida = $(this).attr('data-idUnidadMedida');

        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/producto/update?id=' + id, function() {
            $("#producto-id_producto_grupo").prop("disabled", true);
            $("#producto-id_producto_genero").prop("disabled", true);
            GruposProducto(idProductoGrupo);
            GenerosProducto(idProductoGrupo, idProductoGenero);
            UnidadesMedidas(idUnidadMedida);
        });
        $('#modal').modal('show');
    });

    //eliminar
    $('body').on('click', '.btn-eliminar-producto', function (e) {
        e.preventDefault();
        idProducto = $(this).attr('data-id');
        Swal.fire({
            title: '¿Está seguro de deshabilitar el registro?',
            //text: "Una vez eliminado el registro ya no se podrá visualizar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, deshabilitar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url : '<?= \Yii::$app->request->BaseUrl ?>/producto/eliminar',
                    method: "POST",
                    data:{_csrf:csrf,idProducto:idProducto},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            Productos();
                            toastr.success('Registro deshabilitado');
                        }
                    },
                    error:function(){
                        toastr.error('Error al realizar el proceso.');
                    }
                });
                
            }
        });
    });

    //grabar

    $('body').on('click', '.btn-grabar-producto', function(e) {

        e.preventDefault();
        var form = $('#formProducto');
        var formData = $('#formProducto').serializeArray();

        var error = "";
        var idProductoGrupo = $.trim($("[name=\"Producto[ID_PRODUCTO_GRUPO]\"]").val());
        var idProductoGenero = $.trim($("[name=\"Producto[ID_PRODUCTO_GENERO]\"]").val());
        var txtProducto = $.trim($("[name=\"Producto[TXT_PRODUCTO]\"]").val());
        var idUnidadMedida = $.trim($("[name=\"Producto[ID_UNIDAD_MEDIDA]\"]").val());
        var flgHabilitado = $.trim($("[name=\"Producto[FLG_HABILITADO]\"]").val());

        if (!idProductoGrupo) {
            $('#producto-id_producto_grupo').addClass('alert-danger');
            $('#producto-id_producto_grupo').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#producto-id_producto_grupo').removeClass('alert-danger');
            $('#producto-id_producto_grupo').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!idProductoGenero) {
            $('#producto-id_producto_genero').addClass('alert-danger');
            $('#producto-id_producto_genero').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#producto-id_producto_genero').removeClass('alert-danger');
            $('#producto-id_producto_genero').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!txtProducto) {
            $('#producto-txt_producto').addClass('alert-danger');
            $('#producto-txt_producto').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#producto-txt_producto').removeClass('alert-danger');
            $('#producto-txt_producto').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!idUnidadMedida) {
            $('#producto-id_unidad_medida').addClass('alert-danger');
            $('#producto-id_unidad_medida').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#producto-id_unidad_medida').removeClass('alert-danger');
            $('#producto-id_unidad_medida').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }


        if (!flgHabilitado) {
            $('#producto-flg_habilitado').addClass('alert-danger');
            $('#producto-flg_habilitado').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#producto-flg_habilitado').removeClass('alert-danger');
            $('#producto-flg_habilitado').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (error != "") {
            return false;
        }

        if (form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                toastr.error('Error al realizar el proceso.');
            },
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results.success) {
                    //setTimeout(function(){ loading.hide(); }, 1000);
                    Productos();
                    $('#modal').modal('hide');
                    toastr.success('Regitro grabado');
                }else{
                    toastr.warning('No se pudo grabar el registro, realizarlo nuevamente'); 
                }
            },
        });
    });
</script>