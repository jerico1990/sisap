<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formProducto','class' => 'form-horizontal']]); ?>

<div class="modal-header">
    <h4 class="modal-title"><?= $model->titulo ?></h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">×</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'ID_PRODUCTO_GRUPO')->dropDownList([], ['prompt' => 'Seleccionar' ])->label('Grupo'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'ID_PRODUCTO_GENERO')->dropDownList([], ['prompt' => 'Seleccionar' ])->label('Genero'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'TXT_PRODUCTO')->label('Producto') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'ID_UNIDAD_MEDIDA')->dropDownList([], ['prompt' => 'Seleccionar' ])->label('Unidad de medida'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'FLG_HABILITADO')->dropDownList([1=>'Habilitado',2=>'Deshabilitado'], ['prompt' => 'Seleccionar' ])->label('Estado'); ?>
        </div>
    </div>

</div>
<div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
    <button type="button" class="btn btn-success btn-grabar-producto">Grabar</button>
</div>
<?php ActiveForm::end(); ?>