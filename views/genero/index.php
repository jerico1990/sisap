<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Genero</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de generos</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sel_grupo">Grupo</label>
                            <select class="form-control" name="sel_grupo" id="sel_grupo">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_codigo">Código</label>
                            <input type="text" id="txt_codigo" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_descripcion">Descripción</label>
                            <input type="text" id="txt_descripcion" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-agregar-genero">Agregar</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <label for="">&nbsp&nbsp</label> <br>
                        <button style="float: right;" class="btn btn-primary btn-buscar-genero">Buscar</button>
                    </div>
                </div>

                <table id="lista-generos" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Grupo</th>
                        <th>Código</th>
                        <th>Descripción</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');

    $('#lista-generos').DataTable();
    GruposGenero("", "sel_grupo");

    PintarGenero(JSON.parse(localStorage.getItem('genero')));

    function PintarGenero(results) {
        var generos = "";
        loading.show();
        $('#lista-generos').DataTable().destroy();
        
        $.each(results.generos, function(index, value) {
            generos = generos + "<tr>";
            generos = generos + "<td> " + value.TXT_CODIGO_GRUPO + "-" +value.TXT_PRODUCTO_GRUPO + "</td>";
            generos = generos + "<td> " + value.TXT_CODIGO_GENERO + "</td>";
            generos = generos + "<td> " + value.TXT_PRODUCTO_GENERO + "</td>";
            generos = generos + "<td> " + value.TXT_DESCRIPCION_HABILITADO + "</td>";
            generos = generos + "<td>";
            generos = generos + '<button data-id="' + value.ID_PRODUCTO_GENERO + '" data-idProductoGrupo="' + value.ID_PRODUCTO_GRUPO + '" class="btn btn-info btn-sm btn-modificar-genero" href="#"><i class="fas fa-pencil-alt"></i></button> ';
            generos = generos + '<button data-id="' + value.ID_PRODUCTO_GENERO + '" class="btn btn-danger btn-sm btn-eliminar-genero" href="#"><i class="fas fa-trash"></i></button>';
            generos = generos + "</td>";
            generos = generos + "</tr>";
        });

        $('#lista-generos tbody').html(generos);
        $('#lista-generos').DataTable({
            "dom": "Bfrtip",
            buttons: [
                'excelHtml5',
                'csvHtml5',
            ],
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "pageLength": 10,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningun dato disponible en esta lista",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });

        setTimeout(function() {
            loading.hide();
        }, 1000);
        $(".dataTables_filter").hide();
    }



    /* Carga Maestras */

     function GruposGenero(idProductoGrupo, idCampoGrupo) {
        console.log("guspos metodo");
        var optionsGrupo = "<option value>Seleccionar</option>";

        $.each(JSON.parse(localStorage.getItem('grupo')).grupos, function(index, value) {
            optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
        });

        $("#" + idCampoGrupo).html(optionsGrupo);

        if (idProductoGrupo) {
            $("#" + idCampoGrupo).val(idProductoGrupo);
        }
        console.log(optionsGrupo);
       
    }

    //Buscar
    $('body').on('click', '.btn-buscar-genero', function(e) {
        e.preventDefault();
        var grupo = $("#sel_grupo option:selected").text();
        var codigo = $("#txt_codigo").val();
        var descripcion = $("#txt_descripcion").val();

        if (grupo != "Seleccionar" && grupo != "") {
            $('#lista-generos').DataTable().column(0).search(grupo).draw();
        } else {
            $('#lista-generos').DataTable().column(0).search("").draw();
        }

        $('#lista-generos').DataTable().column(1).search(codigo).draw();
        $('#lista-generos').DataTable().column(2).search(descripcion).draw();

    });



    //agregar
    $('body').on('click', '.btn-agregar-genero', function(e) {
        e.preventDefault();
        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/genero/create', function() {
            console.log("creo genero");
            GruposGenero("", "genero-id_producto_grupo");
        });
        $('#modal').modal('show');
    });

    //modificar
    $('body').on('click', '.btn-modificar-genero', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var idProductoGrupo = $(this).attr('data-idProductoGrupo');

        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/genero/update?id=' + id, function() {
            GruposGenero(idProductoGrupo, "genero-id_producto_grupo")
        });
        $('#modal').modal('show');
    });


    //eliminar
    $('body').on('click', '.btn-eliminar-genero', function (e) {
        e.preventDefault();
        idProductoGenero = $(this).attr('data-id');
        Swal.fire({
            title: '¿Está seguro de deshabilitar el registro?',
            //text: "Una vez eliminado el registro ya no se podrá visualizar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, deshabilitar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url : '<?= \Yii::$app->request->BaseUrl ?>/genero/eliminar',
                    method: "POST",
                    data:{_csrf:csrf,idProductoGenero:idProductoGenero},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            Generos();
                            PintarGenero(JSON.parse(localStorage.getItem('genero')));
                            toastr.success('Registro deshabilitado');
                        }
                    },
                    error:function(){
                        toastr.error('Error al realizar el proceso.');
                    }
                });
                
            }
        });
    });

    //grabar

    $('body').on('click', '.btn-grabar-genero', function(e) {

        e.preventDefault();
        var form = $('#formGenero');
        var formData = $('#formGenero').serializeArray();

        var error = "";
        var idProductoGrupo = $.trim($("[name=\"Genero[ID_PRODUCTO_GRUPO]\"]").val());
        var txtCodigoGenero = $.trim($("[name=\"Genero[TXT_CODIGO_GENERO]\"]").val());
        var txtProductoGenero = $.trim($("[name=\"Genero[TXT_PRODUCTO_GENERO]\"]").val());
        var flgHabilitado = $.trim($("[name=\"Genero[FLG_HABILITADO]\"]").val());

        if (!idProductoGrupo) {
            $('#genero-id_producto_grupo').addClass('alert-danger');
            $('#genero-id_producto_grupo').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#genero-id_producto_grupo').removeClass('alert-danger');
            $('#genero-id_producto_grupo').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!txtCodigoGenero) {
            $('#genero-txt_codigo_genero').addClass('alert-danger');
            $('#genero-txt_codigo_genero').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#genero-txt_codigo_genero').removeClass('alert-danger');
            $('#genero-txt_codigo_genero').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!txtProductoGenero) {
            $('#genero-txt_producto_genero').addClass('alert-danger');
            $('#genero-txt_producto_genero').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#genero-txt_producto_genero').removeClass('alert-danger');
            $('#genero-txt_producto_genero').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!flgHabilitado) {
            $('#genero-flg_habilitado').addClass('alert-danger');
            $('#genero-flg_habilitado').css('border', '1px solid #DA1414');
            error = error + "error 1";
        } else {
            $('#genero-flg_habilitado').removeClass('alert-danger');
            $('#genero-flg_habilitado').css('border', '');
        }

        if (error != "") {
            return false;
        }
        if (form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                toastr.error('Error al realizar el proceso.');
            },
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results.success) {
                    //setTimeout(function(){ loading.hide(); }, 2000);
                    Generos();
                    PintarGenero(JSON.parse(localStorage.getItem('genero')));
                    $('#modal').modal('hide');
                    toastr.success('Registro grabado');
                }else{
                    if(results.msg==0){
                        toastr.warning('No se pudo grabar el registro, realizarlo nuevamente'); 
                    }else if(results.msg==1){
                        setTimeout(function() {
                            loading.hide();
                        }, 1000);
                        toastr.warning('El código del genero para ese grupo ya existe'); 
                    }
                    
                }
            },
        });
    });
</script>