<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formClave', 'class' => 'form-horizontal']]); ?>
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
    </div>
    <div class="modal-body">

        <div class="row">
            <div class="col-md-12">
                <label for="">Apellidos y Nombres:</label>
                <label for=""><?= Html::encode($model->nombres) ?></label>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="">Anterior Clave</label>
                <input name="claveanterior" id="claveanterior" type="password" class="form-control">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'TXT_CLAVE',)->label('Nueva Clave')->passwordInput() ?>
                
            </div>
        </div>


    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-success btn-grabar-clave" onclick="grabarClave()">Grabar</button>
    </div>
</div>
<?php ActiveForm::end(); ?>

<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');

    function init() {
        setTimeout(function() {
            loading.hide();
        }, 1000);
    }

    init();

    function grabarClave() {
        //debugger;
        var form = $('#formClave');
        var formData = $('#formClave').serializeArray();

        var error = "";
        var txtClave = $.trim($("[name=\"Clave[TXT_CLAVE]\"]").val());
        var txtClaveAnterior = $.trim($("[name=\"claveanterior\"]").val());

        if (!txtClave) {
            $('#clave-txt_clave').addClass('alert-danger');
            $('#clave-txt_clave').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#clave-txt_clave').removeClass('alert-danger');
            $('#clave-txt_clave').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!txtClaveAnterior) {
            $('#claveanterior').addClass('alert-danger');
            $('#claveanterior').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#claveanterior').removeClass('alert-danger');
            $('#claveanterior').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (error != "") {
            return false;
        }


        if (form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                //$('.sidebar-mini').LoadingOverlay("hide", true);
            },
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results.success) {
                    toastr.success('Clave actualizada correctamente.');
                    $("#claveanterior").val("");
                    $("#clave-txt_clave").val("");
                } else {
                    toastr.error('Clave anterior no es válida.');
                }
                loading.hide();
            },
        });
    };
</script>