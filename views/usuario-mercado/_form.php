<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formUsuarioMercado','class' => 'form-horizontal']]); ?>

<div class="modal-header">
    <h4 class="modal-title"><?= $model->titulo ?></h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">×</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <table id="lista-usuario-mercados" class="table table-bordered dt-responsive ">
                <thead>
                    <th>Región</th>
                    <th>Provincia</th>
                    <th>Distrito</th>
                    <th>Mercado</th>
                    <th>Acción</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>