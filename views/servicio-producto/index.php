<style>
    .container {
        margin: 150px auto;
        min-height: 100vh;
    }

    .stylish-input-group .input-group-addon {
        background: white !important;
    }

    .stylish-input-group .form-control {

        box-shadow: 0 0 0;
        border-color: #ccc;
    }

    .stylish-input-group button {
        border: 0;
        background: transparent;
    }

    .h-scroll {
        height: 260px;
        overflow-y: scroll;
    }


    .hummingbird-treeview * {
        list-style: none;
        font-size: 20px;
        line-height: 24px;
    }

    .hummingbird-treeview label {
        font-weight: normal;
    }


    .hummingbird-treeview input[type=checkbox] {
        width: 16px;
        height: 16px;
        padding: 0px;
        margin: 0px;
    }

    .hummingbird-treeview ul:not(.hummingbird-base) {
        display: none;
    }

    .hummingbird-treeview .fa {
        font-style: normal;
        cursor: pointer;
    }

    .hummingbird-treeview .fas {
        font-style: normal;
        cursor: pointer;
    }
</style>
<script src="<?= \Yii::$app->request->BaseUrl ?>/treeview/hummingbird-treeview.js"></script>
<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Servicio Productos</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de Servicio por Productos</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formProductoMercado', 'class' => 'form-horizontal']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Servicio</label>
                            <select id="producto_mercado-servicio_producto_id" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Región</label>
                            <select id="producto_mercado-id_region" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Mercado</label>
                            <select name="ProductoMercado[ID_MERCADO]" id="producto_mercado-id_mercado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Grupo</label>
                            <select id="producto_mercado-id_producto_grupo" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Genero</label>
                            <select name="ProductoMercado[ID_PRODUCTO_GENERO]" id="producto_mercado-id_producto_genero" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div id="treeview_container" class="hummingbird-treeview" style="height: 400px; overflow-y: scroll;">
                    <ul id="treeview" class="hummingbird-base">
                        <li data-id="0">
                            <i class="fa fa-plus"></i>
                            <label>
                                <input id="idPadre0" onclick="ActualizarTodo(0)" data-id="custom-0" type="checkbox" /> Todos
                            </label>
                            <ul id="idarbol">
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <table id="lista-productos-mercado" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Región</th>
                        <th>Tipo de Mercado</th>
                        <th>Mercado</th>
                        <th>Grupo</th>
                        <th>Genero</th>
                        <th>Producto</th>
                        <th>Envase</th>
                        <th>Publicado</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');
    var idMercado;
    var idRegion;
    var idProductoGrupo;
    var idProductoGenero;
    var idServicio;



    $("#treeview").hummingbird();
    /*$("#checkAll").click(function() {
        $("#treeview").hummingbird("checkAll");
    });
    $("#uncheckAll").click(function() {
        $("#treeview").hummingbird("uncheckAll");
    });
    $("#collapseAll").click(function() {
        $("#treeview").hummingbird("collapseAll");
    });
    $("#checkNode").click(function() {
        $("#treeview").hummingbird("checkNode", {
            attr: "id",
            name: "node-0-2-2",
            expandParents: false
        });
    });
*/


    $('#lista-productos-mercado').DataTable({
        "dom": "Bfrtip",
        buttons: [
            'excelHtml5',
            'csvHtml5',
        ],
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "pageLength": 10,
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningun dato disponible en esta lista",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

    async function ProductosMercado() {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/servicio-producto/get-lista-servicio-productos',
            method: 'POST',
            data: {
                _csrf: csrf,
                idMercado: idMercado,
                idRegion: idRegion,
                idProductoGrupo: idProductoGrupo,
                idProductoGenero: idProductoGenero,
                idServicio: idServicio
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var productosMercado = "";
                    $('#lista-productos-mercado').DataTable().destroy();

                    $.each(results.productosMercado, function(index, value) {

                        txtUnidadMedida = ((value.TXT_UNIDAD_MEDIDA) ? value.TXT_UNIDAD_MEDIDA : "");
                        numEquivalencia = ((value.NUM_EQUIVALENCIA) ? value.NUM_EQUIVALENCIA : "");

                        productosMercado = productosMercado + "<tr>";
                        productosMercado = productosMercado + "<td> " + ((value.TXT_DEPARTAMENTO) ? value.TXT_DEPARTAMENTO : "") + "</td>";
                        productosMercado = productosMercado + "<td> " + ((value.TXT_MERCADO_TIPO) ? value.TXT_MERCADO_TIPO : "") + "</td>";
                        productosMercado = productosMercado + "<td> " + ((value.TXT_MERCADO) ? value.TXT_MERCADO : "") + "</td>";
                        productosMercado = productosMercado + "<td> " + ((value.TXT_PRODUCTO_GRUPO) ? value.TXT_CODIGO_GRUPO + "-" + value.TXT_PRODUCTO_GRUPO : "") + "</td>";
                        productosMercado = productosMercado + "<td> " + ((value.TXT_PRODUCTO_GENERO) ? value.TXT_CODIGO_GENERO + "-" + value.TXT_PRODUCTO_GENERO : "") + "</td>";
                        productosMercado = productosMercado + "<td> " + ((value.TXT_PRODUCTO) ? value.TXT_CODIGO_PRODUCTO + '-' + value.TXT_PRODUCTO : "") + "</td>";
                        productosMercado = productosMercado + "<td> " + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + txtUnidadMedida + "-" + numEquivalencia : "") + "</td>";
                        productosMercado = productosMercado + "<td> " + ((value.TXT_PUBLICADO) ? value.TXT_PUBLICADO : "") + "</td>";
                        productosMercado = productosMercado + "</tr>";
                    });

                    $('#lista-productos-mercado tbody').html(productosMercado);
                    $('#lista-productos-mercado').DataTable({
                        "dom": "Bfrtip",
                        buttons: [
                            'excelHtml5',
                            'csvHtml5',
                        ],
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 10,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });

                    setTimeout(function() {
                        loading.hide();
                    }, 500);
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }


    async function RegionesProductoMercado() {

        var optionsDepartamentos = "<option value>Seleccionar</option>";

        $.each(JSON.parse(localStorage.getItem('region')), function(index, value) {
            optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
        });

        $("#producto_mercado-id_region").html(optionsDepartamentos);



    }

    async function Mercados(idRegion) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-mercados",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsMercados = "<option value>Seleccionar</option>";

                    $.each(results.mercados, function(index, value) {
                        optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                    });

                    $("#producto_mercado-id_mercado").html(optionsMercados);

                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    async function GruposProductoMercado() {

        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-opciones-grupos-todos",
            method: "POST",
            data: {
                _csrf: csrf,
                idMercado: idMercado
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {

                    var optionsGrupo = "<option value>Seleccionar</option>";

                    $.each(results.grupos, function(index, value) {
                        optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
                    });

                    $("#producto_mercado-id_producto_grupo").html(optionsGrupo);
                }
                idProductoGrupo = $('#producto_mercado-id_producto_grupo').val();
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }







    async function GenerosProductoMercado(idProductoGrupo) {

        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/genero/get-lista-opciones-generos-todos",
            method: "POST",
            data: {
                _csrf: csrf,
                idProductoGrupo: idProductoGrupo,
                idMercado: idMercado
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {

                    var optionsGenero = "<option value>Seleccionar</option>";

                    $.each(results.generos, function(index, value) {
                        optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_CODIGO_GENERO}-${value.TXT_PRODUCTO_GENERO}</option>`;
                    });

                    $("#producto_mercado-id_producto_genero").html(optionsGenero);
                }
                idProductoGenero = $('#producto_mercado-id_producto_genero').val();
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });

    }




    $('body').on('change', '#producto_mercado-id_region', function(e) {
        e.preventDefault();
        idRegion = $(this).val();
        idProductoGenero = "";
        idProductoGrupo = "";
        idMercado = "";
        $("#producto_mercado-id_producto_grupo").html("<option value>Seleccionar</option>");
        $("#producto_mercado-id_producto_genero").html("<option value>Seleccionar</option>");

        Mercados(idRegion);
        //GruposProductoMercado();
        $("#idarbol").html("");
    });

    $('body').on('change', '#producto_mercado-id_mercado', function(e) {
        e.preventDefault();

        if ($("#producto_mercado-servicio_producto_id").val() == "") {
            toastr.error('Seleccione un servicio');
            $(this).val('');
        } else {
            if ($(this).val() == "") {
                toastr.error('Seleccione un mercado');
                idMercado="";
                $("#producto_mercado-id_producto_grupo").html("<option value>Seleccionar</option>");
                $("#producto_mercado-id_producto_genero").html("<option value>Seleccionar</option>");

                $("#idarbol").html("");
            } else {
                idMercado = $(this).val();
                idProductoGenero = "";
                idProductoGrupo = "";
                $("#producto_mercado-id_producto_grupo").html("<option value>Seleccionar</option>");
                $("#producto_mercado-id_producto_genero").html("<option value>Seleccionar</option>");


                GruposProductoMercado();
                ProductosMercado();
                ServicioProductos();
            }

        }
    });



    $('body').on('change', '#producto_mercado-id_producto_grupo', function(e) {
        e.preventDefault();
        idProductoGrupo = $(this).val();
        idProductoGenero = "";
        GenerosProductoMercado(idProductoGrupo);
        ProductosMercado();
        ServicioProductos();
    });

    $('body').on('change', '#producto_mercado-id_producto_genero', function(e) {
        e.preventDefault();
        idProductoGenero = $(this).val();
        ProductosMercado();
        ServicioProductos();
    });





    $('body').on('change', '#producto_mercado-servicio_producto_id', function(e) {
        e.preventDefault();

        if ($("#producto_mercado-servicio_producto_id").val() == "") {
            toastr.error('Seleccione un servicio');
        } else {
            idServicio = $(this).val();
            if (idMercado != ""  && idRegion != "" && idMercado != undefined  && idRegion != undefined) {
                ServicioProductos();
            }
        }
    });



    async function ServicioProductos() {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/servicio-producto/get-lista-servicio-productos-todos',
            method: 'POST',
            data: {
                _csrf: csrf,
                idMercado: idMercado,
                idRegion: idRegion,
                idProductoGrupo: idProductoGrupo,
                idProductoGenero: idProductoGenero,
                idServicio: idServicio,
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var productosMercado = "";
                    var html = "";
                    var codigo = "";
                    var index = 0;
                    var indexGrupo = 0;
                    var cadenaIndex = "";
                    $.each(results.productosMercado, function(index, value) {
                        txtUnidadMedida = ((value.TXT_UNIDAD_MEDIDA) ? value.TXT_UNIDAD_MEDIDA : "");
                        numEquivalencia = ((value.NUM_EQUIVALENCIA) ? value.NUM_EQUIVALENCIA : "");
                        var idServicioProducto = ((value.ID_SERVICIO_PRODUCTO) ? value.ID_SERVICIO_PRODUCTO : "0");
                        index++;
                        var check = value.ID_SERVICIO_PRODUCTO > 0 ? "checked" : "";
                        if (value.ID_SERVICIO_PRODUCTO > 0) {
                            cadenaIndex = index;
                        }

                        if (codigo == "") {
                            indexGrupo++;
                            html = html + '<li id=idGrupoParent' + indexGrupo + '><i class="fa fa-plus"></i><label><input id=idGrupo' + indexGrupo + ' onclick="ActualizarGrupo(' + indexGrupo + ')" class="checkGrupo" type="checkbox" />' + value.TXT_CODIGO_GENERO + '-' + value.TXT_PRODUCTO_GENERO + '</label><ul>'
                            html = html + '<li><label><input class="hummingbird-end-node checkItem" onclick="Actualizar(' + index + ')" id=' + index + ' ' + check + ' data-idServicioProducto=' + idServicioProducto + ' data-idProductoMercado=' + value.ID_PRODUCTO_MERCADO + ' type="checkbox" />' + value.TXT_CODIGO_PRODUCTO + '-' + value.TXT_PRODUCTO + '-' + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + txtUnidadMedida + "-" + numEquivalencia : "") + '</label></li>'
                        } else if (codigo == value.TXT_CODIGO_GENERO) {
                            html = html + '<li><label><input class="hummingbird-end-node checkItem" onclick="Actualizar(' + index + ')" id=' + index + ' ' + check + ' data-idServicioProducto=' + idServicioProducto + ' data-idProductoMercado=' + value.ID_PRODUCTO_MERCADO + ' type="checkbox"  />' + value.TXT_CODIGO_PRODUCTO + '-' + value.TXT_PRODUCTO + '-' + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + txtUnidadMedida + "-" + numEquivalencia : "") + '</label></li>'
                        } else if (codigo != value.TXT_CODIGO_GENERO) {
                            html = html + '</ul>'
                            indexGrupo++;
                            html = html + '<li id=idGrupoParent' + indexGrupo + '><i class="fa fa-plus"></i><label><input id=idGrupo' + indexGrupo + ' onclick="ActualizarGrupo(' + indexGrupo + ')" class="checkGrupo"  type="checkbox" />' + value.TXT_CODIGO_GENERO + '-' + value.TXT_PRODUCTO_GENERO + '</label><ul>'
                            html = html + '<li><label><input class="hummingbird-end-node checkItem" onclick="Actualizar(' + index + ')" id=' + index + ' ' + check + ' data-idServicioProducto=' + value.ID_SERVICIO_PRODUCTO + ' data-idProductoMercado=' + value.ID_PRODUCTO_MERCADO + ' type="checkbox" />' + value.TXT_CODIGO_PRODUCTO + '-' + value.TXT_PRODUCTO + '-' + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + txtUnidadMedida + "-" + numEquivalencia : "") + '</label></li>'
                        }
                        codigo = value.TXT_CODIGO_GENERO;
                    });

                    $("#idarbol").html(html);

                    if (results.productosMercado.length > 0) {
                        setTimeout(function() {
                            loading.hide();
                            $("#treeview").hummingbird();
                            $("#treeview").hummingbird("checkNode", {
                                sel: "id",
                                vals: [cadenaIndex]
                            });
                            $("#treeview").hummingbird("expandAll");
                        }, 500);
                    } else {
                        $("#idPadre0").prop("checked", false);
                        $("#treeview").hummingbird("expandAll");
                    }

                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }


    async function Servicio() {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/servicio-producto/get-lista-servicio",
            method: "POST",
            data: {
                _csrf: csrf
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsServicio = "<option value>Seleccionar</option>";
                    $.each(results.productoServicio, function(index, value) {
                        optionsServicio = optionsServicio + `<option value='${value.ID_SERVICIO}'>${value.TXT_SERVICIO}</option>`;
                    });
                    $("#producto_mercado-servicio_producto_id").html(optionsServicio);
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    $("#treeview").on("nodeChecked", function() {
        alert("0000");
    });

    async function ActualizarTodo(id) {
        var lista = $(".checkItem");
        if ($(".checkItem").length > 0) {
            var flgHabilitadoTodos = $("#idPadre" + id).prop("checked") ? "1" : "0";
            jQuery.each(lista, function(i, element) {
                var id = $(this).attr("id");
                console.log(id);
                Actualizar(id, flgHabilitadoTodos)
            });
            toastr.success('Producto grabado');
        }

    }

    async function ActualizarGrupo(id) {
        var lista = $("#idGrupo" + id).parent().next().children();
        var flgHabilitadoTodos = $("#idGrupo" + id).prop("checked") ? "1" : "0";
        jQuery.each(lista, function(i, element) {
            var id = $(this).children().children().attr("id");
            Actualizar(id, flgHabilitadoTodos)
        });
        toastr.success('Producto grabado');
    }



    async function Actualizar(id, flgTodos) {
        //var id = $("#" + id).attr("data-idServicioProducto") == "null" ? "0" : $("#" + id).attr("data-idServicioProducto");
        var idProductoMercado = $("#" + id).attr("data-idProductoMercado");
        var idServicio = $("#producto_mercado-servicio_producto_id").val();
        var flgHabilitado = flgTodos != undefined ? flgTodos : $("#" + id).prop("checked") ? "1" : "0";

        ActualizarServicioProducto(idServicio, idProductoMercado, flgHabilitado, flgTodos);
    }

    async function ActualizarServicioProducto(idServicio, idProductoMercado, flgHabilitado, flgTodos) {
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/servicio-producto/update?idServicio=" + idServicio + "&idProductoMercado=" + idProductoMercado,
            method: "POST",
            data: {
                _csrf: csrf,
                flgHabilitado: flgHabilitado
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    ProductosMercado();
                    if (flgTodos == undefined) {
                        toastr.success('Producto grabado');
                    }
                } else {
                    if (flgTodos == undefined) {
                        if (results.msg == 0) {
                            toastr.warning('No se pudo grabar el registro, realizarlo nuevamente');
                        }
                    }
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }


    function init() {
        RegionesProductoMercado();

        Servicio();
        setTimeout(function() {
            loading.hide();
        }, 500);
    }

    init();
</script>