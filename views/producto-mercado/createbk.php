<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?= \Yii::$app->request->BaseUrl ?>/Editor-2.0.5/css/editor.dataTables.css">
 
<script type="text/javascript" src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="<?= \Yii::$app->request->BaseUrl ?>/Editor-2.0.5/js/dataTables.editor.js"></script>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Producto x Mercado</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de productos x mercado</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formProductoMercado','class' => 'form-horizontal']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Región</label>
                            <select id="producto_mercado-id_region" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Mercado</label>
                            <select name="ProductoMercado[ID_MERCADO]" id="producto_mercado-id_mercado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Grupo</label>
                            <select id="producto_mercado-id_producto_grupo" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Genero</label>
                            <select name="ProductoMercado[ID_PRODUCTO_GENERO]" id="producto_mercado-id_producto_genero" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button type="button" class="btn btn-success btn-agregar-producto-mercado">Agregar</button>
                        </div>
                    </div> -->
                </div>

                <table id="lista-productos-mercado" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Acciones</th>
                        <th>Tipo de mercado</th>
                        <th>Región</th>
                        <th>Mercado</th>
                        <th>Grupo</th>
                        <th>Genero</th>
                        <th>Producto</th>
                        <th>Envase</th>
                        <th>Publicar</th>
                        <th>Equivalencia</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<script>

var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('.staticBackdrop');

$('#lista-productos-mercado').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });
var editor;
   
// Activate an inline edit on click of a table cell
$('#lista-productos-mercado').on( 'click', 'tbody td:not(:first-child)', function (e) {
    

    editor.inline( this, {
        onBlur: 'submit'
    } );
} );




/*
editor.dependent('ID_PRODUCTO', function(val, data, callback) {
    var idGenero = $('#producto_mercado-id_producto_genero').val();
    $.ajax({
      url: '<?= \Yii::$app->request->BaseUrl ?>/producto/get-lista-opciones-productos',
      type: "POST",
      data:{_csrf:csrf,idGenero:idGenero},
      dataType: 'json',
      success: function(json) {
        callback(json);
      }
    });
});*/

async function ProductosMercado(idProductoGenero,idMercado){
    //console.log(optionsA);
    $('#lista-productos-mercado').DataTable().destroy();

    editor = new $.fn.dataTable.Editor( {
        ajax: {
            "url": "<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/update",
            "data":{_csrf:csrf},
        },
        formOptions: {
            inline: {
                onBlur: false
            }
        },
        table: "#lista-productos-mercado",
        idSrc: "DT_RowId",
        fields: [ 
            {
                label: "Tipo de mercado:",
                name: "TXT_MERCADO_TIPO",
                type: 'readonly',
                submit: false,
            },
            {
                label: "Región:",
                name: "TXT_DEPARTAMENTO",
                type: 'readonly',
                submit: false,
            },
            {
                label: "Mercado:",
                name: "TXT_MERCADO",
                type: 'readonly',
                submit: false,
            },
            {
                label: "Grupo:",
                name: "TXT_PRODUCTO_GRUPO",
                type: 'readonly',
                submit: false,
            },
            {
                label: "Genero:",
                name: "TXT_PRODUCTO_GENERO",
                type: 'readonly',
                submit: false,
            },
            {
                label: "Producto:",
                name: "ID_PRODUCTO",
                type: "select",
                placeholder: "Seleccionar producto",
                options:listaProductos
            },
            {
                label: "Envase:",
                name: "ID_ENVASE",
                type:"select",
                placeholder:"Seleccionar envase",
                options:listaEnvases
            },
            {
                label: "Publicar:",
                name: "FLG_PUBLICADO",
                type:"select",
                placeholder:"Seleccionar publicado",
                options:[
                    { label: "Publicar", value: "1" },
                    { label: "Despublicar", value: "2" },
                ]
            },
            {
                label: "Equivalencia:",
                name: "NUM_EQUIVALENCIA",
                type: 'text',
            },
        ]
    } );

    $('#lista-productos-mercado').DataTable( {
        "dom": "Bfrtip",
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/get-lista-productos-mercado",
            "type": "POST",
            "data":{_csrf:csrf,idProductoGenero:idProductoGenero,idMercado:idMercado},
        },
        columns: [ 
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            { data: "TXT_MERCADO_TIPO" },
            { data: "TXT_DEPARTAMENTO" },
            { data: "TXT_MERCADO" },
            { data: "TXT_PRODUCTO_GRUPO" },
            { data: "TXT_PRODUCTO_GENERO" },
            { data: "TXT_PRODUCTO", editField: "ID_PRODUCTO" },
            { data: "TXT_ENVASE", editField: "ID_ENVASE" },
            { data: "FLG_PUBLICADO", editField: "FLG_PUBLICADO" , render: function (val, type, row) {
                    let publicado = "";
                    if(val == "1"){
                        publicado = "Publicar";
                    }else if(val == "2"){
                        publicado = "Despublicar";
                    }
                    return publicado;
                }
            },
            { data: "NUM_EQUIVALENCIA" },
        ],
        "select": {
            style:    'os',
            selector: 'td:first-child'
        },
        buttons: [
            {
                text: 'Agregar',
                className: 'btn-success btn-agregar-producto-mercado float-left'
            },
            { extend: 'excelHtml5', className: 'float-right' },
            'csvHtml5',
        ],
        "paging": true,
        "lengthChange": true,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "pageLength" : 10,
        "language": {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningun dato disponible en esta lista",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":    "Último",
                "sNext":    "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    } );

    setTimeout(function(){ loading.hide(); }, 1000);
    /*
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/get-lista-productos-mercado',
                method: 'POST',
                data:{_csrf:csrf,idGenero:idGenero,idMercado:idMercado},
                dataType:'Json',
                beforeSend:function(){
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var productosMercado ="";
                        $('#lista-productos-mercado').DataTable().destroy();
                        
                        $.each(results.productosMercado, function( index, value ) {
                            
                            productosMercado = productosMercado + "<tr>";
                            productosMercado = productosMercado + "<td> " + ((value.ID_PRODUCTO_MERCADO)?value.ID_PRODUCTO_MERCADO:"") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.TXT_MERCADO_TIPO)?value.TXT_MERCADO_TIPO:"") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.TXT_DEPARTAMENTO)?value.TXT_DEPARTAMENTO:"") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.TXT_MERCADO)?value.TXT_MERCADO:"") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.ID_PRODUCTO_MERCADO)?value.ID_PRODUCTO_MERCADO:"") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.ID_PRODUCTO_MERCADO)?value.ID_PRODUCTO_MERCADO:"") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.ID_PRODUCTO_MERCADO)?value.ID_PRODUCTO_MERCADO:"") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.ID_PRODUCTO_MERCADO)?value.ID_PRODUCTO_MERCADO:"") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.ID_PRODUCTO_MERCADO)?value.ID_PRODUCTO_MERCADO:"") + "</td>";
                            productosMercado = productosMercado + "<td>" ;
                                productosMercado = productosMercado + '<button data-id="' + value.ID_USUARIO + '" class="btn btn-danger btn-sm btn-eliminar-usuario" href="#"><i class="fas fa-trash"></i></button>';
                            productosMercado = productosMercado +"</td>";
                            productosMercado = productosMercado + "</tr>";
                        });
                        
                        $('#lista-productos-mercado tbody').html(productosMercado);
                        $('#lista-productos-mercado').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.hide(); }, 1000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });*/

}

async function Regiones(){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsDepartamentos = "<option value>Seleccionar</option>";

                        $.each(results.regiones, function( index, value ) {
                            optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                        });

                        $("#producto_mercado-id_region").html(optionsDepartamentos);

                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

async function Mercados(idRegion){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-mercados",
                method: "POST",
                data:{_csrf:csrf,idRegion:idRegion},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsMercados = "<option value>Seleccionar</option>";

                        $.each(results.mercados, function( index, value ) {
                            optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                        });

                        $("#producto_mercado-id_mercado").html(optionsMercados);

                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

async function Grupos(){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-grupos",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsGrupo = "<option value>Seleccionar</option>";

                        $.each(results.grupos, function( index, value ) {
                            optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_PRODUCTO_GRUPO}</option>`;
                        });

                        $("#producto_mercado-id_producto_grupo").html(optionsGrupo);
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

async function Generos(idProductoGrupo){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/genero/get-lista-generos",
                method: "POST",
                data:{_csrf:csrf,idProductoGrupo:idProductoGrupo},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsGenero = "<option value>Seleccionar</option>";

                        $.each(results.generos, function( index, value ) {
                            optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_PRODUCTO_GENERO}</option>`;
                        });

                        $("#producto_mercado-id_producto_genero").html(optionsGenero);
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

var listaProductos = [];
async function Productos(idProductoGenero){
    listaProductos = [];
    await   $.ajax({
                async:false,
                url: "<?= \Yii::$app->request->BaseUrl ?>/producto/get-lista-opciones-productos",
                method: "POST",
                data:{_csrf:csrf,idProductoGenero:idProductoGenero},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        $.each(results.productos, function( index, value ) {
                            listaProductos.push(value);
                        });
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

var listaEnvases = [];
async function Envases(){
    await   $.ajax({
                async:false,
                url: "<?= \Yii::$app->request->BaseUrl ?>/envase/get-lista-opciones-envases",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        $.each(results.envases, function( index, value ) {
                            listaEnvases.push(value);
                        });
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

$('body').on('change', '#producto_mercado-id_producto_grupo', function (e) {
    e.preventDefault();
    idProductoGrupo = $(this).val();
    Generos(idProductoGrupo);
});

$('body').on('change', '#producto_mercado-id_region', function (e) {
    e.preventDefault();
    idRegion = $(this).val();
    Mercados(idRegion);
});

$('body').on('change', '#producto_mercado-id_producto_genero', function (e) {
    e.preventDefault();
    idProductoGenero = $(this).val();
    idMercado = $('#producto_mercado-id_mercado').val();
    Productos(idProductoGenero);
    ProductosMercado(idProductoGenero,idMercado);
});

$('body').on('click', '.btn-agregar-producto-mercado', function (e) {
    e.preventDefault();
    var idMercado = $('#producto_mercado-id_mercado').val();
    var idProductoGenero = $('#producto_mercado-id_producto_genero').val();
    var form = $('#formProductoMercado');
    var formData = $('#formProductoMercado').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.show();
        },
        success: function (results) {
            if(results.success){
                //setTimeout(function(){ loading.hide(); }, 1000);
                //Usuarios();
                ProductosMercado(idProductoGenero,idMercado)
                $('#modal').modal('hide');
            }
        },
    });
});

function init() {
    Regiones();
    Grupos();
    Envases();
    //Productos();
    //ProductosMercado();
}

init();
</script>