<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Producto x Mercado</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de productos x mercado</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formProductoMercado', 'class' => 'form-horizontal']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Región</label>
                            <select id="producto_mercado-id_region" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Mercado</label>
                            <select name="ProductoMercado[ID_MERCADO]" id="producto_mercado-id_mercado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Grupo</label>
                            <select id="producto_mercado-id_producto_grupo" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Genero</label>
                            <select name="ProductoMercado[ID_PRODUCTO_GENERO]" id="producto_mercado-id_producto_genero" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button type="button" class="btn btn-success btn-agregar-producto-mercado">Agregar</button>
                            <button type="button" class="btn btn-danger btn-eliminar-producto-mercado">Eliminar</button>
                        </div>
                    </div>
                </div>

                <table id="lista-productos-mercado" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Acciones</th>
                        <th>Tipo de mercado</th>
                        <!-- <th>Región</th>
                        <th>Mercado</th> -->
                        <th>Grupo</th>
                        <th>Genero</th>
                        <th>Producto</th>
                        <th>Envase</th>
                        <th>Código Anexo</th>
                        <th>Publicar</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');

    function ListaProductosMercado() {
        $('#lista-productos-mercado').DataTable().destroy();
        $('#lista-productos-mercado tbody').html("");
        $('#lista-productos-mercado').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "pageLength": 10,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningun dato disponible en esta lista",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });

        setTimeout(function() {
                        loading.hide();
                    }, 500);
    }



    async function ProductosMercado(idProductoGenero, idMercado) {

        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/get-lista-productos-mercado',
            method: 'POST',
            data: {
                _csrf: csrf,
                idProductoGenero: idProductoGenero,
                idMercado: idMercado
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var productosMercado = "";
                    $('#lista-productos-mercado').DataTable().destroy();
                    //listaProductos

                    $.each(results.productosMercado, function(index, value) {
                        var optionsProductos = "<option value>Seleccionar</option>";
                        var optionsEnvases = "<option value>Seleccionar</option>";
                        var optionsPublicados = "<option value>Seleccionar</option>";

                        productosMercado = productosMercado + "<tr>";
                        productosMercado = productosMercado + "<td class='text-center'> ";
                            productosMercado = productosMercado + '<div class="form-check mb-3"><input class="form-check-input check-producto-mercado" type="checkbox" data-id="' + value.ID_PRODUCTO_MERCADO + '" id="defaultCheck1"></div>';
                        productosMercado = productosMercado + "</td>";

                        //productosMercado = productosMercado + "<td> " + ((value.ID_PRODUCTO_MERCADO) ? value.ID_PRODUCTO_MERCADO : "") + "</td>";
                        productosMercado = productosMercado + "<td> " + ((value.TXT_MERCADO_TIPO) ? value.TXT_MERCADO_TIPO : "") + "</td>";
                        /*  productosMercado = productosMercado + "<td> " + ((value.TXT_DEPARTAMENTO)?value.TXT_DEPARTAMENTO:"") + "</td>";
                         productosMercado = productosMercado + "<td> " + ((value.TXT_MERCADO)?value.TXT_MERCADO:"") + "</td>"; */
                        productosMercado = productosMercado + "<td> " + ((value.TXT_PRODUCTO_GRUPO) ? value.TXT_CODIGO_GRUPO + "-" + value.TXT_PRODUCTO_GRUPO : "") + "</td>";
                        productosMercado = productosMercado + "<td> " + ((value.TXT_PRODUCTO_GENERO) ? value.TXT_CODIGO_GENERO + "-" + value.TXT_PRODUCTO_GENERO : "") + "</td>";
                        productosMercado = productosMercado + "<td> ";
                        productosMercado = productosMercado + "<select data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' class='form-control btn-modificar-producto'> ";
                        $.each(listaProductos, function(index, value2) {
                            if (value2.ID_PRODUCTO == value.ID_PRODUCTO) {
                                optionsProductos = optionsProductos + `<option value='${value2.ID_PRODUCTO}' selected>${value2.TXT_CODIGO_PRODUCTO}-${value2.TXT_PRODUCTO}</option>`;
                            } else {
                                optionsProductos = optionsProductos + `<option value='${value2.ID_PRODUCTO}' >${value2.TXT_CODIGO_PRODUCTO}-${value2.TXT_PRODUCTO}</option>`;
                            }
                        });
                        productosMercado = productosMercado + optionsProductos;
                        productosMercado = productosMercado + "</select> ";

                        productosMercado = productosMercado + "</td>";
                        productosMercado = productosMercado + "<td> ";
                        productosMercado = productosMercado + "<select data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' class='form-control btn-modificar-envase'> ";

                        $.each(listaEnvases, function(index, value2) {
                            txtUnidadMedida= ((value2.TXT_UNIDAD_MEDIDA)?value2.TXT_UNIDAD_MEDIDA:"");
                            numEquivalencia= ((value2.NUM_EQUIVALENCIA)?value2.NUM_EQUIVALENCIA:"");

                            if (value2.ID_ENVASE == value.ID_ENVASE) {
                                optionsEnvases = optionsEnvases + `<option value='${value2.ID_ENVASE}' selected>${value2.TXT_ENVASE}-${txtUnidadMedida}-${numEquivalencia}</option>`;
                            } else {
                                optionsEnvases = optionsEnvases + `<option value='${value2.ID_ENVASE}' >${value2.TXT_ENVASE}-${txtUnidadMedida}-${numEquivalencia}</option>`;
                            }
                        });
                        productosMercado = productosMercado + optionsEnvases;
                        productosMercado = productosMercado + "</select> ";
                        productosMercado = productosMercado + "</td>";
                        productosMercado = productosMercado + "<td>";
                            productosMercado = productosMercado + "<input type='text' class='form-control btn-modificar-txt_codigo_producto_mercado_alternativo ' data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' value='" + ((value.TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO) ? value.TXT_CODIGO_PRODUCTO_MERCADO_ALTERNATIVO : "") + "'>";
                        productosMercado = productosMercado + "</td>";

                        //listaEstadosPublicados

                        productosMercado = productosMercado + "<td> ";

                        productosMercado = productosMercado + "<select data-idProductoMercado='" + value.ID_PRODUCTO_MERCADO + "' class='form-control btn-modificar-habilitado'> ";

                        $.each(listaEstadosPublicados, function(index2, value2) {
                            if (value2.VALOR == value.FLG_PUBLICADO) {
                                optionsPublicados = optionsPublicados + `<option value='${value2.VALOR}' selected>${value2.TXT_DESCRIPCION}</option>`;
                            } else {
                                optionsPublicados = optionsPublicados + `<option value='${value2.VALOR}' >${value2.TXT_DESCRIPCION}</option>`;
                            }
                        });
                        productosMercado = productosMercado + optionsPublicados;
                        productosMercado = productosMercado + "</select> ";

                        //+ ((value.FLG_PUBLICADO)?value.FLG_PUBLICADO:"") + 

                        productosMercado = productosMercado + "</td>";
                        /* productosMercado = productosMercado + "<td>" ;
                            productosMercado = productosMercado + '<button data-id="' + value.ID_USUARIO + '" class="btn btn-danger btn-sm btn-eliminar-usuario" href="#"><i class="fas fa-trash"></i></button>';
                        productosMercado = productosMercado +"</td>"; */
                        productosMercado = productosMercado + "</tr>";
                    });

                    $('#lista-productos-mercado tbody').html(productosMercado);
                    $('#lista-productos-mercado').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": false,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 10,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });

                    setTimeout(function() {
                        loading.hide();
                    }, 500);
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });

    }

    async function RegionesProductoMercado() {

        var optionsDepartamentos = "<option value>Seleccionar</option>";

        $.each(JSON.parse(localStorage.getItem('region')), function(index, value) {
            optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
        });

        $("#producto_mercado-id_region").html(optionsDepartamentos);


        /*
        await   $.ajax({
                    url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones",
                    method: "POST",
                    data:{_csrf:csrf},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            var optionsDepartamentos = "<option value>Seleccionar</option>";

                            $.each(results.regiones, function( index, value ) {
                                optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                            });

                            $("#producto_mercado-id_region").html(optionsDepartamentos);

                        }
                    },
                    error:function(){
                        alert("Error al realizar el proceso.");
                    }
                });*/
    }

    async function Mercados(idRegion) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-mercados",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsMercados = "<option value>Seleccionar</option>";

                    $.each(results.mercados, function(index, value) {
                        optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                    });

                    $("#producto_mercado-id_mercado").html(optionsMercados);

                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    async function GruposProductoMercado() {

        var optionsGrupo = "<option value>Seleccionar</option>";

        $.each(JSON.parse(localStorage.getItem('grupo')).grupos, function(index, value) {
            optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
        });

        $("#producto_mercado-id_producto_grupo").html(optionsGrupo);
        /* await $.ajax({
             url: "<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-grupos",
             method: "POST",
             data: {
                 _csrf: csrf
             },
             dataType: "Json",
             beforeSend: function(xhr, settings) {
                 //loading.show();
             },
             success: function(results) {
                 if (results && results.success) {
                     var optionsGrupo = "<option value>Seleccionar</option>";

                     $.each(results.grupos, function(index, value) {
                         optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_PRODUCTO_GRUPO}</option>`;
                     });

                     $("#producto_mercado-id_producto_grupo").html(optionsGrupo);
                 }
             },
             error: function() {
                 alert("Error al realizar el proceso.");
             }
         });*/
    }

    async function GenerosProductoMercado(idProductoGrupo) {

        var generoLista = $.grep(JSON.parse(localStorage.getItem('genero')).generos, function(v) {
            return v.ID_PRODUCTO_GRUPO === idProductoGrupo;
        });

        var optionsGenero = "<option value>Seleccionar</option>";

        $.each(generoLista, function(index, value) {
            optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_CODIGO_GENERO}-${value.TXT_PRODUCTO_GENERO}</option>`;
        });

        $("#producto_mercado-id_producto_genero").html(optionsGenero);

        /*await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/genero/get-lista-generos",
            method: "POST",
            data: {
                _csrf: csrf,
                idProductoGrupo: idProductoGrupo
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsGenero = "<option value>Seleccionar</option>";

                    $.each(results.generos, function(index, value) {
                        optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_PRODUCTO_GENERO}</option>`;
                    });

                    $("#producto_mercado-id_producto_genero").html(optionsGenero);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });*/
    }

    var listaProductos = [];
    async function Productos(idProductoGenero) {
        listaProductos = [];
        await $.ajax({
            async: false,
            url: "<?= \Yii::$app->request->BaseUrl ?>/producto/get-lista-opciones-productos",
            method: "POST",
            data: {
                _csrf: csrf,
                idProductoGenero: idProductoGenero
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    $.each(results.productos, function(index, value) {
                        listaProductos.push(value);
                    });
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    var listaEnvases = [];
    async function Envases() {
        await $.ajax({
            async: false,
            url: "<?= \Yii::$app->request->BaseUrl ?>/envase/get-lista-opciones-envases",
            method: "POST",
            data: {
                _csrf: csrf
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    $.each(results.envases, function(index, value) {
                        listaEnvases.push(value);
                    });
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    var listaEstadosPublicados = [];
    async function EstadosPublicados() {
        await $.ajax({
            async: false,
            url: "<?= \Yii::$app->request->BaseUrl ?>/maestro/get-lista-maestros",
            method: "POST",
            data: {
                _csrf: csrf,
                idMaestro: 1
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    $.each(results.maestros, function(index, value) {
                        listaEstadosPublicados.push(value);
                    });
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    $('body').on('change', '#producto_mercado-id_producto_grupo', function(e) {
        e.preventDefault();
        idProductoGrupo = $(this).val();
        GenerosProductoMercado(idProductoGrupo);
    });

    $('body').on('change', '#producto_mercado-id_region', function(e) {
        e.preventDefault();
        idRegion = $(this).val();
        Mercados(idRegion);
    });

    var listaIdsProductosMercado = [];
    $('body').on('change', '.check-producto-mercado', function(e) {
        e.preventDefault();
        let idProductoMercado = $(this).attr('data-id');
        let bProductoMercado = $(this).is(":checked");

        if (bProductoMercado) {
            listaIdsProductosMercado.push(idProductoMercado);
        } else {
            listaIdsProductosMercado.splice($.inArray(idProductoMercado, listaIdsProductosMercado), 1);
        }

        console.log(listaIdsProductosMercado);
    });


    $('body').on('click', '.btn-eliminar-producto-mercado', function (e) {
        e.preventDefault();

        Swal.fire({
            title: '¿Está seguro de eliminar los registros seleccionados?',
            text: "Una vez eliminado los registros ya no se podrá visualizar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url : '<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/eliminar',
                    method: "POST",
                    data:{_csrf:csrf,listaIdsProductosMercado:listaIdsProductosMercado},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            idProductoGenero = $('#producto_mercado-id_producto_genero').val();
                            idMercado = $('#producto_mercado-id_mercado').val();
                            ProductosMercado(idProductoGenero,idMercado);
                            toastr.success('Registro eliminado');
                        }
                    },
                    error:function(){
                        toastr.error('Error al realizar el proceso.');
                    }
                });
                
            }
        })  
    });


    $('body').on('change', '#producto_mercado-id_producto_genero', function(e) {
        e.preventDefault();
        idProductoGenero = $(this).val();
        idMercado = $('#producto_mercado-id_mercado').val();
        if (!idProductoGenero) {
            ListaProductosMercado();
        }
        if (idProductoGenero && idMercado) {
            Productos(idProductoGenero);
            ProductosMercado(idProductoGenero, idMercado);
        }
    });

    $('body').on('change', '#producto_mercado-id_mercado', function(e) {
        e.preventDefault();
        idMercado = $(this).val();
        idProductoGenero = $('#producto_mercado-id_producto_genero').val();
        if (!idMercado) {
            ListaProductosMercado();
        }

        if (idProductoGenero && idMercado) {
            Productos(idProductoGenero);
            ProductosMercado(idProductoGenero, idMercado);
        }
    });



    $('body').on('click', '.btn-agregar-producto-mercado', function(e) {
        e.preventDefault();
        var idMercado = $('#producto_mercado-id_mercado').val();
        var idProductoGenero = $('#producto_mercado-id_producto_genero').val();

        var form = $('#formProductoMercado');
        var formData = $('#formProductoMercado').serializeArray();
        if (form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                toastr.error('Error al realizar el proceso.');
            },
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results.success) {
                    //setTimeout(function(){ loading.hide(); }, 2000);
                    //Usuarios();
                    ProductosMercado(idProductoGenero, idMercado)
                    $('#modal').modal('hide');
                    toastr.success('Registro grabado');
                }
            },
        });
    });

    $('body').on('change', '.btn-modificar-producto', function(e) {
        e.preventDefault();
        let idProducto = $(this).val();
        let idProductoMercado = $(this).attr('data-idProductoMercado');
        let selectProducto = $(this);
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/update?id=" + idProductoMercado,
            method: "POST",
            data: {
                _csrf: csrf,
                idProducto: idProducto
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Producto grabado');
                }else{
                    if(results.msg==0){
                        toastr.warning('No se pudo grabar el registro, realizarlo nuevamente'); 
                    }else if(results.msg==1){
                        toastr.warning('El producto,envase y equivalencia ya existe');
                        selectProducto.val('');
                    }
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });

    $('body').on('change', '.btn-modificar-envase', function(e) {
        e.preventDefault();
        let idEnvase = $(this).val();
        let idProductoMercado = $(this).attr('data-idProductoMercado');
        let selectEnvase = $(this);
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/update?id=" + idProductoMercado,
            method: "POST",
            data: {
                _csrf: csrf,
                idEnvase: idEnvase
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Envase grabado');
                }else{
                    if(results.msg==0){
                        toastr.warning('No se pudo grabar el registro, realizarlo nuevamente'); 
                    }else if(results.msg==1){
                        toastr.warning('El producto,envase y equivalencia ya existe');
                        selectEnvase.val('');
                    }
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });

    $('body').on('change', '.btn-modificar-habilitado', function(e) {
        e.preventDefault();
        let flgHabilitado = $(this).val();
        let idProductoMercado = $(this).attr('data-idProductoMercado');
        
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/update?id=" + idProductoMercado,
            method: "POST",
            data: {
                _csrf: csrf,
                flgHabilitado: flgHabilitado
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Estado grabado');
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });


    $('body').on('blur', '.btn-modificar-txt_codigo_producto_mercado_alternativo', function(e) {
        e.preventDefault();
        let txtCodigoProductoMercadoAlternativo = $(this).val();
        let idProductoMercado = $(this).attr('data-idProductoMercado');
        let inputtxtCodigoProductoMercadoAlternativo = $(this);

        $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/update?id=' + idProductoMercado,
            method: "POST",
            data: {
                _csrf: csrf,
                txtCodigoProductoMercadoAlternativo: txtCodigoProductoMercadoAlternativo
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Código de producto mercado Alternativo grabado');
                }else{
                    if(results.msg==0){
                        toastr.warning('No se pudo grabar el registro, realizarlo nuevamente'); 
                    }else if(results.msg==1){
                        toastr.warning('El producto,envase y equivalencia ya existe');
                        inputtxtCodigoProductoMercadoAlternativo.val('');
                    }
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });

    function init() {
        ListaProductosMercado();
        RegionesProductoMercado();
        GruposProductoMercado();
        Envases();
        EstadosPublicados();
        //Productos();
        //ProductosMercado();
    }

    init();
</script>