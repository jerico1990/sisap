<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Productos por Mercado</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de Productos por Mercado</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formProductoMercado', 'class' => 'form-horizontal']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Región</label>
                            <select id="producto_mercado-id_region" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Mercado</label>
                            <select name="ProductoMercado[ID_MERCADO]" id="producto_mercado-id_mercado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Grupo</label>
                            <select id="producto_mercado-id_producto_grupo" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Genero</label>
                            <select name="ProductoMercado[ID_PRODUCTO_GENERO]" id="producto_mercado-id_producto_genero" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/create" class="btn btn-success">Agregar</a>
                        </div>
                    </div>
                </div> -->

                <table id="lista-productos-mercado" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Región</th>
                        <th>Tipo de Mercado</th>
                        <th>Mercado</th>
                        <th>Grupo</th>
                        <th>Genero</th>
                        <th>Producto</th>
                        <th>Envase</th>
                        <th>Publicado</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');
    var idMercado;
    var idRegion;
    var idProductoGrupo;
    var idProductoGenero;

    function listarProductosMercado(){
        $('#lista-productos-mercado').DataTable({
            "dom": "Bfrtip",
            buttons: [
                'excelHtml5',
                'csvHtml5',
            ],
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "pageLength": 10,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningun dato disponible en esta lista",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });

        setTimeout(function() {
                        loading.hide();
                    }, 1000);
    }
    
    
    async function ProductosMercado() {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/get-lista-productos-mercado',
            method: 'POST',
            data: {
                _csrf: csrf,
                idMercado: idMercado,
                idRegion: idRegion,
                idProductoGrupo: idProductoGrupo,
                idProductoGenero: idProductoGenero,
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var productosMercado = "";
                    $('#lista-productos-mercado').DataTable().destroy();

                    $.each(results.productosMercado, function(index, value) {
                        txtUnidadMedida= ((value.TXT_UNIDAD_MEDIDA)?value.TXT_UNIDAD_MEDIDA:"");
                        numEquivalencia= ((value.NUM_EQUIVALENCIA)?value.NUM_EQUIVALENCIA:"");

                        productosMercado = productosMercado + "<tr>";
                            productosMercado = productosMercado + "<td> " + ((value.TXT_DEPARTAMENTO) ? value.TXT_DEPARTAMENTO : "") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.TXT_MERCADO_TIPO) ? value.TXT_MERCADO_TIPO : "") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.TXT_MERCADO) ? value.TXT_MERCADO : "") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.TXT_PRODUCTO_GRUPO) ? value.TXT_CODIGO_GRUPO + "-" + value.TXT_PRODUCTO_GRUPO : "") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.TXT_PRODUCTO_GENERO) ? value.TXT_CODIGO_GENERO + "-" + value.TXT_PRODUCTO_GENERO : "") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.TXT_PRODUCTO) ? value.TXT_CODIGO_PRODUCTO + '-' + value.TXT_PRODUCTO : "") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.TXT_ENVASE) ? value.TXT_ENVASE + "-" + txtUnidadMedida + "-" + numEquivalencia : "") + "</td>";
                            productosMercado = productosMercado + "<td> " + ((value.TXT_PUBLICADO) ? value.TXT_PUBLICADO : "") + "</td>";
                        productosMercado = productosMercado + "</tr>";
                    });

                    $('#lista-productos-mercado tbody').html(productosMercado);
                    $('#lista-productos-mercado').DataTable({
                        "dom": "Bfrtip",
                        buttons: [
                            'excelHtml5',
                            'csvHtml5',
                        ],
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 10,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });

                    setTimeout(function() {
                        loading.hide();
                    }, 1000);
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }


    async function RegionesProductoMercado() {

        var optionsDepartamentos = "<option value>Seleccionar</option>";

        $.each(JSON.parse(localStorage.getItem('region')), function(index, value) {
            optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
        });

        $("#producto_mercado-id_region").html(optionsDepartamentos);




        /* await   $.ajax({
                     url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones",
                     method: "POST",
                     data:{_csrf:csrf},
                     dataType:"Json",
                     beforeSend:function(xhr, settings)
                     {
                         //loading.show();
                     },
                     success:function(results)
                     {   
                         if(results && results.success){
                             var optionsDepartamentos = "<option value>Seleccionar</option>";

                             $.each(results.regiones, function( index, value ) {
                                 optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                             });

                             $("#producto_mercado-id_region").html(optionsDepartamentos);

                         }
                     },
                     error:function(){
                         alert("Error al realizar el proceso.");
                     }
                 });*/


    }

    async function Mercados(idRegion) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-mercados",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsMercados = "<option value>Seleccionar</option>";

                    $.each(results.mercados, function(index, value) {
                        optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                    });

                    $("#producto_mercado-id_mercado").html(optionsMercados);

                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    }

    async function GruposProductoMercado() {
        var optionsGrupo = "<option value>Seleccionar</option>";

        $.each(JSON.parse(localStorage.getItem('grupo')).grupos, function(index, value) {
            optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
        });

        $("#producto_mercado-id_producto_grupo").html(optionsGrupo);

        /*await   $.ajax({
                    url: "<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-grupos",
                    method: "POST",
                    data:{_csrf:csrf},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            var optionsGrupo = "<option value>Seleccionar</option>";

                            $.each(results.grupos, function( index, value ) {
                                optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_PRODUCTO_GRUPO}</option>`;
                            });

                            $("#producto_mercado-id_producto_grupo").html(optionsGrupo);
                        }
                    },
                    error:function(){
                        alert("Error al realizar el proceso.");
                    }
                });*/
    }

    async function GenerosProductoMercado(idProductoGrupo) {


        var generoLista = $.grep(JSON.parse(localStorage.getItem('genero')).generos, function(v) {
            return v.ID_PRODUCTO_GRUPO === idProductoGrupo;
        });


        var optionsGenero = "<option value>Seleccionar</option>";

        $.each(generoLista, function(index, value) {
            optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_CODIGO_GENERO}-${value.TXT_PRODUCTO_GENERO}</option>`;
        });

        $("#producto_mercado-id_producto_genero").html(optionsGenero);

        /*await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/genero/get-lista-generos",
            method: "POST",
            data: {
                _csrf: csrf,
                idProductoGrupo: idProductoGrupo
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsGenero = "<option value>Seleccionar</option>";

                    $.each(results.generos, function(index, value) {
                        optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_PRODUCTO_GENERO}</option>`;
                    });

                    $("#producto_mercado-id_producto_genero").html(optionsGenero);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });*/
    }

    function init() {
        listarProductosMercado();
        RegionesProductoMercado();
        GruposProductoMercado();
    }

    init();

    $('body').on('change', '#producto_mercado-id_producto_grupo', function(e) {
        e.preventDefault();
        idProductoGrupo = $(this).val();
        GenerosProductoMercado(idProductoGrupo);
        ProductosMercado();
    });

    $('body').on('change', '#producto_mercado-id_region', function(e) {
        e.preventDefault();
        idRegion = $(this).val();
        Mercados(idRegion);
        ProductosMercado();
    });

    $('body').on('change', '#producto_mercado-id_mercado', function(e) {
        e.preventDefault();
        idMercado = $(this).val();
        ProductosMercado();
    });

    $('body').on('change', '#producto_mercado-id_producto_genero', function(e) {
        e.preventDefault();
        idProductoGenero = $(this).val();
        ProductosMercado();
    });
</script>