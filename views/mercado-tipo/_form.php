<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formMercadoTipo','class' => 'form-horizontal']]); ?>

<div class="modal-header">
    <h4 class="modal-title"><?= $model->titulo ?></h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">×</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'TXT_MERCADO_TIPO')->label('Descripción') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <?= $form->field($model, 'FLG_HABILITADO')->dropDownList([1=>'Habilitado',2=>'Deshabilitado'], ['prompt' => 'Seleccionar' ])->label('Estado'); ?>
        </div>
    </div>
</div>
<div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
    <button type="button" class="btn btn-success btn-grabar-mercado-tipo">Grabar</button>
</div>

<?php ActiveForm::end(); ?>