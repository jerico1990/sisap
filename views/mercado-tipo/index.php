<style>
    #lista-mercados-tipo_filter{
        display:none
    }
</style>
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Mercados tipo</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de mercados tipo</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Descripción</label>
                            <input type="text" id="txt_mercado_tipo" class="form-control" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <button type="button" class="btn btn-success btn-agregar-mercado-tipo">Agregar</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <label for="">&nbsp&nbsp</label> <br>
                        <button style="float: right;" class="btn btn-primary btn-buscar-mercado-tipo">Buscar</button>
                    </div>
                </div>

                <table id="lista-mercados-tipo" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Descripción</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('.staticBackdrop');

$('#lista-mercados-tipo').DataTable();

MercadosTipo();
async function MercadosTipo(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/mercado-tipo/get-lista-mercados-tipo',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var mercadosTipo ="";
                        $('#lista-mercados-tipo').DataTable().destroy();
                        
                        $.each(results.mercadosTipo, function( index, value ) {
                            mercadosTipo = mercadosTipo + "<tr>";
                                mercadosTipo = mercadosTipo + "<td> " + value.TXT_MERCADO_TIPO + "</td>";
                                mercadosTipo = mercadosTipo + "<td> " + value.TXT_DESCRIPCION_HABILITADO + "</td>";
                                mercadosTipo = mercadosTipo + "<td>" ;
                                    mercadosTipo = mercadosTipo + '<button data-id="' + value.ID_MERCADO_TIPO + '" class="btn btn-info btn-sm btn-modificar-mercado-tipo" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    mercadosTipo = mercadosTipo + '<button data-id="' + value.ID_MERCADO_TIPO + '" class="btn btn-danger btn-sm btn-eliminar-mercado-tipo" href="#"><i class="fas fa-trash"></i></button>';
                                mercadosTipo = mercadosTipo +"</td>";
                            mercadosTipo = mercadosTipo + "</tr>";
                        });
                        
                        $('#lista-mercados-tipo tbody').html(mercadosTipo);
                        $('#lista-mercados-tipo').DataTable({
                            "dom": "Bfrtip",
                            buttons: [
                                'excelHtml5',
                                'csvHtml5',
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": true,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.hide(); }, 1000);
                    }
                },
                error:function(){
                    toastr.error('Error al realizar el proceso.');
                }
            });
}


//buscar
$('body').on('click', '.btn-buscar-mercado-tipo', function(e) {
        e.preventDefault();
        var descripcion=$("#txt_mercado_tipo").val(); 
        $('#lista-mercados-tipo').DataTable().column(0).search(descripcion).draw();
    });

//agregar

$('body').on('click', '.btn-agregar-mercado-tipo', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/mercado-tipo/create');
    $('#modal').modal('show');
});

//modificar
$('body').on('click', '.btn-modificar-mercado-tipo', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/mercado-tipo/update?id='+id);
    $('#modal').modal('show');
});

//eliminar
$('body').on('click', '.btn-eliminar-mercado-tipo', function (e) {
    e.preventDefault();
    idMercadoTipo = $(this).attr('data-id');
    Swal.fire({
        title: '¿Está seguro de deshabilitar el registro?',
        //text: "Una vez eliminado el registro ya no se podrá visualizar",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, deshabilitar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {

            $.ajax({
                url : '<?= \Yii::$app->request->BaseUrl ?>/mercado-tipo/eliminar',
                method: "POST",
                data:{_csrf:csrf,idMercadoTipo:idMercadoTipo},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        MercadosTipo();
                        toastr.success('Registro deshabilitado');
                    }
                },
                error:function(){
                    toastr.error('Error al realizar el proceso.');
                }
            });
            
        }
    });
});

//grabar

$('body').on('click', '.btn-grabar-mercado-tipo', function (e) {

    e.preventDefault();
    var form = $('#formMercadoTipo');
    var formData = $('#formMercadoTipo').serializeArray();


    var error = "";
    var txtMercadoTipo = $.trim($("[name=\"MercadoTipo[TXT_MERCADO_TIPO]\"]").val());
    var flgHabilitado = $.trim($("[name=\"MercadoTipo[FLG_HABILITADO]\"]").val());


    if (!txtMercadoTipo) {
        $('#mercadotipo-txt_mercado_tipo').addClass('alert-danger');
        $('#mercadotipo-txt_mercado_tipo').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#mercadotipo-txt_mercado_tipo').removeClass('alert-danger');
        $('#mercadotipo-txt_mercado_tipo').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (!flgHabilitado) {
        $('#mercadotipo-flg_habilitado').addClass('alert-danger');
        $('#mercadotipo-flg_habilitado').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#mercadotipo-flg_habilitado').removeClass('alert-danger');
        $('#mercadotipo-flg_habilitado').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }


    if (error != "") {
        return false;
    }

    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
            toastr.error('Error al realizar el proceso.');
        },
        beforeSend:function()
        {
            loading.show();
        },
        success: function (results) {
            if(results.success){
                //setTimeout(function(){ loading.hide(); }, 1000);
                MercadosTipo();
                $('#modal').modal('hide');
                toastr.success('Regitro grabado');
            }else{
                toastr.warning('No se pudo grabar el registro, realizarlo nuevamente'); 
            }
        },
    });
});





</script>
