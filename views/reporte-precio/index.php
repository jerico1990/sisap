<!-- start page title -->

<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="<?= \Yii::$app->request->BaseUrl ?>/bootstrap-multiselect-master/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/bootstrap-multiselect-master/css/bootstrap-multiselect.css" type="text/css"/>
<style>
.multiselect-native-select .btn-group,.multiselect-native-select .btn-group .multiselect {
    width: 100% !important;
}

.dropdown-menu.show{
    width:100%;
}
</style>
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Reportes</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Reporte de Precio</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for>Región</label>
                            <select class="form-control" id="id_region" multiple="multiple">
                                <!-- <option value>Seleccionar</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for>Tipo de mercado</label>
                            <select class="form-control" id="id_tipo_mercado" multiple="multiple">
                                <!-- <option value>Seleccionar</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for>Mercado</label>
                            <select class="form-control" id="id_mercado" multiple="multiple">
                                <!-- <option value>Seleccionar</option> -->
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for>Temporalidad</label>
                            <select class="form-control" id="temporalidad">
                                <option value>Seleccionar</option>
                                <option value="1">Diario</option>
                                <option value="2">Mensual</option>
                                <option value="3">Anual</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 diario" style="display:none">
                        <div class="form-group">
                            <label for>Día Inicio</label>
                            <input type="date" class="form-control" id="diario_inicio">
                        </div>
                    </div>
                    <div class="col-md-4 diario" style="display:none">
                        <div class="form-group">
                            <label for>Día Fin</label>
                            <input type="date" class="form-control" id="diario_fin">
                        </div>
                    </div>
                    <div class="col-md-4 meses" style="display:none">
                        <div class="form-group">
                            <label for>Meses</label>
                            <select multiple="multiple" class="form-control" id="meses" >
                                <option value="01">Enero</option>
                                <option value="02">Febrero</option>
                                <option value="03">Marzo</option>
                                <option value="04">Abril</option>
                                <option value="05">Mayo</option>
                                <option value="06">Junio</option>
                                <option value="07">Julio</option>
                                <option value="08">Agosto</option>
                                <option value="09">Septiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>
                            </select>
                        </div>
                        
                    </div>
                    <div class="col-md-4 anios" style="display:none">
                        <div class="form-group">
                            <label for="">Años</label>
                            <select id="anios" class="form-control" multiple="multiple" >
                                <?php for($i=1997;$i<=date("Y");$i++){ ?>
                                    <option value="<?= $i ?>"><?= $i ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for>Producto/Variedad</label>
                            <select id="variedades" multiple="multiple" class="form-control">
                                <option value>Seleccionar</option>
                            </select>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <table id="lista-productos" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Fecha</th>
                        <th>Región</th>
                        <!-- <th>Tipo de Mercado</th>-->
                        <th>Mercado</th> 
                        <th>Variedad</th>
                        <th>Minimo</th>
                        <th>Maximo</th>
                        <th>Promedio</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<script>
$("#modalInformativo").modal('show');

var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
var loading = $('.staticBackdrop');
var idRegion;
var idMercadoTipo;
var temporalidad;
var idMercado;
var variedades;
var diario_inicio;
var diario_fin;
var meses;
var anios;
var configuracionMultiselect = {
                    selectAllText:' Todos',
                    //allSelectedText: 'Todos',
                    enableClickableOptGroups: true,
                    enableCollapsibleOptGroups: true,
                    enableFiltering: true,
                    includeSelectAllOption: true,
                    filterPlaceholder: 'Búsqueda',
                    buttonClass : "col-md-12 form-control",
                    maxHeight: 200,
                    onInitialized: function(select, container) {
                        
                    },
                    buttonText: function(options) {
                        if (options.length === 0) {
                            return 'Ningún seleccionado';
                        }
                        else if (options.length > 3) {
                            return options.length + ' seleccionados';
                        }
                        else {
                            var selected = [];
                            options.each(function() {
                                selected.push([$(this).text(), $(this).data('order')]);
                            });

                            selected.sort(function(a, b) {
                                return a[1] - b[1];
                            });

                            var text = '';
                            for (var i = 0; i < selected.length; i++) {
                                text += selected[i][0] + ', ';
                            }

                            return text.substr(0, text.length -2);
                        }
                    }
                };

setTimeout(function() {
    loading.hide();
}, 1000);

$('body').on('change', '#temporalidad', function (e) {
    e.preventDefault();
    temporalidad = $(this).val();
    $('.diario').hide();
    $('.meses').hide();
    $('.anios').hide();

    $('#diario_inicio').val('');
    $('#diario_fin').val('');
    $('#meses').val('');
    $('#anios').val('');

    if(temporalidad=="1"){
        $('.diario').show();
    }else if(temporalidad=="2"){
        $('.meses').show();
        $('.anios').show();
    }else if(temporalidad=="3"){
        $('.anios').show();
    }
});


function ListaPrecios(){
    
    $('#lista-productos').DataTable().destroy();
    $('#lista-productos tbody').html("");
    $('#lista-productos').DataTable({
                            "dom": "Bfrtip",
                            buttons: [
                                {extend:'excelHtml5',className: 'btn-success',},
                                {extend:'csvHtml5',className: 'btn-success',},
                                {text: 'Consultar',className: 'btn-info btn-consultar',}
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });
}


async function Precios(){
        
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/reporte-precio/get-lista-precios',
                method: 'POST',
                data:{_csrf:csrf,diario_inicio:diario_inicio,diario_fin:diario_fin,meses:meses,anios:anios,temporalidad:temporalidad,idRegion:idRegion,idMercadoTipo:idMercadoTipo,idMercado:idMercado,variedades:variedades},
                dataType:'Json',
                beforeSend:function(){
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var precios ="";
                        $('#lista-productos').DataTable().destroy();

                        $.each(results.precios, function( index, value ) {
                            fecha="";
                            if(temporalidad == "1"){
                                fecha = value.TXT_ANIO + "-" + value.TXT_MES + "-" + value.TXT_DIA;
                            }else if(temporalidad == "2"){
                                fecha = value.TXT_ANIO + "-" + value.TXT_MES;
                            }else if(temporalidad == "3"){
                                fecha = value.TXT_ANIO;
                            }


                            precios = precios + "<tr>";
                                precios = precios + "<td> " + ((fecha)?fecha:"") + "</td>";
                                precios = precios + "<td> " + ((value.TXT_DEPARTAMENTO)?value.TXT_DEPARTAMENTO:"") + "</td>";
                                precios = precios + "<td> " + ((value.TXT_MERCADO)?value.TXT_MERCADO :"") + "</td>";
                                precios = precios + "<td> " + ((value.TXT_PRODUCTO)?value.TXT_CODIGO_PRODUCTO + "-" +value.TXT_PRODUCTO:"") + "</td>";
                                precios = precios + "<td> " + ((value.NUM_PRECIO_MINIMO)?parseFloat(value.NUM_PRECIO_MINIMO).toFixed(2):"") + "</td>";
                                precios = precios + "<td> " + ((value.NUM_PRECIO_MAXIMO)?parseFloat(value.NUM_PRECIO_MAXIMO).toFixed(2):"") + "</td>";
                                precios = precios + "<td> " + ((value.NUM_PRECIO_PROMEDIO)?parseFloat(value.NUM_PRECIO_PROMEDIO).toFixed(2):"") + "</td>";
                                
                            precios = precios + "</tr>";
                        });
                        
                        $('#lista-productos tbody').html(precios);
                        $('#lista-productos').DataTable({
                            "dom": "Bfrtip",
                            buttons: [
                                {extend:'excelHtml5',className: 'btn-success',},
                                {extend:'csvHtml5',className: 'btn-success',},
                                {text: 'Consultar',className: 'btn-info btn-consultar',}
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.hide(); }, 500);
                    }
                },
                error:function(){
                    toastr.error('Error al realizar el proceso.');
                }
            });

}


async function Regiones() {

    await   $.ajax({
                    url: "<?= \Yii::$app->request->BaseUrl ?>/reporte-precio/get-regiones",
                    method: "POST",
                    data:{_csrf:csrf},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            //var optionsDepartamentos = "<option value>Seleccionar</option>";
                            var optionsDepartamentos = "";

                            $.each(results.regiones, function( index, value ) {
                                optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                            });

                            $("#id_region").html(optionsDepartamentos);

                            $('#id_region').multiselect(configuracionMultiselect);

                        }
                    },
                    error:function(){
                        alert("Error al realizar el proceso.");
                    }
                });
            
}

async function MercadosTipo() {
    await $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/reporte-precio/get-lista-mercados-tipo",
        method: "POST",
        data: {
            _csrf: csrf
        },
        dataType: "Json",
        beforeSend: function(xhr, settings) {
            //loading.show();
        },
        success: function(results) {
            if (results && results.success) {
                var optionsTipoMercado = "";

                $.each(results.mercadosTipo, function(index, value) {
                    optionsTipoMercado = optionsTipoMercado + `<option value='${value.ID_MERCADO_TIPO}'>${value.TXT_MERCADO_TIPO}</option>`;
                });

                $("#id_tipo_mercado").html(optionsTipoMercado);

                $('#id_tipo_mercado').multiselect(configuracionMultiselect);
            }
        },
        error: function() {
            alert("Error al realizar el proceso.");
        }
    });
}
$('#id_mercado').multiselect(configuracionMultiselect);

async function Mercados(idRegion,idMercadoTipo) {
    await $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/reporte-precio/get-lista-opciones-mercados",
        method: "POST",
        data: {
            _csrf: csrf,
            idRegion: idRegion,
            idMercadoTipo:idMercadoTipo
        },
        dataType: "Json",
        beforeSend: function(xhr, settings) {
            //loading.show();
        },
        success: function(results) {
            if (results && results.success) {
                $('#id_mercado').multiselect('destroy');
                var optionsMercados = "";

                $.each(results.mercados, function(index, value) {
                    optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                });

                $("#id_mercado").html(optionsMercados);

                $('#id_mercado').multiselect(configuracionMultiselect);

            }
        },
        error: function() {
            toastr.error('Error al realizar el proceso.');
        }
    });
}

async function Variedades(idMercado) {

    await $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/reporte-precio/get-lista-opciones-generos-variedades",
        method: "POST",
        data: {
            _csrf: csrf,
            idMercado: idMercado
        },
        dataType: "Json",
        beforeSend: function(xhr, settings) {
            loading.show();
        },
        success: function(results) {
            if (results && results.success) {
                $('#variedades').multiselect('destroy');
                var optionsGeneros = "";
                $.each(results.generos, function(index, value) {
                    optionsGeneros = optionsGeneros + `<optgroup label="${value.txtProductoGenero}">`;
                        $.each(value.variedades, function(index2, value2) {
                            optionsGeneros = optionsGeneros + `<option value="${value2.idProducto}">${value2.txtProducto}</option>`;
                        });
                    optionsGeneros = optionsGeneros + `</optgroup>`;
                });

                $("#variedades").html(optionsGeneros);

                
                $('#variedades').multiselect(configuracionMultiselect);
                setTimeout(function() {
                        loading.hide();
                    }, 1000);
            }
        },
        error: function() {
            toastr.error('Error al realizar el proceso.');
        }
    });
}

$('body').on('change', '#id_region', function(e) {
    e.preventDefault();
    idRegion = $(this).val();

    if(idRegion && idMercadoTipo){
        Mercados(idRegion,idMercadoTipo);
    }

});

$('body').on('change', '#id_tipo_mercado', function(e) {
    e.preventDefault();
    idMercadoTipo = $(this).val();
    if(idRegion && idMercadoTipo){
        Mercados(idRegion,idMercadoTipo);
    }
});

$('body').on('change', '#id_mercado', function(e) {
    e.preventDefault();
    idMercado = $(this).val();
    if(idMercado){
        Variedades(idMercado);
    }
});

$('body').on('change', '#variedades', function(e) {
    e.preventDefault();
    variedades = $(this).val();
});


$('body').on('click', '.btn-consultar', function(e) {
    e.preventDefault();
    diario_inicio = $("#diario_inicio").val();
    diario_fin = $("#diario_fin").val();
    meses = $("#meses").val();
    anios = $("#anios").val();
    
    if(diario_inicio!='' || meses!='' || anios!=''){
        Precios();
    }else{
        alert("Debe seleccionar una temporalidad y llenar la información")
    }
});


$('#variedades').multiselect(configuracionMultiselect);

$('#meses').multiselect(configuracionMultiselect);

$('#anios').multiselect(configuracionMultiselect);

function init() {
    ListaPrecios();
    Regiones();
    MercadosTipo();
}

init();

</script>