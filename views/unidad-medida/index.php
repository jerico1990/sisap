<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Unidad de medida</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de unidades de medidas</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_codigo">Código</label>
                            <input type="text" id="txt_codigo" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_descripcion">Descripción</label>
                            <input type="text" id="txt_descripcion" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-agregar-unidad-medida">Agregar</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <label for="">&nbsp&nbsp</label> <br>
                        <button style="float: right;" class="btn btn-primary btn-buscar-unidad-medida">Buscar</button>
                    </div>
                </div>

                <table id="lista-unidades-medidas" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Abreviatura</th>
                        <th>Descripción</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('.staticBackdrop');

$('#lista-unidades-medidas').DataTable();

UnidadesMedidas();
async function UnidadesMedidas(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/get-lista-unidades-medidas',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var unidadesMedidas ="";
                        $('#lista-unidades-medidas').DataTable().destroy();
                        
                        $.each(results.unidadesMedidas, function( index, value ) {
                            unidadesMedidas = unidadesMedidas + "<tr>";
                                unidadesMedidas = unidadesMedidas + "<td> " + value.TXT_CODIGO_UNIDAD_MEDIDA + "</td>";
                                unidadesMedidas = unidadesMedidas + "<td> " + value.TXT_UNIDAD_MEDIDA + "</td>";
                                unidadesMedidas = unidadesMedidas + "<td> " + value.TXT_DESCRIPCION_HABILITADO + "</td>";
                                unidadesMedidas = unidadesMedidas + "<td>" ;
                                    unidadesMedidas = unidadesMedidas + '<button data-id="' + value.ID_UNIDAD_MEDIDA + '" class="btn btn-info btn-sm btn-modificar-unidad-medida" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    unidadesMedidas = unidadesMedidas + '<button data-id="' + value.ID_UNIDAD_MEDIDA + '" class="btn btn-danger btn-sm btn-eliminar-unidad-medida" href="#"><i class="fas fa-trash"></i></button>';
                                unidadesMedidas = unidadesMedidas +"</td>";
                            unidadesMedidas = unidadesMedidas + "</tr>";
                        });
                        
                        $('#lista-unidades-medidas tbody').html(unidadesMedidas);
                        $('#lista-unidades-medidas').DataTable({
                            "dom": "Bfrtip",
                            buttons: [
                                'excelHtml5',
                                'csvHtml5',
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": true,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.hide(); }, 1000);
                        $(".dataTables_filter").hide();
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


    //Buscar
    $('body').on('click', '.btn-buscar-unidad-medida', function(e) {
        e.preventDefault();
        var codigo=$("#txt_codigo").val();
        var descripcion=$("#txt_descripcion").val();

        $('#lista-unidades-medidas').DataTable().column(0).search(codigo).draw();
        $('#lista-unidades-medidas').DataTable().column(1).search(descripcion).draw();
              
    });


//agregar

$('body').on('click', '.btn-agregar-unidad-medida', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/create');
    $('#modal').modal('show');
});

//modificar
$('body').on('click', '.btn-modificar-unidad-medida', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/update?id='+id);
    $('#modal').modal('show');
});

//eliminar
$('body').on('click', '.btn-eliminar-unidad-medida', function (e) {
    e.preventDefault();
    idUnidadMedida = $(this).attr('data-id');
    Swal.fire({
        title: '¿Está seguro de deshabilitar el registro?',
        //text: "Una vez eliminado el registro ya no se podrá visualizar",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, deshabilitar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {

            $.ajax({
                url : '<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/eliminar',
                method: "POST",
                data:{_csrf:csrf,idUnidadMedida:idUnidadMedida},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        UnidadesMedidas();
                        toastr.success('Registro deshabilitado');
                    }
                },
                error:function(){
                    toastr.error('Error al realizar el proceso.');
                }
            });
            
        }
    });
});

//grabar

$('body').on('click', '.btn-grabar-unidad-medida', function (e) {

    e.preventDefault();
    var form = $('#formUnidadMedida');
    var formData = $('#formUnidadMedida').serializeArray();

    var error = "";
    var txtCodigoUnidadMedida = $.trim($("[name=\"UnidadMedida[TXT_CODIGO_UNIDAD_MEDIDA]\"]").val());
    var txtUnidadMedida = $.trim($("[name=\"UnidadMedida[TXT_UNIDAD_MEDIDA]\"]").val());
    var flgHabilitado = $.trim($("[name=\"UnidadMedida[FLG_HABILITADO]\"]").val());

    if (!txtCodigoUnidadMedida) {
        $('#unidadmedida-txt_codigo_unidad_medida').addClass('alert-danger');
        $('#unidadmedida-txt_codigo_unidad_medida').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#unidadmedida-txt_codigo_unidad_medida').removeClass('alert-danger');
        $('#unidadmedida-txt_codigo_unidad_medida').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (!txtUnidadMedida) {
        $('#unidadmedida-txt_unidad_medida').addClass('alert-danger');
        $('#unidadmedida-txt_unidad_medida').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#unidadmedida-txt_unidad_medida').removeClass('alert-danger');
        $('#unidadmedida-txt_unidad_medida').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (!flgHabilitado) {
        $('#unidadmedida-flg_habilitado').addClass('alert-danger');
        $('#unidadmedida-flg_habilitado').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#unidadmedida-flg_habilitado').removeClass('alert-danger');
        $('#unidadmedida-flg_habilitado').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (error != "") {
        return false;
    }

    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            toastr.error('Error al realizar el proceso.');
        },
        beforeSend:function()
        {
            loading.show();
        },
        success: function (results) {
            if(results.success){
                //setTimeout(function(){ loading.hide(); }, 1000);
                UnidadesMedidas();
                $('#modal').modal('hide');
                toastr.success('Registro grabado');
            }else{
                    if(results.msg==0){
                        toastr.warning('No se pudo grabar el registro, realizarlo nuevamente'); 
                    }else if(results.msg==1){
                        setTimeout(function() {
                            loading.hide();
                        }, 1000);
                        toastr.warning('La abreviatura para la unidad de medida ya existe'); 
                    }
                    
                }
        },
    });
});




</script>
