<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Grupo</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de grupos</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_codigo">Código</label>
                            <input type="text" id="txt_codigo" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_descripcion">Descripción</label>
                            <input type="text" id="txt_descripcion" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-agregar-grupo">Agregar</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <label for="">&nbsp&nbsp</label> <br>
                        <button style="float: right;" class="btn btn-primary btn-buscar-grupo">Buscar</button>
                    </div>
                </div>

                <table id="lista-grupos" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Código</th>
                        <th>Descripción</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');

    $('#lista-grupos').DataTable();


    PintarGrupo(JSON.parse(localStorage.getItem('grupo')));


    function PintarGrupo(results) {

        var grupos = "";
        loading.show();

        $('#lista-grupos').DataTable().destroy();

        $.each(results.grupos, function(index, value) {
            grupos = grupos + "<tr>";
            grupos = grupos + "<td> " + value.TXT_CODIGO_GRUPO + "</td>";
            grupos = grupos + "<td> " + value.TXT_PRODUCTO_GRUPO + "</td>";
            grupos = grupos + "<td> " + value.TXT_DESCRIPCION_HABILITADO + "</td>";
            grupos = grupos + "<td>";
            grupos = grupos + '<button data-id="' + value.ID_PRODUCTO_GRUPO + '" class="btn btn-info btn-sm btn-modificar-grupo" href="#"><i class="fas fa-pencil-alt"></i></button> ';
            grupos = grupos + '<button data-id="' + value.ID_PRODUCTO_GRUPO + '" class="btn btn-danger btn-sm btn-eliminar-grupo" href="#"><i class="fas fa-trash"></i></button>';
            grupos = grupos + "</td>";
            grupos = grupos + "</tr>";
        });

        $('#lista-grupos tbody').html(grupos);
        $('#lista-grupos').DataTable({
            "dom": "Bfrtip",
            buttons: [
                'excelHtml5',
                'csvHtml5',
            ],
            "paging": true,
            "bFilter": false,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "pageLength": 10,

            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningun dato disponible en esta lista",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });

        setTimeout(function() {
            loading.hide();
        }, 1000);
        $(".dataTables_filter").hide();
    }


    //Buscar
    $('body').on('click', '.btn-buscar-grupo', function(e) {
        e.preventDefault();
        var codigo = $("#txt_codigo").val();
        var descripcion = $("#txt_descripcion").val();
        $('#lista-grupos').DataTable().column(0).search(codigo).draw();
        $('#lista-grupos').DataTable().column(1).search(descripcion).draw();
    });


    //agregar

    $('body').on('click', '.btn-agregar-grupo', function(e) {
        e.preventDefault();
        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/grupo/create');
        $('#modal').modal('show');
    });

    //modificar
    $('body').on('click', '.btn-modificar-grupo', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');

        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/grupo/update?id=' + id);
        $('#modal').modal('show');
    });

    //eliminar
    $('body').on('click', '.btn-eliminar-grupo', function (e) {
        e.preventDefault();
        idProductoGrupo = $(this).attr('data-id');
        Swal.fire({
            title: '¿Está seguro de deshabilitar el registro?',
            //text: "Una vez eliminado el registro ya no se podrá visualizar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, deshabilitar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url : '<?= \Yii::$app->request->BaseUrl ?>/grupo/eliminar',
                    method: "POST",
                    data:{_csrf:csrf,idProductoGrupo:idProductoGrupo},
                    dataType:"Json",
                    beforeSend:function(xhr, settings)
                    {
                        //loading.show();
                    },
                    success:function(results)
                    {   
                        if(results && results.success){
                            Grupos();
                            PintarGrupo(JSON.parse(localStorage.getItem('grupo')));
                            toastr.success('Registro deshabilitado');
                        }
                    },
                    error:function(){
                        toastr.error('Error al realizar el proceso.');
                    }
                });
                
            }
        });
    });


    //grabar

    $('body').on('click', '.btn-grabar-grupo', function(e) {

        e.preventDefault();
        var form = $('#formGrupo');
        var formData = $('#formGrupo').serializeArray();
        var error = "";
        var txtCodigoGrupo = $.trim($("[name=\"Grupo[TXT_CODIGO_GRUPO]\"]").val());
        var txtProductoGrupo = $.trim($("[name=\"Grupo[TXT_PRODUCTO_GRUPO]\"]").val());
        var flgHabilitado = $.trim($("[name=\"Grupo[FLG_HABILITADO]\"]").val());

        if (!txtCodigoGrupo) {
            $('#grupo-txt_codigo_grupo').addClass('alert-danger');
            $('#grupo-txt_codigo_grupo').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#grupo-txt_codigo_grupo').removeClass('alert-danger');
            $('#grupo-txt_codigo_grupo').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!txtProductoGrupo) {
            $('#grupo-txt_producto_grupo').addClass('alert-danger');
            $('#grupo-txt_producto_grupo').css('border', '1px solid #DA1414');
            //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
            error = error + "error 1";
        } else {
            $('#grupo-txt_producto_grupo').removeClass('alert-danger');
            $('#grupo-txt_producto_grupo').css('border', '');
            //$('.error_apellido_paterno_text').html(``);
        }

        if (!flgHabilitado) {
            $('#grupo-flg_habilitado').addClass('alert-danger');
            $('#grupo-flg_habilitado').css('border', '1px solid #DA1414');
            error = error + "error 1";
        } else {
            $('#grupo-flg_habilitado').removeClass('alert-danger');
            $('#grupo-flg_habilitado').css('border', '');
        }

        if (error != "") {
            return false;
        }

        if (form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                toastr.error('Error al realizar el proceso.');
            },
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results.success) {
                    //setTimeout(function(){ loading.hide(); }, 2000);
                    Grupos();
                    PintarGrupo(JSON.parse(localStorage.getItem('grupo')));
                    $('#modal').modal('hide');
                    toastr.success('Registro grabado');
                }else{
                    if(results.msg==0){
                        toastr.warning('No se pudo grabar el registro, realizarlo nuevamente'); 
                    }else if(results.msg==1){
                        //$('#modal').modal('hide');
                        //PintarGrupo(JSON.parse(localStorage.getItem('grupo')));
                        setTimeout(function() {
                            loading.hide();
                        }, 1000);
                        toastr.warning('El código del grupo ya existe'); 
                    }
                    
                }
            },
        });
    });


</script>