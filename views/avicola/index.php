<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'de';
?>
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Productos por Avicola</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de Productos por Avicola</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- end page title -->
<?php
$form = ActiveForm::begin(['options' => ['id' => 'formAvicola', 'class' => 'form-horizontal']]); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Región</label>
                            <select id="avicola-id_region" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Mercado</label>
                            <select id="avicola-id_mercado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha</label>
                            <input name="Avicola[FEC_REGISTRO]" type="date" id="avicola-fec_registro" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Grupo</label>
                            <select id="avicola-id_producto_grupo" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Especie</label>
                            <select id="avicola-id_producto_genero" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Tipo</label>
                            <select name="Avicola[ID_PRODUCTO_MERCADO]" id="avicola-id_producto" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>


<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="lista-productos-mercado-avicola" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Fecha Registro</th>
                        <th>Región</th>
                        <th>Mercado</th>
                        <th>Grupo</th>
                        <th>Especie</th>
                        <th>Tipo</th>
                        <th>Unidad Produccion</th>
                        <th>Cant. Unidades</th>
                        <th>Total</th>
                        <th>Peso Promedio</th>
                        <th>Precio Granja</th>
                        <th>Precio CA</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');
    var idRegion;
    var idMercado;
    var fecRegistro;
    var idProductoGrupo;
    var idProductoGenero;
    var idProductoMercado;


    function ListaProductosMercadoAvicola() {

        $('#lista-productos-mercado-avicola').DataTable().destroy();
        $('#lista-productos-mercado-avicola tbody').html("");
        $('#lista-productos-mercado-avicola').DataTable({
            "dom": "Bfrtip",
            buttons: [
                'excelHtml5',
                'csvHtml5',
            ],
            "paging": true,
            "lengthChange": true,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "pageLength": 10,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningun dato disponible en esta lista",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });
    }


    async function ProductosMercadoAvicola() {
        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/avicola/get-lista-avicola-index',
            method: 'POST',
            data: {
                _csrf: csrf,
                fecRegistro: fecRegistro,
                idRegion: idRegion,
                idMercado: idMercado,
                idProductoGrupo: idProductoGrupo,
                idProductoGenero: idProductoGenero,
                idProductoMercado: idProductoMercado
            },
            dataType: 'Json',
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var productosMercadoAvicola = "";
                    $('#lista-productos-mercado-avicola').DataTable().destroy();

                    $.each(results.productosMercadoAvicola, function(index, value) {
                        productosMercadoAvicola = productosMercadoAvicola + "<tr>";
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.FEC_REGISTRO) ? value.FEC_REGISTRO : "") + "</td>";
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.TXT_DEPARTAMENTO) ? value.TXT_DEPARTAMENTO : "") + "</td>";
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.TXT_MERCADO) ? value.TXT_MERCADO : "") + "</td>";
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.TXT_PRODUCTO_GRUPO) ? value.TXT_CODIGO_GRUPO + "-" + value.TXT_PRODUCTO_GRUPO : "") + "</td>";
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.TXT_PRODUCTO_GENERO) ? value.TXT_CODIGO_GENERO + "-" + value.TXT_PRODUCTO_GENERO : "") + "</td>";
                       
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.TXT_PRODUCTO) ? value.TXT_CODIGO_PRODUCTO + "-" + value.TXT_PRODUCTO : "") + "</td>";
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.UNIDAD_PRODUCCION) ? value.UNIDAD_PRODUCCION : "") + "</td>";
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.NUM_CANTIDAD_PRODUCTO) ? value.NUM_CANTIDAD_PRODUCTO : "") + "</td>";
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.NUM_TOTAL) ? value.NUM_TOTAL : "") + "</td>";
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.NUM_PESO_PROMEDIO) ? value.NUM_PESO_PROMEDIO : "") + "</td>";
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.NUM_PRECIO_GRANJA) ? value.NUM_PRECIO_GRANJA : "") + "</td>";
                        productosMercadoAvicola = productosMercadoAvicola + "<td> " + ((value.NUM_PRECIO_CENTRO_ACOPIO) ? value.NUM_PRECIO_CENTRO_ACOPIO : "") + "</td>";


                        productosMercadoAvicola = productosMercadoAvicola + "</tr>";
                    });

                    $('#lista-productos-mercado-avicola tbody').html(productosMercadoAvicola);
                    $('#lista-productos-mercado-avicola').DataTable({
                        "dom": "Bfrtip",
                            buttons: [
                                'excelHtml5',
                                'csvHtml5',
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                    setTimeout(function() {
                        loading.hide();
                    }, 1000);
                }
            },
            error: function() {
                alert('Error al realizar el proceso.');
            }
        });
    }






    async function RegionesAvicola() {
       /* var optionsDepartamentos = "<option value>Seleccionar</option>";
        $.each(JSON.parse(localStorage.getItem('region')), function(index, value) {
            optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
        });
        $("#avicola-id_region").html(optionsDepartamentos);*/

        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                proceso: 'avicola'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsDepartamentos = "<option value>Seleccionar</option>";

                    $.each(results.regiones, function(index, value) {
                        optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                    });

                    $("#avicola-id_region").html(optionsDepartamentos);

                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });

    }


    async function Mercados(idRegion) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-opciones-mercados-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion,
                proceso: 'avicola'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsMercados = "<option value>Seleccionar</option>";
                    $.each(results.mercados, function(index, value) {
                        optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                    });
                    $("#avicola-id_mercado").html(optionsMercados);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    $('body').on('change', '#avicola-fec_registro', function(e) {
        e.preventDefault();
        fecRegistro = $(this).val();

        if (!fecRegistro) {
            ListaProductosMercadoAvicola();
        }

        if (fecRegistro) {
            ProductosMercadoAvicola();
        }
    });


    async function GruposAvicola(idMercado) {
       /* var optionsGrupo = "<option value>Seleccionar</option>";
        $.each(JSON.parse(localStorage.getItem('grupo')).grupos, function(index, value) {
            optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_PRODUCTO_GRUPO}</option>`;
        });
        $("#avicola-id_producto_grupo").html(optionsGrupo);
        $("#avicola-id_producto_grupo").val("13");
        $("#avicola-id_producto_grupo").change();
        $("#avicola-id_producto_grupo").prop("disabled", true);*/

        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-opciones-grupos-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idMercado: idMercado,
                proceso: 'avicola'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsGrupo = "<option value>Seleccionar</option>";

                    $.each(results.grupos, function(index, value) {
                        optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
                    });

                    $("#avicola-id_producto_grupo").html(optionsGrupo);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    $('body').on('change', '#avicola-id_producto_grupo', function(e) {
        e.preventDefault();
        idProductoGrupo = $(this).val();
        GenerosAvicola(idProductoGrupo);
    });


    async function GenerosAvicola(idProductoGrupo) {
        var generoLista = $.grep(JSON.parse(localStorage.getItem('genero')).generos, function(v) {
            return v.ID_PRODUCTO_GRUPO === idProductoGrupo;
        });
        var optionsGenero = "<option value>Seleccionar</option>";
        $.each(generoLista, function(index, value) {
            optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_PRODUCTO_GENERO}</option>`;
        });
        $("#avicola-id_producto_genero").html(optionsGenero);
    }

    $('body').on('change', '#avicola-id_region', function(e) {
        e.preventDefault();
        idRegion = $(this).val();
        Mercados(idRegion);

        if (!idRegion) {
            ListaProductosMercadoAvicola();
        }
        if (fecRegistro) {
            ProductosMercadoAvicola();
        }
    });

    $('body').on('change', '#avicola-id_producto_genero', function(e) {
        e.preventDefault();
        idProductoGenero = $(this).val();
        idMercado = $('#avicola-id_mercado').val();
        if (idMercado && idProductoGenero) {
            Productos(idProductoGenero, idMercado);
        }
        if (fecRegistro) {
            ProductosMercadoAvicola();
        }
    });

    async function Productos(idProductoGenero, idMercado) {
        await $.ajax({
            async: false,
            url: "<?= \Yii::$app->request->BaseUrl ?>/producto/get-lista-opciones-productos-mercado",
            method: "POST",
            data: {
                _csrf: csrf,
                idProductoGenero: idProductoGenero,
                idMercado: idMercado
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                //debugger;
                if (results && results.success) {
                    var optionsProducto = "<option value>Seleccionar</option>";

                    $.each(results.productos, function(index, value) {
                        optionsProducto = optionsProducto + `<option value='${value.ID_PRODUCTO_MERCADO}'>${value.TXT_PRODUCTO}</option>`;
                    });

                    $("#avicola-id_producto").html(optionsProducto);
                }

            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    $('body').on('change', '#avicola-id_producto', function(e) {
        e.preventDefault();
        idProductoMercado = $(this).val();
        fecRegistro = $('#avicola-fec_registro').val();

        if (!idProductoMercado) {
            ListaProductosMercadoAvicola();
        }

        if (fecRegistro) {
            ProductosMercadoAvicola();
        }
    });


    $('body').on('change', '#avicola-id_mercado', function(e) {
        e.preventDefault();

        idMercado = $(this).val();
        GruposAvicola(idMercado);
        
        idMercado = $(this).val();
        idProductoGenero = $('#avicola-id_producto_genero').val();

        if (idMercado && idProductoGenero) {
            Productos(idProductoGenero, idMercado);
        }

        if (!idMercado) {
            ListaProductosMercadoAvicola();
        }
        if (fecRegistro) {
            ProductosMercadoAvicola();
        }
    });

    function init() {
        RegionesAvicola();
        GruposAvicola();

        setTimeout(function() {
                        loading.hide();
                    }, 500);
    }

    init();
</script>