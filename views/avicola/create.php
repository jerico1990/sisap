<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Registro Avícola</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de productos x avícola</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formAvicola', 'class' => 'form-horizontal']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Región</label>
                            <select id="avicola-id_region" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Mercado</label>
                            <select id="avicola-id_mercado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha</label>
                            <input type="date" name="Avicola[FEC_REGISTRO]" id="avicola-fec_registro" class="form-control" value="<?= $fechaActual ?>" onkeydown="return false">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Grupo</label>
                            <select id="avicola-id_producto_grupo" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Especie</label>
                            <select id="avicola-id_producto_genero" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Tipo</label>
                            <select name="Avicola[ID_PRODUCTO_MERCADO]" id="avicola-id_producto" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Unidad de Producción</label>
                            <select name="Avicola[ID_MERCADO_UNIDAD_PRODUCCION]" id="avicola-id_unidad_produccion" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button type="button" class="btn btn-success btn-agregar-avicola">Agregar</button>
                            <button type="button" class="btn btn-danger btn-eliminar-avicola">Eliminar</button>
                        </div>
                    </div>
                </div>

                <table id="lista-productos-avicola" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Acciones</th>
                        <th>Especie</th>
                        <th>Tipo</th>
                        <th>Unidad de Producción</th>
                        <th>Cant. Unidades</th>
                        <th>&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;</th>
                        <th>Peso Promedio</th>
                        <th>Precio Granja</th>
                        <th>Precio CA</th>
                        <!-- <th>Observación</th> -->
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<script>
    var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
    var loading = $('.staticBackdrop');

    function ListaProductosMercadoAvicola() {
        $('#lista-productos-avicola').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": false,
            "pageLength": 10,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningun dato disponible en esta lista",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });


        setTimeout(function() {
            loading.hide();
        }, 500);
    }

    async function ProductosAvicola(idProductoMercado, fecRegistro, idUnidadProduccion) {

        await $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/avicola/get-lista-producto-avicola',
            method: 'POST',
            data: {
                _csrf: csrf,
                idProductoMercado: idProductoMercado,
                fecRegistro: fecRegistro,
                idUnidadProduccion: idUnidadProduccion
            },
            dataType: 'Json',
            beforeSend: function() {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var productosMercado = "";
                    $('#lista-productos-avicola').DataTable().destroy();;
                    $.each(results.productosMercadoAvicola, function(index, value) {

                        var optionsEnvases = "<option value>Seleccionar</option>";
                        var optionsPublicados = "<option value>Seleccionar</option>";

                        productosMercado = productosMercado + "<tr>";
                        productosMercado = productosMercado + "<td class='text-center'> ";
                        productosMercado = productosMercado + '<div class="form-check mb-3"><input class="form-check-input check-avicola" type="checkbox" data-id="' + value.ID_AVICOLA + '" id="defaultCheck1"></div>';
                        productosMercado = productosMercado + "</td>";



                        productosMercado = productosMercado + "<td> " + ((value.TXT_PRODUCTO_GENERO) ? value.TXT_PRODUCTO_GENERO : "") + "</td>";
                        productosMercado = productosMercado + "<td> " + ((value.TXT_PRODUCTO) ? value.TXT_PRODUCTO : "") + "</td>";
                        productosMercado = productosMercado + "<td> " + ((value.UNIDAD_PRODUCCION) ? value.UNIDAD_PRODUCCION : "") + "</td>";


                        productosMercado = productosMercado + "<td>";
                        productosMercado = productosMercado + "<input type='text' data-inputmask=\"'alias': 'numeric', 'groupSeparator': ' ', 'digits': 0, 'digitsOptional': false, 'prefix': '', 'placeholder': '0'\" class='form-control input-mask btn-modificar-cantidad-unidades btn-modificar-cantidad-unidades" + value.ID_AVICOLA + "' data-idAvicola='" + value.ID_AVICOLA + "' value='" + ((value.NUM_CANTIDAD_PRODUCTO) ? value.NUM_CANTIDAD_PRODUCTO : "") + "'>";
                        productosMercado = productosMercado + "</td>";

                        productosMercado = productosMercado + "<td>";
                        productosMercado = productosMercado + "<input type='text' data-inputmask=\"'alias': 'numeric', 'groupSeparator': ' ', 'digits': 2, 'digitsOptional': false, 'prefix': '', 'placeholder': '0'\"  class='form-control input-mask btn-modificar-total btn-modificar-total" + value.ID_AVICOLA + "'  data-idAvicola='" + value.ID_AVICOLA + "' value='" + ((value.NUM_TOTAL) ? value.NUM_TOTAL : 0)  + "'>";
                        productosMercado = productosMercado + "</td>";

                        productosMercado = productosMercado + "<td>";
                        productosMercado = productosMercado + "<input type='text' disabled data-inputmask=\"'alias': 'numeric', 'groupSeparator': ' ', 'digits': 2, 'digitsOptional': false, 'prefix': '', 'placeholder': '0'\"  class='form-control input-mask btn-modificar-peso-promedio btn-modificar-peso-promedio" + value.ID_AVICOLA + "' data-idAvicola='" + value.ID_AVICOLA + "' value='" + ((value.NUM_PESO_PROMEDIO) ? value.NUM_PESO_PROMEDIO : "") + "'>";
                        productosMercado = productosMercado + "</td>";



                        productosMercado = productosMercado + "<td>";
                        productosMercado = productosMercado + "<input type='text' data-inputmask=\"'alias': 'numeric', 'groupSeparator': ' ', 'digits': 2, 'digitsOptional': false, 'prefix': 'S/ ', 'placeholder': '0'\"  class='form-control input-mask btn-modificar-precio-granja' data-idAvicola='" + value.ID_AVICOLA + "' value='" + ((value.NUM_PRECIO_GRANJA) ? value.NUM_PRECIO_GRANJA : "") + "'>";
                        productosMercado = productosMercado + "</td>";
                        productosMercado = productosMercado + "<td>";
                        productosMercado = productosMercado + "<input type='text' data-inputmask=\"'alias': 'numeric', 'groupSeparator': ' ', 'digits': 2, 'digitsOptional': false, 'prefix': 'S/ ', 'placeholder': '0'\"  class='form-control input-mask btn-modificar-precio-centroacopio' data-idAvicola='" + value.ID_AVICOLA + "' value='" + ((value.NUM_PRECIO_CENTRO_ACOPIO) ? value.NUM_PRECIO_CENTRO_ACOPIO : "") + "'>";
                        productosMercado = productosMercado + "</td>";
                        productosMercado = productosMercado + "</tr>";
                    });

                    $('#lista-productos-avicola tbody').html(productosMercado);
                    $(".input-mask").inputmask();


                    $('.form-control').on("keypress", function(e) {
                        if (e.keyCode == 13) {
                            var that = $(this);
                            setTimeout(function() {
                                that.parent().next().find('.form-control').focus();
                            }, 50);
                        }
                    });


                    $('#lista-productos-avicola').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": false,
                        "ordering": false,
                        "info": true,
                        "autoWidth": false,
                        "pageLength": 10,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningun dato disponible en esta lista",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                    });

                    setTimeout(function() {
                        loading.hide();
                    }, 500);
                }
            },
            error: function() {
                alert('Error al realizar el proceso.');
            }
        });

    }

    async function RegionesAvicola() {

        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                proceso: 'avicola'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsDepartamentos = "<option value>Seleccionar</option>";

                    $.each(results.regiones, function(index, value) {
                        optionsDepartamentos = optionsDepartamentos + `<option value='${value.ID_DEPARTAMENTO}'>${value.TXT_DEPARTAMENTO}</option>`;
                    });

                    $("#avicola-id_region").html(optionsDepartamentos);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }



    async function UnidadProduccion() {

        await $.ajax({
            async: false,
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-mercados-unidad-produccion",
            method: "POST",
            data: {
                _csrf: csrf
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsUnidadProduccion = "<option value>Seleccionar</option>";

                    $.each(results.mercados, function(index, value) {
                        optionsUnidadProduccion = optionsUnidadProduccion + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                    });

                    $("#avicola-id_unidad_produccion").html(optionsUnidadProduccion);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });


    }


    async function Mercados(idRegion) {
        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/mercado/get-lista-opciones-mercados-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idRegion: idRegion,
                proceso: 'avicola'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsMercados = "<option value>Seleccionar</option>";
                    $.each(results.mercados, function(index, value) {
                        optionsMercados = optionsMercados + `<option value='${value.ID_MERCADO}'>${value.TXT_MERCADO}</option>`;
                    });
                    $("#avicola-id_mercado").html(optionsMercados);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    async function GruposAvicola(idMercado) {

        await $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-opciones-grupos-usuario",
            method: "POST",
            data: {
                _csrf: csrf,
                idMercado: idMercado,
                proceso: 'avicola'
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    var optionsGrupo = "<option value>Seleccionar</option>";
                    $.each(results.grupos, function(index, value) {
                        optionsGrupo = optionsGrupo + `<option value='${value.ID_PRODUCTO_GRUPO}'>${value.TXT_CODIGO_GRUPO}-${value.TXT_PRODUCTO_GRUPO}</option>`;
                    });
                    $("#avicola-id_producto_grupo").html(optionsGrupo);
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }

    async function GenerosAvicola(idProductoGrupo) {
        var generoLista = $.grep(JSON.parse(localStorage.getItem('genero')).generos, function(v) {
            return v.ID_PRODUCTO_GRUPO === idProductoGrupo;
        });
        var optionsGenero = "<option value>Seleccionar</option>";

        $.each(generoLista, function(index, value) {
            optionsGenero = optionsGenero + `<option value='${value.ID_PRODUCTO_GENERO}'>${value.TXT_PRODUCTO_GENERO}</option>`;
        });

        $("#avicola-id_producto_genero").html(optionsGenero);
    }


    var listaProductos = [];
    async function Productos(idProductoGenero, idMercado) {
        listaProductos = [];
        await $.ajax({
            async: false,
            url: "<?= \Yii::$app->request->BaseUrl ?>/producto/get-lista-opciones-productos-mercado",
            method: "POST",
            data: {
                _csrf: csrf,
                idProductoGenero: idProductoGenero,
                idMercado: idMercado
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    $.each(results.productos, function(index, value) {
                        listaProductos.push(value);
                    });
                }
                //debugger;
                if (results && results.success) {
                    var optionsProducto = "<option value>Seleccionar</option>";

                    $.each(results.productos, function(index, value) {
                        optionsProducto = optionsProducto + `<option value='${value.ID_PRODUCTO_MERCADO}'>${value.TXT_PRODUCTO}</option>`;
                    });

                    $("#avicola-id_producto").html(optionsProducto);
                }

            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }


    var listaEstadosPublicados = [];
    async function EstadosPublicados() {
        await $.ajax({
            async: false,
            url: "<?= \Yii::$app->request->BaseUrl ?>/maestro/get-lista-maestros",
            method: "POST",
            data: {
                _csrf: csrf,
                idMaestro: 1
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    $.each(results.maestros, function(index, value) {
                        listaEstadosPublicados.push(value);
                    });
                }
            },
            error: function() {
                alert("Error al realizar el proceso.");
            }
        });
    }


    $('body').on('change', '#avicola-id_producto_grupo', function(e) {
        e.preventDefault();
        idProductoGrupo = $(this).val();
        GenerosAvicola(idProductoGrupo);
    });

    $('body').on('change', '#avicola-id_region', function(e) {
        e.preventDefault();
        idRegion = $(this).val();
        Mercados(idRegion);
    });

    $('body').on('change', '#avicola-id_producto_genero', function(e) {
        e.preventDefault();
        idProductoGenero = $(this).val();
        idMercado = $('#avicola-id_mercado').val();
        Productos(idProductoGenero, idMercado);
    });

    $('body').on('change', '#avicola-fec_registro', function(e) {
        e.preventDefault();
        fecRegistro = $(this).val();

        if (!fecRegistro) {
            console.log("fec_registro");
            ListaProductosMercadoAvicola();
        }

        if ($('#avicola-id_producto').val() != "") {
            idProductoMercado = $('#avicola-id_producto').val();
            fechaRegistro = $('#avicola-fec_registro').val();
            idUnidadProduccion = $('#avicola-id_unidad_produccion').val();
            ProductosAvicola(idProductoMercado, fechaRegistro, idUnidadProduccion);
        }


    });

    $('body').on('change', '#avicola-id_producto', function(e) {
        e.preventDefault();

        if ($('#avicola-fec_registro').val() != "") {

            if ($('#avicola-id_producto').val() != "" && $("#avicola-id_unidad_produccion").val() != "") {
                idProductoMercado = $('#avicola-id_producto').val();
                fechaRegistro = $('#avicola-fec_registro').val();
                idUnidadProduccion = $('#avicola-id_unidad_produccion').val();
                ProductosAvicola(idProductoMercado, fechaRegistro, idUnidadProduccion);
            } else {
                $('#lista-productos-avicola').DataTable().destroy();
                ListaProductosMercadoAvicola();
                $('#lista-productos-avicola tbody').html("");
            }
        } else {
            $('#lista-productos-avicola').DataTable().destroy();
            ListaProductosMercadoAvicola();
            $('#lista-productos-avicola tbody').html("");
        }
    });

    $('body').on('change', '#avicola-id_unidad_produccion', function(e) {
        e.preventDefault();


        if ($('#avicola-fec_registro').val() != "") {
            if ($('#avicola-id_producto').val() != "" && $("#avicola-id_unidad_produccion").val() != "") {

                idProductoMercado = $('#avicola-id_producto').val();
                fechaRegistro = $('#avicola-fec_registro').val();
                idUnidadProduccion = $('#avicola-id_unidad_produccion').val();
                ProductosAvicola(idProductoMercado, fechaRegistro, idUnidadProduccion);

            } else {
                $('#lista-productos-avicola').DataTable().destroy();
                ListaProductosMercadoAvicola();
                $('#lista-productos-avicola tbody').html("");
            }
        } else {
            $('#lista-productos-avicola').DataTable().destroy();
            ListaProductosMercadoAvicola();
            $('#lista-productos-avicola tbody').html("");
        }
    });




    $('body').on('click', '.btn-agregar-avicola', function(e) {
        e.preventDefault();
        var idProductoMercado = $('#avicola-id_producto').val();
        var fechaRegistro = $('#avicola-fec_registro').val();
        var idUnidadProduccion = $('#avicola-id_unidad_produccion').val();

        if (idProductoMercado == ""  || fechaRegistro == ""  ||  idUnidadProduccion == "") {
            toastr.error('Verifique que este seleccionado Fecha y Tipo y Unidad de Producción');
            return false;
        }


        var form = $('#formAvicola');
        var formData = $('#formAvicola').serializeArray();
        if (form.find('.has-error').length) {
            return false;
        }
        console.log(formData);
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                console.log(XMLHttpRequest);
                console.log(errorThrown);
                //$('.sidebar-mini').LoadingOverlay("hide", true);
            },
            beforeSend: function() {
                loading.show();
            },
            success: function(results) {
                if (results.success) {
                    setTimeout(function() {
                        loading.hide();
                    }, 500);
                    //Usuarios();
                    ProductosAvicola(idProductoMercado, fechaRegistro, idUnidadProduccion)
                    $('#modal').modal('hide');
                }
            },
        });
    });

    $('body').on('change', '.btn-modificar-unidad-produccion', function(e) {
        e.preventDefault();
        let idUnidadProduccion = $(this).val();
        let idAvicola = $(this).attr('data-idAvicola');
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/avicola/update?id=" + idAvicola,
            method: "POST",
            data: {
                _csrf: csrf,
                idUnidadProduccion: idUnidadProduccion
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Avícola unidad producción grabado');
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });

    $('body').on('change', '.btn-modificar-cantidad-unidades', function(e) {
        e.preventDefault();
        let idCantidadUnidades = $(this).val();
        let idAvicola = $(this).attr('data-idAvicola');
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/avicola/update?id=" + idAvicola,
            method: "POST",
            data: {
                _csrf: csrf,
                idCantidadUnidades: idCantidadUnidades
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Avícola cantidad unidades grabado');
                }
                $(".btn-modificar-peso-promedio" + idAvicola).val(parseFloat($(".btn-modificar-total" + idAvicola).val()) / parseFloat($(".btn-modificar-cantidad-unidades" + idAvicola).val()));
                //$(".btn-modificar-total" + idAvicola).val(parseFloat($(".btn-modificar-cantidad-unidades" + idAvicola).val()) * parseFloat($(".btn-modificar-peso-promedio" + idAvicola).val()));
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });

    $('body').on('change', '.btn-modificar-peso-promedio', function(e) {
        e.preventDefault();
        debugger;
        console.log("peso:");
        let idPesoPromedio = $(this).val();
        let idAvicola = $(this).attr('data-idAvicola');
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/avicola/update?id=" + idAvicola,
            method: "POST",
            data: {
                _csrf: csrf,
                idPesoPromedio: idPesoPromedio
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                //if (results && results.success) {
                //    toastr.success('Avícola peso promedio grabado');
                //}
                //$(".btn-modificar-total" + idAvicola).val(parseFloat($(".btn-modificar-cantidad-unidades" + idAvicola).val()) * parseFloat($(".btn-modificar-peso-promedio" + idAvicola).val()));
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });


    $('body').on('change', '.btn-modificar-total', function(e) {
        e.preventDefault();
        debugger;
        let idTotal = $(this).val();
        let idAvicola = $(this).attr('data-idAvicola');
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/avicola/update?id=" + idAvicola,
            method: "POST",
            data: {
                _csrf: csrf,
                idTotal: idTotal
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Avícola total grabado');
                }
                $(".btn-modificar-peso-promedio" + idAvicola).val(parseFloat($(".btn-modificar-total" + idAvicola).val()) / parseFloat($(".btn-modificar-cantidad-unidades" + idAvicola).val()));
                $(".btn-modificar-peso-promedio" + idAvicola).change();
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });





        $(".btn-modificar-peso-promedio" + idAvicola).val(parseFloat($(".btn-modificar-total" + idAvicola).val()) / parseFloat($(".btn-modificar-cantidad-unidades" + idAvicola).val()));
        //$(".btn-modificar-peso-promedio" + idAvicola).change();
    });



    $('body').on('change', '.btn-modificar-precio-granja', function(e) {
        e.preventDefault();
        let idPrecioGranja = $(this).val();
        let idAvicola = $(this).attr('data-idAvicola');
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/avicola/update?id=" + idAvicola,
            method: "POST",
            data: {
                _csrf: csrf,
                idPrecioGranja: idPrecioGranja
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Avícola precio granja grabado');
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });


    $('body').on('change', '.btn-modificar-precio-centroacopio', function(e) {
        e.preventDefault();
        let idPrecioCentroAcopio = $(this).val();
        let idAvicola = $(this).attr('data-idAvicola');
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/avicola/update?id=" + idAvicola,
            method: "POST",
            data: {
                _csrf: csrf,
                idPrecioCentroAcopio: idPrecioCentroAcopio
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Avícola precio centro acopio grabado');
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });


    $('body').on('change', '.btn-modificar-habilitado', function(e) {
        e.preventDefault();
        let flgHabilitado = $(this).val();
        let idProductoMercado = $(this).attr('data-idProductoMercado');
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/update?id=" + idProductoMercado,
            method: "POST",
            data: {
                _csrf: csrf,
                flgHabilitado: flgHabilitado
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Avícola habilitado grabado');
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });

    $('body').on('keyup', '.btn-modificar-num_equivalencia', function(e) {
        e.preventDefault();
        let numEquivalencia = $(this).val();
        let idProductoMercado = $(this).attr('data-idProductoMercado');
        $.ajax({
            url: "<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/update?id=" + idProductoMercado,
            method: "POST",
            data: {
                _csrf: csrf,
                numEquivalencia: numEquivalencia
            },
            dataType: "Json",
            beforeSend: function(xhr, settings) {
                //loading.show();
            },
            success: function(results) {
                if (results && results.success) {
                    toastr.success('Avícola equivalencia grabado');
                }
            },
            error: function() {
                toastr.error('Error al realizar el proceso.');
            }
        });
    });



    $('body').on('click', '.btn-eliminar-avicola', function(e) {
        e.preventDefault();
        Swal.fire({
            title: '¿Está seguro de eliminar los registros seleccionados?',
            text: "Una vez eliminado los registros ya no se podrá visualizar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url: '<?= \Yii::$app->request->BaseUrl ?>/avicola/eliminar',
                    method: "POST",
                    data: {
                        _csrf: csrf,
                        listaIdsAvicola: listaIdsAvicola
                    },
                    dataType: "Json",
                    beforeSend: function(xhr, settings) {
                        //loading.show();
                    },
                    success: function(results) {
                        if (results && results.success) {
                            idProductoMercado = $('#avicola-id_producto').val();
                            fecRegistro = $('#avicola-fec_registro').val();
                            idUnidadProduccion = $('#avicola-id_unidad_produccion').val();
                            ProductosAvicola(idProductoMercado, fecRegistro, idUnidadProduccion);
                            toastr.success('Registro eliminado');
                        }
                    },
                    error: function() {
                        toastr.error('Error al realizar el proceso.');
                    }
                });

            }
        });

    });

    var listaIdsAvicola = [];
    $('body').on('change', '.check-avicola', function(e) {
        e.preventDefault();
        let idAvicola = $(this).attr('data-id');
        let bAvicola = $(this).is(":checked");

        if (bAvicola) {
            listaIdsAvicola.push(idAvicola);
        } else {
            listaIdsAvicola.splice($.inArray(idAvicola, listaIdsAvicola), 1);
        }

        console.log(listaIdsAvicola);
    });


    $('body').on('change', '#avicola-id_mercado', function(e) {
        e.preventDefault();
        idMercado = $(this).val();
        GruposAvicola(idMercado);
        idProductoGenero = $('#avicola-id_producto_genero').val();
        if (!idMercado) {
            ListaProductosMercadoAvicola();
        }
        if (idMercado && idProductoGenero) {
            Productos(idProductoGenero, idMercado);
        }
    });


    function init() {

        RegionesAvicola();
        UnidadProduccion();
        EstadosPublicados();
        UnidadProduccion();
        setTimeout(function() {
            loading.hide();
        }, 500);
    }

    init();
</script>