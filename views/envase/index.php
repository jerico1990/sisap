<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Envase</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Lista de envases</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_codigo">Código</label>
                            <input type="text" id="txt_codigo" class="form-control">
                        </div>
                    </div> -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_descripcion">Descripción</label>
                            <input type="text" id="txt_descripcion" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-agregar-envase">Agregar</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <label for="">&nbsp&nbsp</label> <br>
                        <button style="float: right;" class="btn btn-primary btn-buscar-envase">Buscar</button>
                    </div>
                </div>

                <table id="lista-envases" class="table table-bordered dt-responsive ">
                    <thead>
                        <th>Descripción</th>
                        <th>Unidad de Medida</th>
                        <th>Equivalencia</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('.staticBackdrop');

$('#lista-envases').DataTable();

Envases();
async function Envases(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/envase/get-lista-envases',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var envases ="";
                        $('#lista-envases').DataTable().destroy();
                        
                        $.each(results.envases, function( index, value ) {
                            envases = envases + "<tr>";
                                envases = envases + "<td> " + value.TXT_ENVASE + "</td>";
                                envases = envases + "<td> " + ((value.TXT_UNIDAD_MEDIDA)?value.TXT_UNIDAD_MEDIDA:"") + "</td>";
                                envases = envases + "<td> " + ((value.NUM_EQUIVALENCIA)?value.NUM_EQUIVALENCIA:"") + "</td>";
                                envases = envases + "<td> " + value.TXT_DESCRIPCION_HABILITADO + "</td>";
                                envases = envases + "<td>" ;
                                    envases = envases + '<button data-id="' + value.ID_ENVASE + '" data-idUnidadMedida="' + value.ID_UNIDAD_MEDIDA + '" class="btn btn-info btn-sm btn-modificar-envase" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    envases = envases + '<button data-id="' + value.ID_ENVASE + '" class="btn btn-danger btn-sm btn-eliminar-envase" href="#"><i class="fas fa-trash"></i></button>';
                                envases = envases +"</td>";
                            envases = envases + "</tr>";
                        });
                        
                        $('#lista-envases tbody').html(envases);
                        $('#lista-envases').DataTable({
                            "dom": "Bfrtip",
                            buttons: [
                                'excelHtml5',
                                'csvHtml5',
                            ],
                            "paging": true,
                            "lengthChange": true,
                            "searching": true,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.hide(); }, 1000);
                        $(".dataTables_filter").hide();
                    }
                },
                error:function(){
                    toastr.error('Error al realizar el proceso.');
                }
            });
}

//Buscar
$('body').on('click', '.btn-buscar-envase', function(e) {
    e.preventDefault();
    //var codigo=$("#txt_codigo").val();
    var descripcion=$("#txt_descripcion").val();

    //$('#lista-envases').DataTable().column(0).search(codigo).draw();
    $('#lista-envases').DataTable().column(0).search(descripcion).draw();
            
});



//agregar

$('body').on('click', '.btn-agregar-envase', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/envase/create',function(){
        UnidadesMedidas();
    });
    $('#modal').modal('show');
});

//modificar
$('body').on('click', '.btn-modificar-envase', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var idUnidadMedida = $(this).attr('data-idUnidadMedida');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/envase/update?id='+id,function(){
        UnidadesMedidas(idUnidadMedida);
    });
    $('#modal').modal('show');
});

//eliminar
$('body').on('click', '.btn-eliminar-envase', function (e) {
    e.preventDefault();
    idEnvase = $(this).attr('data-id');
    Swal.fire({
        title: '¿Está seguro de deshabilitar el registro?',
        //text: "Una vez eliminado el registro ya no se podrá visualizar",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, deshabilitar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {

            $.ajax({
                url : '<?= \Yii::$app->request->BaseUrl ?>/envase/eliminar',
                method: "POST",
                data:{_csrf:csrf,idEnvase:idEnvase},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.show();
                },
                success:function(results)
                {   
                    if(results && results.success){
                        Envases();
                        toastr.success('Registro deshabilitado');
                    }
                },
                error:function(){
                    toastr.error('Error al realizar el proceso.');
                }
            });
            
        }
    });
});


//grabar

$('body').on('click', '.btn-grabar-envase', function (e) {

    e.preventDefault();
    var form = $('#formEnvase');
    var formData = $('#formEnvase').serializeArray();

    var error = "";
    var txtEnvase = $.trim($("[name=\"Envase[TXT_ENVASE]\"]").val());
    var flgHabilitado = $.trim($("[name=\"Envase[FLG_HABILITADO]\"]").val());
    var idUnidadMedida = $.trim($("[name=\"Envase[ID_UNIDAD_MEDIDA]\"]").val());
    var numEquivalencia = $.trim($("[name=\"Envase[NUM_EQUIVALENCIA]\"]").val());

    if (!txtEnvase) {
        $('#envase-txt_envase').addClass('alert-danger');
        $('#envase-txt_envase').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#envase-txt_envase').removeClass('alert-danger');
        $('#envase-txt_envase').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (!numEquivalencia) {
        $('#envase-num_equivalencia').addClass('alert-danger');
        $('#envase-num_equivalencia').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#envase-num_equivalencia').removeClass('alert-danger');
        $('#envase-num_equivalencia').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (!idUnidadMedida) {
        $('#envase-id_unidad_medida').addClass('alert-danger');
        $('#envase-id_unidad_medida').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#envase-id_unidad_medida').removeClass('alert-danger');
        $('#envase-id_unidad_medida').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (!flgHabilitado) {
        $('#envase-flg_habilitado').addClass('alert-danger');
        $('#envase-flg_habilitado').css('border', '1px solid #DA1414');
        //$('.error_apellido_paterno_text').html(`<div class="alert alert-danger mt-1" style="color:#DA1414;border:0px" role="alert"> <span class="bx bx-error-circle"></span> Este campo es obligatorio</div>`);
        error = error + "error 1";
    } else {
        $('#envase-flg_habilitado').removeClass('alert-danger');
        $('#envase-flg_habilitado').css('border', '');
        //$('.error_apellido_paterno_text').html(``);
    }

    if (error != "") {
        return false;
    }


    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            toastr.error('Error al realizar el proceso.');
        },
        beforeSend:function()
        {
            loading.show();
        },
        success: function (results) {
            if(results.success){
                //setTimeout(function(){ loading.hide(); }, 1000);
                Envases();
                $('#modal').modal('hide');
                toastr.success('Registro grabado');
            }else{
                if(results.msg==0){
                    toastr.warning('No se pudo grabar el registro, realizarlo nuevamente'); 
                }else if(results.msg==1){
                    setTimeout(function() {
                        loading.hide();
                    }, 1000);
                    toastr.warning('El envase ya existe'); 
                }
                
            }
        },
    });
});


async function UnidadesMedidas(idUnidadMedida) {
    await $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/get-lista-unidades-medidas",
        method: "POST",
        data: {
            _csrf: csrf
        },
        dataType: "Json",
        beforeSend: function(xhr, settings) {
            //loading.show();
        },
        success: function(results) {
            if (results && results.success) {
                var optionsUnidadMedida = "<option value>Seleccionar</option>";

                $.each(results.unidadesMedidas, function(index, value) {
                    optionsUnidadMedida = optionsUnidadMedida + `<option value='${value.ID_UNIDAD_MEDIDA}'>${value.TXT_UNIDAD_MEDIDA}</option>`;
                });

                $("#envase-id_unidad_medida").html(optionsUnidadMedida);

                if (idUnidadMedida) {
                    $("#envase-id_unidad_medida").val(idUnidadMedida);
                }
            }
        },
        error: function() {
            toastr.error('Error al realizar el proceso.');
        }
    });
}

</script>
