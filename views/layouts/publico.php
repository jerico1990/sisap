<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>SISAP | Publico</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />

    <!-- App favicon -->
    <!-- <link rel="shortcut icon" href="<?= \Yii::$app->request->BaseUrl ?>/skote/images/favicon.ico"> -->
    <link href="http://sistemas.midagri.gob.pe/sisap/portal2/mayorista/./patron/iconos/./favicon.ico" type="image/x-icon" rel="icon">
    <!-- JAVASCRIPT -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jquery/jquery.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/metismenu/metisMenu.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/simplebar/simplebar.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jquery.easing/jquery.easing.min.js"></script>

    <!-- Required datatable js -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net/js/jquery.dataTables.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>

    <!-- owl.carousel css -->
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/owl.carousel/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/owl.carousel/assets/owl.theme.default.min.css">

    <!-- Bootstrap Css -->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

    
    <link href="<?= \Yii::$app->request->BaseUrl ?>/jstree.bundle.css"  rel="stylesheet" type="text/css" />
    <link href="<?= \Yii::$app->request->BaseUrl ?>/plugins.bundle.css"  rel="stylesheet" type="text/css" />


    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" type="text/css" href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/toastr/build/toastr.min.css">


    <!-- Required datatable js -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jszip/jszip.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/toastr/build/toastr.min.js"></script>
   

    <!-- Plugins js-->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jquery-countdown/jquery.countdown.min.js"></script>

    <!-- owl.carousel js -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/owl.carousel/owl.carousel.min.js"></script>

    <!-- ICO landing init -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/js/pages/ico-landing.init.js"></script>

    <style>
        body[data-layout=horizontal] .container-fluid{
            max-width: 100%;
        }
    </style>

</head>

<body data-topbar="dark" data-layout="horizontal">
    <div id="layout-wrapper">
        <header id="page-topbar" class="bg-success">
            <div class="navbar-header ">
                <div class="d-flex text-white">
                    <!-- LOGO -->
                    SISTEMA DE ABASTECIMIENTO Y PRECIOS - SISAP

                </div>

            </div>
        </header>

        <div class="main-content" id="result">
            <div class="page-content"><?= $content ?></div>
             
        </div>
    </div>
    


   
    


    <!-- Footer start -->
    <!-- <footer class="landing-footer">
        <div class="container">

            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="mb-4 mb-lg-0">
                        <h5 class="mb-3 footer-list-title">Company</h5>
                        <ul class="list-unstyled footer-list-menu">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Features</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">News</a></li>
                            <li><a href="#">FAQs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="mb-4 mb-lg-0">
                        <h5 class="mb-3 footer-list-title">Resources</h5>
                        <ul class="list-unstyled footer-list-menu">
                            <li><a href="#">Whitepaper</a></li>
                            <li><a href="#">Token sales</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="mb-4 mb-lg-0">
                        <h5 class="mb-3 footer-list-title">Links</h5>
                        <ul class="list-unstyled footer-list-menu">
                            <li><a href="#">Tokens</a></li>
                            <li><a href="#">Roadmap</a></li>
                            <li><a href="#">FAQs</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6">
                    <div class="mb-4 mb-lg-0">
                        <h5 class="mb-3 footer-list-title">Latest News</h5>
                        <div class="blog-post">
                            <a href="#" class="post">
                                <div class="badge badge-soft-success font-size-11 mb-3">Cryptocurrency</div>
                                <h5 class="post-title">Donec pede justo aliquet nec</h5>
                                <p class="mb-0"><i class="bx bx-calendar mr-1"></i> 04 Mar, 2020</p>
                            </a>
                            <a href="#" class="post">
                                <div class="badge badge-soft-success font-size-11 mb-3">Cryptocurrency</div>
                                <h5 class="post-title">In turpis, Pellentesque</h5>
                                <p class="mb-0"><i class="bx bx-calendar mr-1"></i> 12 Mar, 2020</p>
                            </a>

                        </div>
                    </div>
                </div>
            </div>

            <hr class="footer-border my-5">

            <div class="row">
                <div class="col-lg-6">
                    <div class="mb-4">
                        <img src="assets/images/logo-light.png" alt="" height="20">
                    </div>

                    <p class="mb-2">2020 © Skote. Design & Develop by Themesbrand</p>
                    <p>It will be as simple as occidental in fact, it will be to an english person, it will seem like
                        simplified English, as a skeptical</p>
                </div>

            </div>
        </div>
    </footer> -->
    <!-- Footer end -->

    

</body>

</html>