<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>SISAP | Administrador</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="SISAP | Administrador" name="description" />
    <meta content="SISAP" name="author" />
    <!-- App favicon -->
    <!-- <link rel="shortcut icon" href="<?= \Yii::$app->request->BaseUrl ?>//img/conectaideas_login.png">
 -->
    <link href="http://sistemas.midagri.gob.pe/sisap/portal2/mayorista/./patron/iconos/./favicon.ico" type="image/x-icon" rel="icon">
    <!-- Bootstrap Css -->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/app.min.css" rel="stylesheet" type="text/css" />
    <!-- Custom Css-->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/custom.css" rel="stylesheet" type="text/css" />
    <!-- JAVASCRIPT -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jquery/jquery.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/metismenu/metisMenu.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/simplebar/simplebar.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/node-waves/waves.min.js"></script>

    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" type="text/css" href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/toastr/build/toastr.min.css">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>




    <!-- Required datatable js -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jszip/jszip.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/select2/js/select2.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/inputmask/min/jquery.inputmask.bundle.min.js"></script>

    <!-- Responsive examples -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/toastr/build/toastr.min.js"></script>





    <script src="https://cdn.rawgit.com/kevindb/jquery-load-json/v1.3.4/dist/jquery.loadJSON.min.js" integrity="sha384-ivtX4sn4dcdfHiO4e0/956wIQSerxsy2QZ6EHzdCVLlyGYYjSb8bqdxKY8IsfDGh" crossorigin="anonymous"></script>

    <!-- <link href="<?= \Yii::$app->request->BaseUrl ?>/css/pagination.min.css" rel="stylesheet" type="text/css" />
    <script src="<?= \Yii::$app->request->BaseUrl ?>/js/pagination.min.js"></script> -->



    <script>
        toastr.options = {positionClass: 'toast-bottom-right'};
        
        $.fn.serializeObject = function() {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    </script>

    <style>
        .form-control:disabled,
        .form-control[readonly] {
            background-color: #dfdfdf;
        }

        body[data-sidebar=dark] #sidebar-menu ul li a {
            color: #ffffff;
        }

        body[data-sidebar=dark] #sidebar-menu ul li ul.sub-menu li a {
            color: #ffffff;
        }

        .staticBackdrop {
            position:absolute;
            top:0;
            width: 100%;
            height:  100%;
            z-index: 1060;
            background-color: rgb(0,0,0,0.2);
        }

        .text-center-spinner {
            width: 100%;
            position: relative;
            height: 100%;
        }
    </style>
</head>

<body data-sidebar="dark">

    <div class="staticBackdrop" >
        <div class="text-center">
            Cargando <br> <div class="spinner-border text-primary m-1" role="status"></div>
        </div>
    </div>

    <!-- <div class="modal fade" style="z-index:1060;overflow-y:hidden;" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="status" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog text-center" role="document">
            Cargando <br>
            <div class="spinner-border text-primary m-1" role="status"></div>
        </div>
    </div> -->

    
    <!-- Begin page -->
    <div id="layout-wrapper">

        <header id="page-topbar">
            <div class="navbar-header">
                <div class="d-flex">
                    <!-- LOGO -->
                    <div class="navbar-brand-box bg-success">

                    </div>

                    <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                        <i class="fa fa-fw fa-bars"></i>
                    </button>

                </div>

                <div class="d-flex">


                    <div class="dropdown d-inline-block">
                        <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <!-- <img class="rounded-circle header-profile-user" src="<?= \Yii::$app->request->BaseUrl ?>/skote/images/users/avatar-1.jpg"
                                alt="Header Avatar"> -->
                            <span class="d-none d-xl-inline-block ml-1"><?= Yii::$app->user->identity->username ?></span>
                            <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">

                            <!-- <a class="dropdown-item" href="#"><i class="bx bx-user font-size-16 align-middle mr-1"></i>
                                Profile</a>
                            <a class="dropdown-item" href="#"><i
                                    class="bx bx-wallet font-size-16 align-middle mr-1"></i> My Wallet</a>
                            <a class="dropdown-item d-block" href="#"><span
                                    class="badge badge-success float-right">11</span><i
                                    class="bx bx-wrench font-size-16 align-middle mr-1"></i> Settings</a>
                            <a class="dropdown-item" href="#"><i
                                    class="bx bx-lock-open font-size-16 align-middle mr-1"></i> Lock screen</a>
                            <div class="dropdown-divider"></div> -->
                            <a id="idlogout" class="dropdown-item text-danger" href="<?= \Yii::$app->request->BaseUrl ?>/login/logout"><i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> Cerrar sesión</a>
                        </div>
                    </div>


                </div>
            </div>
        </header> <!-- ========== Left Sidebar Start ========== -->
        <div class="vertical-menu bg-success">

            <div data-simplebar class="h-100">

                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <!-- Left Menu Start -->
                    <!-- <ul class="metismenu list-unstyled" id="side-menu">
                        <li class="menu-title">Menu</li>

                        <li>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/panel" class=" waves-effect">
                                <i class="bx bx-home-circle"></i>
                                <span>Panel</span>
                            </a>
                        </li>

                        <li >
                            <a href="#" class=" waves-effect has-arrow">
                                <i class="bx bx-collection"></i>
                                <span>Mantenimiento</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li>
                                    <a href="javascript: void(0);" class="has-arrow">Mercados</a>
                                    <ul class="sub-menu" aria-expanded="true">
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/mercado-tipo/index">Tipos mercados</a></li>
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/mercado/index">Mercados</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" class="has-arrow">Productos</a>
                                    <ul class="sub-menu" aria-expanded="true">
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/grupo/index">Grupo</a></li>
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/genero/index">Genero</a></li>
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/variedad/index">Producto</a></li>
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/procedencia/index">Procedencia</a></li>
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/index">Unidad de medida</a></li>
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/envase/index">Envase</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" class="has-arrow">Productos x Mercado</a>
                                    <ul class="sub-menu" aria-expanded="true">
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/index">Listar</a></li>
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/producto-mercado/create">Registrar</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript: void(0);" class="has-arrow">Seguridad</a>
                                    <ul class="sub-menu" aria-expanded="true">
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/perfil/index">Perfil</a></li>
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/usuario/index">Usuario</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        

                    </ul> -->
                </div>
                <!-- Sidebar -->
            </div>
        </div>
        <!-- Left Sidebar End -->

        <div class="main-content" id="result">
            <div class="page-content">
                <div class="container-fluid">
                    <?= $content ?>
                </div>
            </div>
        </div>

    </div>
    <!-- END layout-wrapper -->


    <!-- /Right-bar -->
    <!-- Right bar overlay-->
    <script>
        $('.numerico').keypress(function(tecla) {
            var reg = /^[0-9.\s]+$/;
            if (!reg.test(String.fromCharCode(tecla.which))) {
                return false;
            }
            return true;
        });
        $('.texto').keypress(function(tecla) {
            var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;
            if (!reg.test(String.fromCharCode(tecla.which))) {
                return false;
            }
            return true;
        });
        $('#idlogout').click(function() {
            //debugger;
            localStorage.removeItem('menu');
        });
        var csrf = "<?= Yii::$app->request->getCsrfToken() ?>";
        if (localStorage.getItem("menu") == null) {
            Menus();
        } else {
            PintarMenu(JSON.parse(localStorage.getItem('menu')));
        }

        if (localStorage.getItem("ubigeo") == null) {
            Ubigeos();
        }
        if (localStorage.getItem("grupo") == null) {
            Grupos();
        }
        if (localStorage.getItem("genero") == null) {
            Generos();
        }

        function Generos() {
            $.ajax({
                async: false,
                url: '<?= \Yii::$app->request->BaseUrl ?>/genero/get-lista-opciones-generos',
                method: 'POST',
                data: {
                    _csrf: csrf
                },
                dataType: 'Json',
                beforeSend: function() {
                    //loading.show();
                },
                success: function(results) {
                    if (results && results.success) {
                        localStorage.setItem('genero', JSON.stringify(results));
                    }
                },
                error: function() {
                    alert('Error al realizar el proceso.');
                }
            });
        }

        function Grupos() {
            $.ajax({
                async: false,
                url: '<?= \Yii::$app->request->BaseUrl ?>/grupo/get-lista-opciones-grupos',
                method: 'POST',
                data: {
                    _csrf: csrf
                },
                dataType: 'Json',
                beforeSend: function() {
                    //loading.show();
                },
                success: function(results) {
                    if (results && results.success) {
                        localStorage.setItem('grupo', JSON.stringify(results));
                    }
                },
                error: function() {
                    alert('Error al realizar el proceso.');
                }
            });
        }


        function Ubigeos() {
            //debugger;
            $.ajax({
                async: false,
                url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-ubigeos",
                method: "POST",
                data: {
                    _csrf: csrf
                },
                dataType: "Json",
                beforeSend: function(xhr, settings) {
                    //loading.show();
                },
                success: function(results) {
                    localStorage.setItem('ubigeo', JSON.stringify(results));

                    var region = [];
                    $.each(results.regiones, function(index, event) {

                        var events = $.grep(region, function(e) {
                            return event.TXT_DEPARTAMENTO === e.TXT_DEPARTAMENTO;
                        });
                        if (events.length === 0) {
                            region.push(event);
                        }
                    });
                    localStorage.setItem('region', JSON.stringify(region.sort((a, b) => (a.TXT_DEPARTAMENTO > b.TXT_DEPARTAMENTO) ? 1 : -1)))
                },
                error: function() {
                    alert("Error al realizar el proceso.");
                }
            });
        }

        function Menus() {
            //debugger;
            $.ajax({
                async: false,
                url: "<?= \Yii::$app->request->BaseUrl ?>/menu/usuario-menu",
                method: "POST",
                data: {
                    _csrf: csrf
                },
                dataType: "Json",
                beforeSend: function(xhr, settings) {
                    //loading.show();
                },
                success: function(results) {
                    localStorage.setItem('menu', JSON.stringify(results));
                    PintarMenu(results);
                },
                error: function() {
                    alert("Error al realizar el proceso.");
                }
            });
        }

        function PintarMenu(results) {
            if (results && results.success) {
                var opcionesMenu = "";

                opcionesMenu = opcionesMenu + '<ul class="metismenu list-unstyled" id="side-menu">';
                opcionesMenu = opcionesMenu + '<li class="menu-title">Menu</li>';
                opcionesMenu = opcionesMenu + '<li><a href="<?= \Yii::$app->request->BaseUrl ?>/panel" class=" waves-effect"><i class="bx bx-home-circle"></i><span>Panel</span></a></li>';


                $.each(results.listaModulos, function(index, value) {
                    opcionesMenu = opcionesMenu + '<li>';
                    opcionesMenu = opcionesMenu + '<a href="#" class=" waves-effect has-arrow"><i class="bx bx-collection"></i><span>' + value.txtModulo + '</span></a>';
                    opcionesMenu = opcionesMenu + '<ul class="sub-menu" aria-expanded="true">';
                    $.each(value.listaCategorias, function(index, value2) {
                        opcionesMenu = opcionesMenu + '<li>';
                        opcionesMenu = opcionesMenu + '<a href="javascript: void(0);" class="has-arrow">' + value2.txtCategoria + '</a>';
                        opcionesMenu = opcionesMenu + '<ul class="sub-menu" aria-expanded="true">';
                        $.each(value2.listaMenus, function(index, value3) {
                            opcionesMenu = opcionesMenu + '<li><a href="<?= \Yii::$app->request->BaseUrl ?>/' + value3.txtLink + '">' + value3.txtMenu + '</a></li>';
                        });
                        opcionesMenu = opcionesMenu + '</ul>';
                        opcionesMenu = opcionesMenu + '</li>';

                    });
                    opcionesMenu = opcionesMenu + '</ul>';
                    opcionesMenu = opcionesMenu + '</li>';
                });

                opcionesMenu = opcionesMenu + '</ul>';
                $('#sidebar-menu').html(opcionesMenu);

            }
        }
    </script>
    <!-- App js -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/js/app.js"></script>


</body>

</html>