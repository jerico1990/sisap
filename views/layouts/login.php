<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="bootstrap admin template">
        <meta name="author" content="">
        
        <title> SISAP - MIDAGRI </title>
        <!-- <link rel="shortcut icon" href="<?= \Yii::$app->request->BaseUrl ?>//img/conectaideas_login.png"> -->
		<!-- Bootstrap Css -->
		<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/bootstrap.min.css" type="text/css">
		<!-- Icons Css -->
		<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/icons.min.css" type="text/css">
		<!-- App Css-->
		<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/app.min.css" type="text/css">
		<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/gh/kevindb/jquery-load-json@1.3.4/dist/jquery.loadJSON.min.js" integrity="sha384-ivtX4sn4dcdfHiO4e0/956wIQSerxsy2QZ6EHzdCVLlyGYYjSb8bqdxKY8IsfDGh" crossorigin="anonymous"></script>
       
		<link href="<?= \Yii::$app->request->BaseUrl ?>/captcha/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    	<link href="<?= \Yii::$app->request->BaseUrl ?>/captcha/disk/slidercaptcha.min.css" rel="stylesheet" />
		<script src="<?= \Yii::$app->request->BaseUrl ?>/captcha/disk/longbow.slidercaptcha.js"></script>
		<script>
			/*$.fn.serializeObject = function() {
				var o = {};
				var a = this.serializeArray();
				$.each(a, function() {
					if (o[this.name]) {
						if (!o[this.name].push) {
							o[this.name] = [o[this.name]];
						}
						o[this.name].push(this.value || '');
					} else {
						o[this.name] = this.value || '';
					}
				});
				return o;
			};*/
		</script>
		<style>
			.slidercaptcha {
				margin: 0 auto;
				width: 314px;
				height: 286px;
				border-radius: 4px;
				box-shadow: 0 0 10px rgba(0, 0, 0, 0.125);
				margin-top: 40px;
			}

			.slidercaptcha .card-body {
				padding: 1rem;
			}

			.slidercaptcha canvas:first-child {
				border-radius: 4px;
				border: 1px solid #e6e8eb;
			}

			.slidercaptcha.card .card-header {
				background-image: none;
				background-color: rgba(0, 0, 0, 0.03);
			}

			.refreshIcon {
				top: -54px;
			}
		</style>
    </head>
    <body >
		<?= $content ?>
		
	
		<!-- JAVASCRIPT -->
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jquery/jquery.min.js"></script>
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/metismenu/metisMenu.min.js"></script>
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/simplebar/simplebar.min.js"></script>
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/node-waves/waves.min.js"></script>
		<!-- App js -->
		<script src="<?= \Yii::$app->request->BaseUrl ?>/skote/js/app.js"></script>

		
        
    </body>
</html>
